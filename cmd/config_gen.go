package cmd

import (
	"context"
	"flag"
	"fmt"
	"os"
	"strings"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/command/gen"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/logging"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/service"
)

type runtimeNames []string

func (r *runtimeNames) String() string {
	return fmt.Sprintf("%v", *r)
}

func (r *runtimeNames) Set(v string) error {
	*r = append(*r, v)
	return nil
}

type ConfigGenCommand struct {
	ServiceName             string
	RuntimeEnvironmentNames runtimeNames
	OutputConfigFilePath    string
	Context                 context.Context
}

func (c *ConfigGenCommand) Help() string {
	return fmt.Sprintf(`Usage: epona-aws-starter-cli %s [options]

  %s

Options:
  -service-name=name         構築するサービスの名前を指定します
  -s=name                    -service-nameの短縮形です
  -runtime-name=name         構築するRuntime環境名を指定します
                             複数のRuntime環境を指定する場合は、 -runtime-name [name] -runtime-name [name] のように繰り返し指定してください
  -r=name                    -runtime-nameの短縮形です
  -output=path               次のinitコマンドで使用する、設定ファイルの出力先を指定します (default: %s)


使用例:
  たとえば、サービス名 "my-service" 、Runtime環境を "staging" と "production" の2つを作成する場合は、以下のように指定します。

    epona-aws-starter-cli config-gen -s my-service -r staging -r production

    epona-aws-starter-cli config-gen -service-name my-service -runtime-name staging -runtime-name production
`,
		configGenCommand,
		c.Synopsis(),
		defaultInitConfigFilePath,
	)
}

func (c *ConfigGenCommand) Synopsis() string {
	return "指定されたサービス名とRuntime環境名を元に、次のinitコマンドのための設定ファイルを生成するコマンドです"
}

func (c *ConfigGenCommand) Run(args []string) int {
	if err := c.actualRun(args); err != nil {
		logging.Errorf("%sの実行に失敗しました"+logging.ErrTraceDelimiter+"%v", configGenCommand, err)
		return ExitStatusError
	}

	return ExitStatusSuccess
}

func (c *ConfigGenCommand) actualRun(args []string) error {
	// config-gen は、ログファイルは不要
	logging.InitializeStdOutLogger()

	flags := flag.NewFlagSet(configGenCommand, flag.ContinueOnError)
	flags.StringVar(&c.ServiceName, "s", "", "構築するサービスの名前を指定します")
	flags.StringVar(&c.ServiceName, "service-name", "", "構築するサービスの名前を指定します")
	flags.Var(&c.RuntimeEnvironmentNames, "r", "Runtime環境名を指定します (複数回指定可)")
	flags.Var(&c.RuntimeEnvironmentNames, "runtime-name", "Runtime環境名を指定します (複数回指定可)")
	flags.StringVar(&c.OutputConfigFilePath, "output", defaultInitConfigFilePath, "次のinitコマンドで使用する、設定ファイルの出力先を指定します")
	flags.Usage = func() { c.Help() }

	if err := flags.Parse(args); err != nil {
		fmt.Println()

		logging.Errorf("引数を解析できませんでした: %v", err)
		logging.Info("以下のヘルプを見て、コマンドの使い方やオプションを確認してください\n\n")
		fmt.Println(c.Help())

		os.Exit(ExitStatusError)
	}

	printStarterCliVersion()

	if err := checkEnvironment(&cmdExecutor{}); err != nil {
		return err
	}

	ctx := c.Context
	ctx = service.SetPrefix(ctx, os.Getenv("EPONA_STARTER_PREFIX"))
	ctx = service.SetName(ctx, c.ServiceName)
	c.Context = ctx

	if err := gen.Validate(c.ServiceName, c.RuntimeEnvironmentNames, c.OutputConfigFilePath); err != nil {
		return err
	}

	logging.Info("次の情報で設定ファイルの作成を行います")
	logging.Infof("  サービス名: %s", c.ServiceName)
	logging.Infof("  Runtime環境名: [%s]", strings.Join(c.RuntimeEnvironmentNames, ", "))

	logging.Infof("initコマンドで使用する設定ファイル %s を作成します", c.OutputConfigFilePath)

	if err := gen.OutputInitConfigFileSeed(ctx, c.RuntimeEnvironmentNames, c.OutputConfigFilePath); err != nil {
		return err
	}

	logging.Infof("initコマンドで使用する設定ファイル %s を作成しました", c.OutputConfigFilePath)

	logging.Info("次は init コマンドを実行し、Terraformの実行環境を構築してください")
	logging.Infof("※設定ファイル %s は修正が必要です", c.OutputConfigFilePath)

	return nil
}
