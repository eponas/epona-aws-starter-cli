package cmd

import (
	"bytes"
	"context"
	"fmt"
	"os"
	"os/exec"
	"regexp"
	"sort"
	"strings"

	"github.com/mitchellh/cli"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/logging"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/version"

	semver "github.com/hashicorp/go-version"
)

const (
	ApplicationName = "Epona AWS Starter CLI"

	ExitStatusSuccess = 0
	ExitStatusError   = 1

	configGenCommand = "config-gen"
	initCommandName  = "init"
	applyCommandName = "apply"
)

const (
	defaultInitConfigFilePath  = "starter-configs/init.yml"
	defaultApplyConfigFilePath = "starter-configs/apply.yml"
)

var (
	Version  string = "[development-version]" // epona-aws-starter-cliのバージョン
	Revision string = "[development-build]"   // epona-aws-starter-cliのコミットハッシュ

	EponaVersion       = "v0.2.4"
	TerraformVersion   = "0.14.10"
	AwsProviderVersion = "3.37.0"
	// TerraformVersionConstraint は、本 CLI 実行端末で利用する terraform のバージョン制約を表現する文字列
	TerraformVersionConstraint = "= " + TerraformVersion
	// AWSCLIVersionContraint は、本 CLI 実行端末で利用する aws のバージョン制約を表現する。
	// v2 以上であれば問題なしとする。
	AWSCLIVersionContraint = ">= 2.0.0"

	EponaStaterCliFullVersion = fmt.Sprintf("Epona AWS Starter CLI version %s, build %s", Version, Revision)
)

type tool string

const (
	terraform tool = "terraform"
	awscli    tool = "awscli"
)

type Command interface {
	Execute()
}

func printStarterCliVersion() {
	logging.Info(EponaStaterCliFullVersion)
}

func printRelatedSoftwareVersions(ctx context.Context) error {
	v, err := version.GetEponaVersion(ctx)

	if err != nil {
		return err
	}

	logging.Infof("  Epona Version: %s", v)

	v, err = version.GetTerraformVersion(ctx)

	if err != nil {
		return err
	}

	logging.Infof("  Terraform Version: %s", v)

	v, err = version.GetAwsProviderVersion(ctx)

	if err != nil {
		return err
	}

	logging.Infof("  AWS Provider Version: %s", v)

	return nil
}

func RunStarter() {
	ctx := context.Background()

	ctx = version.SetEponaVersion(ctx, EponaVersion)
	ctx = version.SetTerraformVersion(ctx, TerraformVersion)
	ctx = version.SetAwsProviderVersion(ctx, AwsProviderVersion)

	c := cli.NewCLI(ApplicationName, EponaStaterCliFullVersion)
	c.HelpFunc = cliHelp
	c.Args = os.Args[1:]

	var subcommand string
	if len(os.Args) > 1 {
		subcommand = os.Args[1]
	} else {
		subcommand = ""
	}

	isUnknownCommand :=
		subcommand != configGenCommand &&
			subcommand != initCommandName &&
			subcommand != applyCommandName

	if isUnknownCommand {
		logging.InitializeStdOutLogger()
	}

	c.Commands = map[string]cli.CommandFactory{
		configGenCommand: func() (cli.Command, error) {
			command := &ConfigGenCommand{Context: ctx}
			return command, nil
		},
		initCommandName: func() (cli.Command, error) {
			command := &InitCommand{Context: ctx}
			return command, nil
		},
		applyCommandName: func() (cli.Command, error) {
			command := &ApplyCommand{Context: ctx}
			return command, nil
		},
	}

	exitStatus, err := c.Run()

	if err != nil {
		logging.Error(err)
	}

	if !isUnknownCommand && subcommand != configGenCommand {
		logging.PrintLogFileLocation()
	}

	// 127はコマンドが見つからなかった場合
	if exitStatus == 0 || exitStatus == 127 {
		os.Exit(0)
	} else {
		os.Exit(exitStatus)
	}
}

func cliHelp(commands map[string]cli.CommandFactory) string {
	writer := new(strings.Builder)

	fmt.Fprintf(writer, "Usage: %s [--version] [--help] <command> [<args>]\n\n", ApplicationName)
	fmt.Fprintln(writer, "利用可能なコマンド:")

	keys := make([]string, 0, len(commands))
	maxKeyLen := 0
	for key := range commands {
		if len(key) > maxKeyLen {
			maxKeyLen = len(key)
		}

		keys = append(keys, key)
	}

	sort.Strings(keys)

	for _, key := range keys {
		commandFunc, ok := commands[key]
		if !ok {
			logging.Fatalf("コマンド %s が見つかりませんでした", key)
		}

		command, err := commandFunc()
		if err != nil {
			logging.Fatalf("コマンド '%s' がロードできませんでした: %s", key, err)
			continue
		}

		key = fmt.Sprintf("%s%s", key, strings.Repeat(" ", maxKeyLen-len(key)))
		fmt.Fprintf(writer, "    %s    %s\n", key, command.Synopsis())
	}

	fmt.Fprint(writer, `

コマンドの利用順:
    コマンドには、実行する順番があります。最後の apply 以外のコマンドは、すべて次のコマンドのための入力ファイルを生成します。
    規定のコマンドの利用順は、以下の通りです。

      config-gen → init → apply

    各コマンドは次のコマンドへの入力ファイルを生成しますが、それぞれ編集する必要があることに注意してください。
`)

	return writer.String()
}

func checkEnvironment(e executor) error {
	// Terraform のバージョンが本ツールのサポートバージョンであることを確認する
	if err := checkTerraformVersion(e); err != nil {
		return err
	}
	// AWS CLI のバージョンが本ツールのサポートバージョンであることを確認する
	if err := checkAWSCLIVersion(e); err != nil {
		return err
	}
	return nil
}

// executor は、コマンドを実行する interface
type executor interface {
	Exec(name string, args ...string) (outstr, errstr string, err error)
}

// cmdExecutor は executor の実装
type cmdExecutor struct{}

// Exec は $ name args という形式でコマンドを実行し、標準出力を返却する
func (ce *cmdExecutor) Exec(name string, args ...string) (outstr, errstr string, err error) {
	var outBuf, errBuf bytes.Buffer
	cmd := exec.Command(name, args...)
	cmd.Stdout, cmd.Stderr = &outBuf, &errBuf

	if err := cmd.Run(); err != nil {
		return outBuf.String(), errBuf.String(),
			fmt.Errorf("%s %s: %s", name, strings.Join(args, " "), errBuf.String())
	}
	return outBuf.String(), errBuf.String(), nil
}

// checkTerraformVersion は PC にインストールされた terraform のバージョンが
// 本ツールが想定するバージョンになっているかを確認する。
// 意図しているバージョンになっている場合にのみ、最初の戻り値として true を返却する。
func checkTerraformVersion(e executor) error {
	outStr, _, err := e.Exec("terraform", "version")
	if err != nil {
		return fmt.Errorf("terraformの実行ファイルへのパスが通っていないか、インストールされていません。未インストールの場合は https://releases.hashicorp.com/terraform/%s/ から対象バージョンをダウンロードください"+logging.ErrTraceDelimiter+"%v", TerraformVersion, err)
	}

	// terraform version の標準出力には "Terraform v0.13.5 (略)" という形式で出力される
	re := regexp.MustCompile(`^Terraform (v\d+.\d+.\d+)`)
	matches := re.FindStringSubmatch(string(outStr))
	if len(matches) < 2 {
		// regexp.FindStringSubmatch は slice の index 0 に正規表現全体にマッチした文字列、
		// index 1 以降に正規表現でキャプチャした内容を順次返却する
		return fmt.Errorf("terraform version の結果にバージョンが検出されませんでした: %s", string(outStr))
	}
	return checkVersionConstraint(terraform, matches[1], TerraformVersionConstraint)
}

// checkVersionConstraint は、`ver` が `constraint` で表現されるバージョン制約を
// 満たす場合にのみ true を返却する
func checkVersionConstraint(target tool, ver, constraint string) error {
	v, err := semver.NewVersion(ver)
	if err != nil {
		return fmt.Errorf("%sのバージョンが正しく解析できませんでした"+logging.ErrTraceDelimiter+"%v", target, err)
	}

	c, err := semver.NewConstraint(constraint)
	if err != nil {
		// 開発者のバグに該当する
		logging.Fatalf("[Bug]%sのバージョン制約が正しく解析できませんでした: %v", target, err)
	}

	if !c.Check(v) {
		switch target {
		case terraform:
			return fmt.Errorf("%sのバージョン[%s]は制約[%s]を満たしていません。https://releases.hashicorp.com/terraform/%s/ から対象バージョンをダウンロードください。",
				target, ver, constraint, TerraformVersion)
		case awscli:
			return fmt.Errorf("%sのバージョン[%s]は制約[%s]を満たしていません。https://docs.aws.amazon.com/ja_jp/cli/latest/userguide/install-cliv2.html を参照してインストールしてください。",
				target, ver, constraint)
		default:
			logging.Fatalf("[Bug]%sは想定しているターゲットではありません", target)
		}
	}
	return nil
}

func checkAWSCLIVersion(e executor) error {
	outStr, _, err := e.Exec("aws", "--version")
	if err != nil {
		return fmt.Errorf("AWSCLIv2の実行ファイルへのパスが通っていないか、インストールされていません"+logging.ErrTraceDelimiter+"%v", err)
	}

	// aws version の標準出力には
	// "aws-cli/2.1.22 Python/3.9.1 Darwin/20.3.0 source/x86_64 prompt/off"
	// という形式で出力される
	re := regexp.MustCompile(`^aws-cli/(\d+.\d+.\d+)`)
	matches := re.FindStringSubmatch(string(outStr))
	if len(matches) < 2 {
		// regexp.FindStringSubmatch は slice の index 0 に正規表現全体にマッチした文字列、
		// index 1 以降に正規表現でキャプチャした内容を順次返却する
		return fmt.Errorf("aws --version の結果にバージョンが検出されませんでした: %s", string(outStr))
	}
	return checkVersionConstraint(awscli, fmt.Sprintf("v%s", matches[1]), AWSCLIVersionContraint)
}
