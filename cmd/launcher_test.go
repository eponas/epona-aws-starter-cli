package cmd

import (
	"errors"
	"fmt"
	"testing"

	"github.com/stretchr/testify/assert"
)

type fixedExecutor struct {
	out string
}

// Exec はコマンドを実行せず、標準出力として固定値 out を返却する
func (te *fixedExecutor) Exec(name string, args ...string) (outStr, errStr string, err error) {
	return te.out, "", nil
}

type errorExecutor struct{}

// Exec はコマンドを実行せず、エラーを返却する
func (te *errorExecutor) Exec(name string, args ...string) (outStr, errStr string, err error) {
	return "", "", errors.New("dummy")
}

const (
	// terraform version の出力テンプレート
	terraformVersionTmpl = `Terraform %s

Your version of Terraform is out of date! The latest version
is 0.14.6. You can update by downloading from https://www.terraform.io/downloads.html`
	// aws --version の出力テンプレート
	awscliVersionTmpl = "aws-cli/%s Python/3.9.1 Darwin/20.3.0 source/x86_64 prompt/off"
)

func TestExec(t *testing.T) {
	ce := &cmdExecutor{}

	t.Run("正常系", func(t *testing.T) {
		outstr, errstr, err := ce.Exec("echo", "hello world")

		assert.Equal(t, "hello world\n", outstr)
		assert.Equal(t, errstr, "")
		assert.NoError(t, err)
	})
	t.Run("異常系", func(t *testing.T) {
		_, _, err := ce.Exec("exit", "1")
		assert.Error(t, err)
	})
}

func TestCheckTerraformVersion(t *testing.T) {
	t.Run("Terraform バージョンチェック", func(t *testing.T) {
		testCases := []struct {
			version   string
			errorcase bool
		}{
			{
				version:   "v0.14.10",
				errorcase: false,
			},
			{
				version:   "v0.14.11",
				errorcase: true,
			},
			{
				version:   "v0.15.0",
				errorcase: true,
			},
		}

		for _, tc := range testCases {
			fe := &fixedExecutor{
				out: fmt.Sprintf(terraformVersionTmpl, tc.version),
			}
			if !tc.errorcase {
				assert.NoError(t, checkTerraformVersion(fe), tc.version)
			} else {
				assert.Error(t, checkTerraformVersion(fe), tc.version)
			}
		}
	})

	t.Run("Terraform がインストールされていない", func(t *testing.T) {
		assert.Error(t, checkTerraformVersion(&errorExecutor{}),
			"terraformがインストールされていないとエラーが返却される")
	})

	t.Run("Terraform versionの出力が想定外", func(t *testing.T) {
		fe := &fixedExecutor{
			out: "hogefuga",
		}
		assert.Error(t, checkTerraformVersion(fe),
			"Terraformの出力にセマンティックバージョンが含まれていないとエラーが返却される")
	})
}

func TestAWSCLIVersion(t *testing.T) {
	t.Run("AWSCLIv2 バージョンチェック", func(t *testing.T) {
		testCases := []struct {
			version   string
			errorcase bool
		}{
			{
				version:   "2.0.0",
				errorcase: false,
			},
			{
				version:   "2.1.22",
				errorcase: false,
			},
			{
				version:   "1.1.1",
				errorcase: true,
			},
			{
				version:   "1.20.1",
				errorcase: true,
			},
		}

		for _, tc := range testCases {
			fe := &fixedExecutor{
				out: fmt.Sprintf(awscliVersionTmpl, tc.version),
			}
			if !tc.errorcase {
				assert.NoError(t, checkAWSCLIVersion(fe), tc.version)
			} else {
				assert.Error(t, checkAWSCLIVersion(fe), tc.version)
			}
		}
	})

	t.Run("Terraform がインストールされていない", func(t *testing.T) {
		assert.Error(t, checkTerraformVersion(&errorExecutor{}),
			"terraformがインストールされていないとエラーが返却される")
	})

	t.Run("Terraform versionの出力が想定外", func(t *testing.T) {
		fe := &fixedExecutor{
			out: "hogefuga",
		}
		assert.Error(t, checkTerraformVersion(fe),
			"Terraformの出力にセマンティックバージョンが含まれていないとエラーが返却される")
	})
}

func TestCheckVersionConstraint(t *testing.T) {
	t.Run("セマンティックバージョンのフォーマット不正", func(t *testing.T) {
		assert.Error(t, checkVersionConstraint(
			awscli,
			fmt.Sprintf(terraformVersionTmpl, "v1.0.0"),
			"hoge",
		))
	})

	t.Run("制約チェック", func(t *testing.T) {
		testCases := []struct {
			version    string
			constraint string
			errorcase  bool
		}{
			{
				version:    "v0.13.1",
				constraint: ">= 0.13.1",
				errorcase:  false,
			},
			{
				version:    "v0.13.5",
				constraint: ">= 0.13.5, < 0.14",
				errorcase:  false,
			},
			{
				version:    "v0.13.5",
				constraint: "> 0.13.5, < 0.14",
				errorcase:  true,
			},
			{
				version:    "v0.14.0",
				constraint: "> 0.13.5, < 0.14",
				errorcase:  true,
			},
			{
				version:    "v0.13.5",
				constraint: "= 0.13.5",
				errorcase:  false,
			},
		}

		for _, tc := range testCases {
			err := checkVersionConstraint(awscli, tc.version, tc.constraint)
			if tc.errorcase {
				assert.Error(t, err,
					fmt.Sprintf("ver: %s, constraint: %s", tc.version, tc.constraint))
			} else {
				assert.NoError(t, err,
					fmt.Sprintf("ver: %s, constraint: %s", tc.version, tc.constraint))
			}
		}
	})
}
