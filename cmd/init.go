package cmd

import (
	"context"
	"flag"
	"fmt"
	"os"
	"strings"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/command/init/config"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/command/init/terraformer"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/logging"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/service"
)

type InitCommand struct {
	InputConfigFilePath  string
	OutputConfigFilePath string
	Context              context.Context
}

func (c *InitCommand) Help() string {
	return fmt.Sprintf(`Usage: epona-aws-starter-cli %s [options]

  %s

Options:

  -input=path                Eponaが前提とするTerraform実行環境を構築するための、設定ファイルを指定します (default: %s)
  -output=path               次のapplyコマンドで使用する、設定ファイルの出力先を指定します (default: %s)
`,
		initCommandName,
		c.Synopsis(),
		defaultInitConfigFilePath,
		defaultApplyConfigFilePath,
	)
}

func (c *InitCommand) Synopsis() string {
	return "設定ファイルに定義されたパラメーターやAWSのクレデンシャルを元に、Eponaが前提とするTerraform実行環境を構築するコマンドです"
}

func (c *InitCommand) Run(args []string) int {
	if err := c.actualRun(args); err != nil {
		logging.Errorf("%sの実行に失敗しました"+logging.ErrTraceDelimiter+"%v", initCommandName, err)
		return ExitStatusError
	}

	return ExitStatusSuccess
}

func (c *InitCommand) actualRun(args []string) error {
	flags := flag.NewFlagSet(initCommandName, flag.ContinueOnError)
	flags.StringVar(&c.InputConfigFilePath, "input", defaultInitConfigFilePath, "Eponaが前提とするTerraform実行環境を構築するための、設定ファイルを指定します")
	flags.StringVar(&c.OutputConfigFilePath, "output", defaultApplyConfigFilePath, "次のapplyコマンドで使用する、設定ファイルの出力先を指定します")
	flags.Usage = func() { c.Help() }

	if err := flags.Parse(args); err != nil {
		fmt.Println()

		logging.InitializeStdOutLogger()
		logging.Errorf("引数を解析できませんでした: %v", err)
		logging.Info("以下のヘルプを見て、コマンドの使い方やオプションを確認してください\n\n")
		fmt.Println(c.Help())

		os.Exit(ExitStatusError)
	}

	logging.InitializeLoggers(initCommandName)

	printStarterCliVersion()

	if err := checkEnvironment(&cmdExecutor{}); err != nil {
		return err
	}

	logging.Info("Terraform実行環境を作成するための、各種バージョンの情報:")

	if err := printRelatedSoftwareVersions(c.Context); err != nil {
		return err
	}

	logging.Infof("設定ファイル [%s] を読み込みます", c.InputConfigFilePath)

	config, err := config.ParseInitConfig(c.InputConfigFilePath)

	if err != nil {
		return err
	}

	// TODO validation

	logging.Infof("設定ファイル [%s] の読み込みが完了しました", c.InputConfigFilePath)

	// Context per Command
	ctx := c.Context
	ctx = service.SetPrefix(ctx, os.Getenv("EPONA_STARTER_PREFIX"))
	ctx = service.SetName(ctx, config.ServiceName)
	c.Context = ctx

	deliveryEnvironment := config.DeliveryEnvironment
	runtimeEnvironments := config.RuntimeEnvironments

	logging.Info("設定ファイルの情報:")

	logging.Infof("  サービス名: %s", config.ServiceName)

	logging.Info("  Terraform実行環境作成対象:")
	logging.Info("    Delivery環境:")
	logging.Infof("      AWSアカウントID: %s", deliveryEnvironment.AwsAccountID)
	logging.Infof("      Delivery環境用Terraform実行ユーザー名: %s", deliveryEnvironment.DeliveryTerraformerName)
	logging.Infof("      各Runtime環境用Terraform実行ユーザー名: [%s]", strings.Join(deliveryEnvironment.RuntimeTerraformerNames, ", "))
	logging.Infof("      Terraform実行ロール名: %s", deliveryEnvironment.TerraformExecutionRoleName)
	logging.Infof("      Terraform Backend S3バケット名: %s", deliveryEnvironment.BackendBucketName)
	logging.Infof("      Terraform State S3バケット名: %s", deliveryEnvironment.StateBucketName)
	logging.Infof("      Terraform Stateロック用テーブル名: %s", deliveryEnvironment.LockTableName)
	logging.Infof("      Terraform Stateアクセスロール名: %s", deliveryEnvironment.StateAccessRoleName)
	logging.Infof("      各Runtime環境用Terraform State S3バケット名: [%s]", strings.Join(deliveryEnvironment.RuntimeStateBucketNames, ", "))
	logging.Infof("      各Runtime環境用Terraform Stateアクセスロール名: [%s]", strings.Join(deliveryEnvironment.RuntimeStateAccessRoleNames, ", "))

	logging.Info("    Runtime環境:")

	for _, r := range runtimeEnvironments {
		logging.Infof("      環境名: %s", r.Name)
		logging.Infof("        AWSアカウントID: %s", r.AwsAccountID)
		logging.Infof("        Terraform実行ロール名: %s", r.TerraformExecutionRoleName)
		logging.Infof("        Terraform Backend S3バケット名: %s", r.BackendBucketName)
		logging.Infof("        Terraform Stateロック用テーブル名: %s", r.LockTableName)
	}

	var deliveryEnvironmentInitializer terraformer.DeliveryEnvironmentInitializer = deliveryEnvironment
	var runtimeEnvironmentInitializers []terraformer.RuntimeEnvironmentInitializer = []terraformer.RuntimeEnvironmentInitializer{}

	for _, r := range runtimeEnvironments {
		runtimeEnvironmentInitializers = append(runtimeEnvironmentInitializers, r)
	}

	logging.Info("各環境に存在するリソースと、作成する主要なリソースの競合確認を行います")

	if err := deliveryEnvironmentInitializer.AlreadyExistingEnvironment(ctx); err != nil {
		return err
	}

	for _, ri := range runtimeEnvironmentInitializers {
		if err := ri.AlreadyExistingEnvironment(ctx); err != nil {
			return err
		}
	}

	logging.Info("各環境に、作成する主要なリソースの競合はありませんでした")

	logging.Info("Delivery環境のTerraform実行環境の構築を開始します")

	if err := deliveryEnvironmentInitializer.CreateTerraformBackendStorage(ctx); err != nil {
		return err
	}

	if err := deliveryEnvironmentInitializer.CreateTerraformersAndSetupBackend(ctx, runtimeEnvironments); err != nil {
		return err
	}

	for _, ri := range runtimeEnvironmentInitializers {
		logging.Infof("Runtime / %s環境のTerraform実行環境の構築を開始します", ri.(*terraformer.RuntimeEnvironment).Name)

		if err := ri.CreateTerraformBackendStorage(ctx); err != nil {
			return err
		}

		if err := ri.CreateExecutionRoleAndSetupBackend(ctx, deliveryEnvironment); err != nil {
			return err
		}

		logging.Infof("Runtime / %s環境のTerraform実行環境の構築が完了しました", ri.(*terraformer.RuntimeEnvironment).Name)
	}

	logging.Info("Terraform実行用ユーザーが、各環境用の実行ロールを使用するための設定を行います")

	if err := deliveryEnvironmentInitializer.AssignExecutionRoleToRuntimeTerraformers(ctx, runtimeEnvironments); err != nil {
		return err
	}

	logging.Info("Terraform実行用ユーザーが、各環境用の実行ロールを使用するための設定を行いました")
	logging.Info("Delivery環境のTerraform実行環境の構築が完了しました")

	logging.Info("Terraform実行用ユーザー用のアクセスキーを作成します")

	if err := deliveryEnvironmentInitializer.CreateTerraformersCredential(ctx); err != nil {
		return err
	}

	logging.Info("Terraform実行用ユーザー用のアクセスキーを作成しました")

	logging.Info("initコマンドで構築した、今後のEponaの利用に必要な情報を表示します")

	logging.Info("TerraformのState管理についての情報を表示します" + terraformExecutionResourceToString(deliveryEnvironment, runtimeEnvironments))

	logging.Info("表示されたTerraformのStateの情報は、各環境でEponaのモジュールを使う時に main.tf や versions.tf に指定してください")

	logging.Info("Terraform実行ユーザーのアクセスキーを表示します" + accessKeyToString(deliveryEnvironment))

	logging.Info("表示されたアクセスキーはTerraform実行時に使用するため、大切に保管してください")

	logging.Infof("applyコマンドで使用する設定ファイル %s を作成します", c.OutputConfigFilePath)

	if err := terraformer.OutputApplyConfigFileSeed(ctx, deliveryEnvironment, runtimeEnvironments, c.OutputConfigFilePath); err != nil {
		return err
	}

	logging.Infof("applyコマンドで使用する設定ファイル %s を作成しました", c.OutputConfigFilePath)

	logging.Info("次は apply コマンドを実行し、アクティビティを実現するpatternの適用を行ってください")
	logging.Infof("※設定ファイル %s は修正が必要です", c.OutputConfigFilePath)

	return nil
}

func terraformExecutionResourceToString(deliveryEnvironment *terraformer.DeliveryEnvironment, runtimeEnvironments []*terraformer.RuntimeEnvironment) string {
	terraformExecutionInfoBuilder := new(strings.Builder)
	terraformExecutionInfoBuilder.WriteString("\n")
	terraformExecutionInfoBuilder.WriteString("\n")

	terraformExecutionInfoBuilder.WriteString("Delivery環境用Terraform実行情報:\n")
	terraformExecutionInfoBuilder.WriteString(fmt.Sprintf("  Terraform実行ロールARN: %s\n", deliveryEnvironment.CreatedTerraformExecutionRoleArn))
	terraformExecutionInfoBuilder.WriteString(fmt.Sprintf("  State用S3バケット名: %s\n", deliveryEnvironment.StateBucketName))
	terraformExecutionInfoBuilder.WriteString(fmt.Sprintf("  State用ロックテーブル名: %s\n", deliveryEnvironment.LockTableName))
	terraformExecutionInfoBuilder.WriteString(fmt.Sprintf("  StateアクセスロールARN: arn:aws:iam::%s:role/%s\n", deliveryEnvironment.AwsAccountID, deliveryEnvironment.StateAccessRoleName))

	for i, runtimeEnvironment := range runtimeEnvironments {
		terraformExecutionInfoBuilder.WriteString("\n")

		terraformExecutionInfoBuilder.WriteString(fmt.Sprintf("Runtime / %s環境用Terraform実行情報:\n", runtimeEnvironment.Name))
		terraformExecutionInfoBuilder.WriteString(fmt.Sprintf("  Terraform実行ロールARN: %s\n", runtimeEnvironment.CreatedTerraformExecutionRoleArn))
		terraformExecutionInfoBuilder.WriteString(fmt.Sprintf("  State用S3バケット名: %s\n", deliveryEnvironment.RuntimeStateBucketNames[i]))
		terraformExecutionInfoBuilder.WriteString(fmt.Sprintf("  State用ロックテーブル名: %s\n", runtimeEnvironment.LockTableName))
		terraformExecutionInfoBuilder.WriteString(fmt.Sprintf("  StateアクセスロールARN: arn:aws:iam::%s:role/%s\n", deliveryEnvironment.AwsAccountID, deliveryEnvironment.RuntimeStateAccessRoleNames[i]))
	}

	return terraformExecutionInfoBuilder.String()
}

func accessKeyToString(deliveryEnvironment *terraformer.DeliveryEnvironment) string {
	accessKeyInfoBuilder := new(strings.Builder)
	accessKeyInfoBuilder.WriteString("\n")
	accessKeyInfoBuilder.WriteString("\n")

	dc := deliveryEnvironment.CreatedTerraformerMappings["delivery"].AwsCredential
	accessKeyInfoBuilder.WriteString("Delivery環境用Terraform実行ユーザーのアクセスキー情報:\n")
	accessKeyInfoBuilder.WriteString(fmt.Sprintf("  ユーザー名： %s\n", deliveryEnvironment.DeliveryTerraformerName))
	accessKeyInfoBuilder.WriteString(fmt.Sprintf("  アクセスキーID： %s\n", dc.AccessKeyID))
	accessKeyInfoBuilder.WriteString(fmt.Sprintf("  シークレットアクセスキー： %s\n", dc.SecretAccessKey))

	for i, rn := range deliveryEnvironment.RuntimeNames {
		accessKeyInfoBuilder.WriteString("\n")

		accessKeyInfoBuilder.WriteString(fmt.Sprintf("Runtime / %s環境用Terraform実行ユーザーのアクセスキー情報:\n", rn))
		accessKeyInfoBuilder.WriteString(fmt.Sprintf("  ユーザー名： %s\n", deliveryEnvironment.RuntimeTerraformerNames[i]))
		accessKeyInfoBuilder.WriteString(fmt.Sprintf("  アクセスキーID： %s\n", deliveryEnvironment.CreatedTerraformerMappings[rn].AwsCredential.AccessKeyID))
		accessKeyInfoBuilder.WriteString(fmt.Sprintf("  シークレットアクセスキー： %s\n", deliveryEnvironment.CreatedTerraformerMappings[rn].AwsCredential.SecretAccessKey))
	}

	return accessKeyInfoBuilder.String()
}
