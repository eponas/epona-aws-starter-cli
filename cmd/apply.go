package cmd

import (
	"context"
	"flag"
	"fmt"
	"os"
	"strings"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/command/apply/config"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/command/apply/execute"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/logging"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/service"
)

type ApplyCommand struct {
	InputConfigFile string
	Context         context.Context
}

func (c *ApplyCommand) Help() string {
	return fmt.Sprintf(`Usage: epona-aws-starter-cli %s [options]

  %s

Options:

  -input=path                Eponaのpatternインスタンスを適用するための、設定ファイルを指定します (default: %s)
`,
		applyCommandName,
		c.Synopsis(),
		defaultApplyConfigFilePath,
	)
}

func (c *ApplyCommand) Synopsis() string {
	return "設定ファイルに定義されたパラメーターやAWSのクレデンシャルを元に、Eponaのpatternインスタンス適用するコマンドです"
}

func (c *ApplyCommand) Run(args []string) int {
	if err := c.actualRun(args); err != nil {
		logging.Errorf("%sの実行に失敗しました: %v", applyCommandName, err)
		return ExitStatusError
	}

	return ExitStatusSuccess
}

func (c *ApplyCommand) actualRun(args []string) error {
	flags := flag.NewFlagSet(applyCommandName, flag.ContinueOnError)
	flags.StringVar(&c.InputConfigFile, "input", defaultApplyConfigFilePath, "Eponaのpatternインスタンスを適用するための、設定ファイルを指定します")
	flags.Usage = func() { c.Help() }

	if err := flags.Parse(args); err != nil {
		fmt.Println()

		logging.InitializeStdOutLogger()
		logging.Errorf("引数を解析できませんでした: %v", err)
		logging.Info("以下のヘルプを見て、コマンドの使い方やオプションを確認してください\n\n")
		fmt.Println(c.Help())

		os.Exit(ExitStatusError)
	}

	logging.InitializeLoggers(applyCommandName)

	printStarterCliVersion()

	if err := checkEnvironment(&cmdExecutor{}); err != nil {
		return err
	}

	logging.Info("patternインスタンスを適用を行うための、各種バージョンの情報:")

	if err := printRelatedSoftwareVersions(c.Context); err != nil {
		return err
	}

	logging.Infof("設定ファイル [%s] を読み込みます", c.InputConfigFile)

	config, err := config.ParseApplyConfig(c.InputConfigFile)
	if err != nil {
		return err
	}

	logging.Infof("設定ファイル [%s] の読み込みが完了しました", c.InputConfigFile)

	// Context per Command
	ctx := c.Context
	ctx = service.SetPrefix(ctx, os.Getenv("EPONA_STARTER_PREFIX"))
	ctx = service.SetName(ctx, config.ServiceName)
	c.Context = ctx

	logging.Info("設定ファイルの情報:")
	logging.Infof("  サービス名: %s", config.ServiceName)
	// logging.Infof("  作成対象IAMユーザー: %s", config.Users)  // IAMユーザー作成をサポートした場合の、ログ出力イメージ

	deliveryActivityNames := []string{}

	for _, a := range config.DeliveryEnvironment.Activities {
		deliveryActivityNames = append(deliveryActivityNames, a.GetName())
	}

	logging.Infof("  Delivery環境に適用するアクティビティ: [%s]", strings.Join(deliveryActivityNames, ", "))

	runtimeActivityNames := map[string][]string{}

	for _, r := range config.RuntimeEnvironments {
		runtimeActivityNames[r.Name] = []string{}

		for _, a := range r.Activities {
			runtimeActivityNames[r.Name] = append(runtimeActivityNames[r.Name], a.GetName())
		}

		logging.Infof("  Runtime環境(%s)に適用するアクティビティ: [%s]", r.Name, strings.Join(runtimeActivityNames[r.Name], ", "))
	}

	terraformExecutor := &execute.TerraformExecutor{
		DeliveryEnvironment: config.DeliveryEnvironment,
		RuntimeEnvironments: config.RuntimeEnvironments,
	}

	logging.Infof("適用するアクティビティからpatternインスタンスの適用順を決定しています...")

	if err := terraformExecutor.BuildExecutionPlan(); err != nil {
		return fmt.Errorf("patternの適用順を決定できませんでした"+logging.ErrTraceDelimiter+"%v", err)
	}

	logging.Infof("patternインスタンスの適用順が決定しました")

	logging.Infof("patternインスタンスの適用を開始します")

	if err := terraformExecutor.Execute(ctx); err != nil {
		return fmt.Errorf("patternインスタンスの適用に失敗しました"+logging.ErrTraceDelimiter+"%v", err)
	}

	logging.Infof("patternインスタンスの適用が終了しました")

	logging.Infof("AWS環境にデプロイを実施するための環境が構築されました。")

	// 初回デプロイを行うための手順が記載されたファイルへのリンクを表示する
	// masterブランチの README を参照するように向けている
	logging.Infof("下記リンクに記載された手順を実行し、アプリケーションの初回デプロイを実施してください。")
	logging.Infof("https://gitlab.com/eponas/epona-aws-starter-cli/-/tree/master#deploy-application")

	return nil
}
