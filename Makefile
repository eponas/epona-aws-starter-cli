NAME     := epona-aws-starter-cli
VERSION  := $(shell git describe --tags --always)
REVISION := $(shell git rev-parse --short HEAD)
GOLANGCI_LINT_VERSION := v1.36.0
TERRAFORM_VERSION := 0.14.10

SRCS    := $(shell find . -type f -name '*.go')
# see: https://golang.org/cmd/link/
LDFLAGS := "-s -w -X gitlab.com/eponas/epona-aws-starter-cli/cmd.Version=$(VERSION) -X gitlab.com/eponas/epona-aws-starter-cli/cmd.Revision=$(REVISION)"

.PHONY: devel-deps
devel-deps:
	awk -F'"' '/_/ {print $$2}' tools.go | xargs -tI {} go install {}
	curl -sSfL https://raw.githubusercontent.com/golangci/golangci-lint/master/install.sh | sh -s -- -b $(shell go env GOPATH)/bin $(GOLANGCI_LINT_VERSION)

.PHONY: ci-deps
ci-deps: devel-deps
	apt-get update -y
	apt-get install -y unzip
	curl -so terraform.zip https://releases.hashicorp.com/terraform/$(TERRAFORM_VERSION)/terraform_$(TERRAFORM_VERSION)_linux_amd64.zip
	unzip terraform.zip
	mv terraform /usr/local/bin/

.PHONY: format
format:
	gofmt -l -w -s .
	go mod tidy

.PHONY: lint
lint: format
	golangci-lint run

.PHONY: license-check
license-check:
	go-licenses check .

.PHONY: test
test: lint
	go test -v -coverpkg=./... -coverprofile=coverage.out ./...

.PHONY: coverage
coverage: test
	go tool cover -func coverage.out
	gocover-cobertura < coverage.out > coverage.xml

.PHONY: clean
clean:
	rm -rf dist/* coverage.out coverage.xml $(MODULE_LICENSE_DIR)

.PHONY: build
build:
	go build -ldflags=$(LDFLAGS) -o $(NAME) .

.PHONY: cross-build
cross-build: license-check build
	NAME=$(NAME) VERSION=$(VERSION) LDFLAGS=$(LDFLAGS) ./build/package/cross-build.sh
