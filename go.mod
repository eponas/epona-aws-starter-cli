module gitlab.com/eponas/epona-aws-starter-cli

go 1.15

require (
	github.com/Songmu/goxz v0.6.0
	github.com/boumenot/gocover-cobertura v1.1.0
	github.com/go-playground/validator/v10 v10.4.1
	github.com/google/go-licenses v0.0.0-20201026145851-73411c8fa237
	github.com/hashicorp/go-version v1.2.1
	github.com/hashicorp/hcl/v2 v2.8.2
	github.com/mitchellh/cli v1.1.2
	github.com/sergi/go-diff v1.1.0
	github.com/stretchr/testify v1.6.1
	gopkg.in/yaml.v2 v2.4.0
)
