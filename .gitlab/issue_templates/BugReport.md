# Bug Report

## Summary

<!-- 
バグの概要を記載してください。
-->

## Terraform Version

<!---
`terraform version` の実行結果を以下にペーストしてください
-->

```shell
ペースト場所
```

## Epona AWS Starter CLI Configuration Files

<!--
問題が発生した Epona AWS Starter CLI の設定ファイルを以下にペーストしてください。

なお、秘匿すべき情報が含まれていないことを確認してください。含まれている場合は、別の文字列に置き換えるなどの対処をお願いします
-->

```yaml
ペースト場所
```

## Terraform Configuration Files

<!--
Epona AWS Starter CLI によって作成され、問題が発生した Terraform の設定ファイルを以下にペーストしてください。

なお、秘匿すべき情報が含まれていないことを確認してください。含まれている場合は、別の文字列に置き換えるなどの対処をお願いします
-->

```terraform
ペースト場所
```

## Error Output

<!--
エラーが発生した結果を以下に貼り付けてください。
なお、秘匿すべき情報が含まれていないことを確認してください。
-->

### Expected Behavior

<!--
期待していた挙動を記載してください。
-->

### Actual Behavior

<!--
確認できた挙動を記載してください。
-->

### Steps to Reproduce

<!--
問題の再現手順を記載してください。以下はその例です。

1. `terraform init`
2. `terraform apply`

-->

### Additional Context

<!--
その他、必要と思われる情報を記載してください。
-->

### References

<!--
関係がある、あるいいは関係が疑われる issue あるいは merge request を記載してください。
以下が例となります。

- #1

-->
