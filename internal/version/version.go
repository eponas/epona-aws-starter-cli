package version

import (
	"context"
	"fmt"
)

type contextVersionKey string

const (
	eponaVersionKey       contextVersionKey = "epona.version"
	terraformVersionKey   contextVersionKey = "terraform.version"
	awsProviderVersionKey contextVersionKey = "aws.provider.version"
)

// SetEponaVersion は`eponaVersion`を含む`context.Context`を返却する
func SetEponaVersion(ctx context.Context, eponaVersion string) context.Context {
	return context.WithValue(ctx, eponaVersionKey, eponaVersion)
}

func GetEponaVersion(ctx context.Context) (string, error) {
	v := ctx.Value(eponaVersionKey)

	if eponaVersion, ok := v.(string); ok {
		return eponaVersion, nil
	}

	return "", fmt.Errorf("%s not set", eponaVersionKey)
}

func SetTerraformVersion(ctx context.Context, terraformVersion string) context.Context {
	return context.WithValue(ctx, terraformVersionKey, terraformVersion)
}

func GetTerraformVersion(ctx context.Context) (string, error) {
	v := ctx.Value(terraformVersionKey)

	if terraformVersion, ok := v.(string); ok {
		return terraformVersion, nil
	}

	return "", fmt.Errorf("%s not set", terraformVersionKey)
}

func SetAwsProviderVersion(ctx context.Context, awsProviderVersion string) context.Context {
	return context.WithValue(ctx, awsProviderVersionKey, awsProviderVersion)
}

func GetAwsProviderVersion(ctx context.Context) (string, error) {
	v := ctx.Value(awsProviderVersionKey)

	if awsProviderVersion, ok := v.(string); ok {
		return awsProviderVersion, nil
	}

	return "", fmt.Errorf("%s not set", awsProviderVersionKey)
}
