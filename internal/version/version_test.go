package version

import (
	"context"
	"testing"
)

func TestSetEponaVersion(t *testing.T) {
	testCases := []struct {
		name    string
		version string
	}{
		{
			name:    "semantic versioning",
			version: "v0.2.1",
		},
		{
			name:    "not semantic versioning",
			version: "0.2.1",
		},
	}

	for _, tc := range testCases {
		ctx := SetEponaVersion(context.Background(), tc.version)
		actual := ctx.Value(eponaVersionKey)
		if actual != tc.version {
			t.Errorf("%s: expected %s, but got %s", tc.name, tc.version, actual)
		}
	}
}
