package aws

import (
	"encoding/json"
	"fmt"
	"os"
	"os/exec"
	"strings"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/logging"
)

const (
	awsCommand = "aws"
)

type AwsCredentialProvider interface {
	GetAccessKeyID() string
	GetSecretAccessKey() string
	GetRegion() string
}

func SetCredential(credentialProvider AwsCredentialProvider) {
	os.Setenv("AWS_ACCESS_KEY_ID", credentialProvider.GetAccessKeyID())
	os.Setenv("AWS_SECRET_ACCESS_KEY", credentialProvider.GetSecretAccessKey())
	os.Setenv("AWS_DEFAULT_REGION", credentialProvider.GetRegion())
}

func UnsetCredential() {
	os.Unsetenv("AWS_ACCESS_KEY_ID")
	os.Unsetenv("AWS_SECRET_ACCESS_KEY")
	os.Unsetenv("AWS_DEFAULT_REGION")
}

func GetAccountID(credentialProvider AwsCredentialProvider) (string, error) {
	SetCredential(credentialProvider)
	defer UnsetCredential()

	args := []string{"sts", "get-caller-identity", "--query", "Account"}

	cmd := exec.Command(awsCommand, args...)

	output, err := cmd.Output()

	if err != nil {
		return "", err
	}

	var accountID string

	err = json.Unmarshal(output, &accountID)

	if err != nil {
		return "", err
	}

	return accountID, nil
}

func GetUsername(credentialProvider AwsCredentialProvider) (string, error) {
	SetCredential(credentialProvider)
	defer UnsetCredential()

	args := []string{"iam", "get-user", "--query", "User.UserName"}

	cmd := exec.Command(awsCommand, args...)

	output, err := cmd.Output()

	if err != nil {
		return "", err
	}

	var username string

	err = json.Unmarshal(output, &username)

	if err != nil {
		return "", err
	}

	return username, nil
}

func ExistsS3Bucket(credentialProvider AwsCredentialProvider, bucketName string) (bool, error) {
	SetCredential(credentialProvider)
	defer UnsetCredential()

	args := []string{"s3api", "head-bucket", "--bucket", bucketName}

	cmd := exec.Command(awsCommand, args...)

	output, err := cmd.CombinedOutput()

	if err != nil {
		outputString := string(output)

		// バケットの存在有無は、エラーメッセージからハンドリング

		if strings.Contains(outputString, "403") && strings.Contains(outputString, "Forbidden") {
			// Forbiddenの場合は参照できないS3バケットが存在する（別のAWSアカウントに存在する場合を含む）
			return true, nil
		} else if strings.Contains(outputString, "404") && strings.Contains(outputString, "Not Found") {
			// S3バケットが存在しない
			return false, nil
		}

		return false, err
	} else {
		// コマンドが成功した場合は、同一AWSアカウント内にS3バケットが存在する
		return true, nil
	}
}

func CreateS3Bucket(credentialProvider AwsCredentialProvider, bucketName string, region string) error {
	iamUsername, _ := GetUsername(credentialProvider)

	SetCredential(credentialProvider)
	defer UnsetCredential()

	args := []string{"s3api", "create-bucket", "--bucket", bucketName, "--create-bucket-configuration", fmt.Sprintf("LocationConstraint=%s", region)}

	logging.Infof(" command: (%s)$ %s %s", iamUsername, awsCommand, strings.Join(args, " "))

	cmd := exec.Command(awsCommand, args...)

	output, err := cmd.CombinedOutput()

	if err != nil {
		return fmt.Errorf("%s: %v", output, err)
	} else {
		return nil
	}
}

func ExistsDynamoDBTable(credentialProvider AwsCredentialProvider, tableName string) (bool, error) {
	SetCredential(credentialProvider)
	defer UnsetCredential()

	args := []string{"dynamodb", "describe-table", "--table-name", tableName}

	cmd := exec.Command(awsCommand, args...)

	output, err := cmd.CombinedOutput()

	if err != nil {
		// エラーになった理由から存在有無を判定する
		outputString := string(output)

		if strings.Contains(outputString, "ResourceNotFoundException") {
			return false, nil
		}
	} else {
		return true, nil
	}

	return false, err
}

func ExistsIamUser(credentialProvider AwsCredentialProvider, userName string) (bool, error) {
	SetCredential(credentialProvider)
	defer UnsetCredential()

	args := []string{"iam", "get-user", "--user-name", userName}

	cmd := exec.Command(awsCommand, args...)

	output, err := cmd.CombinedOutput()

	if err != nil {
		// エラーになった理由から存在有無を判定する
		outputString := string(output)

		if strings.Contains(outputString, "NoSuchEntity") {
			return false, nil
		}
	} else {
		return true, nil
	}

	return false, err
}

func ExistsIamRole(credentialProvider AwsCredentialProvider, roleName string) (bool, error) {
	SetCredential(credentialProvider)
	defer UnsetCredential()

	args := []string{"iam", "get-role", "--role-name", roleName}

	cmd := exec.Command(awsCommand, args...)

	output, err := cmd.CombinedOutput()

	if err != nil {
		// エラーになった理由から存在有無を判定する
		outputString := string(output)

		if strings.Contains(outputString, "NoSuchEntity") {
			return false, nil
		}
	} else {
		return true, nil
	}

	return false, err
}

func CreateAccessKey(credentialProvider AwsCredentialProvider, userName string) (map[string]string, error) {
	SetCredential(credentialProvider)
	defer UnsetCredential()

	args := []string{"iam", "create-access-key", "--user-name", userName}

	cmd := exec.Command(awsCommand, args...)

	output, err := cmd.CombinedOutput()

	if err != nil {
		return map[string]string{}, err
	}

	outputString := string(output)

	var m map[string]interface{}

	r := strings.NewReader(outputString)
	d := json.NewDecoder(r)

	err = d.Decode(&m)

	if err != nil {
		return map[string]string{}, err
	}

	accessKey := m["AccessKey"].(map[string]interface{})

	credentialMap := map[string]string{
		"UserName":        fmt.Sprintf("%s", accessKey["UserName"]),
		"AccessKeyId":     fmt.Sprintf("%s", accessKey["AccessKeyId"]),
		"SecretAccessKey": fmt.Sprintf("%s", accessKey["SecretAccessKey"]),
		"Status":          fmt.Sprintf("%s", accessKey["Status"]),
	}

	return credentialMap, nil
}
