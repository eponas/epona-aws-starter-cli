package validator

import (
	"fmt"
	"reflect"
	"strings"

	pgvalidator "github.com/go-playground/validator/v10"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/logging"
)

// ValidationError は本CLIで発生するバリデーションエラーの内容を示す
type ValidationError struct {
	// ユーザに表示するエラーメッセージの slice。個々のバリデーションエラーごとに 1 つのメッセージとなる。
	msgs []string
	// github.com/go-playground/validator/v10 が返却する Error。テスト時にのみ使用する。
	Cause error
}

// generateMsg
func generateMsg(tag, param, namespace string, value interface{}) string {

	// トップレベルの Struct 名を抜く
	// そうすると、あとは YAML のフィールド名だけが残る
	fieldPath := strings.Join(strings.Split(namespace, ".")[1:], ".")

	switch tag {
	case "required":
		return fmt.Sprintf("  - %sが指定されていません", fieldPath)
	case "printascii":
		return fmt.Sprintf("  - %sにはASCII文字を指定してください: %s", fieldPath, value)
	case "alphanum":
		return fmt.Sprintf("  - %sには英数字を指定してください: %s", fieldPath, value)
	case "max":
		return fmt.Sprintf("  - %sには%s文字以内で指定してください: %s", fieldPath, param, value)
	case "oneof":
		return fmt.Sprintf("  - %sに指定できるのは%sです: %s", fieldPath, param, value)
	case "lt":
		return fmt.Sprintf("  - %sは%s文字以下で指定してください: %s", fieldPath, param, value)
	case "len":
		return fmt.Sprintf("  - %sは%s文字で指定してください: %s", fieldPath, param, value)
	case "number":
		return fmt.Sprintf("  - %sは全て数字で指定してください: %s", fieldPath, value)
	case "hostname_rfc1123":
		return fmt.Sprintf("  - %sは有効なホストゾーン(RFC1123)になっていません: %s", fieldPath, value)
	case "email":
		return fmt.Sprintf("  - %sは有効なメールアドレスの形式になっていません: %s", fieldPath, value)
	case "cidr":
		return fmt.Sprintf("  - %sは有効なCIDRの形式になっていません: %s", fieldPath, value)
	case "dive":
		return fmt.Sprintf(" - %sの子要素でエラーが発生しました: %s", fieldPath, value)
	default:
		logging.Fatalf("[BUG]:%sに対するエラーメッセージが定義されていません", tag)
		return ""
	}
}

// NewValidationError は初期化された ValidationError を返却する
func NewValidationError(cause error) *ValidationError {
	msgs := []string{}
	for _, err := range cause.(pgvalidator.ValidationErrors) {
		msgs = append(msgs, generateMsg(err.Tag(), err.Param(), err.Namespace(), err.Value()))
	}

	return &ValidationError{
		msgs:  msgs,
		Cause: cause,
	}
}

// Error はバリデーションエラーに関する説明文言を返却する
func (v *ValidationError) Error() string {
	return fmt.Sprintf("以下のエラーが発生しました:\n%s", strings.Join(v.msgs, "\n"))
}

// Validate は c で表現される構造体に対するバリデーションを行う。
func Validate(c interface{}, tagName string) error {

	v := pgvalidator.New()

	// play-ground/validator でバリデーションエラーを補足した際、そのエラー対象フィールド名を
	// tagNameの先頭の値から取得する
	// 例: tagNameを"yaml"と指定し、フィールドに`yaml:hoge,fuga,piyo"というタグが設定されていた場合、"hoge"が対象フィールド名となる
	v.RegisterTagNameFunc(func(f reflect.StructField) string {
		name := strings.SplitN(f.Tag.Get(tagName), ",", 2)[0]
		if name == "-" {
			return ""
		}
		return name
	})

	if err := v.Struct(c); err != nil {
		// fmt.Errorf("%w")を使わないのは、go-playground/validator のエラーメッセージが
		// そのまま表示されるのを避けるため
		return NewValidationError(err)
	}
	return nil
}
