package validator

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

type Parent struct {
	Name     string   `yaml:"name" validate:"required"`
	Children []*Child `yaml:"children" validate:"dive,required"`
}

type Child struct {
	Name string `yaml:"child_name,fuga,piyo" validate:"required,lt=4"`
	Age  int    `yaml:"child_age,fuga,piyo" validate:"required"`
}

type errorField struct {
	tag   string
	param string
	field string
	value interface{}
}

// createValidationError はテスト時の期待値を作成する
func createValidationError(efs []errorField) *ValidationError {
	msgs := []string{}
	for _, ef := range efs {
		msgs = append(msgs, generateMsg(ef.tag, ef.param, ef.field, ef.value))
	}
	return &ValidationError{
		msgs: msgs,
	}
}

func TestValidate(t *testing.T) {
	testCases := []struct {
		name     string
		target   interface{}
		expected []errorField
	}{
		{
			name:   "シンプルな構造体アクセス",
			target: Child{Name: "taro", Age: 10},
			expected: []errorField{{
				tag:   "lt",
				param: "4",
				field: "Child.child_name",
				value: "taro",
			}},
		},
		{
			name:   "複数のバリデーションエラー",
			target: Child{},
			expected: []errorField{
				{
					tag:   "required",
					param: "",
					field: "Child.child_name",
					value: "",
				},
				{
					tag:   "required",
					param: "",
					field: "Child.child_age",
					value: "",
				},
			},
		},
		{
			name: "Structの入れ子",
			target: Parent{
				Name: "parent",
				Children: []*Child{
					{Name: "taro", Age: 10},
					{Name: "abc", Age: 10},
				},
			},
			expected: []errorField{{
				tag:   "lt",
				param: "4",
				field: "Parent.children[0].child_name",
				value: "taro",
			}},
		},
	}

	for _, tc := range testCases {
		err := Validate(tc.target, "yaml")
		if assert.Error(t, err, tc.name) {
			assert.EqualError(t, err, createValidationError(tc.expected).Error(), tc.name)
		}
	}
}
