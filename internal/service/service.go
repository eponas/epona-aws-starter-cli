package service

import (
	"context"
	"fmt"
)

type contextServiceKey string

const (
	prefixKey      contextServiceKey = "prefix"
	serviceNameKey contextServiceKey = "service.name"
)

func SetPrefix(ctx context.Context, prefix string) context.Context {
	return context.WithValue(ctx, prefixKey, prefix)
}

func GetPrefix(ctx context.Context) string {
	v := ctx.Value(prefixKey)

	if prefix, ok := v.(string); ok {
		return prefix
	}

	return ""
}

func SetName(ctx context.Context, serviceName string) context.Context {
	return context.WithValue(ctx, serviceNameKey, serviceName)
}

func GetName(ctx context.Context) (string, error) {
	v := ctx.Value(serviceNameKey)

	if serviceName, ok := v.(string); ok {
		return serviceName, nil
	}

	return "", fmt.Errorf("%s not set", serviceNameKey)
}
