package lang

import (
	"strings"
)

// Terraformの組み込み関数互換のものを必要に応じて作成
// https://github.com/hashicorp/terraform/blob/master/lang/functions.go
// https://github.com/zclconf/go-cty/blob/main/cty/function/stdlib

func Title(s string) string {
	return strings.Title(s)
}
