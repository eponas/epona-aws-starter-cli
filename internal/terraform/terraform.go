package terraform

import (
	"bytes"
	"fmt"
	"io"
	"os"
	"os/exec"
	"strings"
	"sync"
	"time"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/aws"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/logging"
)

const (
	terraformCommand = "terraform"
)

var (
	SetAwsCredential   = aws.SetCredential
	UnsetAwsCredential = aws.UnsetCredential
)

type AwsCredentialProvider interface {
	GetAccessKeyID() string
	GetSecretAccessKey() string
	GetRegion() string
}

func Init(credentialProvider AwsCredentialProvider, directory string, initArgs ...string) error {
	return executeCommandWithLogging(credentialProvider, directory, func(iamUsername string) *exec.Cmd {
		subCommand := "init"

		loggingArgs := []string{}
		loggingArgs = append(loggingArgs, subCommand)
		loggingArgs = append(loggingArgs, initArgs...)

		actualArgs := []string{}
		actualArgs = append(actualArgs, loggingArgs...)
		actualArgs = append(actualArgs, "-no-color")

		if iamUsername != "" {
			logging.Infof("  command: (%s:%s)$ %s %s", iamUsername, directory, terraformCommand, strings.Join(loggingArgs, " "))
		} else {
			logging.Infof("  command: (%s)$ %s %s", directory, terraformCommand, strings.Join(loggingArgs, " "))
		}

		return exec.Command(terraformCommand, actualArgs...)
	})
}

func Format(directory string) error {
	return executeCommandSimply(directory, func() *exec.Cmd {
		subCommand := "fmt"

		return exec.Command(terraformCommand, subCommand)
	})
}

func Import(credentialProvider AwsCredentialProvider, directory string, importArgs ...string) error {
	return executeCommandWithLogging(credentialProvider, directory, func(iamUsername string) *exec.Cmd {
		subCommand := "import"

		loggingArgs := []string{}
		loggingArgs = append(loggingArgs, subCommand)
		loggingArgs = append(loggingArgs, importArgs...)

		actualArgs := []string{}
		actualArgs = append(actualArgs, loggingArgs...)
		actualArgs = append(actualArgs, "-no-color")

		if iamUsername != "" {
			logging.Infof("  command: (%s:%s)$ %s %s", iamUsername, directory, terraformCommand, strings.Join(loggingArgs, " "))
		} else {
			logging.Infof(" command: (%s)$ %s %s", directory, terraformCommand, strings.Join(loggingArgs, " "))
		}

		return exec.Command(terraformCommand, actualArgs...)
	})
}

func Apply(credentialProvider AwsCredentialProvider, directory string, applyArgs ...string) error {
	return executeCommandWithLogging(credentialProvider, directory, func(iamUsername string) *exec.Cmd {
		subCommand := "apply"

		loggingArgs := []string{}
		loggingArgs = append(loggingArgs, subCommand)
		loggingArgs = append(loggingArgs, applyArgs...)

		actualArgs := []string{}
		actualArgs = append(actualArgs, loggingArgs...)
		actualArgs = append(actualArgs, "-auto-approve")
		actualArgs = append(actualArgs, "-no-color")

		if iamUsername != "" {
			logging.Infof("  command: (%s:%s)$ %s %s", iamUsername, directory, terraformCommand, strings.Join(loggingArgs, " "))
		} else {
			logging.Infof("  command: (%s)$ %s %s", directory, terraformCommand, strings.Join(loggingArgs, " "))
		}

		return exec.Command(terraformCommand, actualArgs...)
	})
}

func Output(credentialProvider AwsCredentialProvider, directory string) (string, error) {
	return executeCommandCaptureOutput(credentialProvider, directory, func() *exec.Cmd {
		subCommand := "output"
		args := []string{
			subCommand,
			"-no-color",
		}

		return exec.Command(terraformCommand, args...)
	})
}

func executeCommandWithLogging(credentialProvider AwsCredentialProvider, directory string, commandBuilder func(string) *exec.Cmd) error {
	var iamUsername string = ""

	if credentialProvider != nil {
		iamUsername, _ = aws.GetUsername(credentialProvider)

		SetAwsCredential(credentialProvider)
		defer UnsetAwsCredential()
	}

	currentWorkingDir, _ := os.Getwd()
	defer os.Chdir(currentWorkingDir) // nolint:errcheck

	if iamUsername != "" {
		logging.Infof("  command: (%s:[current directory])$ cd %s", iamUsername, directory)
	} else {
		logging.Infof("  command: ([current directory])$ cd %s", directory)
	}

	err := os.Chdir(directory)

	if err != nil {
		return err
	}

	cmd := commandBuilder(iamUsername)

	stdout, _ := cmd.StdoutPipe()
	stderr, _ := cmd.StderrPipe()

	outputReader := io.MultiReader(stdout, stderr)

	err = cmd.Start()

	if err != nil {
		return err
	}

	ticker := time.NewTicker(5 * time.Second)
	defer ticker.Stop()

	c := make(chan bool, 1)

	var wg sync.WaitGroup
	wg.Add(1)

	go func() {
		io.Copy(logging.DirectFileLogger, outputReader) // nolint:errcheck

		stdout.Close()
		wg.Done()
	}()

	go func() {
		for {
			select {
			case <-c:
				// end
				break
			case <-ticker.C:
				logging.DirectStandardOutLogger.Write([]byte(".")) // nolint:errcheck
			}
		}
	}()

	wg.Wait()

	err = cmd.Wait()

	c <- true

	logging.DirectStandardOutLogger.Write([]byte(" (done)\n")) // nolint:errcheck

	if err != nil {
		return err
	}

	return nil
}

func executeCommandCaptureOutput(credentialProvider AwsCredentialProvider, directory string, commandBuilder func() *exec.Cmd) (string, error) {
	SetAwsCredential(credentialProvider)
	defer UnsetAwsCredential()

	currentWorkingDir, _ := os.Getwd()
	defer os.Chdir(currentWorkingDir) // nolint:errcheck

	err := os.Chdir(directory)

	if err != nil {
		return "", err
	}

	cmd := commandBuilder()

	output, err := cmd.Output()

	if err != nil {
		return "", err
	}

	return string(output), nil
}

func executeCommandSimply(directory string, commandBuilder func() *exec.Cmd) error {
	currentWorkingDir, _ := os.Getwd()
	defer os.Chdir(currentWorkingDir) // nolint:errcheck

	err := os.Chdir(directory)

	if err != nil {
		return err
	}

	cmd := commandBuilder()
	var outbuf, errbuf bytes.Buffer
	cmd.Stdout = &outbuf
	cmd.Stderr = &errbuf

	if err := cmd.Run(); err != nil {
		logging.Errorf(errbuf.String())
		return fmt.Errorf("%sを実行する際にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", cmd, err)
	}
	return err
}
