package template

import (
	"bufio"
	"fmt"
	"io/ioutil"
	"math/rand"
	"os"
	"path/filepath"
	"reflect"
	"strings"
	text "text/template"
	"time"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/logging"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/terraform/lang"
)

func init() {
	rand.Seed(time.Now().UnixNano())
}

func EvalTemplateAsString(templateString string, data interface{}) (string, error) {
	templateName := fmt.Sprintf("%d.tmpl", rand.Int63()) // nolint:gosec

	tmpl, err := text.New(templateName).Parse(templateString)

	if err != nil {
		return "", err
	}

	writer := &strings.Builder{}

	if err := tmpl.Execute(writer, data); err != nil {
		return "", err
	}

	return writer.String(), nil
}

func EvalTerraformTemplateWriteToFile(templateString string, data interface{}, outputPath string) error {
	parentDir := filepath.Dir(outputPath)

	if _, err := os.Stat(parentDir); os.IsNotExist(err) {
		if err := os.MkdirAll(parentDir, 0755); err != nil {
			return fmt.Errorf("ディレクトリ[%s]が作成できませんでした"+logging.ErrTraceDelimiter+"%v", parentDir, err)
		}
	}

	templateName := fmt.Sprintf("%s.tmpl", outputPath)

	funcMap := text.FuncMap{
		"tfTitle": lang.Title,
		"add":     func(a, b int) int { return a + b }, // intの加算
		"hasField": func(v interface{}, name string) bool {
			rv := reflect.ValueOf(v)
			if rv.Kind() == reflect.Ptr {
				rv = rv.Elem()
			}
			if rv.Kind() != reflect.Struct {
				return false
			}
			return rv.FieldByName(name).IsValid()
		}, // 構造体のField有無チェック
	}

	tmpl, err := text.New(templateName).Funcs(funcMap).Parse(templateString)

	if err != nil {
		return err
	}

	file, err := os.OpenFile(outputPath, os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0644)

	if err != nil {
		return err
	}

	defer file.Close()

	logging.Infof("  Terraform構成ファイルを作成しています ... %s", outputPath)

	bufferedWriter := bufio.NewWriter(file)

	err = tmpl.Execute(bufferedWriter, data)

	if err != nil {
		return err
	}

	bufferedWriter.Flush()

	return nil
}

func EvalTemplateWriteToFile(templateString string, data interface{}, outputPath string) error {
	parentDir := filepath.Dir(outputPath)
	if _, err := os.Stat(parentDir); os.IsNotExist(err) {
		if err := os.MkdirAll(parentDir, 0755); err != nil {
			return fmt.Errorf("ディレクトリ[%s]が作成できませんでした"+logging.ErrTraceDelimiter+"%v", parentDir, err)
		}
	}

	templateName := fmt.Sprintf("%s.tmpl", outputPath)

	tmpl, err := text.New(templateName).Parse(templateString)

	if err != nil {
		return err
	}

	file, err := os.OpenFile(outputPath, os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0644)

	if err != nil {
		return err
	}

	defer file.Close()

	logging.Infof("  ファイルを作成しています ... %s", outputPath)

	bufferedWriter := bufio.NewWriter(file)

	err = tmpl.Execute(bufferedWriter, data)

	if err != nil {
		return err
	}

	bufferedWriter.Flush()

	return nil
}

func EvalTemplateMergeToFile(templateString string, data interface{}, outputPath string) error {
	parentDir := filepath.Dir(outputPath)
	if _, err := os.Stat(parentDir); os.IsNotExist(err) {
		if err := os.MkdirAll(parentDir, 0755); err != nil {
			return fmt.Errorf("ディレクトリ[%s]が作成できませんでした"+logging.ErrTraceDelimiter+"%v", parentDir, err)
		}
	}

	templateName := fmt.Sprintf("%s.tmpl", outputPath)

	tmpl, err := text.New(templateName).Parse(templateString)

	if err != nil {
		return err
	}

	writeTarget := new(strings.Builder)

	err = tmpl.Execute(writeTarget, data)

	if err != nil {
		return err
	}

	file, err := os.OpenFile(outputPath, os.O_CREATE|os.O_RDWR, 0644)

	if err != nil {
		return err
	}

	defer file.Close()

	bufferedReader := bufio.NewReader(file)
	bytes, err := ioutil.ReadAll(bufferedReader)

	if err != nil {
		return err
	}

	contents := string(bytes)

	if strings.Contains(contents, writeTarget.String()) {
		return nil
	}

	logging.Infof("  ファイルを作成しています ... %s", outputPath)

	bufferedWriter := bufio.NewWriter(file)
	if _, err := bufferedWriter.WriteString(writeTarget.String()); err != nil {
		return err
	}
	if _, err := bufferedWriter.WriteString("\n"); err != nil {
		return err
	}
	bufferedWriter.Flush()

	return nil
}
