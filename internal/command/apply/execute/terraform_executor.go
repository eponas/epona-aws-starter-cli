package execute

import (
	"context"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/command/apply/delivery"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/command/apply/pattern"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/command/apply/runtime"
)

type TerraformExecutor struct {
	DeliveryEnvironment *delivery.DeliveryEnvironment
	RuntimeEnvironments []*runtime.RuntimeEnvironment
	PatternInstances    []pattern.PatternInstance
}

func (t *TerraformExecutor) BuildExecutionPlan() error {
	t.PatternInstances = []pattern.PatternInstance{}

	deliveryPatternInstances := uniqDeliveryPatternInstances(t.DeliveryEnvironment)

	runtimePatternInstances := map[string][]pattern.PatternInstance{}

	for _, runtime := range t.RuntimeEnvironments {
		runtimePatternInstances[runtime.Name] = uniqRuntimePatternInstances(runtime)
	}

	t.PatternInstances = t.calcPatternInstanceApplyOrder(deliveryPatternInstances, runtimePatternInstances)

	return nil
}

func uniqDeliveryPatternInstances(deliveryEnvironment *delivery.DeliveryEnvironment) []pattern.PatternInstance {
	patternInstances := []pattern.PatternInstance{}

	for _, activity := range deliveryEnvironment.Activities {
		patternInstances = append(patternInstances, activity.GetPatternInstances()...)
	}
	return uniqPatternInstances(patternInstances)
}

func uniqRuntimePatternInstances(runtimeEnvironment *runtime.RuntimeEnvironment) []pattern.PatternInstance {
	patternInstances := []pattern.PatternInstance{}

	for _, activity := range runtimeEnvironment.Activities {
		patternInstances = append(patternInstances, activity.GetPatternInstances()...)
	}

	return uniqPatternInstances(patternInstances)
}

func uniqPatternInstances(originPatternInstances []pattern.PatternInstance) []pattern.PatternInstance {
	tmp := map[string]bool{}

	uniqued := []pattern.PatternInstance{}

	for _, p := range originPatternInstances {
		directory := p.GetDirectory()

		if !tmp[directory] {
			tmp[directory] = true
			uniqued = append(uniqued, p)
		}
	}

	return uniqued
}

// calcPatternInstanceApplyOrderは、各環境ごとのPatternInstanceの適用順を決定する。
func (t *TerraformExecutor) calcPatternInstanceApplyOrder(deliveryPatternInstances []pattern.PatternInstance, runtimePatternInstances map[string][]pattern.PatternInstance) []pattern.PatternInstance { // nolint: gocognit, gocyclo
	patternInstances := []pattern.PatternInstance{}

	plannedDeliveryPatternInstances := []pattern.PatternInstance{}
	plannedRuntimePatternInstances := map[string][]pattern.PatternInstance{}

	var deliveryNetwork *pattern.Network
	var ciPipeline *pattern.CiPipeline
	/**  Delivery環境上にCI/CDパイプラインを構築する **/
	if nw, ok := queryPatternInstance(deliveryPatternInstances,
		func(p pattern.PatternInstance) bool {
			_, isNetwork := p.(*pattern.Network)
			return isNetwork
		}); ok {
		plannedDeliveryPatternInstances = append(plannedDeliveryPatternInstances, nw)
		deliveryNetwork, _ = nw.(*pattern.Network)
	}

	if cp, ok := queryPatternInstance(deliveryPatternInstances,
		func(p pattern.PatternInstance) bool {
			_, isCiPipeline := p.(*pattern.CiPipeline)
			return isCiPipeline
		}); ok {
		plannedDeliveryPatternInstances = append(plannedDeliveryPatternInstances, cp)

		if ciPipeline, ok = cp.(*pattern.CiPipeline); ok {
			ciPipeline.RegisterInstance(deliveryNetwork)
		}
	}

	/** Delivery環境で、Runtime環境数に依存するインスタンスの定義 **/
	for _, r := range t.RuntimeEnvironments {
		// cd_pipeline_backend_triggerパターンインスタンス
		for _, name := range []string{"backend", "notifier"} {
			if trigger, ok := queryPatternInstance(deliveryPatternInstances,
				func(p pattern.PatternInstance) bool {
					ct, isCdPipelineBackendTrigger := p.(*pattern.CdPipelineBackendTrigger)

					return isCdPipelineBackendTrigger && ct.GetName() == "cd_pipeline_backend_trigger_"+name && ct.PeerEnvironmentName == r.Name
				}); ok {
				plannedDeliveryPatternInstances = append(plannedDeliveryPatternInstances, trigger)

				if cdbt, ok := trigger.(*pattern.CdPipelineBackendTrigger); ok {
					cdbt.RegisterInstance(ciPipeline)
				}
			}
		}

		// cd_pipeline_frontend_trigger パターンインスタンス
		if cdft, ok := queryPatternInstance(deliveryPatternInstances,
			func(p pattern.PatternInstance) bool {
				ct, isCdPipelineFrontendTrigger := p.(*pattern.CdPipelineFrontendTrigger)

				return isCdPipelineFrontendTrigger && ct.PeerEnvironmentName == r.Name
			}); ok {
			plannedDeliveryPatternInstances = append(plannedDeliveryPatternInstances, cdft)

			if f, ok := cdft.(*pattern.CdPipelineFrontendTrigger); ok {
				f.RegisterInstance(ciPipeline)
			}
		}
	}

	/** Runtime環境にアプリケーションのCDパイプラインを構築する **/
	for _, r := range t.RuntimeEnvironments {
		runtimeName := r.Name

		var network *pattern.Network
		var encryptionKey *pattern.EncryptionKey
		var database *pattern.Database
		var redis *pattern.Redis
		var webacl *pattern.Webacl
		var sesSMTPUser *pattern.SesSMTPUser
		var cacheableFrontend *pattern.CacheableFrontend
		var cdPipelineFrontend *pattern.CdPipelineFrontend

		if n, ok := queryPatternInstance(runtimePatternInstances[runtimeName],
			func(p pattern.PatternInstance) bool {
				_, isNetwork := p.(*pattern.Network)

				return isNetwork && p.GetName() == "network"
			}); ok {
			plannedRuntimePatternInstances[runtimeName] = append(plannedRuntimePatternInstances[runtimeName], n)
			network, _ = n.(*pattern.Network)
		}

		if e, ok := queryPatternInstance(runtimePatternInstances[runtimeName],
			func(p pattern.PatternInstance) bool {
				_, isEncryptionKey := p.(*pattern.EncryptionKey)
				return isEncryptionKey
			}); ok {
			plannedRuntimePatternInstances[runtimeName] = append(plannedRuntimePatternInstances[runtimeName], e)
			encryptionKey, _ = e.(*pattern.EncryptionKey)
		}

		if d, ok := queryPatternInstance(runtimePatternInstances[runtimeName],
			func(p pattern.PatternInstance) bool {
				_, isDatabase := p.(*pattern.Database)
				return isDatabase
			}); ok {
			plannedRuntimePatternInstances[runtimeName] = append(plannedRuntimePatternInstances[runtimeName], d)

			database, _ = d.(*pattern.Database)
			database.RegisterInstance(network)
			database.RegisterInstance(encryptionKey)
		}

		if r, ok := queryPatternInstance(runtimePatternInstances[runtimeName],
			func(p pattern.PatternInstance) bool {
				_, isRedis := p.(*pattern.Redis)
				return isRedis
			}); ok {
			plannedRuntimePatternInstances[runtimeName] = append(plannedRuntimePatternInstances[runtimeName], r)

			redis, _ = r.(*pattern.Redis)
			redis.RegisterInstance(network)
			redis.RegisterInstance(encryptionKey)
		}

		for _, name := range []string{"backend", "notifier"} {
			var backend *pattern.PublicTrafficContainerService

			if p, ok := queryPatternInstance(runtimePatternInstances[runtimeName],
				func(p pattern.PatternInstance) bool {
					_, isPublicTrafficContainerService := p.(*pattern.PublicTrafficContainerService)

					return isPublicTrafficContainerService && p.GetName() == "public_traffic_container_service_"+name
				}); ok {
				plannedRuntimePatternInstances[runtimeName] = append(plannedRuntimePatternInstances[runtimeName], p)

				if backend, ok = p.(*pattern.PublicTrafficContainerService); ok {
					backend.RegisterInstance(network)
				}
			}

			var cdb *pattern.CdPipelineBackend
			if p, ok := queryPatternInstance(runtimePatternInstances[runtimeName],
				func(p pattern.PatternInstance) bool {
					_, isCdPipelineBackend := p.(*pattern.CdPipelineBackend)

					return isCdPipelineBackend && p.GetName() == "cd_pipeline_backend_"+name
				}); ok {
				plannedRuntimePatternInstances[runtimeName] = append(plannedRuntimePatternInstances[runtimeName], p)

				if cdb, ok = p.(*pattern.CdPipelineBackend); ok {
					cdb.RegisterInstance(backend)
				}
			}

			// cd_pipeline_backend の Output をバインドして、対応するtrigger patternを再適用対象として登録する
			if tp, ok := queryPatternInstance(deliveryPatternInstances,
				func(p pattern.PatternInstance) bool {
					ct, isCdPipelineBackendTrigger := p.(*pattern.CdPipelineBackendTrigger)

					return isCdPipelineBackendTrigger && ct.GetName() == "cd_pipeline_backend_trigger_"+name && ct.PeerEnvironmentName == r.Name
				}); ok {
				if trigger, ok := tp.(*pattern.CdPipelineBackendTrigger); ok {
					// 対応する trigger pattern の Output を Bind する
					cdb.BindOutputInstance(trigger)

					// 対応する trigger pattern にも cd_pipeline_backend の Output を Bind して、再適用対象として登録する
					trigger.BindOutputInstance(cdb)
					cdb.RegisterReApplyInstance(trigger)
				}
			}
		}

		if w, ok := queryPatternInstance(runtimePatternInstances[runtimeName],
			func(p pattern.PatternInstance) bool {
				_, isWebacl := p.(*pattern.Webacl)

				return isWebacl && p.GetName() == "webacl_frontend"
			}); ok {
			plannedRuntimePatternInstances[runtimeName] = append(plannedRuntimePatternInstances[runtimeName], w)
			webacl, _ = w.(*pattern.Webacl)
		}

		if c, ok := queryPatternInstance(runtimePatternInstances[runtimeName],
			func(p pattern.PatternInstance) bool {
				_, isCacheableFrontend := p.(*pattern.CacheableFrontend)

				return isCacheableFrontend
			}); ok {
			plannedRuntimePatternInstances[runtimeName] = append(plannedRuntimePatternInstances[runtimeName], c)

			if cacheableFrontend, ok = c.(*pattern.CacheableFrontend); ok {
				cacheableFrontend.RegisterInstance(webacl)
			}
		}

		if cdf, ok := queryPatternInstance(runtimePatternInstances[runtimeName],
			func(p pattern.PatternInstance) bool {
				_, isCdPipelineFrontend := p.(*pattern.CdPipelineFrontend)

				return isCdPipelineFrontend
			}); ok {
			plannedRuntimePatternInstances[runtimeName] = append(plannedRuntimePatternInstances[runtimeName], cdf)

			if cdPipelineFrontend, ok = cdf.(*pattern.CdPipelineFrontend); ok {
				cdPipelineFrontend.RegisterInstance(cacheableFrontend)
			}
		}

		// cd_pipeline_frontend の Output をバインドして、対応する trigger pattern を再適用対象として登録する
		if tp, ok := queryPatternInstance(deliveryPatternInstances,
			func(p pattern.PatternInstance) bool {
				ct, isCdPipelineFrontendTrigger := p.(*pattern.CdPipelineFrontendTrigger)

				return isCdPipelineFrontendTrigger && ct.PeerEnvironmentName == runtimeName
			}); ok {
			if cdft, ok := tp.(*pattern.CdPipelineFrontendTrigger); ok {
				// 対応する trigger pattern の Output を Bind する
				cdPipelineFrontend.BindOutputInstance(cdft)

				// 対応する trigger pattern にも cd_pipeline_frontend の Output を Bind して、再適用対象として登録する
				cdft.BindOutputInstance(cdPipelineFrontend)
				cdPipelineFrontend.RegisterReApplyInstance(cdft)
			}
		}

		if s, ok := queryPatternInstance(runtimePatternInstances[runtimeName],
			func(p pattern.PatternInstance) bool {
				_, isSesSMTPUser := p.(*pattern.SesSMTPUser)
				return isSesSMTPUser
			}); ok {
			plannedRuntimePatternInstances[runtimeName] = append(plannedRuntimePatternInstances[runtimeName], s)

			sesSMTPUser, _ = s.(*pattern.SesSMTPUser)
		}

		if p, ok := queryPatternInstance(runtimePatternInstances[runtimeName],
			func(p pattern.PatternInstance) bool {
				_, isParameterStore := p.(*pattern.ParameterStore)
				return isParameterStore
			}); ok {
			plannedRuntimePatternInstances[runtimeName] = append(plannedRuntimePatternInstances[runtimeName], p)

			if parameterStore, ok := p.(*pattern.ParameterStore); ok {
				parameterStore.RegisterInstance(encryptionKey)
				parameterStore.BindOutputInstance(database)
				parameterStore.BindOutputInstance(redis)
				parameterStore.BindOutputInstance(sesSMTPUser)
			}
		}
	}

	/** Delivery環境にアプリケーションのCDパイプラインを起動する仕組みを構築する **/

	/** Delivery環境にユーザーを作成する **/

	/** Runtime環境にロールを作成する **/

	/** Runtime環境の証跡やインフラ変更を記録、第三者による不正アクセスを通知する **/

	patternInstances = append(patternInstances, plannedDeliveryPatternInstances...)

	for _, r := range t.RuntimeEnvironments {
		runtimeName := r.Name

		patternInstances = append(patternInstances, plannedRuntimePatternInstances[runtimeName]...)
	}

	return patternInstances
}

func queryPatternInstance(patternInstances []pattern.PatternInstance, predicate func(pattern.PatternInstance) bool) (pattern.PatternInstance, bool) {
	for _, p := range patternInstances {
		if predicate(p) {
			return p, true
		}
	}

	return nil, false
}

func (t *TerraformExecutor) Execute(ctx context.Context) error {
	for _, patternInstance := range t.PatternInstances {
		if err := patternInstance.Apply(ctx); err != nil {
			return err
		}
	}

	return nil
}
