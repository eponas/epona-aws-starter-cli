package pattern

import (
	"context"
	"fmt"
	"path/filepath"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/logging"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/template"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/terraform"
)

// CdPipelineFrontend はインスタンスに必要な構造体を定義する
type CdPipelineFrontend struct {
	PatternInstanceAttributes
	PatternInstanceDependencyAttributes
	PatternInstanceOutputBinderAttributes
	PatternInstanceReApplyAttributes // CdPipelineFrontendTriggerの再適用が必要になるため
	Variables                        *CdPipelineFrontendVariables
	CacheableFrontend                *CacheableFrontend
}

// CdPipelineFrontendVariables は生成するファイルで必要になるパラメータを定義する
type CdPipelineFrontendVariables struct {
	Tags                            map[string]string
	PipelineName                    string
	SourceBucketName                string
	SourceObjectKey                 string
	ArtifactStoreBucketName         string
	ArtifactStoreBucketForceDestroy bool
	DeploymentRequireApproval       bool
	EnableCacheInvalidation         bool
	CrossAcountAccessRoleArn        string
}

// NewCdPipelineFrontendVariables は、 CdPipelineFrontendVariables を返却する
func NewCdPipelineFrontendVariables(
	tags map[string]string,
	pipelineName,
	sourceBucketName,
	sourceObjectKey,
	artifactStoreBucketName string,
	artifactStoreBucketForceDestroy,
	deploymentRequireApproval,
	enableCacheInvalidation bool,
	crossAcountAccessRoleArn string,
) *CdPipelineFrontendVariables {
	return &CdPipelineFrontendVariables{
		Tags:                            tags,
		PipelineName:                    pipelineName,
		SourceBucketName:                sourceBucketName,
		SourceObjectKey:                 sourceObjectKey,
		ArtifactStoreBucketName:         artifactStoreBucketName,
		ArtifactStoreBucketForceDestroy: artifactStoreBucketForceDestroy,
		DeploymentRequireApproval:       deploymentRequireApproval,
		EnableCacheInvalidation:         enableCacheInvalidation,
		CrossAcountAccessRoleArn:        crossAcountAccessRoleArn,
	}
}

// NewCdPipelineFrontend は、自身のインスタンスを返却する(コンストラクタ)
func NewCdPipelineFrontend() *CdPipelineFrontend {
	cdf := &CdPipelineFrontend{}
	cdf.PatternName = "cd_pipeline_frontend"
	cdf.Variables = &CdPipelineFrontendVariables{}
	cdf.DependencyInstances = []PatternInstance{}

	return cdf
}

// SetupInstance は、インスタンスに必要な情報を設定する
func (cdf *CdPipelineFrontend) SetupInstance(name, env string, deliveryCred, runtimeCred *AwsCredential, vars *CdPipelineFrontendVariables) *CdPipelineFrontend {
	cdf.Name = name
	cdf.EnvironmentName = env
	cdf.EnvironmentDirectory = filepath.Join("runtimes", env)
	// クレデンシャル
	cdf.DeliveryAwsCredential = deliveryCred
	cdf.TargetAwsCredential = runtimeCred
	// pattern の入力パラメータ
	cdf.Variables = vars

	return cdf
}

// RegisterInstance は、関連するパターンインスタンスを登録する
func (cdf *CdPipelineFrontend) RegisterInstance(pi PatternInstance) {
	if cf, ok := pi.(*CacheableFrontend); ok {
		cdf.DependencyInstances = append(cdf.DependencyInstances, pi)
		cdf.CacheableFrontend = cf
		return
	}
	logging.Fatalf("BUG: %s patternに意図しない依存patternが指定されています: %s", cdf.PatternName, pi)
}

func (cdf *CdPipelineFrontend) generateTerraformFiles(ctx context.Context) error {
	// 適用時に、CdPipelineFrontendTriggerのOutputを取得する
	cdf.RunOutputCallbacks()

	td := newTemplateData(ctx, cdf)

	if err := template.EvalTerraformTemplateWriteToFile(
		cdPipelineFrontendMainTfTemplate,
		td,
		filepath.Join(cdf.GetDirectory(), "main.tf"),
	); err != nil {
		return fmt.Errorf("%sのmain.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", cdf.PatternName, err)
	}

	if err := template.EvalTerraformTemplateWriteToFile(
		awsProviderVersionTfTemplate,
		td,
		filepath.Join(cdf.GetDirectory(), "versions.tf"),
	); err != nil {
		return fmt.Errorf("%sのversions.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", cdf.PatternName, err)
	}

	if err := template.EvalTerraformTemplateWriteToFile(
		simpleOutputsTfTemplate,
		td,
		filepath.Join(cdf.GetDirectory(), "outputs.tf"),
	); err != nil {
		return fmt.Errorf("%sのoutputs.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", cdf.PatternName, err)
	}

	if err := template.EvalTerraformTemplateWriteToFile(
		instanceDependenciesTfTemplate,
		td,
		filepath.Join(cdf.GetDirectory(), "instance_dependencies.tf"),
	); err != nil {
		return fmt.Errorf("%sのinstance_dependencies.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", cdf.PatternName, err)
	}

	if err := terraform.Format(cdf.GetDirectory()); err != nil {
		return err
	}

	return nil
}

// Apply は、関連するファイルの生成とパターンの適用を実施する
func (cdf *CdPipelineFrontend) Apply(ctx context.Context) error {
	if err := applySimply(ctx, cdf); err != nil {
		return fmt.Errorf("%sのApplyに失敗"+logging.ErrTraceDelimiter+"%v", cdf.PatternName, err)
	}

	if err := cdf.RunReApply(ctx); err != nil {
		return fmt.Errorf("%s patternに関連するパターンインスタンスの再適用に失敗"+logging.ErrTraceDelimiter+"%v", cdf.PatternName, err)
	}

	return nil
}

var (
	cdPipelineFrontendMainTfTemplate = `provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::{{.Instance.TargetAwsCredential.ActualEnvironmentAccountID}}:role/{{tfTitle .Prefix}}TerraformExecutionRole"
  }
}

module "{{.Instance.Name}}" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/{{.Instance.PatternName}}?ref={{.EponaVersion}}"

  tags = {
{{- range $name, $value := .Instance.Variables.Tags}}
    {{$name}} = "{{$value}}"
{{- end}}
  }

  delivery_account_id = "{{.Instance.DeliveryAwsCredential.ActualEnvironmentAccountID}}"

  pipeline_name = "{{.Instance.Variables.PipelineName}}"

  artifact_store_bucket_name          = "{{.Instance.Variables.ArtifactStoreBucketName}}"
  artifact_store_bucket_force_destroy = "{{.Instance.Variables.ArtifactStoreBucketForceDestroy}}"

  # Delivery環境のS3にCodePipelineからクロスアカウントでアクセスするためのロール
  # cd_pipeline_frontend_trigger の output として出力される
  cross_account_codepipeline_access_role_arn = "{{.Instance.Variables.CrossAcountAccessRoleArn}}"

  # ci_pipeline の bucket_name と同じ名前を指定する
  source_bucket_name = "{{.Instance.Variables.SourceBucketName}}"
  source_object_key  = "{{.Instance.Variables.SourceObjectKey}}"

  deployment_bucket_name = data.terraform_remote_state.{{.Instance.EnvironmentName}}_{{.Instance.CacheableFrontend.Name}}.outputs.{{.Instance.CacheableFrontend.Name}}.frontend_bucket_name
  deployment_object_path = data.terraform_remote_state.{{.Instance.EnvironmentName}}_{{.Instance.CacheableFrontend.Name}}.outputs.{{.Instance.CacheableFrontend.Name}}.frontend_bucket_origin_path

  deployment_require_approval = {{.Instance.Variables.DeploymentRequireApproval}}

  cache_invalidation_config = {
    enable                     = {{.Instance.Variables.EnableCacheInvalidation}}
    cloudfront_distribution_id = data.terraform_remote_state.{{.Instance.EnvironmentName}}_{{.Instance.CacheableFrontend.Name}}.outputs.{{.Instance.CacheableFrontend.Name}}.cloudfront_id
  }
}
`
)
