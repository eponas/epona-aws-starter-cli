package pattern

import (
	"context"
	"fmt"
	"path/filepath"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/logging"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/template"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/terraform"
)

type Database struct {
	PatternInstanceAttributes
	PatternInstanceDependencyAttributes
	Variables     *DatabaseVariables
	Network       *Network
	EncryptionKey *EncryptionKey
}

type DatabaseVariables struct {
	Name                          string
	Tags                          map[string]string
	Identifier                    string
	Engine                        string
	EngineVersion                 string
	Port                          int
	InstanceClass                 string
	AllocatedStorage              int
	MaxAllocatedStorage           int
	DeleteProtection              bool
	SkipFinalSnapshot             bool
	AutoMinorVersionUpgrade       bool
	MaintenanceWindow             string
	BackupRetentionPeriod         int
	BackupWindow                  string
	Username                      string
	Password                      string
	KmsKeyAlias                   string
	AvailabilityZone              string
	DBSubnetGroupName             string
	ParameterGroupName            string
	ParameterGroupFamily          string
	OptionGroupName               string
	OptionGroupEngineName         string
	OptionGroupMajorEngineVersion int
}

func NewDatabase() *Database {
	database := &Database{}
	database.PatternName = "database"

	return database
}

func (d *Database) RegisterInstance(patternInstance PatternInstance) {
	if d.DependencyInstances == nil {
		d.DependencyInstances = []PatternInstance{}
	}

	d.DependencyInstances = append(d.DependencyInstances, patternInstance)

	if network, ok := patternInstance.(*Network); ok {
		d.Network = network
	}

	if encryptionKey, ok := patternInstance.(*EncryptionKey); ok {
		d.EncryptionKey = encryptionKey
	}
}

func (d *Database) generateTerraformFiles(ctx context.Context) error {
	mainData := newTemplateData(ctx, d)

	err := template.EvalTerraformTemplateWriteToFile(databaseMainTfTemplate, mainData, filepath.Join(d.GetDirectory(), "main.tf"))

	if err != nil {
		return fmt.Errorf("%sのmain.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", d.PatternName, err)
	}

	versionsData := newTemplateData(ctx, d)

	err = template.EvalTerraformTemplateWriteToFile(awsProviderVersionTfTemplate, versionsData, filepath.Join(d.GetDirectory(), "versions.tf"))

	if err != nil {
		return fmt.Errorf("%sのversions.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", d.PatternName, err)
	}

	outputsData := newTemplateData(ctx, d)

	err = template.EvalTerraformTemplateWriteToFile(simpleOutputsTfTemplate, outputsData, filepath.Join(d.GetDirectory(), "outputs.tf"))

	if err != nil {
		return fmt.Errorf("%sのoutputs.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", d.PatternName, err)
	}

	instanceDependenciesData := newTemplateData(ctx, d)

	err = template.EvalTerraformTemplateWriteToFile(instanceDependenciesTfTemplate, instanceDependenciesData, filepath.Join(d.GetDirectory(), "instance_dependencies.tf"))

	if err != nil {
		return fmt.Errorf("%sのinstance_dependencies.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", d.PatternName, err)
	}

	err = terraform.Format(d.GetDirectory())

	if err != nil {
		return err
	}

	return nil
}

func (d *Database) Apply(ctx context.Context) error {
	return applySimply(ctx, d)
}

var (
	databaseMainTfTemplate = `provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::{{.Instance.TargetAwsCredential.ActualEnvironmentAccountID}}:role/{{tfTitle .Prefix}}TerraformExecutionRole"
  }
}

module "{{.Instance.Name}}" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/{{.Instance.PatternName}}?ref={{.EponaVersion}}"

  name = "{{.Instance.Variables.Name}}"

  tags = {
{{- range $name, $value := .Instance.Variables.Tags}}
    {{$name}}       = "{{$value}}"
{{- end}}
  }

  identifier            = "{{.Instance.Variables.Identifier}}"
  engine                = "{{.Instance.Variables.Engine}}"
  engine_version        = "{{.Instance.Variables.EngineVersion}}"
  port                  = {{.Instance.Variables.Port}}
  instance_class        = "{{.Instance.Variables.InstanceClass}}"
  allocated_storage     = {{.Instance.Variables.AllocatedStorage}}
  max_allocated_storage = {{.Instance.Variables.MaxAllocatedStorage}}

  deletion_protection = {{.Instance.Variables.DeleteProtection}}
  skip_final_snapshot = {{.Instance.Variables.SkipFinalSnapshot}}

  auto_minor_version_upgrade = {{.Instance.Variables.AutoMinorVersionUpgrade}}
  maintenance_window         = "{{.Instance.Variables.MaintenanceWindow}}" # UTC Timezone
  backup_retention_period    = "{{.Instance.Variables.BackupRetentionPeriod}}"
  backup_window              = "{{.Instance.Variables.BackupWindow}}" #  UTC Timezone

  username = "{{.Instance.Variables.Username}}"
  password = "{{.Instance.Variables.Password}}"

  kms_key_id = data.terraform_remote_state.{{.Instance.EnvironmentName}}_{{.Instance.EncryptionKey.Name}}.outputs.{{.Instance.EncryptionKey.Name}}.keys["{{.Instance.Variables.KmsKeyAlias}}"].key_arn

  vpc_id            = data.terraform_remote_state.{{.Instance.EnvironmentName}}_{{.Instance.Network.Name}}.outputs.{{.Instance.Network.Name}}.vpc_id
  availability_zone = "{{.Instance.Variables.AvailabilityZone}}"

  db_subnets           = data.terraform_remote_state.{{.Instance.EnvironmentName}}_{{.Instance.Network.Name}}.outputs.network.private_subnets
  db_subnet_group_name = "{{.Instance.Variables.DBSubnetGroupName}}"

  parameter_group_name   = "{{.Instance.Variables.ParameterGroupName}}"
  parameter_group_family = "{{.Instance.Variables.ParameterGroupFamily}}"

  option_group_name                 = "{{.Instance.Variables.OptionGroupName}}"
  option_group_engine_name          = "{{.Instance.Variables.OptionGroupEngineName}}"
  option_group_major_engine_version = "{{.Instance.Variables.OptionGroupMajorEngineVersion}}"

  performance_insights_kms_key_id = data.terraform_remote_state.{{.Instance.EnvironmentName}}_{{.Instance.EncryptionKey.Name}}.outputs.{{.Instance.EncryptionKey.Name}}.keys["{{.Instance.Variables.KmsKeyAlias}}"].key_arn

  parameters = [
  ]

  option_group_options = [
  ]
}
`
)
