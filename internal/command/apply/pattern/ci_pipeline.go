package pattern

import (
	"context"
	"fmt"
	"path/filepath"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/logging"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/template"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/terraform"
)

// CiPipeline は `ci_pipeline` patternを表現する
type CiPipeline struct {
	PatternInstanceAttributes
	PatternInstanceDependencyAttributes
	Variables *CiPipelineVariables

	Network *Network // 依存パターン
}

// CiPipelineVariables は、ci_pipeline pattern の全インプットパラメータを持つ struct。
type CiPipelineVariables struct {
	Name                          string
	Tags                          map[string]string
	CiRunnerInstanceType          string
	CiRunnerRootBlockVolumeSize   string
	ContainerImageRepositories    []map[string]string
	GitlabRunnerPullImageAlways   bool
	GitlabRunnerRegistrationToken string
	StaticResourceBuckets         []map[string]string
	EcrAllowAccessAccountIDs      []string // ECRへのアクセスを許可するAWSアカウントIDのリスト
}

// NewCiPipeline は `ci_pipeline` pattern を返却する。
//
// NewCiPipeline の返却値には、patternのインスタンス化に必要なインプットパラメータが設定されて
// いないことに注意すること。これをセットする場合は `SetupInstance` を呼び出せば良い。
func NewCiPipeline() *CiPipeline {
	cp := &CiPipeline{}
	cp.PatternName = "ci_pipeline" // nolint:goconst
	cp.Variables = &CiPipelineVariables{}
	cp.DependencyInstances = []PatternInstance{}

	return cp
}

// SetupInstance は、ci_pipeline patternのインスタンスに必要なパラメータを設定する
func (cp *CiPipeline) SetupInstance(name, env, envdir string, cred *AwsCredential, vars *CiPipelineVariables) *CiPipeline {

	cp.Name = name
	cp.EnvironmentName = env
	cp.EnvironmentDirectory = envdir
	// クレデンシャル
	cp.DeliveryAwsCredential = cred
	cp.TargetAwsCredential = cred
	// pattern の入力パラメータ
	cp.Variables = vars

	return cp
}

// NewCiPipelineVariable は、pattern のインスタンスに必要なパラメータを引数とし、それを struct として返却する。
func NewCiPipelineVariable(
	name string,
	tags map[string]string,
	instanceType, volSize, token string,
	buckets []map[string]string,
	repos []map[string]string,
	ecrAccountIds []string,
) *CiPipelineVariables {
	return &CiPipelineVariables{
		Name:                          name,
		Tags:                          tags,
		CiRunnerInstanceType:          instanceType,
		CiRunnerRootBlockVolumeSize:   volSize,
		GitlabRunnerRegistrationToken: token,
		StaticResourceBuckets:         buckets,
		ContainerImageRepositories:    repos,
		EcrAllowAccessAccountIDs:      ecrAccountIds,
	}
}

// RegisterInstance は、ci_pipeline インスタンスの依存しているインスタンスを追加する
func (cp *CiPipeline) RegisterInstance(pi PatternInstance) {
	if nw, ok := pi.(*Network); ok {
		cp.DependencyInstances = append(cp.DependencyInstances, pi)
		cp.Network = nw
		return
	}
	logging.Fatalf("BUG: ci_pipeline patternに意図しない依存patternが指定されています: %s", pi)
}

func (cp *CiPipeline) generateTerraformFiles(ctx context.Context) error {
	td := newTemplateData(ctx, cp)

	if err := template.EvalTerraformTemplateWriteToFile(
		ciPipelineTemplate,
		td,
		filepath.Join(cp.GetDirectory(), "main.tf"),
	); err != nil {
		return fmt.Errorf("%sのmain.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", cp.PatternName, err)
	}

	if err := template.EvalTerraformTemplateWriteToFile(
		awsProviderVersionTfTemplate,
		td,
		filepath.Join(cp.GetDirectory(), "versions.tf"),
	); err != nil {
		return fmt.Errorf("%sのversions.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", cp.PatternName, err)
	}

	if err := template.EvalTerraformTemplateWriteToFile(
		simpleOutputsTfTemplate,
		td,
		filepath.Join(cp.GetDirectory(), "outputs.tf"),
	); err != nil {
		return fmt.Errorf("%sのoutputs.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", cp.PatternName, err)
	}

	if err := template.EvalTerraformTemplateWriteToFile(
		instanceDependenciesTfTemplate,
		td,
		filepath.Join(cp.GetDirectory(), "instance_dependencies.tf"),
	); err != nil {
		return fmt.Errorf("%sのinstance_dependencies.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", cp.PatternName, err)
	}

	// variables.tfを作成する
	if err := template.EvalTerraformTemplateWriteToFile(
		variablesTfTemplate,
		[]variables{
			{
				Name: "gitlab_runner_registration_token",
				Desc: "GitLab Runner登録のためのトークン（cloud-initスクリプトを独自定義する場合は、この変数は不要になる可能性がある）",
				Type: "string",
			},
		},
		filepath.Join(cp.GetDirectory(), "variables.tf"),
	); err != nil {
		return fmt.Errorf("%sのvariables.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", cp.PatternName, err)
	}

	// terraform.tfvarsを作成する
	if err := template.EvalTerraformTemplateWriteToFile(
		tfvarsTemplate,
		map[string]string{
			"gitlab_runner_registration_token": cp.Variables.GitlabRunnerRegistrationToken,
		},
		filepath.Join(cp.GetDirectory(), "terraform.tfvars"),
	); err != nil {
		return fmt.Errorf("%sのterraform.tfvarsの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", cp.PatternName, err)
	}

	if err := terraform.Format(cp.GetDirectory()); err != nil {
		return err
	}

	// ECRのライフサイクルポリシーを作成する
	if err := template.EvalTemplateWriteToFile(
		lifeCyclePolicy,
		map[string]interface{}{},
		filepath.Join(cp.GetDirectory(), "lifecycle_policy.json"),
	); err != nil {
		return fmt.Errorf("%sのlifecycle_policy.jsonの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", cp.PatternName, err)
	}

	// .gitignoreの指定
	if err := template.EvalTemplateWriteToFile(
		"terraform.tfvars\n",
		map[string]interface{}{},
		filepath.Join(cp.GetDirectory(), ".gitignore"),
	); err != nil {
		return fmt.Errorf("%sの.gitignoreの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", cp.PatternName, err)
	}

	return nil
}

func (cp *CiPipeline) Apply(ctx context.Context) error {
	return applySimply(ctx, cp)
}

var ciPipelineTemplate = `provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::{{.Instance.TargetAwsCredential.ActualEnvironmentAccountID}}:role/{{tfTitle .Prefix}}TerraformExecutionRole"
  }
}

# Amazon Linux 2 の最新版を取得する
data "aws_ami" "recent_amazon_linux_2" {
  most_recent = true
  owners      = ["amazon"]

  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]
  }

  filter {
    name   = "state"
    values = ["available"]
  }
}

module "{{.Instance.Name}}" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/{{.Instance.PatternName}}?ref={{.EponaVersion}}"

  name = "{{.Instance.Variables.Name}}"
  tags = {
{{- range $name, $value := .Instance.Variables.Tags}}
    {{$name}}       = "{{$value}}"
{{- end}}
  }

  vpc_id = data.terraform_remote_state.{{.Instance.EnvironmentName}}_{{.Instance.Network.Name}}.outputs.{{.Instance.Network.Name}}.vpc_id

  static_resource_buckets = [{{- range $bucket := .Instance.Variables.StaticResourceBuckets}}
    {
  {{- range $k, $v := $bucket}}
    {{- if ne $k "force_destroy" }}
      {{$k}} = "{{$v}}"
    {{- else}}
      {{$k}} = {{$v}}
    {{- end}}
  {{- end}}
    },
{{- end}}
  ]

  container_image_repositories = [{{- range $repo := .Instance.Variables.ContainerImageRepositories }} 
    {
  {{- range $k, $v := $repo}}
      {{$k}} = "{{$v}}"
  {{- end}}
      repository_policy = data.aws_iam_policy_document.cross_account_pull_policy.json
      lifecycle_policy = file("${path.module}/lifecycle_policy.json")
    }, 
{{- end}}
  ]

  ci_runner_ami                    = data.aws_ami.recent_amazon_linux_2.image_id
  ci_runner_instance_type          = "{{.Instance.Variables.CiRunnerInstanceType}}"
  ci_runner_root_block_volume_size = "{{.Instance.Variables.CiRunnerRootBlockVolumeSize}}"
  ci_runner_subnet                 = data.terraform_remote_state.{{.Instance.EnvironmentName}}_{{.Instance.Network.Name}}.outputs.{{.Instance.Network.Name}}.private_subnets[0]

  gitlab_runner_registration_token = var.gitlab_runner_registration_token
}

# Runtime環境からクロスアカウントでPull Accessを可能にするポリシー
# see: https://stackoverflow.com/questions/52914713/aws-ecs-fargate-pull-image-from-a-cross-account-ecr-repo/52934781
data "aws_iam_policy_document" "cross_account_pull_policy" {
  statement {
    actions = [
      "ecr:GetDownloadUrlForLayer",
      "ecr:BatchCheckLayerAvailability",
      "ecr:BatchGetImage"
    ]
    principals {
      type        = "AWS"
      identifiers = [
{{- range $accountId := .Instance.Variables.EcrAllowAccessAccountIDs }}
        "arn:aws:iam::{{$accountId}}:root",
{{- end}}
      ]
    }
  }
}
`

var lifeCyclePolicy = `{
    "rules": [
        {
            "rulePriority": 1,
            "description": "Expire images older than 14 days",
            "selection": {
                "tagStatus": "untagged",
                "countType": "sinceImagePushed",
                "countUnit": "days",
                "countNumber": 14
            },
            "action": {
                "type": "expire"
            }
        }
    ]
}
`
