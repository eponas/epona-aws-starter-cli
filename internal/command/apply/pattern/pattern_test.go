package pattern

import (
	"context"
	"io/ioutil"
	"os"
	"path/filepath"
	"testing"

	"github.com/sergi/go-diff/diffmatchpatch"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/service"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/version"
)

const (
	testEponaVersion       = "v0.2.4"
	testTerraformVersion   = "0.14.10"
	testAwsProviderVersion = "3.37.0"
)

var (
	deliveryCredential = &AwsCredential{
		ActualEnvironmentAccountID: "123456789012",
		AccessKeyID:                "ACCESSKEYID",
		SecretAccessKey:            "SECRETACCESSKEY",
		Region:                     "ap-northeast-1",
	}
	runtimeCredential = &AwsCredential{
		ActualEnvironmentAccountID: "987654321098",
		AccessKeyID:                "accesskeyid",
		SecretAccessKey:            "secretaccesskeyid",
		Region:                     "ap-northeast-1",
	}
)

func testContext(eponaVer, terraformVer, awsProvVer, serviceName, prefix string, instance PatternInstance) context.Context {
	ctx := context.Background()
	ctx = version.SetEponaVersion(ctx, eponaVer)
	ctx = version.SetTerraformVersion(ctx, terraformVer)
	ctx = version.SetAwsProviderVersion(ctx, awsProvVer)
	ctx = service.SetName(ctx, serviceName)

	return ctx
}

func AssertGenerateTerraformFiles(t *testing.T, pattern, serviceName string, sut PatternInstance) {

	// 生成された Terraform 構成ファイルを配置するディレクトリ
	dir := sut.GetDirectory()
	ctx := testContext(
		testEponaVersion,
		testTerraformVersion,
		testAwsProviderVersion,
		serviceName,
		"",
		sut,
	)

	if err := sut.generateTerraformFiles(ctx); err != nil {
		t.Fatalf("generateTerraformFiles failed: %s", err)
	}

	expectedDir := filepath.Join("testdata", pattern) // 期待値となるファイルの格納ディレクトリ

	fileNames := listFileNames(t, expectedDir)
	for _, f := range fileNames {
		expectedFilePath := filepath.Join(expectedDir, f)
		expected, err := ioutil.ReadFile(expectedFilePath)
		if err != nil {
			t.Errorf("cannot read expetec file [%s]: %s", expectedFilePath, err)
			continue
		}

		actualPath := filepath.Join(dir, f)
		actual, err := ioutil.ReadFile(actualPath)
		if err != nil {
			t.Errorf("cannot read generated file [%s]: %s", actualPath, err)
			continue
		}

		if string(expected) != string(actual) {
			dmp := diffmatchpatch.New()
			a, b, c := dmp.DiffLinesToChars(string(expected), string(actual))
			diffs := dmp.DiffMain(a, b, false)
			diffs = dmp.DiffCharsToLines(diffs, c)
			t.Errorf("%sに差分があります\n%s", f, dmp.DiffPrettyText(diffs))
		}
	}
}

func listFileNames(t *testing.T, dir string) []string {
	var paths []string
	err := filepath.Walk(dir, func(path string, info os.FileInfo, err error) error {
		// 何かエラーが発生したら走査を中止
		if err != nil {
			return err
		}
		// ディレクトリは対象としない
		if info.IsDir() {
			return nil
		}
		relpath, err := filepath.Rel(dir, path)
		if err != nil {
			return err
		}
		paths = append(paths, relpath)
		return nil
	})
	if err != nil {
		t.Fatalf("%sを走査中にエラーが発生しました: %v", dir, err)
	}
	return paths
}
