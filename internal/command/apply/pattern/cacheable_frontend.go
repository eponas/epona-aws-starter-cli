package pattern

import (
	"context"
	"fmt"
	"path/filepath"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/logging"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/template"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/terraform"
)

type CacheableFrontend struct {
	PatternInstanceAttributes
	PatternInstanceDependencyAttributes
	Variables *CacheableFrontendVariables
	Webacl    *Webacl
}

type CacheableFrontendVariables struct {
	S3FrontendBucketName         string
	ZoneName                     string
	RecordName                   string
	DefaultRootObject            string
	OriginPath                   string
	LoggingBucketName            string
	CreateLoggingBucket          bool
	LoggingBucketPrefix          string
	IncludeCookieLogging         bool
	ViewerResponseLambdaFunction string
	Tags                         map[string]string
}

func NewCacheableFrontend() *CacheableFrontend {
	cacheableFrontend := &CacheableFrontend{}
	cacheableFrontend.PatternName = "cacheable_frontend" // nolint:goconst

	return cacheableFrontend
}

func (c *CacheableFrontend) RegisterInstance(patternInstance PatternInstance) {
	if c.DependencyInstances == nil {
		c.DependencyInstances = []PatternInstance{}
	}

	c.DependencyInstances = append(c.DependencyInstances, patternInstance)

	if webacl, ok := patternInstance.(*Webacl); ok {
		c.Webacl = webacl
	}
}

func (c *CacheableFrontend) generateTerraformFiles(ctx context.Context) error {
	mainData := newTemplateData(ctx, c)

	err := template.EvalTerraformTemplateWriteToFile(cacheableFrontendMainTfTemplate, mainData, filepath.Join(c.GetDirectory(), "main.tf"))

	if err != nil {
		return fmt.Errorf("%sのmain.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", c.PatternName, err)
	}

	versionsData := newTemplateData(ctx, c)

	err = template.EvalTerraformTemplateWriteToFile(awsProviderVersionTfTemplate, versionsData, filepath.Join(c.GetDirectory(), "versions.tf"))

	if err != nil {
		return fmt.Errorf("%sのversions.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", c.PatternName, err)
	}

	outputsData := newTemplateData(ctx, c)

	err = template.EvalTerraformTemplateWriteToFile(simpleOutputsTfTemplate, outputsData, filepath.Join(c.GetDirectory(), "outputs.tf"))

	if err != nil {
		return fmt.Errorf("%sのoutputs.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", c.PatternName, err)
	}

	instanceDependenciesData := newTemplateData(ctx, c)

	err = template.EvalTerraformTemplateWriteToFile(instanceDependenciesTfTemplate, instanceDependenciesData, filepath.Join(c.GetDirectory(), "instance_dependencies.tf"))

	if err != nil {
		return fmt.Errorf("%sのinstance_dependencies.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", c.PatternName, err)
	}

	err = terraform.Format(c.GetDirectory())

	if err != nil {
		return err
	}

	return nil
}

func (c *CacheableFrontend) Apply(ctx context.Context) error {
	return applySimply(ctx, c)
}

var (
	cacheableFrontendMainTfTemplate = `# OriginとなるS3バケットを配置するリージョンを定義したAWS Provider
provider "aws" {
  alias = "origin_provider"
  assume_role {
    role_arn = "arn:aws:iam::{{.Instance.TargetAwsCredential.ActualEnvironmentAccountID}}:role/{{tfTitle .Prefix}}TerraformExecutionRole"
  }
}

# CloudFront関連リソースを配置するリージョンを定義したAWS Provider
provider "aws" {
  alias  = "cache_provider"
  region = "us-east-1"
  assume_role {
    role_arn = "arn:aws:iam::{{.Instance.TargetAwsCredential.ActualEnvironmentAccountID}}:role/{{tfTitle .Prefix}}TerraformExecutionRole"
  }
}

module "{{.Instance.Name}}" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/{{.Instance.PatternName}}?ref={{.EponaVersion}}"

  providers = {
    aws.origin_provider = aws.origin_provider
    aws.cache_provider  = aws.cache_provider
  }

  s3_frontend_bucket_name = "{{.Instance.Variables.S3FrontendBucketName}}"
  zone_name               = "{{.Instance.Variables.ZoneName}}"
  record_name             = "{{.Instance.Variables.RecordName}}"
  tags = {
{{- range $name, $value := .Instance.Variables.Tags}}
    {{$name}}       = "{{$value}}"
{{- end}}
  }

  cloudfront_default_root_object = "{{.Instance.Variables.DefaultRootObject}}"
  cloudfront_origin = {
    origin_path = "{{.Instance.Variables.OriginPath}}"
  }

  cloudfront_default_cache_behavior = {
    allowed_methods        = ["GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]
    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = null
    default_ttl            = null
    max_ttl                = null
    compress               = false
  }

  cloudfront_custom_error_responses = [
    {
      error_code            = 404
      error_caching_min_ttl = 300
      response_code         = 200
      response_page_path    = "/"
    }
  ]

  cloudfront_logging_config = {
    bucket_name           = "{{.Instance.Variables.LoggingBucketName}}"
    create_logging_bucket = {{.Instance.Variables.CreateLoggingBucket}}
    prefix                = "{{.Instance.Variables.LoggingBucketPrefix}}"
    include_cookies       = {{.Instance.Variables.IncludeCookieLogging}}
  }

  # webacl_frontendで作成したAWS WAFをCloudFrontと紐付け
  cloudfront_web_acl_id = try(data.terraform_remote_state.{{.Instance.EnvironmentName}}_{{.Instance.Webacl.Name}}.outputs.{{.Instance.Webacl.Name}}.webacl_arn, null)

  # レスポンスヘッダーに情報を付与するラムダ関数名
  viewer_response_lambda_function_name = "{{.Instance.Variables.ViewerResponseLambdaFunction}}"
}
`
)
