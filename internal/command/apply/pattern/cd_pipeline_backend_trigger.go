package pattern

import (
	"context"
	"fmt"
	"path/filepath"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/logging"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/template"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/terraform"
)

// CdPipelineBackendTrigger はインスタンスに必要な構造体を定義する
type CdPipelineBackendTrigger struct {
	PatternInstanceAttributes
	PatternInstanceDependencyAttributes
	PatternInstanceOutputBinderAttributes
	DeliveryRuntimeInstanceAttributes // 依存するRuntime環境の情報
	Variables                         *CdPipelineBackendTriggerVariables
	CiPipeline                        *CiPipeline
}

// CdPipelineBackendTriggerVariables は生成するファイルで必要になるパラメータを定義する
type CdPipelineBackendTriggerVariables struct {
	Name                          string
	Tags                          map[string]string
	BucketName                    string
	BucketForceDestroy            bool
	EcrRepositories               []map[string]string
	ArtifactStoreBucketArn        string
	ArtifactStoreEncryptionKeyArn string
}

// NewCdPipelineBackendTriggerVariables は、 CdPipelineBackendTriggerVariables を返却する
func NewCdPipelineBackendTriggerVariables(
	name string,
	tags map[string]string,
	bucketName string,
	bucketForceDestroy bool,
	ecrRepositories []map[string]string,
	artifactStoreBucketArn,
	artifactStoreEncryptionKeyArn string,
) *CdPipelineBackendTriggerVariables {
	return &CdPipelineBackendTriggerVariables{
		Name:                          name,
		Tags:                          tags,
		BucketName:                    bucketName,
		BucketForceDestroy:            bucketForceDestroy,
		EcrRepositories:               ecrRepositories,
		ArtifactStoreBucketArn:        artifactStoreBucketArn,
		ArtifactStoreEncryptionKeyArn: artifactStoreEncryptionKeyArn,
	}
}

// NewCdPipelineBackendTrigger は、自身のインスタンスを返却する(コンストラクタ)
func NewCdPipelineBackendTrigger() *CdPipelineBackendTrigger {
	cdbt := &CdPipelineBackendTrigger{}
	cdbt.PatternName = "cd_pipeline_backend_trigger"
	cdbt.Variables = &CdPipelineBackendTriggerVariables{}
	cdbt.DependencyInstances = []PatternInstance{}

	return cdbt
}

// SetupInstance は、インスタンスに必要な情報を設定する
func (cdbt *CdPipelineBackendTrigger) SetupInstance(name, env, targetRuntimeName, targetRuntimeAccountID string, cred *AwsCredential, vars *CdPipelineBackendTriggerVariables) *CdPipelineBackendTrigger {
	cdbt.Name = name
	cdbt.EnvironmentName = env
	cdbt.EnvironmentDirectory = filepath.Join(env, "runtime_instances", targetRuntimeName)
	// クレデンシャル
	cdbt.DeliveryAwsCredential = cred
	cdbt.TargetAwsCredential = cred
	// pattern の入力パラメータ
	cdbt.Variables = vars
	// 依存するRuntime環境の情報
	cdbt.PeerEnvironmentName = targetRuntimeName
	cdbt.PeerAwsAccountID = targetRuntimeAccountID

	return cdbt
}

// RegisterInstance は、関連するパターンインスタンスを登録する
func (cdbt *CdPipelineBackendTrigger) RegisterInstance(pi PatternInstance) {
	if cp, ok := pi.(*CiPipeline); ok {
		cdbt.DependencyInstances = append(cdbt.DependencyInstances, pi)
		cdbt.CiPipeline = cp
		return
	}
	logging.Fatalf("BUG: %s patternに意図しない依存patternが指定されています: %s", cdbt.PatternName, pi)
}

func (cdbt *CdPipelineBackendTrigger) generateTerraformFiles(ctx context.Context) error {
	// 再適用時には、CdPipelineBackendのOutputを取得する
	cdbt.RunOutputCallbacks()

	td := newTemplateData(ctx, cdbt)

	if err := template.EvalTerraformTemplateWriteToFile(
		cdPipelineBackendTriggerMainTfTemplate,
		td,
		filepath.Join(cdbt.GetDirectory(), "main.tf"),
	); err != nil {
		return fmt.Errorf("%sのmain.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", cdbt.PatternName, err)
	}

	if err := template.EvalTerraformTemplateWriteToFile(
		awsProviderVersionTfTemplate,
		td,
		filepath.Join(cdbt.GetDirectory(), "versions.tf"),
	); err != nil {
		return fmt.Errorf("%sのversions.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", cdbt.PatternName, err)
	}

	if err := template.EvalTerraformTemplateWriteToFile(
		simpleOutputsTfTemplate,
		td,
		filepath.Join(cdbt.GetDirectory(), "outputs.tf"),
	); err != nil {
		return fmt.Errorf("%sのoutputs.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", cdbt.PatternName, err)
	}

	if err := template.EvalTerraformTemplateWriteToFile(
		instanceDependenciesTfTemplate,
		td,
		filepath.Join(cdbt.GetDirectory(), "instance_dependencies.tf"),
	); err != nil {
		return fmt.Errorf("%sのinstance_dependencies.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", cdbt.PatternName, err)
	}

	if err := terraform.Format(cdbt.GetDirectory()); err != nil {
		return err
	}

	return nil
}

// Apply は、関連するファイルの生成とパターンの適用を実施する
func (cdbt *CdPipelineBackendTrigger) Apply(ctx context.Context) error {
	return applySimply(ctx, cdbt)
}

var (
	cdPipelineBackendTriggerMainTfTemplate = `provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::{{.Instance.TargetAwsCredential.ActualEnvironmentAccountID}}:role/{{tfTitle .Prefix}}TerraformExecutionRole"
  }
}

locals {
  runtime_account_id = "{{.Instance.PeerAwsAccountID}}"
}

data "aws_region" "current" {}

module "{{.Instance.Name}}" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/{{.Instance.PatternName}}?ref={{.EponaVersion}}"

  name                 = "{{.Instance.Variables.Name}}"
  bucket_name          = "{{.Instance.Variables.BucketName}}"
  bucket_force_destroy = {{.Instance.Variables.BucketForceDestroy}}
  runtime_account_id   = local.runtime_account_id

  tags = {
{{- range $name, $value := .Instance.Variables.Tags}}
    {{$name}} = "{{$value}}"
{{- end}}
  }

  # リポジトリ名と、その ARN およびデプロイ対象タグのマップ
  ecr_repositories = {
{{- range $map := .Instance.Variables.EcrRepositories}}
  {{- range $repo, $tag := $map}}
    "{{$repo}}" = {
      arn  = data.terraform_remote_state.delivery_ci_pipeline.outputs.ci_pipeline.container_image_repository_arns["{{$repo}}"]
      tags = ["{{$tag}}"]
    }
  {{- end}}
{{- end}}
  }

  # CodePipeline が用いる artifact store のバケット ARN
  target_event_bus_arn = "arn:aws:events:${data.aws_region.current.name}:${local.runtime_account_id}:event-bus/default"

  artifact_store_bucket_arn                = "{{.Instance.Variables.ArtifactStoreBucketArn}}"
  artifact_store_bucket_encryption_key_arn = "{{.Instance.Variables.ArtifactStoreEncryptionKeyArn}}"
}
`
)
