package pattern

import (
	"context"
	"fmt"
	"path/filepath"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/logging"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/template"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/terraform"
)

type Network struct {
	PatternInstanceAttributes
	PatternInstanceDependencyAttributes
	Variables *NetworkVariables
}

type NetworkVariables struct {
	Name                    string
	Tags                    map[string]string
	CidrBlock               string
	AvailabilityZones       []string
	PublicSubnets           []string
	PrivateSubnets          []string
	NatGatewayDeploySubnets []string
}

func (n *Network) RegisterInstance(patternInstance PatternInstance) {
	if n.DependencyInstances == nil {
		n.DependencyInstances = []PatternInstance{}
	}

	n.DependencyInstances = append(n.DependencyInstances, patternInstance)
}

func NewNetwork() *Network {
	network := &Network{}
	network.PatternName = "network" // nolint:goconst
	network.Variables = &NetworkVariables{}

	return network
}

func (n *Network) generateTerraformFiles(ctx context.Context) error {
	mainData := newTemplateData(ctx, n)

	err := template.EvalTerraformTemplateWriteToFile(networkMainTfTemplate, mainData, filepath.Join(n.GetDirectory(), "main.tf"))

	if err != nil {
		return fmt.Errorf("%sのmain.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", n.PatternName, err)
	}

	versionsData := newTemplateData(ctx, n)

	err = template.EvalTerraformTemplateWriteToFile(awsProviderVersionTfTemplate, versionsData, filepath.Join(n.GetDirectory(), "versions.tf"))

	if err != nil {
		return fmt.Errorf("%sのversions.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", n.PatternName, err)
	}

	outputsData := newTemplateData(ctx, n)

	err = template.EvalTerraformTemplateWriteToFile(simpleOutputsTfTemplate, outputsData, filepath.Join(n.GetDirectory(), "outputs.tf"))

	if err != nil {
		return fmt.Errorf("%sのoutputs.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", n.PatternName, err)
	}

	err = terraform.Format(n.GetDirectory())

	if err != nil {
		return err
	}

	return nil
}

func (n *Network) Apply(ctx context.Context) error {
	return applySimply(ctx, n)
}

var (
	networkMainTfTemplate = `provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::{{.Instance.TargetAwsCredential.ActualEnvironmentAccountID}}:role/{{tfTitle .Prefix}}TerraformExecutionRole"
  }
}

module "{{.Instance.Name}}" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/{{.Instance.PatternName}}?ref={{.EponaVersion}}"

  name = "{{.Instance.Variables.Name}}"

  tags = {
{{- range $name, $value := .Instance.Variables.Tags}}
    {{$name}}       = "{{$value}}"
{{- end}}
  }

  cidr_block = "{{.Instance.Variables.CidrBlock}}"

  availability_zones = [{{range $az := .Instance.Variables.AvailabilityZones -}} "{{$az}}", {{- end}}]

  public_subnets             = [{{range $s := .Instance.Variables.PublicSubnets -}} {{$s}}, {{- end}}]
  private_subnets            = [{{range $s := .Instance.Variables.PrivateSubnets -}} {{$s}}, {{- end}}]
  nat_gateway_deploy_subnets = [{{range $s := .Instance.Variables.NatGatewayDeploySubnets -}} {{$s}}, {{- end}}]
}
`
)
