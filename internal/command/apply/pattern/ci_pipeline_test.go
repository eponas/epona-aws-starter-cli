package pattern

import (
	"fmt"
	"testing"
)

func sutCiPipeline(serviceName, dir string) *CiPipeline {
	cred := &AwsCredential{
		ActualEnvironmentAccountID: "123456789012",
		AccessKeyID:                "ACCESSKEYID",
		SecretAccessKey:            "SECRETACCESSKEY",
		Region:                     "ap-northeast-1",
	}

	sut := NewCiPipeline().
		SetupInstance(
			"ci_pipeline",
			"delivery",
			dir,
			cred,
			NewCiPipelineVariable(
				"ci_pipeline_test",
				map[string]string{
					"Owner":       serviceName,
					"Environment": "delivery",
					"ManagedBy":   "epona",
				},
				"t3.small",
				"30",
				"GITLAB-TOKEN", // ダミー値
				[]map[string]string{
					{
						"bucket_name":        fmt.Sprintf("%s-static-resource", serviceName),
						"force_destroy":      "false",
						"runtime_account_id": cred.ActualEnvironmentAccountID,
					},
				},
				[]map[string]string{
					{
						"name":                 "chat-example-backend",
						"image_tag_mutability": "MUTABLE",
					},
					{
						"name":                 "chat-example-notifier",
						"image_tag_mutability": "MUTABLE",
					},
					{
						"name":                 "backend-nginx",
						"image_tag_mutability": "MUTABLE",
					},
					{
						"name":                 "notifier-nginx",
						"image_tag_mutability": "MUTABLE",
					},
				},
				[]string{cred.ActualEnvironmentAccountID},
			),
		)

	nw := NewNetwork()
	nw.Name = "network"
	nw.EnvironmentName = "delivery"
	sut.RegisterInstance(nw)
	return sut
}

func TestGenerateTerraformFilesCiPileline(t *testing.T) {
	const serviceName = "serviceName"

	AssertGenerateTerraformFiles(
		t,
		"ci_pipeline",
		serviceName,
		sutCiPipeline(serviceName, t.TempDir()),
	)
}
