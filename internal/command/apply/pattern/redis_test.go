package pattern

import (
	"crypto/md5" // nolint:gosec
	"fmt"
	"path/filepath"
	"testing"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/terraform/lang"
)

func sutRedis(serviceName, dir string) *Redis {

	const runtimeName = "staging"

	sut := NewRedis()

	sut.Name = "redis"
	sut.EnvironmentName = runtimeName
	sut.EnvironmentDirectory = filepath.Join("runtimes", runtimeName)

	sut.DeliveryAwsCredential = deliveryCredential
	sut.TargetAwsCredential = runtimeCredential

	variables := &RedisVariables{}
	sut.Variables = variables

	sut.Variables.ReplicationGroupID = fmt.Sprintf("%s-redis-group", serviceName)
	sut.Variables.ReplicationGroupDescription = fmt.Sprintf("%s Redis Cluster", lang.Title(serviceName))

	sut.Variables.ClusterModeEnabled = false
	sut.Variables.NumberCacheClusters = 1

	// initial password
	sut.Variables.AuthToken = fmt.Sprintf("%x", md5.Sum([]byte(serviceName+":redis"))) // nolint:gosec

	sut.Variables.KmsKeyAlias = fmt.Sprintf("alias/%s-common-encryption-key", serviceName)

	sut.Variables.SnapshotWindow = "00:00-01:00"
	sut.Variables.SnapshotRetentionLimit = 7
	sut.Variables.MaintenanceWindow = "thu:15:00-thu:16:00"

	sut.Variables.EngineVersion = "5.0.6"
	sut.Variables.NodeType = "cache.t3.medium"

	sut.Variables.AutomaticFailoverEnabled = false

	sut.Variables.ApplyImmediately = false

	sut.Variables.Family = "redis5.0"

	sut.Variables.Tags = map[string]string{
		"Owner":              serviceName,
		"Environment":        "runtime",
		"RuntimeEnvironment": runtimeName,
		"ManagedBy":          "epona",
	}

	nw := NewNetwork()
	nw.Name = "network"
	nw.EnvironmentName = runtimeName
	sut.RegisterInstance(nw)

	key := NewEncryptionKey()
	key.Name = "encryption_key"
	key.EnvironmentName = runtimeName
	sut.RegisterInstance(key)

	return sut
}

func TestGenerateTerraformFilesRedis(t *testing.T) {
	const serviceName = "serviceName"

	AssertGenerateTerraformFiles(
		t,
		"redis",
		serviceName,
		sutRedis(serviceName, t.TempDir()),
	)
}
