package pattern

import (
	"fmt"
	"path/filepath"
	"testing"
)

func sutEncryptionKey(serviceName, dir string) *EncryptionKey {
	const runtimeName = "staging"

	sut := NewEncryptionKey()

	sut.Name = "encryption_key"
	sut.EnvironmentName = runtimeName
	sut.EnvironmentDirectory =
		filepath.Join(filepath.Join("runtimes", runtimeName))

	sut.DeliveryAwsCredential = deliveryCredential
	sut.TargetAwsCredential = runtimeCredential

	variables := &EncryptionKeyVariables{}
	sut.Variables = variables

	sut.Variables.KmsKeys = []map[string]string{{
		"alias_name": fmt.Sprintf("alias/%s-common-encryption-key", serviceName),
	}}
	sut.Variables.Tags = map[string]string{
		"Owner":              serviceName,
		"Environment":        "runtime",
		"RuntimeEnvironment": runtimeName,
		"ManagedBy":          "epona",
	}

	return sut
}

func TestGenerateTerraformFilesEncryptionKey(t *testing.T) {
	const serviceName = "serviceName"

	AssertGenerateTerraformFiles(
		t,
		"encryption_key",
		serviceName,
		sutEncryptionKey(serviceName, t.TempDir()),
	)
}
