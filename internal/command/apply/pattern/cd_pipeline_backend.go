package pattern

import (
	"context"
	"fmt"
	"path/filepath"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/logging"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/template"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/terraform"
)

// CdPipelineBackend はインスタンスに必要な構造体を定義する
type CdPipelineBackend struct {
	PatternInstanceAttributes
	PatternInstanceDependencyAttributes
	PatternInstanceOutputBinderAttributes
	PatternInstanceReApplyAttributes // CdPipelineBackendTriggerの再適用が必要になるため
	Variables                        *CdPipelineBackendVariables
	PublicTrafficContainerService    *PublicTrafficContainerService
}

// CdPipelineBackendVariables は生成するファイルで必要になるパラメータを定義する
type CdPipelineBackendVariables struct {
	PipelineName                    string
	Tags                            map[string]string
	EcrRepositories                 []map[string]string
	SourceBucketName                string
	ArtifactStoreBucketName         string
	CodeDeployAppName               string
	DeploymentGroupNameEcs          string
	DeploymentRequireApproval       bool
	ArtifactStoreBucketForceDestroy bool
	TaskdefSecrets                  string
	TaskdefLogStreamPrefix          string
	CrossAcountAccessRoleArn        string
}

// NewCdPipelineBackendVariables は、 CdPipelineBackendVariables を返却する
func NewCdPipelineBackendVariables(
	pipelineName string,
	tags map[string]string,
	ecrRepositories []map[string]string,
	sourceBucketName,
	artifactSotreBucketName,
	codeDeployAppName,
	deploymentGroupNameEcs string,
	deploymentRequireApproval,
	artifactStoreBucketForceDestroy bool,
	taskdefSecrets,
	taskdefLogStreamPrefix,
	crossAcountAccessRoleArn string,
) *CdPipelineBackendVariables {
	return &CdPipelineBackendVariables{
		PipelineName:                    pipelineName,
		Tags:                            tags,
		EcrRepositories:                 ecrRepositories,
		SourceBucketName:                sourceBucketName,
		ArtifactStoreBucketName:         artifactSotreBucketName,
		CodeDeployAppName:               codeDeployAppName,
		DeploymentGroupNameEcs:          deploymentGroupNameEcs,
		DeploymentRequireApproval:       deploymentRequireApproval,
		ArtifactStoreBucketForceDestroy: artifactStoreBucketForceDestroy,
		TaskdefSecrets:                  taskdefSecrets,
		TaskdefLogStreamPrefix:          taskdefLogStreamPrefix,
		CrossAcountAccessRoleArn:        crossAcountAccessRoleArn,
	}
}

// NewCdPipelineBackend は、自身のインスタンスを返却する(コンストラクタ)
func NewCdPipelineBackend() *CdPipelineBackend {
	cdb := &CdPipelineBackend{}
	cdb.PatternName = "cd_pipeline_backend"
	cdb.Variables = &CdPipelineBackendVariables{}
	cdb.DependencyInstances = []PatternInstance{}

	return cdb
}

// SetupInstance は、インスタンスに必要な情報を設定する
func (cdb *CdPipelineBackend) SetupInstance(name, env string, deliveryCred, runtimeCred *AwsCredential, vars *CdPipelineBackendVariables) *CdPipelineBackend {
	cdb.Name = name
	cdb.EnvironmentName = env
	cdb.EnvironmentDirectory = filepath.Join("runtimes", env)
	// クレデンシャル
	cdb.DeliveryAwsCredential = deliveryCred
	cdb.TargetAwsCredential = runtimeCred
	// pattern の入力パラメータ
	cdb.Variables = vars

	return cdb
}

// RegisterInstance は、関連するパターンインスタンスを登録する
func (cdb *CdPipelineBackend) RegisterInstance(pi PatternInstance) {
	if ptcs, ok := pi.(*PublicTrafficContainerService); ok {
		cdb.DependencyInstances = append(cdb.DependencyInstances, pi)
		cdb.PublicTrafficContainerService = ptcs
		return
	}
	logging.Fatalf("BUG: %s patternに意図しない依存patternが指定されています: %s", cdb.PatternName, pi)
}

func (cdb *CdPipelineBackend) generateTerraformFiles(ctx context.Context) error {
	// 適用時に、CdPipelineBackendTriggerのOutputを取得する
	cdb.RunOutputCallbacks()

	td := newTemplateData(ctx, cdb)

	if err := template.EvalTerraformTemplateWriteToFile(
		cdPipelineBackendMainTfTemplate,
		td,
		filepath.Join(cdb.GetDirectory(), "main.tf"),
	); err != nil {
		return fmt.Errorf("%sのmain.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", cdb.PatternName, err)
	}

	if err := template.EvalTerraformTemplateWriteToFile(
		awsProviderVersionTfTemplate,
		td,
		filepath.Join(cdb.GetDirectory(), "versions.tf"),
	); err != nil {
		return fmt.Errorf("%sのversions.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", cdb.PatternName, err)
	}

	if err := template.EvalTerraformTemplateWriteToFile(
		simpleOutputsTfTemplate,
		td,
		filepath.Join(cdb.GetDirectory(), "outputs.tf"),
	); err != nil {
		return fmt.Errorf("%sのoutputs.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", cdb.PatternName, err)
	}

	if err := template.EvalTerraformTemplateWriteToFile(
		instanceDependenciesTfTemplate,
		td,
		filepath.Join(cdb.GetDirectory(), "instance_dependencies.tf"),
	); err != nil {
		return fmt.Errorf("%sのinstance_dependencies.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", cdb.PatternName, err)
	}

	if err := template.EvalTemplateWriteToFile(
		cdPipelineBackendAppSpecTemplate,
		td,
		filepath.Join(cdb.GetDirectory(), "sample-deploy-settings", "appspec.yaml"),
	); err != nil {
		return fmt.Errorf("%sのappspec.yamlの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", cdb.PatternName, err)
	}

	if err := template.EvalTemplateWriteToFile(
		cdPipelineBackendTaskdefTemplate,
		td,
		filepath.Join(cdb.GetDirectory(), "sample-deploy-settings", "taskdef.json"),
	); err != nil {
		return fmt.Errorf("%sのtaskdef.jsonの生成エラー時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", cdb.PatternName, err)
	}

	if err := terraform.Format(cdb.GetDirectory()); err != nil {
		return err
	}

	return nil
}

// Apply は、関連するファイルの生成とパターンの適用を実施する
func (cdb *CdPipelineBackend) Apply(ctx context.Context) error {
	if err := applySimply(ctx, cdb); err != nil {
		return fmt.Errorf("%sのapplyに失敗しました"+logging.ErrTraceDelimiter+"%v", cdb.PatternName, err)
	}

	if err := cdb.RunReApply(ctx); err != nil {
		return fmt.Errorf("%s patternに関連する、patternインスタンスの再適用に失敗しました"+logging.ErrTraceDelimiter+"%v", cdb.PatternName, err)
	}

	return nil
}

var (
	cdPipelineBackendMainTfTemplate = `provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::{{.Instance.TargetAwsCredential.ActualEnvironmentAccountID}}:role/{{tfTitle .Prefix}}TerraformExecutionRole"
  }
}

module "{{.Instance.Name}}" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/{{.Instance.PatternName}}?ref={{.EponaVersion}}"

  delivery_account_id = "{{.Instance.DeliveryAwsCredential.ActualEnvironmentAccountID}}"
  ecr_repositories = {
{{- range $index, $map := .Instance.Variables.EcrRepositories}}
  {{- range $repo, $tag := $map}}
    {{$repo}} = {
      tag            = "{{$tag}}"
      artifact_name  = "{{$repo}}"
      container_name = "IMAGE{{add $index 1}}_NAME"
    }
  {{- end}}
{{- end}}
  }

  # Delivery環境のECRにCodePipelineからクロスアカウントでアクセスするためのロール
  # cd_pipeline_backend_trigger_backend の output として出力される
  cross_account_codepipeline_access_role_arn = "{{.Instance.Variables.CrossAcountAccessRoleArn}}"
  # cd_pipeline_backend_trigger_backend の bucket_name と同じ名前を指定する
  source_bucket_name = "{{.Instance.Variables.SourceBucketName}}"

  cluster_name = data.terraform_remote_state.{{.Instance.EnvironmentName}}_{{.Instance.PublicTrafficContainerService.Name}}.outputs.{{.Instance.PublicTrafficContainerService.Name}}.ecs_cluster_name
  service_name = data.terraform_remote_state.{{.Instance.EnvironmentName}}_{{.Instance.PublicTrafficContainerService.Name}}.outputs.{{.Instance.PublicTrafficContainerService.Name}}.ecs_service_name

  pipeline_name                       = "{{.Instance.Variables.PipelineName}}"
  artifact_store_bucket_name          = "{{.Instance.Variables.ArtifactStoreBucketName}}"
  artifact_store_bucket_force_destroy = {{.Instance.Variables.ArtifactStoreBucketForceDestroy}}

  prod_listener_arns = [data.terraform_remote_state.{{.Instance.EnvironmentName}}_{{.Instance.PublicTrafficContainerService.Name}}.outputs.{{.Instance.PublicTrafficContainerService.Name}}.load_balancer_prod_listener_arn]
  target_group_names = data.terraform_remote_state.{{.Instance.EnvironmentName}}_{{.Instance.PublicTrafficContainerService.Name}}.outputs.{{.Instance.PublicTrafficContainerService.Name}}.load_balancer_target_group_names

  codedeploy_app_name         = "{{.Instance.Variables.CodeDeployAppName}}"
  deployment_group_name_ecs   = "{{.Instance.Variables.DeploymentGroupNameEcs}}"
  deployment_require_approval = {{.Instance.Variables.DeploymentRequireApproval}}

  tags = {
{{- range $name, $value := .Instance.Variables.Tags}}
    {{$name}} = "{{$value}}"
{{- end}}
  }
}
`

	cdPipelineBackendAppSpecTemplate = `version: 0.0
Resources:
  - TargetService:
      Type: AWS::ECS::Service
      Properties:
        TaskDefinition: <TASK_DEFINITION>
        LoadBalancerInfo:
          ContainerName: "{{.Instance.PublicTrafficContainerService.Variables.ContainerClusterName}}-task"
          ContainerPort: {{.Instance.PublicTrafficContainerService.Variables.ContainerPort}}
        PlatformVersion: "1.4.0"
`

	cdPipelineBackendTaskdefTemplate = `{
  "ipcMode": null,
  "taskRoleArn": "arn:aws:iam::{{.Instance.TargetAwsCredential.ActualEnvironmentAccountID}}:role/{{.Instance.PublicTrafficContainerService.Variables.DefaultEcsTaskIamRoleName}}",
  "executionRoleArn": "arn:aws:iam::{{.Instance.TargetAwsCredential.ActualEnvironmentAccountID}}:role/{{.Instance.PublicTrafficContainerService.Variables.DefaultEcsTaskExecutionIamRoleName}}",
  "containerDefinitions": [
    {
      "essential": true,
      "image": "906394416424.dkr.ecr.ap-northeast-1.amazonaws.com/aws-for-fluent-bit:2.9.0",
      "name": "log_router",
      "firelensConfiguration": {
        "type": "fluentbit"
      },
      "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-group": "{{index .Instance.PublicTrafficContainerService.Variables.ContainerLogGroupNames 1}}",
          "awslogs-region": "{{.Instance.TargetAwsCredential.Region}}",
          "awslogs-stream-prefix": "{{.Instance.Variables.TaskdefLogStreamPrefix}}"
        }
      },
      "memoryReservation": 50
    },
    {
      "name": "{{.Instance.PublicTrafficContainerService.Variables.ContainerClusterName}}-task",
      "image": "<IMAGE2_NAME>",
      "essential": true,
      "portMappings": [
        {
          "protocol": "tcp",
          "containerPort": {{.Instance.PublicTrafficContainerService.Variables.ContainerPort}}
        }
      ],
      "logConfiguration": {
        "logDriver": "awsfirelens",
        "options": {
          "Name": "cloudwatch",
          "region": "{{.Instance.TargetAwsCredential.Region}}",
          "log_group_name": "{{index .Instance.PublicTrafficContainerService.Variables.ContainerLogGroupNames 2}}",
          "auto_create_group": "false",
          "log_stream_prefix": "from-fluent-bit/"
        }
      }
    },
    {
      "name": "{{.Instance.PublicTrafficContainerService.Variables.ContainerClusterName}}-app-task",
      "image": "<IMAGE1_NAME>",
      "essential": true,
      "portMappings": [
        {
          "protocol": "tcp",
          "containerPort": 8080
        }
      ],
      "logConfiguration": {
        "logDriver": "awsfirelens",
        "options": {
          "Name": "cloudwatch",
          "region": "{{.Instance.TargetAwsCredential.Region}}",
          "log_group_name": "{{index .Instance.PublicTrafficContainerService.Variables.ContainerLogGroupNames 0}}",
          "auto_create_group": "false",
          "log_stream_prefix": "from-fluent-bit/"
        }
      },
      "secrets": [{{.Instance.Variables.TaskdefSecrets}}]
    }
  ],
  "compatibilities": [
    "FARGATE"
  ],
  "family": "{{.Instance.PublicTrafficContainerService.Variables.ContainerClusterName}}-task",
  "requiresCompatibilities": [
    "FARGATE"
  ],
  "networkMode": "awsvpc",
  "cpu": "512",
  "memory": "1024"
}
`
)
