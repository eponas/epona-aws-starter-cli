package pattern

import (
	"context"
	"fmt"
	"path/filepath"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/logging"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/template"
)

// CdPipelineFrontendTrigger はインスタンスに必要な構造体を定義する
type CdPipelineFrontendTrigger struct {
	PatternInstanceAttributes
	PatternInstanceDependencyAttributes
	PatternInstanceOutputBinderAttributes
	DeliveryRuntimeInstanceAttributes // 依存するRuntime環境の情報
	Variables                         *CdPipelineFrontendTriggerVariables
	CiPipeline                        *CiPipeline
}

// CdPipelineFrontendTriggerVariables は生成するファイルで必要になるパラメータを定義する
type CdPipelineFrontendTriggerVariables struct {
	PipelineName                  string
	RuntimeAccountID              string
	SourceBucketName              string
	ArtifactStoreBucketArn        string
	ArtifactStoreEncryptionKeyArn string
}

// NewCdPipelineFrontendTriggerVariables は、 CdPipelineFrontendTriggerVariables を返却する
func NewCdPipelineFrontendTriggerVariables(
	pipelineName,
	runtimeAccountID,
	sourceBucketName,
	artifactStoreBucketArn,
	artifactStoreEncryptionKeyArn string,
) *CdPipelineFrontendTriggerVariables {
	return &CdPipelineFrontendTriggerVariables{
		RuntimeAccountID:              runtimeAccountID,
		PipelineName:                  pipelineName,
		SourceBucketName:              sourceBucketName,
		ArtifactStoreBucketArn:        artifactStoreBucketArn,
		ArtifactStoreEncryptionKeyArn: artifactStoreEncryptionKeyArn,
	}
}

// NewCdPipelineFrontendTrigger は、自身のインスタンスを返却する(コンストラクタ)
func NewCdPipelineFrontendTrigger() *CdPipelineFrontendTrigger {
	cdft := &CdPipelineFrontendTrigger{}
	cdft.PatternName = "cd_pipeline_frontend_trigger"
	cdft.Variables = &CdPipelineFrontendTriggerVariables{}
	cdft.DependencyInstances = []PatternInstance{}

	return cdft
}

// SetupInstance は、インスタンスに必要な情報を設定する
func (cdft *CdPipelineFrontendTrigger) SetupInstance(name, env, targetRuntimeName, targetRuntimeAccountID string, cred *AwsCredential, vars *CdPipelineFrontendTriggerVariables) *CdPipelineFrontendTrigger {
	cdft.Name = name
	cdft.EnvironmentName = env
	cdft.EnvironmentDirectory = filepath.Join(env, "runtime_instances", targetRuntimeName)
	// クレデンシャル
	cdft.DeliveryAwsCredential = cred
	cdft.TargetAwsCredential = cred
	// pattern の入力パラメータ
	cdft.Variables = vars
	// 依存するRuntime環境の情報
	cdft.PeerEnvironmentName = targetRuntimeName
	cdft.PeerAwsAccountID = targetRuntimeAccountID

	return cdft
}

// RegisterInstance は、関連するパターンインスタンスを登録する
func (cdft *CdPipelineFrontendTrigger) RegisterInstance(pi PatternInstance) {
	if cp, ok := pi.(*CiPipeline); ok {
		cdft.DependencyInstances = append(cdft.DependencyInstances, pi)
		cdft.CiPipeline = cp
		return
	}
	logging.Fatalf("BUG: %s patternに意図しない依存patternが指定されています: %s", cdft.PatternName, pi)
}

func (cdft *CdPipelineFrontendTrigger) generateTerraformFiles(ctx context.Context) error {
	// 再適用時には、CdPipelineFrontendのOutputを取得する
	cdft.RunOutputCallbacks()

	td := newTemplateData(ctx, cdft)

	if err := template.EvalTerraformTemplateWriteToFile(
		cdPipelineFrontendTriggerMainTfTemplate,
		td,
		filepath.Join(cdft.GetDirectory(), "main.tf"),
	); err != nil {
		return fmt.Errorf("%sのmain.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", cdft.PatternName, err)
	}

	if err := template.EvalTerraformTemplateWriteToFile(
		awsProviderVersionTfTemplate,
		td,
		filepath.Join(cdft.GetDirectory(), "versions.tf"),
	); err != nil {
		return fmt.Errorf("%sのversions.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", cdft.PatternName, err)
	}

	if err := template.EvalTerraformTemplateWriteToFile(
		simpleOutputsTfTemplate,
		td,
		filepath.Join(cdft.GetDirectory(), "outputs.tf"),
	); err != nil {
		return fmt.Errorf("%sのoutputs.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", cdft.PatternName, err)
	}

	if err := template.EvalTerraformTemplateWriteToFile(
		instanceDependenciesTfTemplate,
		td,
		filepath.Join(cdft.GetDirectory(), "instance_dependencies.tf"),
	); err != nil {
		return fmt.Errorf("%sのinstance_dependencies.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", cdft.PatternName, err)
	}

	return nil
}

// Apply は、関連するファイルの生成とパターンの適用を実施する
func (cdft *CdPipelineFrontendTrigger) Apply(ctx context.Context) error {
	return applySimply(ctx, cdft)
}

var (
	cdPipelineFrontendTriggerMainTfTemplate = `provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::{{.Instance.TargetAwsCredential.ActualEnvironmentAccountID}}:role/{{tfTitle .Prefix}}TerraformExecutionRole"
  }
}

locals {
  runtime_account_id = "{{.Instance.Variables.RuntimeAccountID}}"
}

data "aws_region" "current" {}

module "{{.Instance.Name}}" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/{{.Instance.PatternName}}?ref={{.EponaVersion}}"

  pipeline_name      = "{{.Instance.Variables.PipelineName}}"
  source_bucket_arn  = data.terraform_remote_state.{{.Instance.EnvironmentName}}_{{.Instance.CiPipeline.Name}}.outputs.{{.Instance.CiPipeline.Name}}.s3_ci_bucket_arns["{{.Instance.Variables.SourceBucketName}}"]
  runtime_account_id = local.runtime_account_id

  # Runtime環境に構築するアーティファクトストアのバケット名
  target_event_bus_arn = "arn:aws:events:${data.aws_region.current.name}:${local.runtime_account_id}:event-bus/default"

  artifact_store_bucket_arn                = "{{.Instance.Variables.ArtifactStoreBucketArn}}"
  artifact_store_bucket_encryption_key_arn = "{{.Instance.Variables.ArtifactStoreEncryptionKeyArn}}"
}
`
)
