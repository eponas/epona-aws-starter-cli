package pattern

import (
	"context"
	"fmt"
	"path/filepath"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/logging"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/template"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/terraform"
)

type SesSMTPUser struct {
	PatternInstanceAttributes
	Variables *SesSMTPUserVariables
	Sensitive bool
}

type SesSMTPUserVariables struct {
	Tags       map[string]string
	Username   string
	PolicyName string
}

func NewSesSMTPUser() *SesSMTPUser {
	sesSMTPUser := &SesSMTPUser{}
	sesSMTPUser.PatternName = "ses_smtp_user"

	sesSMTPUser.Sensitive = false

	return sesSMTPUser
}

func (s *SesSMTPUser) generateModuleTerraformFiles(ctx context.Context) error {
	moduleDirectory := filepath.Join("modules", "patterns", s.GetName())

	mainData := newTemplateData(ctx, s)

	err := template.EvalTerraformTemplateWriteToFile(sesSMTPUserModuleMainTfTemplate, mainData, filepath.Join(moduleDirectory, "main.tf"))

	if err != nil {
		return err
	}

	versionsData := newTemplateData(ctx, s)

	err = template.EvalTerraformTemplateWriteToFile(sesSMTPUserModuleVersionsTfTemplate, versionsData, filepath.Join(moduleDirectory, "versions.tf"))

	if err != nil {
		return err
	}

	variablesData := newTemplateData(ctx, s)

	err = template.EvalTerraformTemplateWriteToFile(sesSMTPUserModuleVariablesTfTemplate, variablesData, filepath.Join(moduleDirectory, "variables.tf"))

	if err != nil {
		return err
	}

	outputsData := newTemplateData(ctx, s)

	err = template.EvalTerraformTemplateWriteToFile(sesSMTPUserModuleOutputsTfTemplate, outputsData, filepath.Join(moduleDirectory, "outputs.tf"))

	if err != nil {
		return err
	}

	return nil
}

func (s *SesSMTPUser) generateTerraformFiles(ctx context.Context) error {
	err := s.generateModuleTerraformFiles(ctx)

	if err != nil {
		return err
	}

	mainData := newTemplateData(ctx, s)

	err = template.EvalTerraformTemplateWriteToFile(sesSMTPUserMainTfTemplate, mainData, filepath.Join(s.GetDirectory(), "main.tf"))

	if err != nil {
		return fmt.Errorf("%sのmain.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", s.PatternName, err)
	}

	versionsData := newTemplateData(ctx, s)

	err = template.EvalTerraformTemplateWriteToFile(awsProviderVersionTfTemplate, versionsData, filepath.Join(s.GetDirectory(), "versions.tf"))

	if err != nil {
		return fmt.Errorf("%sのversions.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", s.PatternName, err)
	}

	outputsData := newTemplateData(ctx, s)

	err = template.EvalTerraformTemplateWriteToFile(sesSMTPUserOutputsTfTemplate, outputsData, filepath.Join(s.GetDirectory(), "outputs.tf"))

	if err != nil {
		return fmt.Errorf("%sのoutputs.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", s.PatternName, err)
	}

	err = terraform.Format(s.GetDirectory())

	if err != nil {
		return err
	}

	return nil
}

func (s *SesSMTPUser) Apply(ctx context.Context) error {
	err := applySimply(ctx, s)

	if err != nil {
		return err
	}

	s.Sensitive = true

	outputsData := newTemplateData(ctx, s)

	err = template.EvalTerraformTemplateWriteToFile(sesSMTPUserOutputsTfTemplate, outputsData, filepath.Join(s.GetDirectory(), "outputs.tf"))

	if err != nil {
		return err
	}

	err = terraform.Format(s.GetDirectory())

	if err != nil {
		return err
	}

	if isApplySkip(s) {
		return nil
	}

	if err := terraform.Apply(s.GetTargetAwsCredential(), s.GetDirectory()); err != nil {
		return err
	}

	return nil
}

var (
	sesSMTPUserModuleMainTfTemplate = `resource "aws_iam_user" "ses_smtp" {
  name = var.username

  tags = merge(
    {
      "Name" = var.username
    },
    var.tags
  )
}

resource "aws_iam_user_policy" "ses_smtp_user" {
  name   = var.policy_name
  user   = aws_iam_user.ses_smtp.name
  policy = var.policy
}

resource "aws_iam_access_key" "ses_smtp_key" {
  user = aws_iam_user.ses_smtp.name
}
`

	sesSMTPUserModuleVariablesTfTemplate = `variable "username" {
  description = "SES STMPで使用するIAMユーザー名"
  type        = string
  default     = "SesSmtpUser"
}

variable "tags" {
  description = "SES SMTPで使用するIAMユーザーに関するリソースに共通的に付与するタグ"
  type        = map(string)
  default     = {}
}

variable "policy_name" {
  description = "ポリシー名"
  type        = string
  default     = "SesSmtpPolicy"
}

variable "policy" {
  description = "IAMユーザーに適用するポリシーの内容"
  type        = string
  default     = <<JSON
{
  "Version":"2012-10-17",
  "Statement":[
    {
      "Effect":"Allow",
      "Action":[
        "ses:SendEmail",
        "ses:SendRawEmail"
      ],
      "Resource":"*"
    }
  ]
}
JSON
}
`

	sesSMTPUserModuleOutputsTfTemplate = `output "access_key_id" {
  value       = aws_iam_access_key.ses_smtp_key.id
  description = "SMTP認証時に、ユーザー名として使用するアクセスキーID"
}

output "secret" {
  value       = aws_iam_access_key.ses_smtp_key.secret
  description = "シークレットアクセスキー"
}

output "smtp_password_v4" {
  value       = aws_iam_access_key.ses_smtp_key.ses_smtp_password_v4
  description = "SMTP認証時にパスワードとして使用する、シークレットアクセスキーを署名バージョン4アルゴリズムで変換した値"
}
`

	sesSMTPUserModuleVersionsTfTemplate = `terraform {
  required_version = ">= {{.TerraformVersion}}"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = ">= {{.AwsProviderVersion}}"
    }
  }
}
`

	sesSMTPUserMainTfTemplate = `provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::{{.Instance.TargetAwsCredential.ActualEnvironmentAccountID}}:role/{{tfTitle .Prefix}}TerraformExecutionRole"
  }
}

module "ses_smtp_user" {
  source = "../../../modules/patterns/ses_smtp_user"

  username    = "{{.Instance.Variables.Username}}"
  policy_name = "{{.Instance.Variables.PolicyName}}"

  tags = {
{{- range $name, $value := .Instance.Variables.Tags}}
    {{$name}}       = "{{$value}}"
{{- end}}
  }
}
`

	sesSMTPUserOutputsTfTemplate = `output "{{.Instance.Name}}" {
  value = module.{{.Instance.Name}}
{{if .Instance.Sensitive}}  sensitive = true {{end}}
}
`
)
