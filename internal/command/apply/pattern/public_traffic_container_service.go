package pattern

import (
	"context"
	"fmt"
	"path/filepath"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/logging"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/template"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/terraform"
)

type PublicTrafficContainerService struct {
	PatternInstanceAttributes
	PatternInstanceDependencyAttributes
	Variables *PublicTrafficContainerServiceVariables
	Network   *Network
}

type PublicTrafficContainerServiceVariables struct {
	Name                                 string
	Tags                                 map[string]string
	PublicTrafficProtocol                string
	PublicTrafficPort                    int
	PublicTrafficInboundCidrBlocks       []string
	PublicTrafficAccessLogsForceDestroy  bool
	DNS                                  map[string]string
	ContainerProtocol                    string
	ContainerPort                        int
	ContainerHealthCheckPath             string
	ContainerClusterName                 string
	ContainerTrafficInboudCideBlocks     []string
	ContainerServiceDesiredCount         int
	ContainerServicePlatformVersion      string
	ContainerTaskCPU                     int
	ContainerTaskMemory                  int
	DefaultEcsTaskIamRoleName            string
	DefaultEcsTaskIamPolicyName          string
	DefaultEcsTaskExecutionIamRoleName   string
	DefaultEcsTaskExecutionIamPolicyName string
	ContainerLogGroupNames               []string
	ContainerDefinitions                 string
}

func NewPublicTrafficContainerService() *PublicTrafficContainerService {
	publicTrafficContainerService := &PublicTrafficContainerService{}
	publicTrafficContainerService.PatternName = "public_traffic_container_service"
	publicTrafficContainerService.Variables = &PublicTrafficContainerServiceVariables{}

	return publicTrafficContainerService
}

func (p *PublicTrafficContainerService) RegisterInstance(patternInstance PatternInstance) {
	if p.DependencyInstances == nil {
		p.DependencyInstances = []PatternInstance{}
	}

	p.DependencyInstances = append(p.DependencyInstances, patternInstance)

	if network, ok := patternInstance.(*Network); ok {
		p.Network = network
	}
}

func (p *PublicTrafficContainerService) generateTerraformFiles(ctx context.Context) error {
	mainData := newTemplateData(ctx, p)

	err := template.EvalTerraformTemplateWriteToFile(publicTrafficContainerServiceMainTfTemplate, mainData, filepath.Join(p.GetDirectory(), "main.tf"))

	if err != nil {
		return fmt.Errorf("%sのmain.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", p.PatternName, err)
	}

	versionsData := newTemplateData(ctx, p)

	err = template.EvalTerraformTemplateWriteToFile(awsProviderVersionTfTemplate, versionsData, filepath.Join(p.GetDirectory(), "versions.tf"))

	if err != nil {
		return fmt.Errorf("%sのversions.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", p.PatternName, err)
	}

	outputsData := newTemplateData(ctx, p)

	err = template.EvalTerraformTemplateWriteToFile(simpleOutputsTfTemplate, outputsData, filepath.Join(p.GetDirectory(), "outputs.tf"))

	if err != nil {
		return fmt.Errorf("%sのoutputs.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", p.PatternName, err)
	}

	instanceDependenciesData := newTemplateData(ctx, p)

	err = template.EvalTerraformTemplateWriteToFile(instanceDependenciesTfTemplate, instanceDependenciesData, filepath.Join(p.GetDirectory(), "instance_dependencies.tf"))

	if err != nil {
		return fmt.Errorf("%sのinstance_dependencies.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", p.PatternName, err)
	}

	err = terraform.Format(p.GetDirectory())

	if err != nil {
		return err
	}

	return nil
}

func (p *PublicTrafficContainerService) Apply(ctx context.Context) error {
	return applySimply(ctx, p)
}

var (
	publicTrafficContainerServiceMainTfTemplate = `provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::{{.Instance.TargetAwsCredential.ActualEnvironmentAccountID}}:role/{{tfTitle .Prefix}}TerraformExecutionRole"
  }
}

module "{{.Instance.Name}}" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/{{.Instance.PatternName}}?ref={{.EponaVersion}}"

  name = "{{.Instance.Variables.Name}}"

  tags = {
{{- range $name, $value := .Instance.Variables.Tags}}
    {{$name}}       = "{{$value}}"
{{- end}}
  }

  vpc_id                             = data.terraform_remote_state.{{.Instance.EnvironmentName}}_{{.Instance.Network.Name}}.outputs.{{.Instance.Network.Name}}.vpc_id
  public_subnets                     = data.terraform_remote_state.{{.Instance.EnvironmentName}}_{{.Instance.Network.Name}}.outputs.network.public_subnets
  public_traffic_protocol            = "{{.Instance.Variables.PublicTrafficProtocol}}"
  public_traffic_port                = {{.Instance.Variables.PublicTrafficPort}}

  public_traffic_inbound_cidr_blocks = [{{range $c := .Instance.Variables.PublicTrafficInboundCidrBlocks -}} "{{$c}}", {{- end}}]

  public_traffic_access_logs_force_destroy = {{.Instance.Variables.PublicTrafficAccessLogsForceDestroy}}

  dns = {
    zone_name   = "{{.Instance.Variables.DNS.zone_name}}"
    record_name = "{{.Instance.Variables.DNS.record_name}}"
  }

  container_subnets  = data.terraform_remote_state.{{.Instance.EnvironmentName}}_{{.Instance.Network.Name}}.outputs.network.private_subnets
  container_protocol = "{{.Instance.Variables.ContainerProtocol}}"
  container_port     = {{.Instance.Variables.ContainerPort}}

  container_health_check_path           = "{{.Instance.Variables.ContainerHealthCheckPath}}"
  container_cluster_name                = "{{.Instance.Variables.ContainerClusterName}}"
  container_traffic_inbound_cidr_blocks = ["{{.Instance.Network.Variables.CidrBlock}}"]
  container_service_desired_count       = {{.Instance.Variables.ContainerServiceDesiredCount}}
  container_service_platform_version    = "{{.Instance.Variables.ContainerServicePlatformVersion}}"
  container_task_cpu                    = {{.Instance.Variables.ContainerTaskCPU}}
  container_task_memory                 = "{{.Instance.Variables.ContainerTaskMemory}}"

  default_ecs_task_iam_role_name             = "{{.Instance.Variables.DefaultEcsTaskIamRoleName}}"
  default_ecs_task_iam_policy_name           = "{{.Instance.Variables.DefaultEcsTaskIamPolicyName}}"
  default_ecs_task_execution_iam_role_name   = "{{.Instance.Variables.DefaultEcsTaskExecutionIamRoleName}}"
  default_ecs_task_execution_iam_policy_name = "{{.Instance.Variables.DefaultEcsTaskExecutionIamPolicyName}}"

  container_log_group_names = [
{{- range $log := .Instance.Variables.ContainerLogGroupNames}}
    "{{$log}}",
{{- end}}
  ]

  container_definitions = <<-JSON
{{.Instance.Variables.ContainerDefinitions}}
  JSON
}
`
)
