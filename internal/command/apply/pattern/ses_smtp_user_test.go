package pattern

import (
	"fmt"
	"path/filepath"
	"testing"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/terraform/lang"
)

func sutSesSMTPUser(serviceName, dir string) *SesSMTPUser {
	const runtimeName = "staging"

	sut := NewSesSMTPUser()

	sut.Name = "ses_smtp_user"
	sut.EnvironmentName = runtimeName
	sut.EnvironmentDirectory = filepath.Join("runtimes", runtimeName)

	sut.DeliveryAwsCredential = deliveryCredential
	sut.TargetAwsCredential = runtimeCredential

	variables := &SesSMTPUserVariables{}
	sut.Variables = variables

	sut.Variables.Username = fmt.Sprintf("%sChatExampleSesSmtpUser", lang.Title(serviceName))
	sut.Variables.PolicyName = fmt.Sprintf("%sChatExampleSesSmtpPolicy", lang.Title(serviceName))

	sut.Variables.Tags = map[string]string{
		"Owner":              serviceName,
		"Environment":        "runtime",
		"RuntimeEnvironment": runtimeName,
		"ManagedBy":          "epona",
	}

	sut.Sensitive = true

	return sut
}

func TestGenerateTerraformFilesSesSMTPUser(t *testing.T) {
	const serviceName = "serviceName"

	AssertGenerateTerraformFiles(
		t,
		"ses_smtp_user",
		serviceName,
		sutSesSMTPUser(serviceName, t.TempDir()),
	)
}
