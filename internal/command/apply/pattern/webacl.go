package pattern

import (
	"context"
	"fmt"
	"path/filepath"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/logging"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/template"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/terraform"
)

type Webacl struct {
	PatternInstanceAttributes
	PatternInstanceDependencyAttributes
	Variables *WebaclVariables
}

type WebaclVariables struct {
	DefaultAction            string
	Name                     string
	Scope                    string
	CreateLoggingBucket      bool
	LoggingBucketName        string
	LoggingPrefix            string
	LoggingCompressionFormat string
	CloudWatchMetricsEnabled bool
	SampledRequestsEnabled   bool
	Tags                     map[string]string
}

func NewWebacl() *Webacl {
	webacl := &Webacl{}
	webacl.PatternName = "webacl"

	return webacl
}

func (w *Webacl) RegisterInstance(patternInstance PatternInstance) {
	if w.DependencyInstances == nil {
		w.DependencyInstances = []PatternInstance{}
	}

	w.DependencyInstances = append(w.DependencyInstances, patternInstance)
}

func (w *Webacl) generateTerraformFiles(ctx context.Context) error {
	mainData := newTemplateData(ctx, w)

	err := template.EvalTerraformTemplateWriteToFile(webaclFrontendMainTfTemplate, mainData, filepath.Join(w.GetDirectory(), "main.tf"))

	if err != nil {
		return fmt.Errorf("%sのmain.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", w.PatternName, err)
	}

	versionsData := newTemplateData(ctx, w)

	err = template.EvalTerraformTemplateWriteToFile(awsProviderVersionTfTemplate, versionsData, filepath.Join(w.GetDirectory(), "versions.tf"))

	if err != nil {
		return fmt.Errorf("%sのversions.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", w.PatternName, err)
	}

	outputsData := newTemplateData(ctx, w)

	err = template.EvalTerraformTemplateWriteToFile(simpleOutputsTfTemplate, outputsData, filepath.Join(w.GetDirectory(), "outputs.tf"))

	if err != nil {
		return fmt.Errorf("%sのoutputs.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", w.PatternName, err)
	}

	err = terraform.Format(w.GetDirectory())

	if err != nil {
		return err
	}

	return nil
}

func (w *Webacl) Apply(ctx context.Context) error {
	return applySimply(ctx, w)
}

var (
	webaclFrontendMainTfTemplate = `provider "aws" {
  alias  = "global"
  region = "us-east-1"
  assume_role {
    role_arn = "arn:aws:iam::{{.Instance.TargetAwsCredential.ActualEnvironmentAccountID}}:role/{{tfTitle .Prefix}}TerraformExecutionRole"
  }
}

# terraformのAWS WAFv2を利用して、本モジュールで適用するルールグループを作成
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/wafv2_rule_group
# 下記は、SQLインジェクションが含まれるリクエストをブロックする例
resource "aws_wafv2_rule_group" "this" {
  provider = aws.global

  name        = "{{.ServiceName}}-frontend-waf-managed-rule_group"
  description = "{{.ServiceName}} frontend waf rule group"
  scope       = "{{.Instance.Variables.Scope}}"
  capacity    = 60

  rule {
    name     = "{{tfTitle .ServiceName}}FrontendBlockSqliRule"
    priority = 1

    action {
      block {}
    }

    statement {
      sqli_match_statement {
        field_to_match {
          all_query_arguments {}
        }

        text_transformation {
          priority = 1
          type     = "URL_DECODE"
        }
        text_transformation {
          priority = 2
          type     = "HTML_ENTITY_DECODE"
        }
        text_transformation {
          priority = 3
          type     = "COMPRESS_WHITE_SPACE"
        }
      }
    }

    visibility_config {
      cloudwatch_metrics_enabled = {{.Instance.Variables.CloudWatchMetricsEnabled}}
      metric_name                = "{{.ServiceName}}-fronend-rule-metrics"
      sampled_requests_enabled   = {{.Instance.Variables.SampledRequestsEnabled}}
    }
  }

  visibility_config {
    cloudwatch_metrics_enabled = {{.Instance.Variables.CloudWatchMetricsEnabled}}
    metric_name                = "{{.ServiceName}}-fronend-rule_group-metrics"
    sampled_requests_enabled   = {{.Instance.Variables.SampledRequestsEnabled}}
  }
}

module "{{.Instance.Name}}" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/{{.Instance.PatternName}}?ref={{.EponaVersion}}"

  providers = {
    aws = aws.global
  }

  default_action = "{{.Instance.Variables.DefaultAction}}"
  name           = "{{.Instance.Variables.Name}}"
  scope          = "{{.Instance.Variables.Scope}}"
  rule_groups = [{
    arn                        = aws_wafv2_rule_group.this.arn
    override_action            = "none"
    cloudwatch_metrics_enabled = {{.Instance.Variables.CloudWatchMetricsEnabled}}
    sampled_requests_enabled   = {{.Instance.Variables.SampledRequestsEnabled}}
  }]

  web_acl_cloudwatch_metrics_enabled = {{.Instance.Variables.CloudWatchMetricsEnabled}}
  web_acl_sampled_requests_enabled   = {{.Instance.Variables.SampledRequestsEnabled}}

  create_logging_bucket      = {{.Instance.Variables.CreateLoggingBucket}}
  s3_logging_bucket_name     = "{{.Instance.Variables.LoggingBucketName}}"
  logging_prefix             = "{{.Instance.Variables.LoggingPrefix}}"
  logging_compression_format = "{{.Instance.Variables.LoggingCompressionFormat}}"

  tags = {
{{- range $name, $value := .Instance.Variables.Tags}}
    {{$name}}       = "{{$value}}"
{{- end}}
  }
}
`
)
