package pattern

import (
	"fmt"
	"testing"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/terraform/lang"
)

func sutCdPipelineBackend(serviceName, dir string) *CdPipelineBackend {
	const runtimeName = "staging"
	vars := NewCdPipelineBackendVariables(
		fmt.Sprintf("%s-chat-back", runtimeName),
		map[string]string{
			"Owner":              serviceName,
			"Environment":        "runtime",
			"RuntimeEnvironment": runtimeName,
			"ManagedBy":          "epona",
		},
		[]map[string]string{
			{"chat-example-backend": runtimeName},
			{"backend-nginx": runtimeName},
		},
		fmt.Sprintf("%s-%s-chat-back-pipeline-source", serviceName, runtimeName),
		"servi-stagi-back-artfct",
		fmt.Sprintf("%s-backend", serviceName),
		fmt.Sprintf("%s-backend-group", serviceName),
		false,
		false,
		`
        {
          "name": "NABLARCH_DB_URL",
          "valueFrom": "/ServiceName/App/Config/nablarch_db_url"
        }`,
		"backend",
		"dummy",
	)
	sut := NewCdPipelineBackend().
		SetupInstance(
			"cd_pipeline_backend_backend",
			runtimeName,
			deliveryCredential,
			runtimeCredential,
			vars,
		)

	// 必要もののみ設定
	ptcs := NewPublicTrafficContainerService()
	ptcs.Name = "public_traffic_container_service_backend"
	ptcs.EnvironmentName = runtimeName
	ptcs.Variables.ContainerClusterName = fmt.Sprintf("%s-chat-example-backend", serviceName)
	ptcs.Variables.ContainerLogGroupNames = []string{
		fmt.Sprintf("/%s/fargate/%s-chat-example-backend/chat-example-backend", serviceName, serviceName),
		fmt.Sprintf("/%s/fargate/%s-chat-example-backend/fluentd", serviceName, serviceName),
		fmt.Sprintf("/%s/fargate/%s-chat-example-backend/reverse-proxy", serviceName, serviceName),
	}
	ptcs.Variables.ContainerPort = 80
	ptcs.Variables.DefaultEcsTaskIamRoleName =
		fmt.Sprintf("%sBackendContainerServiceTaskRole", lang.Title(serviceName))
	ptcs.Variables.DefaultEcsTaskExecutionIamRoleName =
		fmt.Sprintf("%sBackendContainerServiceTaskExecutionRole", lang.Title(serviceName))

	sut.RegisterInstance(ptcs)

	return sut
}

func TestGenerateTerraformFilesCdPipelineBackend(t *testing.T) {
	const serviceName = "serviceName"

	AssertGenerateTerraformFiles(
		t,
		"cd_pipeline_backend",
		serviceName,
		sutCdPipelineBackend(serviceName, t.TempDir()),
	)
}
