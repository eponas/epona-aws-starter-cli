package pattern

import (
	"crypto/md5" // nolint:gosec
	"fmt"
	"path/filepath"
	"strings"
	"testing"
)

func sutDatabase(serviceName, dir string) *Database {
	const runtimeName = "staging"

	sut := NewDatabase()

	sut.Name = "database"
	sut.EnvironmentName = runtimeName
	sut.EnvironmentDirectory = filepath.Join("runtimes", runtimeName)

	sut.DeliveryAwsCredential = deliveryCredential
	sut.TargetAwsCredential = runtimeCredential

	variables := &DatabaseVariables{}
	sut.Variables = variables

	sut.Variables.Name = fmt.Sprintf("%s_postgres", strings.ReplaceAll(serviceName, "-", "_"))
	sut.Variables.Identifier = fmt.Sprintf("%s-rds-instance", serviceName)

	sut.Variables.Engine = "postgres"
	sut.Variables.EngineVersion = "12.3"
	sut.Variables.Port = 5432
	sut.Variables.InstanceClass = "db.t3.micro"

	sut.Variables.AllocatedStorage = 5
	sut.Variables.MaxAllocatedStorage = 10

	sut.Variables.DeleteProtection = true
	sut.Variables.SkipFinalSnapshot = false

	sut.Variables.AutoMinorVersionUpgrade = true
	sut.Variables.MaintenanceWindow = "Tue:18:00-Tue:18:30"
	sut.Variables.BackupRetentionPeriod = 7
	sut.Variables.BackupWindow = "13:00-14:00"

	sut.Variables.Username = fmt.Sprintf("%s_user", strings.ReplaceAll(serviceName, "-", "_"))
	sut.Variables.Password = fmt.Sprintf("%x", md5.Sum([]byte(serviceName+":database"))) // nolint:gosec

	sut.Variables.KmsKeyAlias = fmt.Sprintf("alias/%s-common-encryption-key", serviceName)
	sut.Variables.AvailabilityZone = "ap-northeast-1a"

	sut.Variables.DBSubnetGroupName = fmt.Sprintf("%s-rds-db-subnet-group", serviceName)

	sut.Variables.ParameterGroupName = fmt.Sprintf("%s-rds-parameter-group", serviceName)
	sut.Variables.ParameterGroupFamily = "postgres12"

	sut.Variables.OptionGroupName = fmt.Sprintf("%s-rds-option-group", serviceName)
	sut.Variables.OptionGroupEngineName = "postgres"
	sut.Variables.OptionGroupMajorEngineVersion = 12
	sut.Variables.Tags = map[string]string{
		"Owner":              serviceName,
		"Environment":        "runtime",
		"RuntimeEnvironment": runtimeName,
		"ManagedBy":          "epona",
	}

	nw := NewNetwork()
	nw.Name = "network"
	nw.EnvironmentName = runtimeName
	sut.RegisterInstance(nw)

	key := NewEncryptionKey()
	key.Name = "encryption_key"
	key.EnvironmentName = runtimeName
	sut.RegisterInstance(key)

	return sut
}

func TestGenerateTerraformFilesDatabase(t *testing.T) {
	const serviceName = "serviceName"

	AssertGenerateTerraformFiles(
		t,
		"database",
		serviceName,
		sutDatabase(serviceName, t.TempDir()),
	)
}
