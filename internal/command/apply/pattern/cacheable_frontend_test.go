package pattern

import (
	"fmt"
	"strings"
	"testing"
)

func sutCacheableFrontend(serviceName, dir string) *CacheableFrontend {
	const runtimeName = "staging"

	sut := NewCacheableFrontend()

	sut.Name = "cacheable_frontend"
	sut.EnvironmentName = runtimeName
	sut.EnvironmentDirectory = dir
	sut.DeliveryAwsCredential = deliveryCredential
	sut.TargetAwsCredential = runtimeCredential

	sut.Variables = &CacheableFrontendVariables{}
	sut.Variables.S3FrontendBucketName =
		fmt.Sprintf("%s-%s-frontend-static",
			strings.ReplaceAll(serviceName, "_", "-"), runtimeName)
	sut.Variables.ZoneName = "staging-myservice.example.com"
	sut.Variables.RecordName = "serviceName.staging-myservice.example.com"

	sut.Variables.DefaultRootObject = "index.html"
	sut.Variables.OriginPath = "/public"
	sut.Variables.LoggingBucketName = fmt.Sprintf("%s-%s-frontend-logging", strings.ReplaceAll(serviceName, "_", "-"), runtimeName)
	sut.Variables.CreateLoggingBucket = true
	sut.Variables.LoggingBucketPrefix = fmt.Sprintf("%s/", strings.ReplaceAll(serviceName, "_", "-"))
	sut.Variables.IncludeCookieLogging = false
	sut.Variables.ViewerResponseLambdaFunction = fmt.Sprintf("%s-frontend-add-header-function", strings.ReplaceAll(serviceName, "_", "-"))
	sut.Variables.Tags = map[string]string{
		"Owner":              serviceName,
		"Environment":        "runtime",
		"RuntimeEnvironment": runtimeName,
		"ManagedBy":          "epona",
	}

	webacl := NewWebacl()
	webacl.Name = "webacl_frontend"
	webacl.EnvironmentName = runtimeName
	sut.RegisterInstance(webacl)
	return sut
}

func TestGenerateTerraformFilesCacheableFrontend(t *testing.T) {
	const serviceName = "serviceName"

	AssertGenerateTerraformFiles(
		t,
		"cacheable_frontend",
		serviceName,
		sutCacheableFrontend(serviceName, t.TempDir()),
	)
}
