package pattern

import (
	"fmt"
	"testing"
)

func sutCdPipelineFrontendTrigger(serviceName, dir string) *CdPipelineFrontendTrigger {

	const runtimeName = "staging"

	vars := NewCdPipelineFrontendTriggerVariables(
		fmt.Sprintf("%s-chat-front", runtimeName),
		runtimeCredential.ActualEnvironmentAccountID,
		fmt.Sprintf("serviceN-%s-static-resource", runtimeName),
		"arn:aws:s3:::dummy-bucket",
		fmt.Sprintf("arn:aws:kms:%s:%s:key/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx", runtimeCredential.Region, runtimeCredential.ActualEnvironmentAccountID),
	)
	sut := NewCdPipelineFrontendTrigger().
		SetupInstance(
			"cd_pipeline_frontend_trigger",
			"delivery",
			runtimeName,
			runtimeCredential.ActualEnvironmentAccountID,
			deliveryCredential,
			vars,
		)

	cp := NewCiPipeline()
	cp.Name = "ci_pipeline"
	cp.EnvironmentName = "delivery"

	sut.RegisterInstance(cp)

	return sut
}

func TestGenerateTerraformFilesCdPipelineFrontendTrigger(t *testing.T) {
	const serviceName = "serviceName"

	AssertGenerateTerraformFiles(
		t,
		"cd_pipeline_frontend_trigger",
		serviceName,
		sutCdPipelineFrontendTrigger(serviceName, t.TempDir()),
	)
}
