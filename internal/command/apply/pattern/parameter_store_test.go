package pattern

import (
	"fmt"
	"path/filepath"
	"testing"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/terraform/lang"
)

func sutParameterStore(serviceName, dir string) *ParameterStore {
	const runtimeName = "staging"

	sut := NewParameterStore()

	sut.Name = "parameter_store"
	sut.EnvironmentName = runtimeName
	sut.EnvironmentDirectory = filepath.Join("runtimes", runtimeName)

	sut.DeliveryAwsCredential = deliveryCredential
	sut.TargetAwsCredential = runtimeCredential

	variables := &ParameterStoreVariables{}
	sut.Variables = variables

	sut.Variables.KmsKeyAlias =
		fmt.Sprintf("alias/%s-common-encryption-key", serviceName)

	sut.Variables.Tags = map[string]string{
		"Owner":              serviceName,
		"Environment":        "runtime",
		"RuntimeEnvironment": runtimeName,
		"ManagedBy":          "epona",
	}

	sut.Variables.Parameters = []map[string]map[string]string{
		{
			"nablarch_db_url": {
				"name":          fmt.Sprintf("/%s/App/Config/nablarch_db_url", lang.Title(serviceName)),
				"type":          "SecureString",
				"terraformType": "string",
				"value":         "[dummy]", // => Database callback
			},
		},
	}

	key := NewEncryptionKey()
	key.EnvironmentName = runtimeName
	key.Name = "encryption_key"
	sut.RegisterInstance(key)

	return sut
}

func TestGenerateTerraformFilesParameterStore(t *testing.T) {
	const serviceName = "serviceName"

	AssertGenerateTerraformFiles(
		t,
		"parameter_store",
		serviceName,
		sutParameterStore(serviceName, t.TempDir()),
	)
}
