package pattern

import (
	"fmt"
	"testing"
)

func sutCdPipelineBackendTrigger(serviceName, dir string) *CdPipelineBackendTrigger {
	const (
		runtimeName  = "staging"
		deliveryName = "delivery"
	)

	vars := NewCdPipelineBackendTriggerVariables(
		"staging-chat-back",
		map[string]string{
			"Owner":              serviceName,
			"Environment":        deliveryName,
			"ManagedBy":          "epona",
			"RuntimeEnvironment": runtimeName,
		},
		fmt.Sprintf("%s-%s-chat-back-pipeline-source", serviceName, runtimeName),
		false,
		[]map[string]string{
			{"chat-example-backend": runtimeName},
			{"backend-nginx": runtimeName},
		},
		"arn:aws:s3:::dummy-bucket",
		fmt.Sprintf("arn:aws:kms:%s:%s:key/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx",
			runtimeCredential.Region, runtimeCredential.ActualEnvironmentAccountID),
	)

	sut := NewCdPipelineBackendTrigger().
		SetupInstance(
			"cd_pipeline_backend_trigger_backend",
			deliveryName,
			runtimeName,
			runtimeCredential.ActualEnvironmentAccountID,
			deliveryCredential,
			vars,
		)

	cp := NewCiPipeline()
	cp.Name = "ci_pipeline"
	cp.EnvironmentName = deliveryName

	sut.RegisterInstance(cp)

	return sut
}

func TestGenerateTerraformFilesCdPipelineBackendTrigger(t *testing.T) {
	const serviceName = "serviceName"

	AssertGenerateTerraformFiles(
		t,
		"cd_pipeline_backend_trigger",
		serviceName,
		sutCdPipelineBackendTrigger(serviceName, t.TempDir()),
	)
}
