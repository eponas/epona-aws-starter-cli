provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::987654321098:role/TerraformExecutionRole"
  }
}

module "database" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/database?ref=v0.2.4"

  name = "serviceName_postgres"

  tags = {
    Environment        = "runtime"
    ManagedBy          = "epona"
    Owner              = "serviceName"
    RuntimeEnvironment = "staging"
  }

  identifier            = "serviceName-rds-instance"
  engine                = "postgres"
  engine_version        = "12.3"
  port                  = 5432
  instance_class        = "db.t3.micro"
  allocated_storage     = 5
  max_allocated_storage = 10

  deletion_protection = true
  skip_final_snapshot = false

  auto_minor_version_upgrade = true
  maintenance_window         = "Tue:18:00-Tue:18:30" # UTC Timezone
  backup_retention_period    = "7"
  backup_window              = "13:00-14:00" #  UTC Timezone

  username = "serviceName_user"
  password = "5515da11b29ab4dd2ee9accc60dba502"

  kms_key_id = data.terraform_remote_state.staging_encryption_key.outputs.encryption_key.keys["alias/serviceName-common-encryption-key"].key_arn

  vpc_id            = data.terraform_remote_state.staging_network.outputs.network.vpc_id
  availability_zone = "ap-northeast-1a"

  db_subnets           = data.terraform_remote_state.staging_network.outputs.network.private_subnets
  db_subnet_group_name = "serviceName-rds-db-subnet-group"

  parameter_group_name   = "serviceName-rds-parameter-group"
  parameter_group_family = "postgres12"

  option_group_name                 = "serviceName-rds-option-group"
  option_group_engine_name          = "postgres"
  option_group_major_engine_version = "12"

  performance_insights_kms_key_id = data.terraform_remote_state.staging_encryption_key.outputs.encryption_key.keys["alias/serviceName-common-encryption-key"].key_arn

  parameters = [
  ]

  option_group_options = [
  ]
}
