provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::987654321098:role/TerraformExecutionRole"
  }
}

module "cd_pipeline_backend_backend" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cd_pipeline_backend?ref=v0.2.4"

  delivery_account_id = "123456789012"
  ecr_repositories = {
    chat-example-backend = {
      tag            = "staging"
      artifact_name  = "chat-example-backend"
      container_name = "IMAGE1_NAME"
    }
    backend-nginx = {
      tag            = "staging"
      artifact_name  = "backend-nginx"
      container_name = "IMAGE2_NAME"
    }
  }

  # Delivery環境のECRにCodePipelineからクロスアカウントでアクセスするためのロール
  # cd_pipeline_backend_trigger_backend の output として出力される
  cross_account_codepipeline_access_role_arn = "dummy"
  # cd_pipeline_backend_trigger_backend の bucket_name と同じ名前を指定する
  source_bucket_name = "serviceName-staging-chat-back-pipeline-source"

  cluster_name = data.terraform_remote_state.staging_public_traffic_container_service_backend.outputs.public_traffic_container_service_backend.ecs_cluster_name
  service_name = data.terraform_remote_state.staging_public_traffic_container_service_backend.outputs.public_traffic_container_service_backend.ecs_service_name

  pipeline_name                       = "staging-chat-back"
  artifact_store_bucket_name          = "servi-stagi-back-artfct"
  artifact_store_bucket_force_destroy = false

  prod_listener_arns = [data.terraform_remote_state.staging_public_traffic_container_service_backend.outputs.public_traffic_container_service_backend.load_balancer_prod_listener_arn]
  target_group_names = data.terraform_remote_state.staging_public_traffic_container_service_backend.outputs.public_traffic_container_service_backend.load_balancer_target_group_names

  codedeploy_app_name         = "serviceName-backend"
  deployment_group_name_ecs   = "serviceName-backend-group"
  deployment_require_approval = false

  tags = {
    Environment        = "runtime"
    ManagedBy          = "epona"
    Owner              = "serviceName"
    RuntimeEnvironment = "staging"
  }
}
