provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::123456789012:role/TerraformExecutionRole"
  }
}

locals {
  runtime_account_id = "987654321098"
}

data "aws_region" "current" {}

module "cd_pipeline_frontend_trigger" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cd_pipeline_frontend_trigger?ref=v0.2.4"

  pipeline_name      = "staging-chat-front"
  source_bucket_arn  = data.terraform_remote_state.delivery_ci_pipeline.outputs.ci_pipeline.s3_ci_bucket_arns["serviceN-staging-static-resource"]
  runtime_account_id = local.runtime_account_id

  # Runtime環境に構築するアーティファクトストアのバケット名
  target_event_bus_arn = "arn:aws:events:${data.aws_region.current.name}:${local.runtime_account_id}:event-bus/default"

  artifact_store_bucket_arn                = "arn:aws:s3:::dummy-bucket"
  artifact_store_bucket_encryption_key_arn = "arn:aws:kms:ap-northeast-1:987654321098:key/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
}
