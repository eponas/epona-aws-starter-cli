provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::987654321098:role/TerraformExecutionRole"
  }
}

module "parameter_store" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/parameter_store?ref=v0.2.4"

  parameters = [
    {
      name   = "/ServiceName/App/Config/nablarch_db_url"
      value  = var.nablarch_db_url
      type   = "SecureString"
      key_id = data.terraform_remote_state.staging_encryption_key.outputs.encryption_key.keys["alias/serviceName-common-encryption-key"].key_alias_name
    },
  ]
}
