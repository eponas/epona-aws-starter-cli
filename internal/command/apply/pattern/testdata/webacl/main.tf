provider "aws" {
  alias  = "global"
  region = "us-east-1"
  assume_role {
    role_arn = "arn:aws:iam::123456789012:role/TerraformExecutionRole"
  }
}

# terraformのAWS WAFv2を利用して、本モジュールで適用するルールグループを作成
# https://registry.terraform.io/providers/hashicorp/aws/latest/docs/resources/wafv2_rule_group
# 下記は、SQLインジェクションが含まれるリクエストをブロックする例
resource "aws_wafv2_rule_group" "this" {
  provider = aws.global

  name        = "serviceName-frontend-waf-managed-rule_group"
  description = "serviceName frontend waf rule group"
  scope       = "CLOUDFRONT"
  capacity    = 60

  rule {
    name     = "ServiceNameFrontendBlockSqliRule"
    priority = 1

    action {
      block {}
    }

    statement {
      sqli_match_statement {
        field_to_match {
          all_query_arguments {}
        }

        text_transformation {
          priority = 1
          type     = "URL_DECODE"
        }
        text_transformation {
          priority = 2
          type     = "HTML_ENTITY_DECODE"
        }
        text_transformation {
          priority = 3
          type     = "COMPRESS_WHITE_SPACE"
        }
      }
    }

    visibility_config {
      cloudwatch_metrics_enabled = true
      metric_name                = "serviceName-fronend-rule-metrics"
      sampled_requests_enabled   = true
    }
  }

  visibility_config {
    cloudwatch_metrics_enabled = true
    metric_name                = "serviceName-fronend-rule_group-metrics"
    sampled_requests_enabled   = true
  }
}

module "webacl" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/webacl?ref=v0.2.4"

  providers = {
    aws = aws.global
  }

  default_action = "allow"
  name           = "serviceName"
  scope          = "CLOUDFRONT"
  rule_groups = [{
    arn                        = aws_wafv2_rule_group.this.arn
    override_action            = "none"
    cloudwatch_metrics_enabled = true
    sampled_requests_enabled   = true
  }]

  web_acl_cloudwatch_metrics_enabled = true
  web_acl_sampled_requests_enabled   = true

  create_logging_bucket      = true
  s3_logging_bucket_name     = "serviceName-staging-frontend-acl-firehose-bucket"
  logging_prefix             = "frontend_waf_log/"
  logging_compression_format = "GZIP"

  tags = {
    Environment        = "runtime"
    ManagedBy          = "epona"
    Owner              = "serviceName"
    RuntimeEnvironment = "staging"
  }
}
