# OriginとなるS3バケットを配置するリージョンを定義したAWS Provider
provider "aws" {
  alias = "origin_provider"
  assume_role {
    role_arn = "arn:aws:iam::987654321098:role/TerraformExecutionRole"
  }
}

# CloudFront関連リソースを配置するリージョンを定義したAWS Provider
provider "aws" {
  alias  = "cache_provider"
  region = "us-east-1"
  assume_role {
    role_arn = "arn:aws:iam::987654321098:role/TerraformExecutionRole"
  }
}

module "cacheable_frontend" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cacheable_frontend?ref=v0.2.4"

  providers = {
    aws.origin_provider = aws.origin_provider
    aws.cache_provider  = aws.cache_provider
  }

  s3_frontend_bucket_name = "serviceName-staging-frontend-static"
  zone_name               = "staging-myservice.example.com"
  record_name             = "serviceName.staging-myservice.example.com"
  tags = {
    Environment        = "runtime"
    ManagedBy          = "epona"
    Owner              = "serviceName"
    RuntimeEnvironment = "staging"
  }

  cloudfront_default_root_object = "index.html"
  cloudfront_origin = {
    origin_path = "/public"
  }

  cloudfront_default_cache_behavior = {
    allowed_methods        = ["GET", "HEAD"]
    cached_methods         = ["GET", "HEAD"]
    viewer_protocol_policy = "redirect-to-https"
    min_ttl                = null
    default_ttl            = null
    max_ttl                = null
    compress               = false
  }

  cloudfront_custom_error_responses = [
    {
      error_code            = 404
      error_caching_min_ttl = 300
      response_code         = 200
      response_page_path    = "/"
    }
  ]

  cloudfront_logging_config = {
    bucket_name           = "serviceName-staging-frontend-logging"
    create_logging_bucket = true
    prefix                = "serviceName/"
    include_cookies       = false
  }

  # webacl_frontendで作成したAWS WAFをCloudFrontと紐付け
  cloudfront_web_acl_id = try(data.terraform_remote_state.staging_webacl_frontend.outputs.webacl_frontend.webacl_arn, null)

  # レスポンスヘッダーに情報を付与するラムダ関数名
  viewer_response_lambda_function_name = "serviceName-frontend-add-header-function"
}
