provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::987654321098:role/TerraformExecutionRole"
  }
}

module "network" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/network?ref=v0.2.4"

  name = "serviceName-staging-network"

  tags = {
    Environment        = "runtime"
    ManagedBy          = "epona"
    Owner              = "serviceName"
    RuntimeEnvironment = "staging"
  }

  cidr_block = "10.250.0.0/16"

  availability_zones = ["ap-northeast-1a", "ap-northeast-1c", ]

  public_subnets             = [cidrsubnet("10.250.0.0/16", 8, 1), cidrsubnet("10.250.0.0/16", 8, 2), ]
  private_subnets            = [cidrsubnet("10.250.0.0/16", 8, 3), cidrsubnet("10.250.0.0/16", 8, 4), ]
  nat_gateway_deploy_subnets = [cidrsubnet("10.250.0.0/16", 8, 1), cidrsubnet("10.250.0.0/16", 8, 2), ]
}
