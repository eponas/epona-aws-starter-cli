data "terraform_remote_state" "staging_network" {
  backend = "s3"

  config = {
    bucket         = "serviceName-staging-terraform-tfstate"
    key            = "network/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "serviceName_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::123456789012:role/StagingTerraformBackendAccessRole"
  }
}

