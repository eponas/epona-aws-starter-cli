provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::987654321098:role/TerraformExecutionRole"
  }
}

module "ses_smtp_user" {
  source = "../../../modules/patterns/ses_smtp_user"

  username    = "ServiceNameChatExampleSesSmtpUser"
  policy_name = "ServiceNameChatExampleSesSmtpPolicy"

  tags = {
    Environment        = "runtime"
    ManagedBy          = "epona"
    Owner              = "serviceName"
    RuntimeEnvironment = "staging"
  }
}
