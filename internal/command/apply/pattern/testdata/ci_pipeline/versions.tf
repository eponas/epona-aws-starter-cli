terraform {
  required_version = "0.14.10"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "3.37.0"
    }
  }

  backend "s3" {
    bucket         = "serviceName-delivery-terraform-tfstate"
    key            = "ci_pipeline/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "serviceName_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::123456789012:role/DeliveryTerraformBackendAccessRole"
  }
}
