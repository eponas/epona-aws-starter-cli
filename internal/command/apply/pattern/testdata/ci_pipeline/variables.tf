
variable "gitlab_runner_registration_token" {
  description = "GitLab Runner登録のためのトークン（cloud-initスクリプトを独自定義する場合は、この変数は不要になる可能性がある）"
  type        = string
}
