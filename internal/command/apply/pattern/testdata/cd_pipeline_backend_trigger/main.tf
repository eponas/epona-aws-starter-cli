provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::123456789012:role/TerraformExecutionRole"
  }
}

locals {
  runtime_account_id = "987654321098"
}

data "aws_region" "current" {}

module "cd_pipeline_backend_trigger_backend" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cd_pipeline_backend_trigger?ref=v0.2.4"

  name                 = "staging-chat-back"
  bucket_name          = "serviceName-staging-chat-back-pipeline-source"
  bucket_force_destroy = false
  runtime_account_id   = local.runtime_account_id

  tags = {
    Environment        = "delivery"
    ManagedBy          = "epona"
    Owner              = "serviceName"
    RuntimeEnvironment = "staging"
  }

  # リポジトリ名と、その ARN およびデプロイ対象タグのマップ
  ecr_repositories = {
    "chat-example-backend" = {
      arn  = data.terraform_remote_state.delivery_ci_pipeline.outputs.ci_pipeline.container_image_repository_arns["chat-example-backend"]
      tags = ["staging"]
    }
    "backend-nginx" = {
      arn  = data.terraform_remote_state.delivery_ci_pipeline.outputs.ci_pipeline.container_image_repository_arns["backend-nginx"]
      tags = ["staging"]
    }
  }

  # CodePipeline が用いる artifact store のバケット ARN
  target_event_bus_arn = "arn:aws:events:${data.aws_region.current.name}:${local.runtime_account_id}:event-bus/default"

  artifact_store_bucket_arn                = "arn:aws:s3:::dummy-bucket"
  artifact_store_bucket_encryption_key_arn = "arn:aws:kms:ap-northeast-1:987654321098:key/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx"
}
