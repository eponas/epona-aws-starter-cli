provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::987654321098:role/TerraformExecutionRole"
  }
}

module "cd_pipeline_frontend" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/cd_pipeline_frontend?ref=v0.2.4"

  tags = {
    Environment        = "runtime"
    ManagedBy          = "epona"
    Owner              = "serviceName"
    RuntimeEnvironment = "staging"
  }

  delivery_account_id = "123456789012"

  pipeline_name = "staging-chat-front"

  artifact_store_bucket_name          = "serviceName-staging-frontend-artfct"
  artifact_store_bucket_force_destroy = "false"

  # Delivery環境のS3にCodePipelineからクロスアカウントでアクセスするためのロール
  # cd_pipeline_frontend_trigger の output として出力される
  cross_account_codepipeline_access_role_arn = "dummy"

  # ci_pipeline の bucket_name と同じ名前を指定する
  source_bucket_name = "serviceN-staging-static-resource"
  source_object_key  = "source.zip"

  deployment_bucket_name = data.terraform_remote_state.staging_cacheable_frontend.outputs.cacheable_frontend.frontend_bucket_name
  deployment_object_path = data.terraform_remote_state.staging_cacheable_frontend.outputs.cacheable_frontend.frontend_bucket_origin_path

  deployment_require_approval = false

  cache_invalidation_config = {
    enable                     = true
    cloudfront_distribution_id = data.terraform_remote_state.staging_cacheable_frontend.outputs.cacheable_frontend.cloudfront_id
  }
}
