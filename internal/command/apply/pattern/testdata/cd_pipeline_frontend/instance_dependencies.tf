data "terraform_remote_state" "staging_cacheable_frontend" {
  backend = "s3"

  config = {
    bucket         = "serviceName-staging-terraform-tfstate"
    key            = "cacheable_frontend/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "serviceName_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::123456789012:role/StagingTerraformBackendAccessRole"
  }
}

