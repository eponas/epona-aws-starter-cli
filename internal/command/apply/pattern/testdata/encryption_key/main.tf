provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::987654321098:role/TerraformExecutionRole"
  }
}

module "encryption_key" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/encryption_key?ref=v0.2.4"

  kms_keys = [
    { alias_name = "alias/serviceName-common-encryption-key" },
  ]

  tags = {
    Environment        = "runtime"
    ManagedBy          = "epona"
    Owner              = "serviceName"
    RuntimeEnvironment = "staging"
  }
}
