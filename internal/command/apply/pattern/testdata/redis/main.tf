provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::987654321098:role/TerraformExecutionRole"
  }
}

module "redis" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/redis?ref=v0.2.4"

  replication_group_id          = "serviceName-redis-group"
  replication_group_description = "ServiceName Redis Cluster"

  vpc_id = data.terraform_remote_state.staging_network.outputs.network.vpc_id

  cluster_mode_enabled = false

  number_cache_clusters = 1

  auth_token = "b157dda7afb55268799d45400ae27ffd"

  kms_key_id = data.terraform_remote_state.staging_encryption_key.outputs.encryption_key.keys["alias/serviceName-common-encryption-key"].key_arn

  snapshot_retention_limit = 7
  snapshot_window          = "00:00-01:00"         # UTC Timezone
  maintenance_window       = "thu:15:00-thu:16:00" # UTC Timezone

  engine_version             = "5.0.6"
  node_type                  = "cache.t3.medium"
  automatic_failover_enabled = false

  apply_immediately = false

  family = "redis5.0"

  subnets = data.terraform_remote_state.staging_network.outputs.network.private_subnets

  parameters = [
  ]

  tags = {
    Environment        = "runtime"
    ManagedBy          = "epona"
    Owner              = "serviceName"
    RuntimeEnvironment = "staging"
  }

  source_security_group_ids = []
}
