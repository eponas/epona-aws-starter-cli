package pattern

import (
	"fmt"
	"path/filepath"
	"strings"
	"testing"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/terraform/lang"
)

func sutPublicTrafficContainerService(serviceName, dir string) *PublicTrafficContainerService {

	const runtimeName = "staging"

	sut := NewPublicTrafficContainerService()

	sut.EnvironmentName = runtimeName
	sut.EnvironmentDirectory = filepath.Join("runtimes", runtimeName)

	sut.DeliveryAwsCredential = deliveryCredential
	sut.TargetAwsCredential = runtimeCredential

	variables := &PublicTrafficContainerServiceVariables{}
	sut.Variables = variables

	sut.Variables.PublicTrafficProtocol = "HTTPS"
	sut.Variables.PublicTrafficPort = 443
	sut.Variables.PublicTrafficInboundCidrBlocks = []string{
		"0.0.0.0/0",
	}
	sut.Variables.DNS = map[string]string{}
	sut.Variables.DNS["zone_name"] =
		fmt.Sprintf("%s-%s.example.com", runtimeName, strings.ToLower(serviceName))

	sut.Variables.ContainerProtocol = "HTTP"
	sut.Variables.ContainerPort = 80

	sut.Variables.ContainerHealthCheckPath = "/api/health"

	sut.Variables.ContainerServiceDesiredCount = 3
	sut.Variables.ContainerServicePlatformVersion = "1.4.0"
	sut.Variables.ContainerTaskCPU = 512
	sut.Variables.ContainerTaskMemory = 1024

	sut.Variables.Tags = map[string]string{
		"Owner":              serviceName,
		"Environment":        "runtime",
		"RuntimeEnvironment": runtimeName,
		"ManagedBy":          "epona",
	}

	nw := NewNetwork()
	nw.Name = "network"
	nw.EnvironmentName = runtimeName
	nw.Variables.CidrBlock = "10.251.0.0/16"
	sut.RegisterInstance(nw)

	sut.Name = "public_traffic_container_service_backend"
	sut.Variables.Name = fmt.Sprintf("%s-chat-example-backend", serviceName)
	sut.Variables.DNS["record_name"] =
		fmt.Sprintf("chat-example-backend.%s", sut.Variables.DNS["zone_name"])
	sut.Variables.ContainerClusterName = fmt.Sprintf("%s-chat-example-backend", serviceName)
	sut.Variables.DefaultEcsTaskIamRoleName =
		fmt.Sprintf("%sBackendContainerServiceTaskRole", lang.Title(serviceName))
	sut.Variables.DefaultEcsTaskIamPolicyName =
		fmt.Sprintf("%sBackendContainerServiceTaskRolePolicy", lang.Title(serviceName))
	sut.Variables.DefaultEcsTaskExecutionIamRoleName =
		fmt.Sprintf("%sBackendContainerServiceTaskExecutionRole", lang.Title(serviceName))
	sut.Variables.DefaultEcsTaskExecutionIamPolicyName =
		fmt.Sprintf("%sBackendContainerServiceTaskExecutionRolePolicy", lang.Title(serviceName))

	sut.Variables.ContainerLogGroupNames = []string{
		fmt.Sprintf("/%s/fargate/%s-chat-example-backend/chat-example-backend", serviceName, serviceName),
		fmt.Sprintf("/%s/fargate/%s-chat-example-backend/fluentd", serviceName, serviceName),
		fmt.Sprintf("/%s/fargate/%s-chat-example-backend/reverse-proxy", serviceName, serviceName),
	}

	sut.Variables.ContainerDefinitions = fmt.Sprintf(`  [
    {
      "name": "%s-task",
      "image": "hashicorp/http-echo:0.2.3",
      "essential": true,
      "portMappings": [
        {
          "protocol": "tcp",
          "containerPort": %d
        }
      ],
      "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-group": "%s",
          "awslogs-region": "ap-northeast-1",
          "awslogs-stream-prefix": "http-echo"
        }
      },
      "command": [
          "-listen",
          ":%d",
          "-text",
          "echo"
      ]
    }
  ]`,
		sut.Variables.ContainerClusterName,
		sut.Variables.ContainerPort,
		sut.Variables.ContainerLogGroupNames[0],
		sut.Variables.ContainerPort,
	)

	return sut
}

func TestGenerateTerraformFilesPublicTrafficContainerService(t *testing.T) {
	const serviceName = "serviceName"

	AssertGenerateTerraformFiles(
		t,
		"public_traffic_container_service",
		serviceName,
		sutPublicTrafficContainerService(serviceName, t.TempDir()),
	)
}
