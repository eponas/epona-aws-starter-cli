package pattern

import (
	"fmt"
	"strings"
	"testing"
)

func sutWebacl(serviceName, dir string) *Webacl {
	sut := NewWebacl()
	sut.Name = "webacl"
	sut.EnvironmentName = "staging"
	sut.EnvironmentDirectory = dir
	// クレデンシャル
	sut.DeliveryAwsCredential = &AwsCredential{
		ActualEnvironmentAccountID: "123456789012",
		AccessKeyID:                "ACCESSKEYID",
		SecretAccessKey:            "SECRETACCESSKEY",
		Region:                     "ap-northeast-1",
	}
	sut.TargetAwsCredential = &AwsCredential{
		ActualEnvironmentAccountID: "123456789012",
		AccessKeyID:                "ACCESSKEYID",
		SecretAccessKey:            "SECRETACCESSKEY",
		Region:                     "ap-northeast-1",
	}
	// パラメータ
	sut.Variables = &WebaclVariables{}
	sut.Variables.DefaultAction = "allow"
	sut.Variables.Name = serviceName
	sut.Variables.Scope = "CLOUDFRONT"
	sut.Variables.CreateLoggingBucket = true
	sut.Variables.LoggingBucketName = fmt.Sprintf("%s-%s-frontend-acl-firehose-bucket", strings.ReplaceAll(serviceName, "_", "-"), "staging")
	sut.Variables.LoggingPrefix = "frontend_waf_log/"
	sut.Variables.LoggingCompressionFormat = "GZIP"
	sut.Variables.CloudWatchMetricsEnabled = true
	sut.Variables.SampledRequestsEnabled = true
	sut.Variables.Tags = map[string]string{"Owner": serviceName, "Environment": "runtime", "RuntimeEnvironment": "staging", "ManagedBy": "epona"}

	return sut
}

func TestGenerateTerraformFilesWebacl(t *testing.T) {
	const serviceName = "serviceName"

	AssertGenerateTerraformFiles(
		t,
		"webacl",
		serviceName,
		sutWebacl(serviceName, t.TempDir()),
	)
}
