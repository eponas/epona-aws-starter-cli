package pattern

import (
	"fmt"
	"path/filepath"
	"testing"
)

func sutNetwork(serviceName, dir string) *Network {
	const runtimeName = "staging"

	sut := NewNetwork()
	sut.Name = "network"
	sut.EnvironmentName = runtimeName
	sut.EnvironmentDirectory = filepath.Join("runtimes", runtimeName)

	sut.DeliveryAwsCredential = deliveryCredential
	sut.TargetAwsCredential = runtimeCredential

	variables := &NetworkVariables{}
	sut.Variables = variables

	cidrBlock := "10.250.0.0/16"

	sut.Variables.Name = fmt.Sprintf("%s-%s-network", serviceName, runtimeName)
	sut.Variables.CidrBlock = cidrBlock
	sut.Variables.AvailabilityZones = []string{"ap-northeast-1a", "ap-northeast-1c"}
	sut.Variables.PublicSubnets = []string{
		fmt.Sprintf("cidrsubnet(\"%s\", 8, 1)", cidrBlock),
		fmt.Sprintf("cidrsubnet(\"%s\", 8, 2)", cidrBlock),
	}
	sut.Variables.PrivateSubnets = []string{
		fmt.Sprintf("cidrsubnet(\"%s\", 8, 3)", cidrBlock),
		fmt.Sprintf("cidrsubnet(\"%s\", 8, 4)", cidrBlock),
	}
	sut.Variables.NatGatewayDeploySubnets = []string{
		fmt.Sprintf("cidrsubnet(\"%s\", 8, 1)", cidrBlock),
		fmt.Sprintf("cidrsubnet(\"%s\", 8, 2)", cidrBlock),
	}
	sut.Variables.Tags = map[string]string{
		"Owner":              serviceName,
		"Environment":        "runtime",
		"RuntimeEnvironment": runtimeName,
		"ManagedBy":          "epona",
	}

	return sut
}

func TestGenerateTerraformFilesNetwork(t *testing.T) {
	const serviceName = "serviceName"

	AssertGenerateTerraformFiles(
		t,
		"network",
		serviceName,
		sutNetwork(serviceName, t.TempDir()),
	)
}
