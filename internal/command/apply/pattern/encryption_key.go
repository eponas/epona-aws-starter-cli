package pattern

import (
	"context"
	"fmt"
	"path/filepath"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/logging"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/template"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/terraform"
)

type EncryptionKey struct {
	PatternInstanceAttributes
	Variables *EncryptionKeyVariables
}

type EncryptionKeyVariables struct {
	KmsKeys []map[string]string
	Tags    map[string]string
}

func NewEncryptionKey() *EncryptionKey {
	encryptionKey := &EncryptionKey{}
	encryptionKey.PatternName = "encryption_key" // nolint:goconst

	return encryptionKey
}

func (e *EncryptionKey) generateTerraformFiles(ctx context.Context) error {
	mainData := newTemplateData(ctx, e)

	err := template.EvalTerraformTemplateWriteToFile(encryptionKeyMainTfTemplate, mainData, filepath.Join(e.GetDirectory(), "main.tf"))

	if err != nil {
		return fmt.Errorf("%sのmain.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", e.PatternName, err)
	}

	versionsData := newTemplateData(ctx, e)

	err = template.EvalTerraformTemplateWriteToFile(awsProviderVersionTfTemplate, versionsData, filepath.Join(e.GetDirectory(), "versions.tf"))

	if err != nil {
		return fmt.Errorf("%sのversions.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", e.PatternName, err)
	}

	outputsData := newTemplateData(ctx, e)

	err = template.EvalTerraformTemplateWriteToFile(simpleOutputsTfTemplate, outputsData, filepath.Join(e.GetDirectory(), "outputs.tf"))

	if err != nil {
		return fmt.Errorf("%sのoutputs.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", e.PatternName, err)
	}

	err = terraform.Format(e.GetDirectory())

	if err != nil {
		return err
	}

	return nil
}

func (e *EncryptionKey) Apply(ctx context.Context) error {
	return applySimply(ctx, e)
}

var (
	encryptionKeyMainTfTemplate = `provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::{{.Instance.TargetAwsCredential.ActualEnvironmentAccountID}}:role/{{tfTitle .Prefix}}TerraformExecutionRole"
  }
}

module "{{.Instance.Name}}" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/{{.Instance.PatternName}}?ref={{.EponaVersion}}"

  kms_keys = [
{{- range .Instance.Variables.KmsKeys}}
{{- range $name, $value := .}}
    { {{$name}} = "{{$value}}" },
{{- end}}
{{- end}}
  ]

  tags = {
{{- range $name, $value := .Instance.Variables.Tags}}
    {{$name}}       = "{{$value}}"
{{- end}}
  }
}
`
)
