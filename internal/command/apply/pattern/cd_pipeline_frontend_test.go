package pattern

import (
	"fmt"
	"testing"
)

func sutCdPipelineFrontend(serviceName, dir string) *CdPipelineFrontend {
	const runtimeName = "staging"

	vars := NewCdPipelineFrontendVariables(
		map[string]string{
			"Owner":              serviceName,
			"Environment":        "runtime",
			"RuntimeEnvironment": runtimeName,
			"ManagedBy":          "epona",
		},
		fmt.Sprintf("%s-chat-front", runtimeName),
		fmt.Sprintf("serviceN-%s-static-resource", runtimeName),
		"source.zip",
		fmt.Sprintf("%s-%s-frontend-artfct", serviceName, runtimeName),
		false,
		false,
		true,
		"dummy", // => CdPipelineFrontendTrigger callback
	)

	sut := NewCdPipelineFrontend().
		SetupInstance(
			"cd_pipeline_frontend",
			runtimeName,
			deliveryCredential,
			runtimeCredential,
			vars,
		)

	cf := NewCacheableFrontend()
	cf.Name = "cacheable_frontend"
	cf.EnvironmentName = runtimeName
	sut.RegisterInstance(cf)

	return sut
}

func TestGenerateTerraformFilesCdPipelineFrontend(t *testing.T) {
	const serviceName = "serviceName"

	AssertGenerateTerraformFiles(
		t,
		"cd_pipeline_frontend",
		serviceName,
		sutCdPipelineFrontend(serviceName, t.TempDir()),
	)
}
