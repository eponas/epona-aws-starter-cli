package pattern

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"github.com/hashicorp/hcl/v2"
	"github.com/hashicorp/hcl/v2/hclparse"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/logging"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/service"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/terraform"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/version"
)

var (
	skipTerraformApply               bool
	skipTerraformApplyExcludePattens map[string]bool
)

func init() {
	// example
	// EPONA_STARTER_SKIP_TERRAFORM_APPLY=true
	if v := os.Getenv("EPONA_STARTER_SKIP_TERRAFORM_APPLY"); strings.ToLower(v) == "true" || strings.ToLower(v) == "yes" || strings.ToLower(v) == "on" {
		skipTerraformApply = true
	} else {
		skipTerraformApply = false
	}

	// example
	// EPONA_STARTER_SKIP_TERRAFORM_APPLY_EXCLUDE_PATTERNS=network
	// EPONA_STARTER_SKIP_TERRAFORM_APPLY_EXCLUDE_PATTERNS=network,encryption_key
	if v := os.Getenv("EPONA_STARTER_SKIP_TERRAFORM_APPLY_EXCLUDE_PATTERNS"); v != "" {
		skipTerraformApplyExcludePattens = map[string]bool{}

		for _, patternName := range strings.Split(v, ",") {
			skipTerraformApplyExcludePattens[patternName] = true
		}
	} else {
		skipTerraformApplyExcludePattens = map[string]bool{}
	}
}

type PatternInstance interface {
	generateTerraformFiles(ctx context.Context) error
	Apply(ctx context.Context) error

	GetDeliveryAwsCredential() *AwsCredential
	GetTargetAwsCredential() *AwsCredential

	GetName() string
	GetPatternName() string
	GetEnvironmentName() string
	GetDirectory() string // インスタンスが配置されるディレクトリパスを返却する

	SetOutputAsHcl(*hcl.File)
}

type PatternInstanceAttributes struct {
	DeliveryAwsCredential *AwsCredential // Delivery環境のAWSアカウントに対して利用するTerraform実行ユーザのクレデンシャル
	TargetAwsCredential   *AwsCredential // 適用対象AWSアカウントに対して利用するTerraform実行ユーザのクレデンシャル

	Name                 string
	PatternName          string
	EnvironmentName      string
	EnvironmentDirectory string

	OutputAsHcl *hcl.File
	Output      hcl.Attributes
}

func (p *PatternInstanceAttributes) GetDeliveryAwsCredential() *AwsCredential {
	return p.DeliveryAwsCredential
}

func (p *PatternInstanceAttributes) GetTargetAwsCredential() *AwsCredential {
	return p.TargetAwsCredential
}

func (p *PatternInstanceAttributes) GetName() string {
	return p.Name
}

func (p *PatternInstanceAttributes) GetPatternName() string {
	return p.PatternName
}

func (p *PatternInstanceAttributes) GetEnvironmentName() string {
	return p.EnvironmentName
}

func (p *PatternInstanceAttributes) GetDirectory() string {
	return filepath.Join(p.EnvironmentDirectory, p.Name)
}

func (p *PatternInstanceAttributes) SetOutputAsHcl(outputAsHcl *hcl.File) {
	p.OutputAsHcl = outputAsHcl

	o, _ := outputAsHcl.Body.JustAttributes()
	p.Output = o
}

func newTemplateData(ctx context.Context, patternInstance PatternInstance) map[string]interface{} {
	data := map[string]interface{}{}

	data["EponaVersion"], _ = version.GetEponaVersion(ctx)
	data["TerraformVersion"], _ = version.GetTerraformVersion(ctx)
	data["AwsProviderVersion"], _ = version.GetAwsProviderVersion(ctx)
	data["ServiceName"], _ = service.GetName(ctx)
	data["Prefix"] = service.GetPrefix(ctx)
	data["Instance"] = patternInstance

	return data
}

func isApplySkip(p PatternInstance) bool {
	if skipTerraformApply {
		logging.Info("[pattern] terraform applyをスキップするモードが有効です")

		if !skipTerraformApplyExcludePattens[p.GetPatternName()] {
			return true
		} else {
			logging.Infof("[pattern] %s patternは除外設定に含まれているため、スキップをキャンセルします", p.GetPatternName())
		}
	}

	return false
}

func applySimply(ctx context.Context, p PatternInstance) error {
	logging.Infof("[pattern] %sインスタンス(%s pattern)を適用します", p.GetDirectory(), p.GetPatternName())

	if err := p.generateTerraformFiles(ctx); err != nil {
		return fmt.Errorf("%sのterraformファイル作成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", p.GetName(), err)
	}

	if isApplySkip(p) {
		return nil
	}

	if err := terraform.Init(p.GetTargetAwsCredential(), p.GetDirectory()); err != nil {
		return fmt.Errorf("%s: terraform init時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", p.GetDirectory(), err)
	}

	if err := terraform.Apply(p.GetTargetAwsCredential(), p.GetDirectory()); err != nil {
		return fmt.Errorf("%s: terraform apply時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", p.GetDirectory(), err)
	}

	if output, err := terraform.Output(p.GetTargetAwsCredential(), p.GetDirectory()); err != nil {
		return err
	} else {
		parser := hclparse.NewParser()
		outputAsHcl, _ := parser.ParseHCL([]byte(output), filepath.Join(p.GetDirectory(), p.GetName()+".hcl"))

		p.SetOutputAsHcl(outputAsHcl)
	}

	logging.Infof("[pattern] %sインスタンス(%s pattern)を適用しました", p.GetDirectory(), p.GetPatternName())

	return nil
}

type PatternInstanceDependency interface {
	RegisterInstance(patternInstance PatternInstance)
}

type PatternInstanceDependencyAttributes struct {
	DependencyInstances []PatternInstance
}

type PatternInstanceOutputBinder interface {
	BindOutputInstance(patternInstance PatternInstance)
	AddOutputCallback(func(PatternInstance))
	RunOutputCallbacks()
}

type PatternInstanceOutputBinderAttributes struct {
	OutputBindInstances []PatternInstance
	OutputBindCallbacks []func(PatternInstance)
}

func (p *PatternInstanceOutputBinderAttributes) BindOutputInstance(patternInstance PatternInstance) {
	if p.OutputBindInstances == nil {
		p.OutputBindInstances = []PatternInstance{}
	}

	p.OutputBindInstances = append(p.OutputBindInstances, patternInstance)
}

func (p *PatternInstanceOutputBinderAttributes) AddOutputCallback(callback func(PatternInstance)) {
	if p.OutputBindCallbacks == nil {
		p.OutputBindCallbacks = []func(PatternInstance){}
	}

	p.OutputBindCallbacks = append(p.OutputBindCallbacks, callback)
}

func (p *PatternInstanceOutputBinderAttributes) RunOutputCallbacks() {
	for _, pi := range p.OutputBindInstances {
		for _, c := range p.OutputBindCallbacks {
			c(pi)
		}
	}
}

// PatternInstanceReApplyTrigger は、再適用時に必要になるインタフェースを定義する
type PatternInstanceReApplyTrigger interface {
	RegisterReApplyInstance(patternInstance PatternInstance)
	RunReApply(ctx context.Context)
}

// PatternInstanceReApplyAttributes は、再適用時のパラメータを定義する
type PatternInstanceReApplyAttributes struct {
	ReApplyPatternInstances []PatternInstance // 再適用対象となるInstanceリスト
}

// RegisterReApplyInstance は、再適用対象となるインスタンスを登録する
func (piraa *PatternInstanceReApplyAttributes) RegisterReApplyInstance(patternInstance PatternInstance) {
	if piraa.ReApplyPatternInstances == nil {
		piraa.ReApplyPatternInstances = []PatternInstance{}
	}

	piraa.ReApplyPatternInstances = append(piraa.ReApplyPatternInstances, patternInstance)
}

// RunReApply は、再適用対象に登録された全インスタンスの Apply を実施する
func (piraa *PatternInstanceReApplyAttributes) RunReApply(ctx context.Context) error {
	logging.Infof("[pattern] 関連パターンの再適用を開始します")
	for _, rapi := range piraa.ReApplyPatternInstances {
		if err := rapi.Apply(ctx); err != nil {
			return err
		}
	}

	logging.Infof("[pattern] 関連パターンを再適用しました")
	return nil
}

/*
 * AWSのクレデンシャル
 *
 * アクセスキーID、シークレットキー、リージョンを使用するIAMユーザーは、Delivery環境に存在することに注意
 *
 * 実際の適用先のAWSアカウントと名前は一致しないため、その適用先AWS環境のアカウントIDはで表現される
 */
type AwsCredential struct {
	ActualEnvironmentAccountID string `validate:"numeric,len=12"` // 実際の適用先環境のAWSアカウントID
	AccessKeyID                string `validate:"printascii"`
	SecretAccessKey            string `validate:"printascii"`
	Region                     string `validate:"printascii"`
}

func (a *AwsCredential) GetAccessKeyID() string {
	return a.AccessKeyID
}

func (a *AwsCredential) GetSecretAccessKey() string {
	return a.SecretAccessKey
}

func (a *AwsCredential) GetRegion() string {
	return a.Region
}

// Terraform variableを表現する struct
type variables struct {
	Name string // 変数名
	Desc string // description
	Type string // type
}

// DeliveryRuntimeInstanceAttributes は、 Delivery 環境に適用する Pattern で、依存する Runtime 環境の情報を表現する
type DeliveryRuntimeInstanceAttributes struct {
	PeerEnvironmentName string // 依存する環境名
	PeerAwsAccountID    string // 依存するAWS AccountID
}

const (
	// versions.tf用テンプレート
	awsProviderVersionTfTemplate = `terraform {
  required_version = "{{.TerraformVersion}}"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "{{.AwsProviderVersion}}"
    }
  }

  backend "s3" {
    bucket         = "{{.ServiceName}}-{{.Instance.EnvironmentName}}-terraform-tfstate"
{{- if hasField .Instance "PeerEnvironmentName"}}
    key            = "{{.Instance.PeerEnvironmentName}}/{{.Instance.Name}}/terraform.tfstate"
{{- else}}
    key            = "{{.Instance.Name}}/terraform.tfstate"
{{- end}}
    encrypt        = true
    dynamodb_table = "{{.ServiceName}}_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::{{.Instance.DeliveryAwsCredential.ActualEnvironmentAccountID}}:role/{{tfTitle .Prefix}}{{tfTitle .Instance.EnvironmentName}}TerraformBackendAccessRole"
  }
}
`
	// output.tf 用テンプレート。単純にモジュール自身のvariables全てを出力する
	simpleOutputsTfTemplate = `output "{{.Instance.Name}}" {
  value = module.{{.Instance.Name}}
}
`
	// instance_dependencies.tf 用テンプレート
	instanceDependenciesTfTemplate = `
{{- $serviceName := .ServiceName -}}
{{- $prefix := .Prefix -}}
{{- $instance := .Instance -}}
{{- range $p := .Instance.DependencyInstances -}}
data "terraform_remote_state" "{{$p.EnvironmentName}}_{{$p.Name}}" {
  backend = "s3"

  config = {
    bucket         = "{{$serviceName}}-{{$p.EnvironmentName}}-terraform-tfstate"
    key            = "{{$p.Name}}/terraform.tfstate"
    encrypt        = true
    dynamodb_table = "{{$serviceName}}_terraform_tfstate_lock"

    role_arn = "arn:aws:iam::{{$instance.DeliveryAwsCredential.ActualEnvironmentAccountID}}:role/{{tfTitle $prefix}}{{tfTitle $instance.EnvironmentName}}TerraformBackendAccessRole"
  }
}

{{end -}}
`
	// runtimeInstanceDepenciesTfTemplate = `` // TODO

	// variables.tf 用テンプレート。データとして[]variablesが渡されてくることを想定。
	variablesTfTemplate = `
{{- range $v := . }}
variable "{{$v.Name}}" {
  description = "{{$v.Desc}}"
  type        = {{$v.Type}}
}
{{- end}}
`

	// terraform.tfvars 用テンプレート。データとしてmap[string]stringが渡されてくることを想定。
	// 現行実装は、valueの型は文字列以外を想定していません。文字列以外がくる場合は拡張してください。
	tfvarsTemplate = `
{{- range $name, $value := . }}
    {{$name}} = "{{$value}}"
{{- end}}
`
)
