package pattern

import (
	"context"
	"fmt"
	"path/filepath"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/logging"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/template"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/terraform"
)

type Redis struct {
	PatternInstanceAttributes
	PatternInstanceDependencyAttributes
	Variables     *RedisVariables
	Network       *Network
	EncryptionKey *EncryptionKey
}

type RedisVariables struct {
	Tags                        map[string]string
	ReplicationGroupID          string
	ReplicationGroupDescription string
	ClusterModeEnabled          bool
	NumberCacheClusters         int
	AuthToken                   string
	KmsKeyAlias                 string
	SnapshotRetentionLimit      int
	SnapshotWindow              string
	MaintenanceWindow           string
	EngineVersion               string
	NodeType                    string
	AutomaticFailoverEnabled    bool
	ApplyImmediately            bool
	Family                      string
}

func NewRedis() *Redis {
	redis := &Redis{}
	redis.PatternName = "redis"

	return redis
}

func (r *Redis) RegisterInstance(patternInstance PatternInstance) {
	if r.DependencyInstances == nil {
		r.DependencyInstances = []PatternInstance{}
	}

	r.DependencyInstances = append(r.DependencyInstances, patternInstance)

	if network, ok := patternInstance.(*Network); ok {
		r.Network = network
	}

	if encryptionKey, ok := patternInstance.(*EncryptionKey); ok {
		r.EncryptionKey = encryptionKey
	}
}

func (r *Redis) generateTerraformFiles(ctx context.Context) error {
	mainData := newTemplateData(ctx, r)

	err := template.EvalTerraformTemplateWriteToFile(redisMainTfTemplate, mainData, filepath.Join(r.GetDirectory(), "main.tf"))

	if err != nil {
		return fmt.Errorf("%sのmain.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", r.PatternName, err)
	}

	versionsData := newTemplateData(ctx, r)

	err = template.EvalTerraformTemplateWriteToFile(awsProviderVersionTfTemplate, versionsData, filepath.Join(r.GetDirectory(), "versions.tf"))

	if err != nil {
		return fmt.Errorf("%sのversions.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", r.PatternName, err)
	}

	outputsData := newTemplateData(ctx, r)

	err = template.EvalTerraformTemplateWriteToFile(simpleOutputsTfTemplate, outputsData, filepath.Join(r.GetDirectory(), "outputs.tf"))

	if err != nil {
		return fmt.Errorf("%sのoutputs.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", r.PatternName, err)
	}

	instanceDependenciesData := newTemplateData(ctx, r)

	err = template.EvalTerraformTemplateWriteToFile(instanceDependenciesTfTemplate, instanceDependenciesData, filepath.Join(r.GetDirectory(), "instance_dependencies.tf"))

	if err != nil {
		return fmt.Errorf("%sのinstance_dependencies.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", r.PatternName, err)
	}

	err = terraform.Format(r.GetDirectory())

	if err != nil {
		return err
	}

	return nil
}

func (r *Redis) Apply(ctx context.Context) error {
	return applySimply(ctx, r)
}

var (
	redisMainTfTemplate = `provider "aws" {
  assume_role {
 role_arn = "arn:aws:iam::{{.Instance.TargetAwsCredential.ActualEnvironmentAccountID}}:role/{{tfTitle .Prefix}}TerraformExecutionRole"
  }
}

module "{{.Instance.Name}}" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/{{.Instance.PatternName}}?ref={{.EponaVersion}}"

  replication_group_id          = "{{.Instance.Variables.ReplicationGroupID}}"
  replication_group_description = "{{.Instance.Variables.ReplicationGroupDescription}}"

  vpc_id = data.terraform_remote_state.{{.Instance.EnvironmentName}}_{{.Instance.Network.Name}}.outputs.{{.Instance.Network.Name}}.vpc_id

  cluster_mode_enabled = {{.Instance.Variables.ClusterModeEnabled}}

  number_cache_clusters = {{.Instance.Variables.NumberCacheClusters}}

  auth_token = "{{.Instance.Variables.AuthToken}}"

  kms_key_id = data.terraform_remote_state.{{.Instance.EnvironmentName}}_{{.Instance.EncryptionKey.Name}}.outputs.{{.Instance.EncryptionKey.Name}}.keys["{{.Instance.Variables.KmsKeyAlias}}"].key_arn

  snapshot_retention_limit = {{.Instance.Variables.SnapshotRetentionLimit}}
  snapshot_window          = "{{.Instance.Variables.SnapshotWindow}}" # UTC Timezone
  maintenance_window       = "{{.Instance.Variables.MaintenanceWindow}}" # UTC Timezone

  engine_version             = "{{.Instance.Variables.EngineVersion}}"
  node_type                  = "{{.Instance.Variables.NodeType}}"
  automatic_failover_enabled = {{.Instance.Variables.AutomaticFailoverEnabled}}

  apply_immediately = {{.Instance.Variables.ApplyImmediately}}

  family = "{{.Instance.Variables.Family}}"

  subnets = data.terraform_remote_state.{{.Instance.EnvironmentName}}_{{.Instance.Network.Name}}.outputs.network.private_subnets

  parameters = [
  ]

  tags = {
{{- range $name, $value := .Instance.Variables.Tags}}
    {{$name}}       = "{{$value}}"
{{- end}}
  }

  source_security_group_ids = []
}
`
)
