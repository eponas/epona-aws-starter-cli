package pattern

import (
	"context"
	"fmt"
	"path/filepath"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/logging"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/template"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/terraform"
)

type ParameterStore struct {
	PatternInstanceAttributes
	PatternInstanceDependencyAttributes
	PatternInstanceOutputBinderAttributes
	Variables     *ParameterStoreVariables
	EncryptionKey *EncryptionKey
}

type ParameterStoreVariables struct {
	Parameters  []map[string]map[string]string
	Tags        map[string]string
	KmsKeyAlias string
}

func NewParameterStore() *ParameterStore {
	parameterStore := &ParameterStore{}
	parameterStore.PatternName = "parameter_store"

	return parameterStore
}

func (p *ParameterStore) RegisterInstance(patternInstance PatternInstance) {
	if p.DependencyInstances == nil {
		p.DependencyInstances = []PatternInstance{}
	}

	p.DependencyInstances = append(p.DependencyInstances, patternInstance)

	if encryptionKey, ok := patternInstance.(*EncryptionKey); ok {
		p.EncryptionKey = encryptionKey
	}
}

func (p *ParameterStore) generateTerraformFiles(ctx context.Context) error {
	p.RunOutputCallbacks()

	mainData := newTemplateData(ctx, p)

	err := template.EvalTerraformTemplateWriteToFile(parameterStoreMainTfTemplate, mainData, filepath.Join(p.GetDirectory(), "main.tf"))

	if err != nil {
		return fmt.Errorf("%sのmain.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", p.PatternName, err)
	}

	versionsData := newTemplateData(ctx, p)

	err = template.EvalTerraformTemplateWriteToFile(awsProviderVersionTfTemplate, versionsData, filepath.Join(p.GetDirectory(), "versions.tf"))

	if err != nil {
		return fmt.Errorf("%sのversions.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", p.PatternName, err)
	}

	outputsData := newTemplateData(ctx, p)

	err = template.EvalTerraformTemplateWriteToFile(simpleOutputsTfTemplate, outputsData, filepath.Join(p.GetDirectory(), "outputs.tf"))

	if err != nil {
		return fmt.Errorf("%sのoutputs.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", p.PatternName, err)
	}

	instanceDependenciesData := newTemplateData(ctx, p)

	err = template.EvalTerraformTemplateWriteToFile(instanceDependenciesTfTemplate, instanceDependenciesData, filepath.Join(p.GetDirectory(), "instance_dependencies.tf"))

	if err != nil {
		return fmt.Errorf("%sのinstance_dependencies.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", p.PatternName, err)
	}

	variablesData := newTemplateData(ctx, p)

	err = template.EvalTerraformTemplateWriteToFile(parameterStoreVariablesTfTemplate, variablesData, filepath.Join(p.GetDirectory(), "variables.tf"))

	if err != nil {
		return fmt.Errorf("%sのvariables.tfの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", p.PatternName, err)
	}

	tfvarsData := newTemplateData(ctx, p)

	err = template.EvalTerraformTemplateWriteToFile(paramterStoreTfvarTemplate, tfvarsData, filepath.Join(p.GetDirectory(), "terraform.tfvars"))

	if err != nil {
		return fmt.Errorf("%sのterraform.tfvarsの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", p.PatternName, err)
	}

	err = template.EvalTemplateWriteToFile("terraform.tfvars", map[string]interface{}{}, filepath.Join(p.GetDirectory(), ".gitignore"))

	if err != nil {
		return fmt.Errorf("%sの.gitignoreの生成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", p.PatternName, err)
	}

	err = terraform.Format(p.GetDirectory())

	if err != nil {
		return err
	}

	return nil
}

func (p *ParameterStore) Apply(ctx context.Context) error {
	return applySimply(ctx, p)
}

var (
	parameterStoreMainTfTemplate = `provider "aws" {
  assume_role {
    role_arn = "arn:aws:iam::{{.Instance.TargetAwsCredential.ActualEnvironmentAccountID}}:role/{{tfTitle .Prefix}}TerraformExecutionRole"
  }
}

module "{{.Instance.Name}}" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/patterns/{{.Instance.PatternName}}?ref={{.EponaVersion}}"

{{- $instance := .Instance}}
{{- $encryptionKey := .Instance.EncryptionKey}}

  parameters = [
{{- range $wrapper := $instance.Variables.Parameters}}
{{- range $name, $entry := $wrapper}}
    {
      name   = "{{$entry.name}}"
      value  = var.{{$name}}
      type   = "{{$entry.type}}"
      key_id = data.terraform_remote_state.{{$instance.EnvironmentName}}_{{$encryptionKey.Name}}.outputs.{{$encryptionKey.Name}}.keys["{{$instance.Variables.KmsKeyAlias}}"].key_alias_name
    },
{{- end}}
{{- end}}
  ]
}
`

	parameterStoreVariablesTfTemplate = `
{{- $encryptionKey := .Instance.EncryptionKey}}
{{- range $wrapper := .Instance.Variables.Parameters}}
{{- range $name, $entry := $wrapper}}
variable "{{$name}}" {
  type = {{$entry.terraformType}}
}
{{- end}}
{{end}}
`

	paramterStoreTfvarTemplate = `
{{- $encryptionKey := .Instance.EncryptionKey}}
{{- range $wrapper := .Instance.Variables.Parameters}}
{{- range $name, $entry := $wrapper}}
{{- if or (eq $entry.terraformType "number") (eq $entry.terraformType "bool")}}
{{- $name}} = {{$entry.value}}
{{- else}}
{{- $name}} = "{{$entry.value}}"
{{end}}
{{- end}}
{{end}}`
)
