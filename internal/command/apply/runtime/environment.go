package runtime

import (
	"gitlab.com/eponas/epona-aws-starter-cli/internal/command/apply/activity"
)

type RuntimeEnvironment struct {
	Name       string
	Activities []activity.Activity
}
