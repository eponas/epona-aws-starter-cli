package runtime

import (
	"gitlab.com/eponas/epona-aws-starter-cli/internal/command/apply/activity"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/command/apply/pattern"
)

type DeploymentPipeline struct {
	activity.ActivityAttributes
}

func NewDeploymentPipeline(serviceName string, runtimeName string, users []string, deliveryCredential *pattern.AwsCredential, runtimeCredential *pattern.AwsCredential, parameters *Parameters) (*DeploymentPipeline, error) {
	deploymentPipeline := &DeploymentPipeline{}

	// runtime環境に適用されるインスタンス定義
	network, err := NewServiceNetwork(serviceName, runtimeName, users, deliveryCredential, runtimeCredential, parameters)

	if err != nil {
		return deploymentPipeline, err
	}

	encryptionKey, err := NewEncryptionKey(serviceName, runtimeName, users, deliveryCredential, runtimeCredential, parameters)

	if err != nil {
		return deploymentPipeline, err
	}

	database, err := NewDatabase(serviceName, runtimeName, users, deliveryCredential, runtimeCredential, parameters)

	if err != nil {
		return deploymentPipeline, err
	}

	redis, err := NewRedis(serviceName, runtimeName, users, deliveryCredential, runtimeCredential, parameters)

	if err != nil {
		return deploymentPipeline, err
	}

	publicTrafficContainerServiceBackend, err := NewPublicTrafficContainerServiceBackend(serviceName, runtimeName, users, deliveryCredential, runtimeCredential, parameters)

	if err != nil {
		return deploymentPipeline, err
	}

	publicTrafficContainerServiceNotifier, err := NewPublicTrafficContainerServiceNotifier(serviceName, runtimeName, users, deliveryCredential, runtimeCredential, parameters)

	if err != nil {
		return deploymentPipeline, err
	}

	pipelineInstances := []pattern.PatternInstance{}
	for _, appType := range []string{"backend", "notifier"} {
		cdb, err := NewCdPipelineBackend(serviceName, runtimeName, appType, users, deliveryCredential, runtimeCredential, parameters)
		if err != nil {
			return deploymentPipeline, err
		}
		pipelineInstances = append(pipelineInstances, cdb)
	}

	cdf, err := NewCdPipelineFrontend(serviceName, runtimeName, users, deliveryCredential, runtimeCredential, parameters)
	if err != nil {
		return deploymentPipeline, err
	}
	pipelineInstances = append(pipelineInstances, cdf)

	webacl, err := NewWebaclFrontend(serviceName, runtimeName, users, deliveryCredential, runtimeCredential, parameters)

	if err != nil {
		return deploymentPipeline, err
	}

	cacheableFrontend, err := NewCacheableFrontend(serviceName, runtimeName, users, deliveryCredential, runtimeCredential, parameters)

	if err != nil {
		return deploymentPipeline, err
	}

	sesSMTPUser, err := NewSesSMTPUser(serviceName, runtimeName, users, deliveryCredential, runtimeCredential, parameters)

	if err != nil {
		return deploymentPipeline, err
	}

	parameterStore, err := NewParameterStore(serviceName, runtimeName, users, deliveryCredential, runtimeCredential, parameters)

	if err != nil {
		return deploymentPipeline, err
	}

	deploymentPipeline.PatternInstances = []pattern.PatternInstance{
		network,
		encryptionKey,
		database,
		redis,
		publicTrafficContainerServiceBackend,
		publicTrafficContainerServiceNotifier,
		webacl,
		cacheableFrontend,
		sesSMTPUser,
		parameterStore,
	}
	// cd_pipeline のインスタンスを追加
	deploymentPipeline.PatternInstances = append(deploymentPipeline.PatternInstances, pipelineInstances...)

	return deploymentPipeline, nil
}
