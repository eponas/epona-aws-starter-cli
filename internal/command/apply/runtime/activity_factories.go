package runtime

import (
	"fmt"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/command/apply/activity"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/command/apply/pattern"
)

var (
	recognitionRuntimeNames = map[string]bool{}
)

func NewActivity(activityName string, serviceName string, runtimeName string, users []string, deliveryCredential *pattern.AwsCredential, runtimeCredential *pattern.AwsCredential, p *Parameters) (activity.Activity, error) {

	if !recognitionRuntimeNames[runtimeName] {
		recognitionRuntimeNames[runtimeName] = true
	}

	var activity activity.Activity

	switch activityName {
	case "deployment_pipeline":
		d, err := NewDeploymentPipeline(serviceName, runtimeName, users, deliveryCredential, runtimeCredential, p)

		if err != nil {
			return nil, err
		}

		d.Name = activityName
		activity = d
	default:
		return nil, fmt.Errorf("未定義のアクティビティ[%s]です", activityName)
	}

	return activity, nil
}
