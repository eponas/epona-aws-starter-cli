package runtime

import (
	"crypto/md5" // nolint:gosec
	"fmt"
	"os"
	"path/filepath"
	"strings"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/command/apply/pattern"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/logging"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/terraform/lang"
)

type Parameters struct {
	VpcCidrBlock               string
	Route53HostZone            string
	SesVerifiedMailAddress     string
	PublicAlbInboundCidrBlocks []string
}

func NewServiceNetwork(serviceName string, runtimeName string, users []string, deliveryCredential *pattern.AwsCredential, runtimeCredential *pattern.AwsCredential, parameters *Parameters) (*pattern.Network, error) {
	network := pattern.NewNetwork()

	network.Name = "network"
	network.EnvironmentName = runtimeName
	network.EnvironmentDirectory = filepath.Join("runtimes", runtimeName)

	network.DeliveryAwsCredential = deliveryCredential
	network.TargetAwsCredential = runtimeCredential

	variables := &pattern.NetworkVariables{}
	network.Variables = variables

	network.Variables.Name = fmt.Sprintf("%s-%s-network", serviceName, runtimeName)
	network.Variables.CidrBlock = parameters.VpcCidrBlock
	network.Variables.AvailabilityZones = []string{"ap-northeast-1a", "ap-northeast-1c"}
	network.Variables.PublicSubnets = []string{
		fmt.Sprintf("cidrsubnet(\"%s\", 8, 1)", parameters.VpcCidrBlock),
		fmt.Sprintf("cidrsubnet(\"%s\", 8, 2)", parameters.VpcCidrBlock),
	}
	network.Variables.PrivateSubnets = []string{
		fmt.Sprintf("cidrsubnet(\"%s\", 8, 3)", parameters.VpcCidrBlock),
		fmt.Sprintf("cidrsubnet(\"%s\", 8, 4)", parameters.VpcCidrBlock),
	}
	network.Variables.NatGatewayDeploySubnets = []string{
		fmt.Sprintf("cidrsubnet(\"%s\", 8, 1)", parameters.VpcCidrBlock),
		fmt.Sprintf("cidrsubnet(\"%s\", 8, 2)", parameters.VpcCidrBlock),
	}
	network.Variables.Tags = map[string]string{"Owner": serviceName, "Environment": "runtime", "RuntimeEnvironment": runtimeName, "ManagedBy": "epona"}

	return network, nil
}

func NewEncryptionKey(serviceName string, runtimeName string, users []string, deliveryCredential *pattern.AwsCredential, runtimeCredential *pattern.AwsCredential, parameters *Parameters) (*pattern.EncryptionKey, error) {
	encryptionKey := pattern.NewEncryptionKey()

	encryptionKey.Name = "encryption_key"
	encryptionKey.EnvironmentName = runtimeName
	encryptionKey.EnvironmentDirectory = filepath.Join(filepath.Join("runtimes", runtimeName))

	encryptionKey.DeliveryAwsCredential = deliveryCredential
	encryptionKey.TargetAwsCredential = runtimeCredential

	variables := &pattern.EncryptionKeyVariables{}
	encryptionKey.Variables = variables

	encryptionKey.Variables.KmsKeys = []map[string]string{{"alias_name": createKmsKeyAliasName(serviceName)}}
	encryptionKey.Variables.Tags = map[string]string{"Owner": serviceName, "Environment": "runtime", "RuntimeEnvironment": runtimeName, "ManagedBy": "epona"}

	return encryptionKey, nil
}

func createKmsKeyAliasName(serviceName string) string {
	return fmt.Sprintf("alias/%s-common-encryption-key", serviceName)
}

func NewDatabase(serviceName string, runtimeName string, users []string, deliveryCredential *pattern.AwsCredential, runtimeCredential *pattern.AwsCredential, parameters *Parameters) (*pattern.Database, error) {
	database := pattern.NewDatabase()

	database.Name = "database"
	database.EnvironmentName = runtimeName
	database.EnvironmentDirectory = filepath.Join("runtimes", runtimeName)

	database.DeliveryAwsCredential = deliveryCredential
	database.TargetAwsCredential = runtimeCredential

	variables := &pattern.DatabaseVariables{}
	database.Variables = variables

	database.Variables.Name = fmt.Sprintf("%s_postgres", strings.ReplaceAll(serviceName, "-", "_"))
	database.Variables.Identifier = fmt.Sprintf("%s-rds-instance", serviceName)

	database.Variables.Engine = "postgres"
	database.Variables.EngineVersion = "12.3"
	database.Variables.Port = 5432
	database.Variables.InstanceClass = "db.t3.micro"

	database.Variables.AllocatedStorage = 5
	database.Variables.MaxAllocatedStorage = 10

	database.Variables.DeleteProtection = true
	database.Variables.SkipFinalSnapshot = false

	database.Variables.AutoMinorVersionUpgrade = true
	database.Variables.MaintenanceWindow = "Tue:18:00-Tue:18:30"
	database.Variables.BackupRetentionPeriod = 7
	database.Variables.BackupWindow = "13:00-14:00"

	database.Variables.Username = fmt.Sprintf("%s_user", strings.ReplaceAll(serviceName, "-", "_"))
	// initial password
	database.Variables.Password = fmt.Sprintf("%x", md5.Sum([]byte(serviceName+":database"))) // nolint:gosec

	database.Variables.KmsKeyAlias = createKmsKeyAliasName(serviceName)

	database.Variables.AvailabilityZone = "ap-northeast-1a"

	database.Variables.DBSubnetGroupName = fmt.Sprintf("%s-rds-db-subnet-group", serviceName)

	database.Variables.ParameterGroupName = fmt.Sprintf("%s-rds-parameter-group", serviceName)
	database.Variables.ParameterGroupFamily = "postgres12"

	database.Variables.OptionGroupName = fmt.Sprintf("%s-rds-option-group", serviceName)
	database.Variables.OptionGroupEngineName = "postgres"
	database.Variables.OptionGroupMajorEngineVersion = 12

	database.Variables.Tags = map[string]string{"Owner": serviceName, "Environment": "runtime", "RuntimeEnvironment": runtimeName, "ManagedBy": "epona"}

	return database, nil
}

func NewRedis(serviceName string, runtimeName string, users []string, deliveryCredential *pattern.AwsCredential, runtimeCredential *pattern.AwsCredential, parameters *Parameters) (*pattern.Redis, error) {
	redis := pattern.NewRedis()

	redis.Name = "redis"
	redis.EnvironmentName = runtimeName
	redis.EnvironmentDirectory = filepath.Join("runtimes", runtimeName)

	redis.DeliveryAwsCredential = deliveryCredential
	redis.TargetAwsCredential = runtimeCredential

	variables := &pattern.RedisVariables{}
	redis.Variables = variables

	redis.Variables.ReplicationGroupID = fmt.Sprintf("%s-redis-group", serviceName)
	redis.Variables.ReplicationGroupDescription = fmt.Sprintf("%s Redis Cluster", lang.Title(serviceName))

	redis.Variables.ClusterModeEnabled = false
	redis.Variables.NumberCacheClusters = 1

	// initial password
	redis.Variables.AuthToken = fmt.Sprintf("%x", md5.Sum([]byte(serviceName+":redis"))) // nolint:gosec

	redis.Variables.KmsKeyAlias = createKmsKeyAliasName(serviceName)

	redis.Variables.SnapshotWindow = "00:00-01:00"
	redis.Variables.SnapshotRetentionLimit = 7
	redis.Variables.MaintenanceWindow = "thu:15:00-thu:16:00"

	redis.Variables.EngineVersion = "5.0.6"
	redis.Variables.NodeType = "cache.t3.medium"

	redis.Variables.AutomaticFailoverEnabled = false

	redis.Variables.ApplyImmediately = false

	redis.Variables.Family = "redis5.0"

	redis.Variables.Tags = map[string]string{"Owner": serviceName, "Environment": "runtime", "RuntimeEnvironment": runtimeName, "ManagedBy": "epona"}

	return redis, nil
}

func newPublicTrafficContainerService(serviceName string, runtimeName string, users []string, deliveryCredential *pattern.AwsCredential, runtimeCredential *pattern.AwsCredential, parameters *Parameters) (*pattern.PublicTrafficContainerService, error) {
	publicTrafficContainerService := pattern.NewPublicTrafficContainerService()

	publicTrafficContainerService.EnvironmentName = runtimeName
	publicTrafficContainerService.EnvironmentDirectory = filepath.Join("runtimes", runtimeName)

	publicTrafficContainerService.DeliveryAwsCredential = deliveryCredential
	publicTrafficContainerService.TargetAwsCredential = runtimeCredential

	variables := &pattern.PublicTrafficContainerServiceVariables{}
	publicTrafficContainerService.Variables = variables

	publicTrafficContainerService.Variables.PublicTrafficProtocol = "HTTPS"
	publicTrafficContainerService.Variables.PublicTrafficPort = 443
	publicTrafficContainerService.Variables.PublicTrafficInboundCidrBlocks = parameters.PublicAlbInboundCidrBlocks
	publicTrafficContainerService.Variables.DNS = map[string]string{}
	publicTrafficContainerService.Variables.DNS["zone_name"] = parameters.Route53HostZone

	publicTrafficContainerService.Variables.ContainerProtocol = "HTTP"
	publicTrafficContainerService.Variables.ContainerPort = 80

	publicTrafficContainerService.Variables.ContainerHealthCheckPath = "/api/health"

	// Networkへの依存関係（container_traffic_inbound_cidr_blocks）は、TerraformExecutorに解決してもらう

	publicTrafficContainerService.Variables.ContainerServiceDesiredCount = 3
	publicTrafficContainerService.Variables.ContainerServicePlatformVersion = "1.4.0"
	publicTrafficContainerService.Variables.ContainerTaskCPU = 512
	publicTrafficContainerService.Variables.ContainerTaskMemory = 1024

	publicTrafficContainerService.Variables.Tags = map[string]string{"Owner": serviceName, "Environment": "runtime", "RuntimeEnvironment": runtimeName, "ManagedBy": "epona"}

	return publicTrafficContainerService, nil
}

func NewPublicTrafficContainerServiceBackend(serviceName string, runtimeName string, users []string, deliveryCredential *pattern.AwsCredential, runtimeCredential *pattern.AwsCredential, parameters *Parameters) (*pattern.PublicTrafficContainerService, error) {
	publicTrafficContainerService, err := newPublicTrafficContainerService(serviceName, runtimeName, users, deliveryCredential, runtimeCredential, parameters)

	if err != nil {
		return publicTrafficContainerService, err
	}

	publicTrafficContainerService.Name = "public_traffic_container_service_backend"

	albSuffix := "-chat-example-backend"
	name := serviceName + albSuffix
	if len(name) > 32 {
		name = fmt.Sprintf("%s%s", name[0:32-len(albSuffix)], albSuffix)
	}
	publicTrafficContainerService.Variables.Name = name

	publicTrafficContainerService.Variables.DNS["record_name"] = createPublicTrafficContainerServiceRecordName("chat-example-backend." + parameters.Route53HostZone)

	publicTrafficContainerService.Variables.ContainerClusterName = fmt.Sprintf("%s-chat-example-backend", serviceName)

	publicTrafficContainerService.Variables.DefaultEcsTaskIamRoleName = fmt.Sprintf("%sBackendContainerServiceTaskRole", lang.Title(serviceName))
	publicTrafficContainerService.Variables.DefaultEcsTaskIamPolicyName = fmt.Sprintf("%sBackendContainerServiceTaskRolePolicy", lang.Title(serviceName))
	publicTrafficContainerService.Variables.DefaultEcsTaskExecutionIamRoleName = fmt.Sprintf("%sBackendContainerServiceTaskExecutionRole", lang.Title(serviceName))
	publicTrafficContainerService.Variables.DefaultEcsTaskExecutionIamPolicyName = fmt.Sprintf("%sBackendContainerServiceTaskExecutionRolePolicy", lang.Title(serviceName))

	publicTrafficContainerService.Variables.ContainerLogGroupNames = []string{
		fmt.Sprintf("/%s/fargate/%s-chat-example-backend/chat-example-backend", serviceName, serviceName),
		fmt.Sprintf("/%s/fargate/%s-chat-example-backend/fluentd", serviceName, serviceName),
		fmt.Sprintf("/%s/fargate/%s-chat-example-backend/reverse-proxy", serviceName, serviceName),
	}

	publicTrafficContainerService.Variables.ContainerDefinitions = fmt.Sprintf(`  [
    {
      "name": "%s-task",
      "image": "hashicorp/http-echo:0.2.3",
      "essential": true,
      "portMappings": [
        {
          "protocol": "tcp",
          "containerPort": %d
        }
      ],
      "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-group": "%s",
          "awslogs-region": "ap-northeast-1",
          "awslogs-stream-prefix": "http-echo"
        }
      },
      "command": [
          "-listen",
          ":%d",
          "-text",
          "echo"
      ]
    }
  ]`,
		publicTrafficContainerService.Variables.ContainerClusterName,
		publicTrafficContainerService.Variables.ContainerPort,
		publicTrafficContainerService.Variables.ContainerLogGroupNames[0],
		publicTrafficContainerService.Variables.ContainerPort,
	)

	return publicTrafficContainerService, nil
}

func NewPublicTrafficContainerServiceNotifier(serviceName string, runtimeName string, users []string, deliveryCredential *pattern.AwsCredential, runtimeCredential *pattern.AwsCredential, parameters *Parameters) (*pattern.PublicTrafficContainerService, error) {
	publicTrafficContainerService, err := newPublicTrafficContainerService(serviceName, runtimeName, users, deliveryCredential, runtimeCredential, parameters)

	if err != nil {
		return publicTrafficContainerService, err
	}

	publicTrafficContainerService.Name = "public_traffic_container_service_notifier"

	albSuffix := "-chat-example-notifier"
	name := serviceName + albSuffix
	if len(name) > 32 {
		name = fmt.Sprintf("%s%s", name[0:32-len(albSuffix)], albSuffix)
	}

	publicTrafficContainerService.Variables.Name = name

	publicTrafficContainerService.Variables.DNS["record_name"] = createPublicTrafficContainerServiceRecordName("chat-example-notifier." + parameters.Route53HostZone)

	publicTrafficContainerService.Variables.ContainerClusterName = fmt.Sprintf("%s-chat-example-notifier", serviceName)

	publicTrafficContainerService.Variables.DefaultEcsTaskIamRoleName = fmt.Sprintf("%sNotifierContainerServiceTaskRole", lang.Title(serviceName))
	publicTrafficContainerService.Variables.DefaultEcsTaskIamPolicyName = fmt.Sprintf("%sNotifierContainerServiceTaskRolePolicy", lang.Title(serviceName))
	publicTrafficContainerService.Variables.DefaultEcsTaskExecutionIamRoleName = fmt.Sprintf("%sNotifierContainerServiceTaskExecutionRole", lang.Title(serviceName))
	publicTrafficContainerService.Variables.DefaultEcsTaskExecutionIamPolicyName = fmt.Sprintf("%sNotifierContainerServiceTaskExecutionRolePolicy", lang.Title(serviceName))

	publicTrafficContainerService.Variables.ContainerLogGroupNames = []string{
		fmt.Sprintf("/%s/fargate/%s-chat-example-notifier/chat-example-notifier", serviceName, serviceName),
		fmt.Sprintf("/%s/fargate/%s-chat-example-notifier/fluentd", serviceName, serviceName),
		fmt.Sprintf("/%s/fargate/%s-chat-example-notifier/reverse-proxy", serviceName, serviceName),
	}

	publicTrafficContainerService.Variables.ContainerDefinitions = fmt.Sprintf(`  [
    {
      "name": "%s-task",
      "image": "hashicorp/http-echo:0.2.3",
      "essential": true,
      "portMappings": [
        {
          "protocol": "tcp",
          "containerPort": %d
        }
      ],
      "logConfiguration": {
        "logDriver": "awslogs",
        "options": {
          "awslogs-group": "%s",
          "awslogs-region": "ap-northeast-1",
          "awslogs-stream-prefix": "http-echo"
        }
      },
      "command": [
          "-listen",
          ":%d",
          "-text",
          "echo"
      ]
    }
  ]`,
		publicTrafficContainerService.Variables.ContainerClusterName,
		publicTrafficContainerService.Variables.ContainerPort,
		publicTrafficContainerService.Variables.ContainerLogGroupNames[0],
		publicTrafficContainerService.Variables.ContainerPort,
	)

	return publicTrafficContainerService, nil
}

func createPublicTrafficContainerServiceRecordName(baseName string) string {
	prefix := os.Getenv("EPONA_STARTER_PREFIX")

	if prefix != "" {
		return prefix + "-" + baseName
	} else {
		return baseName
	}
}

func NewWebaclFrontend(serviceName string, runtimeName string, users []string, deliveryCredential *pattern.AwsCredential, runtimeCredential *pattern.AwsCredential, parameters *Parameters) (*pattern.Webacl, error) {
	webacl := pattern.NewWebacl()

	webacl.Name = "webacl_frontend"
	webacl.EnvironmentName = runtimeName
	webacl.EnvironmentDirectory = filepath.Join("runtimes", runtimeName)
	webacl.DeliveryAwsCredential = deliveryCredential
	webacl.TargetAwsCredential = runtimeCredential

	webacl.Variables = &pattern.WebaclVariables{}
	webacl.Variables.DefaultAction = "allow"
	webacl.Variables.Name = fmt.Sprintf("%s-frontend-webacl", strings.ReplaceAll(serviceName, "_", "-"))
	webacl.Variables.Scope = "CLOUDFRONT"
	webacl.Variables.CreateLoggingBucket = true
	webacl.Variables.LoggingBucketName = fmt.Sprintf("%s-%s-frontend-acl-firehose-bucket", strings.ReplaceAll(serviceName, "_", "-"), runtimeName)
	webacl.Variables.LoggingPrefix = "frontend_waf_log/"
	webacl.Variables.LoggingCompressionFormat = "GZIP"
	webacl.Variables.CloudWatchMetricsEnabled = true
	webacl.Variables.SampledRequestsEnabled = true
	webacl.Variables.Tags = map[string]string{"Owner": serviceName, "Environment": "runtime", "RuntimeEnvironment": runtimeName, "ManagedBy": "epona"}

	return webacl, nil
}

func NewCacheableFrontend(serviceName string, runtimeName string, users []string, deliveryCredential *pattern.AwsCredential, runtimeCredential *pattern.AwsCredential, parameters *Parameters) (*pattern.CacheableFrontend, error) {
	c := pattern.NewCacheableFrontend()

	c.Name = "cacheable_frontend"
	c.EnvironmentName = runtimeName
	c.EnvironmentDirectory = filepath.Join("runtimes", runtimeName)
	c.DeliveryAwsCredential = deliveryCredential
	c.TargetAwsCredential = runtimeCredential

	c.Variables = &pattern.CacheableFrontendVariables{}
	c.Variables.S3FrontendBucketName = fmt.Sprintf("%s-%s-frontend-static", strings.ReplaceAll(serviceName, "_", "-"), runtimeName)
	c.Variables.ZoneName = parameters.Route53HostZone
	c.Variables.RecordName = createCacheableFrontendRecordName(serviceName, parameters)
	c.Variables.DefaultRootObject = "index.html"
	c.Variables.OriginPath = "/public"
	c.Variables.LoggingBucketName = fmt.Sprintf("%s-%s-frontend-logging", strings.ReplaceAll(serviceName, "_", "-"), runtimeName)
	c.Variables.CreateLoggingBucket = true
	c.Variables.LoggingBucketPrefix = fmt.Sprintf("%s/", strings.ReplaceAll(serviceName, "_", "-"))
	c.Variables.IncludeCookieLogging = false
	c.Variables.ViewerResponseLambdaFunction = fmt.Sprintf("%s-frontend-add-header-function", strings.ReplaceAll(serviceName, "_", "-"))
	c.Variables.Tags = map[string]string{"Owner": serviceName, "Environment": "runtime", "RuntimeEnvironment": runtimeName, "ManagedBy": "epona"}

	return c, nil
}

func createCacheableFrontendRecordName(serviceName string, parameters *Parameters) string {
	return fmt.Sprintf("%s.%s", serviceName, parameters.Route53HostZone)
}

func NewSesSMTPUser(serviceName string, runtimeName string, users []string, deliveryCredential *pattern.AwsCredential, runtimeCredential *pattern.AwsCredential, parameters *Parameters) (*pattern.SesSMTPUser, error) {
	sesSMTPUser := pattern.NewSesSMTPUser()

	sesSMTPUser.Name = "ses_smtp_user"
	sesSMTPUser.EnvironmentName = runtimeName
	sesSMTPUser.EnvironmentDirectory = filepath.Join("runtimes", runtimeName)

	sesSMTPUser.DeliveryAwsCredential = deliveryCredential
	sesSMTPUser.TargetAwsCredential = runtimeCredential

	variables := &pattern.SesSMTPUserVariables{}
	sesSMTPUser.Variables = variables

	sesSMTPUser.Variables.Username = fmt.Sprintf("%sChatExampleSesSmtpUser", lang.Title(serviceName))
	sesSMTPUser.Variables.PolicyName = fmt.Sprintf("%sChatExampleSesSmtpPolicy", lang.Title(serviceName))

	sesSMTPUser.Variables.Tags = map[string]string{"Owner": serviceName, "Environment": "runtime", "RuntimeEnvironment": runtimeName, "ManagedBy": "epona"}

	return sesSMTPUser, nil
}

func NewParameterStore(serviceName string, runtimeName string, users []string, deliveryCredential *pattern.AwsCredential, runtimeCredential *pattern.AwsCredential, parameters *Parameters) (*pattern.ParameterStore, error) {
	parameterStore := pattern.NewParameterStore()

	parameterStore.Name = "parameter_store"
	parameterStore.EnvironmentName = runtimeName
	parameterStore.EnvironmentDirectory = filepath.Join("runtimes", runtimeName)

	parameterStore.DeliveryAwsCredential = deliveryCredential
	parameterStore.TargetAwsCredential = runtimeCredential

	variables := &pattern.ParameterStoreVariables{}
	parameterStore.Variables = variables

	parameterStore.Variables.KmsKeyAlias = createKmsKeyAliasName(serviceName)

	parameterStore.Variables.Tags = map[string]string{"Owner": serviceName, "Environment": "runtime", "RuntimeEnvironment": runtimeName, "ManagedBy": "epona"}

	parameterStore.Variables.Parameters = []map[string]map[string]string{
		{
			"nablarch_db_url": {
				"name":          fmt.Sprintf("/%s/App/Config/nablarch_db_url", lang.Title(serviceName)),
				"type":          "SecureString",
				"terraformType": "string",
				"value":         "[dummy]", // => Database callback
			},
		},
		{
			"nablarch_db_user": {
				"name":          fmt.Sprintf("/%s/App/Config/nablarch_db_user", lang.Title(serviceName)),
				"type":          "SecureString",
				"terraformType": "string",
				"value":         "[dummy]", // => Database callback
			},
		},
		{
			"nablarch_db_password": {
				"name":          fmt.Sprintf("/%s/App/Config/nablarch_db_password", lang.Title(serviceName)),
				"type":          "SecureString",
				"terraformType": "string",
				"value":         "", // => Database callback
			},
		},
		{
			"nablarch_db_schema": {
				"name":          fmt.Sprintf("/%s/App/Config/nablarch_db_schema", lang.Title(serviceName)),
				"type":          "SecureString",
				"terraformType": "string",
				"value":         "public",
			},
		},
		{
			"websocket_url": {
				"name":          fmt.Sprintf("/%s/App/Config/websocket_url", lang.Title(serviceName)),
				"type":          "SecureString",
				"terraformType": "string",
				"value":         fmt.Sprintf("wss://%s/notification", createPublicTrafficContainerServiceRecordName("chat-example-notifier."+parameters.Route53HostZone)),
			},
		},
		{
			"mail_smtp_host": {
				"name":          fmt.Sprintf("/%s/App/Config/mail_smtp_host", lang.Title(serviceName)),
				"type":          "SecureString",
				"terraformType": "string",
				"value":         "email-smtp.ap-northeast-1.amazonaws.com",
			},
		},
		{
			"mail_smtp_port": {
				"name":          fmt.Sprintf("/%s/App/Config/mail_smtp_port", lang.Title(serviceName)),
				"type":          "SecureString",
				"terraformType": "number",
				"value":         "587",
			},
		},
		{
			"mail_smtp_user": {
				"name":          fmt.Sprintf("/%s/App/Config/mail_smtp_user", lang.Title(serviceName)),
				"type":          "SecureString",
				"terraformType": "string",
				"value":         "[dummy]", // => SMTPSesUser callback
			},
		},
		{
			"mail_smtp_password": {
				"name":          fmt.Sprintf("/%s/App/Config/mail_smtp_password", lang.Title(serviceName)),
				"type":          "SecureString",
				"terraformType": "string",
				"value":         "[dummy]", // => SMTPSesUser callback
			},
		},
		{
			"mail_from_address": {
				"name":          fmt.Sprintf("/%s/App/Config/mail_from_address", lang.Title(serviceName)),
				"type":          "SecureString",
				"terraformType": "string",
				"value":         parameters.SesVerifiedMailAddress,
			},
		},
		{
			"mail_returnpath": {
				"name":          fmt.Sprintf("/%s/App/Config/mail_returnpath", lang.Title(serviceName)),
				"type":          "SecureString",
				"terraformType": "string",
				"value":         parameters.SesVerifiedMailAddress,
			},
		},
		{
			"application_external_url": {
				"name":          fmt.Sprintf("/%s/App/Config/application_external_url", lang.Title(serviceName)),
				"type":          "SecureString",
				"terraformType": "string",
				"value":         fmt.Sprintf("https://%s", createCacheableFrontendRecordName(serviceName, parameters)),
			},
		},
		{
			"cors_origins": {
				"name":          fmt.Sprintf("/%s/App/Config/cors_origins", lang.Title(serviceName)),
				"type":          "SecureString",
				"terraformType": "string",
				"value":         fmt.Sprintf("https://%s", createCacheableFrontendRecordName(serviceName, parameters)),
			},
		},
		{
			"nablarch_lettuce_simple_url": {
				"name":          fmt.Sprintf("/%s/App/Config/nablarch_lettuce_simple_url", lang.Title(serviceName)),
				"type":          "SecureString",
				"terraformType": "string",
				"value":         "[dummy]", //  => Redis callback
			},
		},
		{
			"nablarch_sessionstorehandler_cookiesecure": {
				"name":          fmt.Sprintf("/%s/App/Config/nablarch_sessionstorehandler_cookiesecure", lang.Title(serviceName)),
				"type":          "SecureString",
				"terraformType": "bool",
				"value":         "true",
			},
		},
	}

	addParameterStoreDatabaseOutputCallback(parameterStore)
	addParameterStoreRedisOutputCallback(parameterStore)
	addParameterStoreSesSMTPUserOutputCallback(parameterStore)

	return parameterStore, nil
}

func addParameterStoreDatabaseOutputCallback(parameterStore *pattern.ParameterStore) {
	parameterStore.AddOutputCallback(func(p pattern.PatternInstance) {
		if database, ok := p.(*pattern.Database); ok {
			for _, parameters := range parameterStore.Variables.Parameters {
				for name, entry := range parameters {
					switch name {
					case "nablarch_db_url":
						// for dry-run
						if database.Output != nil {
							name := database.Name

							instanceOutput, _ := database.Output[name].Expr.Value(nil)
							endpoint := instanceOutput.AsValueMap()["instance_endpoint"].AsString()
							databaseName := database.Variables.Name

							entry["value"] = fmt.Sprintf("jdbc:postgresql://%s/%s", endpoint, databaseName)
						}
					case "nablarch_db_user":
						username := database.Variables.Username
						entry["value"] = username
					case "nablarch_db_password":
						password := database.Variables.Password
						entry["value"] = password
					}
				}
			}
		}
	})
}

func addParameterStoreRedisOutputCallback(parameterStore *pattern.ParameterStore) {
	parameterStore.AddOutputCallback(func(p pattern.PatternInstance) {
		if redis, ok := p.(*pattern.Redis); ok {
			for _, parameters := range parameterStore.Variables.Parameters {
				for name, entry := range parameters {
					if name == "nablarch_lettuce_simple_url" {
						// for dry-run
						if redis.Output != nil {
							name := redis.Name
							instanceOutput, _ := redis.Output[name].Expr.Value(nil)

							authToken := redis.Variables.AuthToken
							endpoint := instanceOutput.AsValueMap()["elasticache_redis_primary_endpoint_address"].AsString()

							entry["value"] = fmt.Sprintf("rediss://%s@%s:6379", authToken, endpoint)
						}
					}
				}
			}
		}
	})
}

func addParameterStoreSesSMTPUserOutputCallback(parameterStore *pattern.ParameterStore) {
	parameterStore.AddOutputCallback(func(p pattern.PatternInstance) {
		if sesSMTPUser, ok := p.(*pattern.SesSMTPUser); ok {
			for _, parameters := range parameterStore.Variables.Parameters {
				for name, entry := range parameters {
					switch name {
					case "mail_smtp_user":
						// for dry-run
						if sesSMTPUser.Output != nil {
							name := sesSMTPUser.Name
							instanceOutput, _ := sesSMTPUser.Output[name].Expr.Value(nil)

							accessKeyID := instanceOutput.AsValueMap()["access_key_id"].AsString()

							entry["value"] = accessKeyID
						}
					case "mail_smtp_password":
						// for dry-run
						if sesSMTPUser.Output != nil {
							name := sesSMTPUser.Name
							instanceOutput, _ := sesSMTPUser.Output[name].Expr.Value(nil)

							smtpPassword := instanceOutput.AsValueMap()["smtp_password_v4"].AsString()

							entry["value"] = smtpPassword
						}
					}
				}
			}
		}
	})
}

// NewCdPipelineBackend は、 CdPipelineBackend パターンのインスタンスを返却する
func NewCdPipelineBackend(serviceName, runtimeName, appType string, users []string, deliveryCredential *pattern.AwsCredential, runtimeCredential *pattern.AwsCredential, parameters *Parameters) (*pattern.CdPipelineBackend, error) {
	cdb := pattern.NewCdPipelineBackend()

	shortenBackendAppType, err := convertShortenBackendAppType(appType)
	if err != nil {
		return cdb, err
	}
	taskdefSecrets, err := getTaskdefSecrets(appType, serviceName)
	if err != nil {
		return cdb, err
	}

	variables := pattern.NewCdPipelineBackendVariables(
		// パイプライン名は、 `[runtime環境名の頭7文字]-chat-[back|ntfr]` で定義
		// EventBridgeのルール名の文字制限により、固定文字列以外で7文字にする必要があったためruntime環境名を7文字で切り出し
		fmt.Sprintf("%s-chat-%s", strings.TrimSuffix(subStrLeft(runtimeName, 7), "-"), shortenBackendAppType),
		map[string]string{
			"Owner":              serviceName,
			"Environment":        "runtime",
			"RuntimeEnvironment": runtimeName,
			"ManagedBy":          "epona",
		},
		[]map[string]string{
			{
				getChatAppEcrRepoName(appType): runtimeName,
			},
			{
				getNginxEcrRepoName(appType): runtimeName,
			},
		},
		// 設定ファイルを配置するS3バケットは `[serviceName]-[runtime環境名の頭7文字]-chat-[back|ntfr]-pipeline-source` で定義
		// `[runtime環境名の頭7文字]-chat-[back|ntfr]`はパイプライン名に合わせて設定
		fmt.Sprintf("%s-%s-chat-%s-pipeline-source", serviceName, strings.TrimSuffix(subStrLeft(runtimeName, 7), "-"), shortenBackendAppType),
		// アーティファクトストアのS3バケットは `[serviceNameの頭5文字]-[runtime環境名の頭5文字]-[back|ntfr]-artfct` で定義
		// EventBridgeのルール名の文字制限により、固定文字列以外で10文字にする必要があったためserviceNameとruntime環境名を5文字で切り出し
		fmt.Sprintf("%s-%s-%s-artfct", strings.TrimSuffix(subStrLeft(serviceName, 5), "-"), strings.TrimSuffix(subStrLeft(runtimeName, 5), "-"), shortenBackendAppType),
		// CodeDeploy の AppName は `[serviceName]-[backend|notifier]` で定義
		fmt.Sprintf("%s-%s", serviceName, appType),
		// CodeDeploy の DeployGroup 名は `[serviceName]-[backend|notifier]-group` で定義
		fmt.Sprintf("%s-%s-group", serviceName, appType),
		false,
		false,
		taskdefSecrets,
		appType,
		"dummy", // => CdPipelineBackendTriggerBackend callback
	)

	cdb.SetupInstance(
		fmt.Sprintf("cd_pipeline_backend_%s", appType),
		runtimeName,
		deliveryCredential,
		runtimeCredential,
		variables,
	)

	// CdPipelineBackendTriggerのOutputを紐付けるコールバック
	addCdPipelineBackendTriggerOutputToCdPipelineBackendCallback(cdb)

	return cdb, nil
}

// addCdPipelineBackendTriggerOutputToCdPipelineBackendCallback は、 CdPipelineBackend に対応する Trigger パターンインスタンスの Output を注入する
func addCdPipelineBackendTriggerOutputToCdPipelineBackendCallback(cdPipelineBackend *pattern.CdPipelineBackend) {
	cdPipelineBackend.AddOutputCallback(func(p pattern.PatternInstance) {
		if cdPipelineBackendTrigger, ok := p.(*pattern.CdPipelineBackendTrigger); ok {
			if cdPipelineBackendTrigger.Output != nil {
				name := cdPipelineBackendTrigger.Name
				instanceOutput, _ := cdPipelineBackendTrigger.Output[name].Expr.Value(nil)

				crossAccessRoleArn := instanceOutput.AsValueMap()["cross_account_access_role_arn"].AsString()

				cdPipelineBackend.Variables.CrossAcountAccessRoleArn = crossAccessRoleArn
			}
		} else {
			logging.Fatalf("[Bug]%sにパイプラインの構築で必要なコールバックが設定できませんでした。コールバック関数を適切に設定してください。", cdPipelineBackend.Name)
		}
	})
}

// getChatAppEcrRepoName は example-chat の EcrRepository 名を返却する
// レポジトリ名は、 `([prefixの頭8文字]-)chat-example-[backend|notifier]` となる
// EventBridgeのルール名の文字制限により、固定文字列以外で9文字にする必要があったためprefixは8文字で切り出し
// FIXME: delivery, runtimeで同一関数の定義がされているため、apply packageなどにnaming.goなどを作成して移植する
func getChatAppEcrRepoName(appType string) string {
	if prefix := os.Getenv("EPONA_STARTER_PREFIX"); prefix == "" {
		return fmt.Sprintf("chat-example-%s", appType)
	} else {
		return fmt.Sprintf("%s-chat-example-%s", strings.TrimSuffix(subStrLeft(prefix, 8), "-"), appType)
	}
}

// getNginxEcrRepoName は バックエンドシステムの前段に置かれる Nginx の EcrRepository 名を返却する
// レポジトリ名は、 `([prefixの頭8文字]-)[backend|notifier]-nginx` となる
// EventBridgeのルール名の文字制限により、固定文字列以外で9文字にする必要があったためprefixは8文字で切り出し
// FIXME: delivery, runtimeで同一関数の定義がされているため、apply packageなどにnaming.goなどを作成して移植する
func getNginxEcrRepoName(appType string) string {
	if prefix := os.Getenv("EPONA_STARTER_PREFIX"); prefix == "" {
		return fmt.Sprintf("%s-nginx", appType)
	} else {
		return fmt.Sprintf("%s-%s-nginx", strings.TrimSuffix(subStrLeft(prefix, 8), "-"), appType)
	}
}

// subStrLeft は、 文字列の前半から length 文字を切りとった文字列を返却する
// FIXME: delivery, runtimeで同一関数の定義がされているため、apply packageなどにnaming.goなどを作成して移植する
func subStrLeft(str string, length int) string {
	if len(str) > length {
		return str[0:length]
	}
	return str
}

// convertShortenBackendAppTypeは、バックエンドのアプリケーション種別を短縮した文字列を返却する
// FIXME: delivery, runtimeで同一関数の定義がされているため、apply packageなどにnaming.goなどを作成して移植する
func convertShortenBackendAppType(appType string) (shorten string, err error) {
	switch appType {
	case "backend":
		shorten = "back"
	case "notifier":
		shorten = "ntfr"
	default:
		err = fmt.Errorf("未定義のバックエンド種別が指定されました。: appType=%s", appType)
	}

	return
}

// getTaskdefSecrets は、 appType に応じた taskdef で利用する secrets 情報を返却する
func getTaskdefSecrets(appType, serviceName string) (taskdefSecrets string, err error) {
	switch appType {
	case "backend":
		taskdefSecrets = strings.ReplaceAll(`
        {
          "name": "NABLARCH_DB_URL",
          "valueFrom": "/%s/App/Config/nablarch_db_url"
        },
        {
          "name": "NABLARCH_DB_USER",
          "valueFrom": "/%s/App/Config/nablarch_db_user"
        },
        {
          "name": "NABLARCH_DB_PASSWORD",
          "valueFrom": "/%s/App/Config/nablarch_db_password"
        },
        {
          "name": "NABLARCH_DB_SCHEMA",
          "valueFrom": "/%s/App/Config/nablarch_db_schema"
        },
        {
          "name": "WEBSOCKET_URI",
          "valueFrom": "/%s/App/Config/websocket_url"
        },
        {
          "name": "MAIL_SMTP_HOST",
          "valueFrom": "/%s/App/Config/mail_smtp_host"
        },
        {
          "name": "MAIL_SMTP_PORT",
          "valueFrom": "/%s/App/Config/mail_smtp_port"
        },
        {
          "name": "MAIL_SMTP_USER",
          "valueFrom": "/%s/App/Config/mail_smtp_user"
        },
        {
          "name": "MAIL_SMTP_PASSWORD",
          "valueFrom": "/%s/App/Config/mail_smtp_password"
        },
        {
          "name": "MAIL_FROM_ADDRESS",
          "valueFrom": "/%s/App/Config/mail_from_address"
        },
        {
          "name": "MAIL_RETURNPATH",
          "valueFrom": "/%s/App/Config/mail_returnpath"
        },
        {
          "name": "APPLICATION_EXTERNAL_URL",
          "valueFrom": "/%s/App/Config/application_external_url"
        },
        {
          "name": "CORS_ORIGINS",
          "valueFrom": "/%s/App/Config/cors_origins"
        },
        {
          "name": "NABLARCH_SESSIONSTOREHANDLER_COOKIESECURE",
          "valueFrom": "/%s/App/Config/nablarch_sessionstorehandler_cookiesecure"
        },
        {
          "name": "NABLARCH_LETTUCE_SIMPLE_URI",
          "valueFrom": "/%s/App/Config/nablarch_lettuce_simple_url"
        }`, "%s", lang.Title(serviceName))
	case "notifier":
		taskdefSecrets = strings.ReplaceAll(`
        {
          "name": "NABLARCH_LETTUCE_SIMPLE_URI",
          "valueFrom": "/%s/App/Config/nablarch_lettuce_simple_url"
        }`, "%s", lang.Title(serviceName))
	default:
		err = fmt.Errorf("未定義のバックエンド種別が指定されました。: appType=%s", appType)
	}

	return
}

// NewCdPipelineFrontend は、 CdPipelineFrontend パターンインスタンスを返却する
func NewCdPipelineFrontend(serviceName, runtimeName string, users []string, deliveryCredential, runtimeCredential *pattern.AwsCredential, parameters *Parameters) (*pattern.CdPipelineFrontend, error) {
	cdf := pattern.NewCdPipelineFrontend()

	variables := pattern.NewCdPipelineFrontendVariables(
		map[string]string{
			"Owner":              serviceName,
			"Environment":        "runtime",
			"RuntimeEnvironment": runtimeName,
			"ManagedBy":          "epona",
		},
		// パイプライン名は、 `[runtime環境名の頭7文字]-chat-front` で定義
		fmt.Sprintf("%s-chat-front", strings.TrimSuffix(subStrLeft(runtimeName, 7), "-")),
		getStaticResourceBucketName(serviceName, runtimeName), // frontendのビルド資材を格納するバケット名
		"source.zip",
		// アーティファクトストアのS3バケットは `[serviceName]-[runtime環境名]-frontend-artfct` で定義
		fmt.Sprintf("%s-%s-frontend-artfct", serviceName, runtimeName),
		false,
		false,
		true,
		"dummy", // => CdPipelineFrontendTrigger callback
	)

	cdf.SetupInstance(
		"cd_pipeline_frontend",
		runtimeName,
		deliveryCredential,
		runtimeCredential,
		variables,
	)

	// CdPipelineFrontendTriggerのOutputを紐付けるコールバック
	addCdPipelineFrontendTriggerOutputToCdPipelineFrontendCallback(cdf)

	return cdf, nil
}

// getStaticResourceBucketName は フロントエンドのビルド済み資材を配置する S3 バケットの名称を返却する
// S3 バケット名は、 `[serviceNameの頭8文字]-[runtime環境名の頭7文字]-static-resource` となる
// EventBridgeのルール名の文字制限により、固定文字列以外で合計15文字にする必要があったためserviceNameを8文字、runtime環境名を7文字で切り出し
// FIXME: delivery, runtimeで同一関数の定義がされているため、apply packageなどにnaming.goなどを作成して移植する
func getStaticResourceBucketName(serviceName, runtimeName string) string {
	return fmt.Sprintf("%s-%s-static-resource", strings.TrimSuffix(subStrLeft(serviceName, 8), "-"), strings.TrimSuffix(subStrLeft(runtimeName, 7), "-"))
}

// addCdPipelineFrontendTriggerOutputToCdPipelineFrontendCallback は、 CdPipelineFrontend に対応する Trigger パターンインスタンスの Output を注入する
func addCdPipelineFrontendTriggerOutputToCdPipelineFrontendCallback(cdPipelineFrontend *pattern.CdPipelineFrontend) {
	cdPipelineFrontend.AddOutputCallback(func(p pattern.PatternInstance) {
		if cdPipelineFrontendTrigger, ok := p.(*pattern.CdPipelineFrontendTrigger); ok {
			if cdPipelineFrontendTrigger.Output != nil {
				name := cdPipelineFrontendTrigger.Name
				instanceOutput, _ := cdPipelineFrontendTrigger.Output[name].Expr.Value(nil)

				crossAccessRoleArn := instanceOutput.AsValueMap()["cross_account_access_role_arn"].AsString()

				cdPipelineFrontend.Variables.CrossAcountAccessRoleArn = crossAccessRoleArn
			}
		} else {
			logging.Fatalf("[Bug]%sにパイプラインの構築で必要なコールバックが設定できませんでした。コールバック関数を適切に設定してください。", cdPipelineFrontend.Name)
		}
	})
}
