package activity

import (
	"gitlab.com/eponas/epona-aws-starter-cli/internal/command/apply/pattern"
)

type Activity interface {
	GetName() string
	GetPatternInstances() []pattern.PatternInstance
}

type ActivityAttributes struct {
	Name             string
	PatternInstances []pattern.PatternInstance
}

func (a *ActivityAttributes) GetName() string {
	return a.Name
}

func (a *ActivityAttributes) GetPatternInstances() []pattern.PatternInstance {
	return a.PatternInstances
}
