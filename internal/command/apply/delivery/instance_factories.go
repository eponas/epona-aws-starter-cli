package delivery

import (
	"fmt"
	"os"
	"strings"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/command/apply/pattern"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/logging"
)

const (
	envName = "delivery"
)

type Parameters struct {
	GitLabRunnerRegistrationToken string
	VpcCidrBlock                  string
}

func NewNetwork(serviceName string, users []string, credential *pattern.AwsCredential, parameters *Parameters) (*pattern.Network, error) {
	network := pattern.NewNetwork()

	network.Name = "network"
	network.EnvironmentName = envName
	network.EnvironmentDirectory = envName

	network.DeliveryAwsCredential = credential
	network.TargetAwsCredential = credential

	variables := &pattern.NetworkVariables{}
	network.Variables = variables

	network.Variables.Name = fmt.Sprintf("%s-delivery-network", serviceName)
	network.Variables.CidrBlock = parameters.VpcCidrBlock
	network.Variables.AvailabilityZones = []string{"ap-northeast-1a", "ap-northeast-1c"}
	network.Variables.PublicSubnets = []string{
		fmt.Sprintf("cidrsubnet(\"%s\", 8, 1)", parameters.VpcCidrBlock),
		fmt.Sprintf("cidrsubnet(\"%s\", 8, 2)", parameters.VpcCidrBlock),
	}
	network.Variables.PrivateSubnets = []string{
		fmt.Sprintf("cidrsubnet(\"%s\", 8, 3)", parameters.VpcCidrBlock),
		fmt.Sprintf("cidrsubnet(\"%s\", 8, 4)", parameters.VpcCidrBlock),
	}
	network.Variables.NatGatewayDeploySubnets = []string{
		fmt.Sprintf("cidrsubnet(\"%s\", 8, 1)", parameters.VpcCidrBlock),
		fmt.Sprintf("cidrsubnet(\"%s\", 8, 2)", parameters.VpcCidrBlock),
	}
	network.Variables.Tags = map[string]string{"Owner": serviceName, "Environment": envName, "ManagedBy": "epona"}

	return network, nil
}

// NewCiPipeline は Delivery 環境用に適用するための CiPipeline インスタンスを返却する
func NewCiPipeline(serviceName, envdir string, users []string, deliveryCredential *pattern.AwsCredential, runtimeCredentials map[string]*pattern.AwsCredential, parameters *Parameters) (*pattern.CiPipeline, error) {
	cp := pattern.NewCiPipeline()

	/** runtime環境の数に応じて可変になるパラメータ定義 **/
	var staticResourceBucketList []map[string]string // 静的コンテンツを格納する S3 Bucket のリスト
	var ecrAllowAccessAccounts []string              // 作成される ECR にアクセスするアカウントIDのリスト
	for rName, rCred := range runtimeCredentials {
		staticResourceBucketList = append(staticResourceBucketList, map[string]string{
			"bucket_name":        getStaticResourceBucketName(serviceName, rName),
			"force_destroy":      "false",
			"runtime_account_id": rCred.ActualEnvironmentAccountID,
		})
		ecrAllowAccessAccounts = append(ecrAllowAccessAccounts, rCred.ActualEnvironmentAccountID)
	}

	cp.SetupInstance(
		"ci_pipeline",
		envName,
		envdir,
		deliveryCredential,
		pattern.NewCiPipelineVariable(
			serviceName,
			map[string]string{
				"Owner":       serviceName,
				"Environment": envName,
				"ManagedBy":   "epona",
			},
			"t3.small",
			"30",
			parameters.GitLabRunnerRegistrationToken,
			staticResourceBucketList,
			[]map[string]string{
				{
					"name":                 getChatAppEcrRepoName("backend"), // example-chat の Backend 用レポジトリ
					"image_tag_mutability": "MUTABLE",
				},
				{
					"name":                 getChatAppEcrRepoName("notifier"), // example-chat の Notifier 用レポジトリ
					"image_tag_mutability": "MUTABLE",
				},
				{
					"name":                 getNginxEcrRepoName("backend"), // nginx の Backend 用レポジトリ
					"image_tag_mutability": "MUTABLE",
				},
				{
					"name":                 getNginxEcrRepoName("notifier"), // nginx の Notifier 用レポジトリ
					"image_tag_mutability": "MUTABLE",
				},
			},
			ecrAllowAccessAccounts,
		),
	)

	return cp, nil
}

// getChatAppEcrRepoName は example-chat の EcrRepository 名を返却する
// レポジトリ名は、 `([prefixの頭8文字]-)chat-example-[backend|notifier]` となる
// EventBridgeのルール名の文字制限により、固定文字列以外で9文字にする必要があったためprefixは8文字で切り出し
// FIXME: delivery, runtimeで同一関数の定義がされているため、apply packageなどにnaming.goなどを作成して移植する
func getChatAppEcrRepoName(appType string) string {
	if prefix := os.Getenv("EPONA_STARTER_PREFIX"); prefix == "" {
		return fmt.Sprintf("chat-example-%s", appType)
	} else {
		return fmt.Sprintf("%s-chat-example-%s", strings.TrimSuffix(subStrLeft(prefix, 8), "-"), appType)
	}
}

// getNginxEcrRepoName は バックエンドシステムの前段に置かれる Nginx の EcrRepository 名を返却する
// レポジトリ名は、 `([prefixの頭8文字]-)[backend|notifier]-nginx` となる
// EventBridgeのルール名の文字制限により、固定文字列以外で9文字にする必要があったためprefixは8文字で切り出し
// FIXME: delivery, runtimeで同一関数の定義がされているため、apply packageなどにnaming.goなどを作成して移植する
func getNginxEcrRepoName(appType string) string {
	if prefix := os.Getenv("EPONA_STARTER_PREFIX"); prefix == "" {
		return fmt.Sprintf("%s-nginx", appType)
	} else {
		return fmt.Sprintf("%s-%s-nginx", strings.TrimSuffix(subStrLeft(prefix, 8), "-"), appType)
	}
}

// getStaticResourceBucketName は フロントエンドのビルド済み資材を配置する S3 バケットの名称を返却する
// S3 バケット名は、 `[serviceNameの頭8文字]-[runtime環境名の頭7文字]-static-resource` となる
// EventBridgeのルール名の文字制限により、固定文字列以外で合計15文字にする必要があったためserviceNameを8文字、runtime環境名を7文字で切り出し
// FIXME: delivery, runtimeで同一関数の定義がされているため、apply packageなどにnaming.goなどを作成して移植する
func getStaticResourceBucketName(serviceName, runtimeName string) string {
	return fmt.Sprintf("%s-%s-static-resource", strings.TrimSuffix(subStrLeft(serviceName, 8), "-"), strings.TrimSuffix(subStrLeft(runtimeName, 7), "-"))
}

// NewCdPipelineBackendTrigger は、 Delivery 環境用に適用するための CdPipelineBackendTrigger インスタンスを返却する
func NewCdPipelineBackendTrigger(serviceName, env, runtimeName, appType string, users []string, deliveryCredential, runtimeCredential *pattern.AwsCredential, parameters *Parameters) (*pattern.CdPipelineBackendTrigger, error) {
	cdbtb := pattern.NewCdPipelineBackendTrigger()

	shortenBackendAppType, err := convertShortenBackendAppType(appType)
	if err != nil {
		return cdbtb, err
	}

	variables := pattern.NewCdPipelineBackendTriggerVariables(
		// パイプライン名は、 `[runtime環境名の頭7文字]-chat-[back|ntfr]` で定義
		// EventBridgeのルール名の文字制限により、固定文字列以外で7文字にする必要があったためruntime環境名を7文字で切り出し
		fmt.Sprintf("%s-chat-%s", strings.TrimSuffix(subStrLeft(runtimeName, 7), "-"), shortenBackendAppType),
		map[string]string{
			"Owner":              serviceName,
			"Environment":        envName,
			"RuntimeEnvironment": runtimeName,
			"ManagedBy":          "epona",
		},
		// 設定ファイルを配置するS3バケットは `[serviceName]-[runtime環境名の頭7文字]-chat-[back|ntfr]-pipeline-source` で定義
		// `[runtime環境名の頭7文字]-chat-[back|ntfr]`はパイプライン名に合わせて設定
		fmt.Sprintf("%s-%s-chat-%s-pipeline-source", serviceName, strings.TrimSuffix(subStrLeft(runtimeName, 7), "-"), shortenBackendAppType),
		false,
		[]map[string]string{
			{
				getChatAppEcrRepoName(appType): runtimeName, // example-chat のレポジトリとデプロイタグの定義
			},
			{
				getNginxEcrRepoName(appType): runtimeName, // nginx のレポジトリとデプロイタグの定義
			},
		},
		"arn:aws:s3:::dummy-bucket", // 初期値としてダミーを設定 => CdPipeline Callback
		fmt.Sprintf("arn:aws:kms:%s:%s:key/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx", runtimeCredential.Region, runtimeCredential.ActualEnvironmentAccountID), // 初期値としてダミーを設定 => CdPipeline Callback
	)

	cdbtb.SetupInstance(
		fmt.Sprintf("cd_pipeline_backend_trigger_%s", appType),
		env,
		runtimeName,
		runtimeCredential.ActualEnvironmentAccountID,
		deliveryCredential,
		variables,
	)

	// CdPipelineBackendのOutputを紐付けるコールバック
	addCdPipelineBackendOutputToCdPipelineBackendTriggerCallback(cdbtb)

	return cdbtb, nil
}

// convertShortenBackendAppTypeは、バックエンドのアプリケーション種別を短縮した文字列を返却する
// FIXME: delivery, runtimeで同一関数の定義がされているため、apply packageなどにnaming.goなどを作成して移植する
func convertShortenBackendAppType(appType string) (shorten string, err error) {
	switch appType {
	case "backend":
		shorten = "back"
	case "notifier":
		shorten = "ntfr"
	default:
		err = fmt.Errorf("未定義のバックエンド種別が指定されました。: appType=%s", appType)
	}

	return
}

// subStrLeft は、 文字列の前半から length 文字を切りとった文字列を返却する
// FIXME: delivery, runtimeで同一関数の定義がされているため、apply packageなどにnaming.goなどを作成して移植する
func subStrLeft(str string, length int) string {
	if len(str) > length {
		return str[0:length]
	}
	return str
}

// addCdPipelineBackendOutputToCdPipelineBackendTriggerCallback は CdPipelineBackend パターンの Output を対応する Trigger パターンの変数として注入するコールバック関数
func addCdPipelineBackendOutputToCdPipelineBackendTriggerCallback(cdPipelineBackendTrigger *pattern.CdPipelineBackendTrigger) {
	cdPipelineBackendTrigger.AddOutputCallback(func(p pattern.PatternInstance) {
		if cdPipelineBackend, ok := p.(*pattern.CdPipelineBackend); ok {
			if cdPipelineBackend.Output != nil {
				name := cdPipelineBackend.Name
				instanceOutput, _ := cdPipelineBackend.Output[name].Expr.Value(nil)
				instanceOutputMap := instanceOutput.AsValueMap()

				artifactStoreBucketArn := instanceOutputMap["artifact_store_bucket_arn"].AsString()
				artifactStoreEncryptionKeyArn := instanceOutputMap["kms_keys"].AsValueMap()["keys"].AsValueMap()[fmt.Sprintf("alias/%s_key", cdPipelineBackend.Variables.ArtifactStoreBucketName)].AsValueMap()["key_arn"].AsString()

				cdPipelineBackendTrigger.Variables.ArtifactStoreBucketArn = artifactStoreBucketArn
				cdPipelineBackendTrigger.Variables.ArtifactStoreEncryptionKeyArn = artifactStoreEncryptionKeyArn
			}
		} else {
			logging.Fatalf("[Bug]%sにパイプラインの構築で必要なコールバックが設定できませんでした。コールバック関数を適切に設定してください。", cdPipelineBackendTrigger.Name)
		}
	})
}

// NewCdPipelineFrontendTrigger は、 Delivery 環境用に適用するための CdPipelineFrontendTrigger インスタンスを返却する
func NewCdPipelineFrontendTrigger(serviceName, env, runtimeName string, users []string, deliveryCredential, runtimeCredential *pattern.AwsCredential, parameters *Parameters) (*pattern.CdPipelineFrontendTrigger, error) {
	cdft := pattern.NewCdPipelineFrontendTrigger()

	variables := pattern.NewCdPipelineFrontendTriggerVariables(
		// パイプライン名は、 `[runtime環境名の頭7文字]-chat-front` で定義
		// EventBridgeのルール名の文字制限により、固定文字列以外で7文字にする必要があったためruntime環境名を7文字で切り出し
		fmt.Sprintf("%s-chat-front", strings.TrimSuffix(subStrLeft(runtimeName, 7), "-")),
		runtimeCredential.ActualEnvironmentAccountID,
		getStaticResourceBucketName(serviceName, runtimeName), // frontendのビルド資材を格納するバケット名
		"arn:aws:s3:::dummy-bucket",                           // 初期値としてダミーを設定 => CdPipeline Callback
		fmt.Sprintf("arn:aws:kms:%s:%s:key/xxxxxxxx-xxxx-xxxx-xxxx-xxxxxxxxxxxx", runtimeCredential.Region, runtimeCredential.ActualEnvironmentAccountID), // 初期値としてダミーを設定 => CdPipeline Callback
	)

	cdft.SetupInstance(
		"cd_pipeline_frontend_trigger",
		env,
		runtimeName,
		runtimeCredential.ActualEnvironmentAccountID,
		deliveryCredential,
		variables,
	)

	// CdPipelineFrontendのOutputを紐付けるコールバック
	addCdPipelineFrontendOutputToCdPipelineFrontendTriggerCallback(cdft)

	return cdft, nil
}

// addCdPipelineFrontendOutputToCdPipelineFrontendTriggerCallback は CdPipelineFrontend パターンの Output を対応する Trigger パターンの変数として注入するコールバック関数
func addCdPipelineFrontendOutputToCdPipelineFrontendTriggerCallback(cdPipelineFrontendTrigger *pattern.CdPipelineFrontendTrigger) {
	cdPipelineFrontendTrigger.AddOutputCallback(func(p pattern.PatternInstance) {
		if cdPipelineFrontend, ok := p.(*pattern.CdPipelineFrontend); ok {
			if cdPipelineFrontend.Output != nil {
				name := cdPipelineFrontend.Name
				instanceOutput, _ := cdPipelineFrontend.Output[name].Expr.Value(nil)
				instanceOutputMap := instanceOutput.AsValueMap()

				artifactStoreBucketArn := instanceOutputMap["artifact_store_arn"].AsString()
				artifactStoreEncryptionKeyArn := instanceOutputMap["kms_keys"].AsValueMap()["keys"].AsValueMap()[fmt.Sprintf("alias/%s_key", cdPipelineFrontend.Variables.ArtifactStoreBucketName)].AsValueMap()["key_arn"].AsString()

				cdPipelineFrontendTrigger.Variables.ArtifactStoreBucketArn = artifactStoreBucketArn
				cdPipelineFrontendTrigger.Variables.ArtifactStoreEncryptionKeyArn = artifactStoreEncryptionKeyArn
			}
		} else {
			logging.Fatalf("[Bug]%sにパイプラインの構築で必要なコールバックが設定できませんでした。コールバック関数を適切に設定してください。", cdPipelineFrontend.Name)
		}
	})
}
