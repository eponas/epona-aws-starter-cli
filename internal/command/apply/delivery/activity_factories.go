package delivery

import (
	"fmt"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/command/apply/activity"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/command/apply/pattern"
)

// NewActivity は、設定ファイルに定義されたDelivery環境用のアクティビティを返却します
func NewActivity(activityName string, serviceName string, users []string, deliveryCredential *pattern.AwsCredential, runtimeCredentials map[string]*pattern.AwsCredential, p *Parameters, runtimeActivities map[string][]string) (activity.Activity, error) {
	switch activityName {
	case "deployment_pipeline":
		d, err := NewDeploymentPipeline(serviceName, users, deliveryCredential, runtimeCredentials, p)
		if err != nil {
			return nil, err
		}

		d.Name = activityName
		return d, nil
	default:
		return nil, fmt.Errorf("未定義のアクティビティ[%s]です", activityName)
	}
}
