package delivery

import (
	"gitlab.com/eponas/epona-aws-starter-cli/internal/command/apply/activity"
)

type DeliveryEnvironment struct {
	Activities []activity.Activity `validate:"dive"`
}
