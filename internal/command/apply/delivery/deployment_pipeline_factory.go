package delivery

import (
	"gitlab.com/eponas/epona-aws-starter-cli/internal/command/apply/activity"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/command/apply/pattern"
)

type DeploymentPipeline struct {
	activity.ActivityAttributes
}

// NewDeploymentPipeline は、必要なパターンインスタンスをもつ DeploymentPipeline のアクティビティを返却する
func NewDeploymentPipeline(serviceName string, users []string, deliveryCredential *pattern.AwsCredential, runtimeCredentials map[string]*pattern.AwsCredential, parameters *Parameters) (*DeploymentPipeline, error) {
	deploymentPipeline := &DeploymentPipeline{}

	network, err := NewNetwork(serviceName, users, deliveryCredential, parameters)
	if err != nil {
		return deploymentPipeline, err
	}
	cp, err := NewCiPipeline(serviceName, "delivery", users, deliveryCredential, runtimeCredentials, parameters)
	if err != nil {
		return deploymentPipeline, err
	}

	/** runtime環境に依存するインスタンスを作成 **/
	var runtimeInstances []pattern.PatternInstance
	for targetRuntimeName, runtimeCredential := range runtimeCredentials {
		// cd_pipeline_backend_trigger
		for _, appType := range []string{"backend", "notifier"} {
			cpbt, err := NewCdPipelineBackendTrigger(serviceName, "delivery", targetRuntimeName, appType, users, deliveryCredential, runtimeCredential, parameters)
			if err != nil {
				return deploymentPipeline, err
			}

			runtimeInstances = append(runtimeInstances, cpbt)
		}

		cdf, err := NewCdPipelineFrontendTrigger(serviceName, "delivery", targetRuntimeName, users, deliveryCredential, runtimeCredential, parameters)
		if err != nil {
			return deploymentPipeline, err
		}
		runtimeInstances = append(runtimeInstances, cdf)
	}

	deploymentPipeline.PatternInstances = []pattern.PatternInstance{network, cp}
	// runtime環境に依存するインスタンスを追加
	deploymentPipeline.PatternInstances = append(deploymentPipeline.PatternInstances, runtimeInstances...)

	return deploymentPipeline, nil
}
