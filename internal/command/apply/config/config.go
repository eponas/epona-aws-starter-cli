package config

import (
	"gitlab.com/eponas/epona-aws-starter-cli/internal/command/apply/delivery"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/command/apply/runtime"
)

type ApplyConfig struct {
	ServiceName         string
	DeliveryEnvironment *delivery.DeliveryEnvironment
	RuntimeEnvironments []*runtime.RuntimeEnvironment
}
