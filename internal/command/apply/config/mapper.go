package config

// ApplyConfigurationFileMapper は apply サブコマンドで読み込む設定ファイルを
// Go で扱えるようにするための Mapper
type ApplyConfigurationFileMapper struct {
	// ServiceName 必須。16文字以内の印字可能ASCII文字で構成。
	ServiceName string `yaml:"service_name" validate:"required,printascii,max=16"`
	// Delivery は Delivery環境用設定。必須。
	Delivery *DeliveryMapper `yaml:"delivery" validate:"required"`
	// Runtimes はRuntime環境用設定。必須。
	Runtimes []*RuntimeMapper `yaml:"runtimes" validate:"required,dive"`
}

// TerraformerAwsCredentialMapper は apply サブコマンド用設定ファイル上の
// `terraformer_aws_credential` キー設定に対応するマッパー
type TerraformerAwsCredentialMapper struct {
	// AccessKeyID は Terraforme 実行ユーザのアクセスキー ID を示す。必須。
	AccessKeyID string `yaml:"access_key_id" validate:"required,printascii"`
	// SecretAccessKey は Terraforme 実行ユーザのシークレットアクセスキーを示す。必須。
	SecretAccessKey string `yaml:"secret_access_key" validate:"required,printascii"`
	// Region は AWS アカウントの対象リージョンを示す。必須。
	Region string `yaml:"region" validate:"required,printascii"`
}

// DeliveryMapper は apply サブコマンド用設定ファイル上の `delivery` キー設定に対応するマッパー
type DeliveryMapper struct {
	// AwsAccountID は AWS アカウント ID を示す。数字 12 桁。https://docs.aws.amazon.com/ja_jp/general/latest/gr/acct-identifiers.html
	AwsAccountID string `yaml:"aws_account_id" validate:"required,number,len=12"`
	// TerraformerAwsCredential は Delivery 環境用の Terraform 実行ユーザのクレデンシャル。必須。
	TerraformerAwsCredential *TerraformerAwsCredentialMapper `yaml:"terraformer_aws_credential" validate:"required"`
	// Parameters は Delivery 環境におけるパラメータ。省略可能
	Parameters *DeliveryParametersMapper `yaml:"parameters"`
	// Activities は Delivery 環境に適用するアクティビティを示す。指定できる値には制限がある。
	Activities []string `yaml:"activities" validate:"required,dive,oneof=deployment_pipeline"`
}

// RuntimeMapper は apply サブコマンド用設定ファイル上の `runtime` キー設定に対応するマッパー
type RuntimeMapper struct {
	// Name は Runtime 環境名を示す。必須。
	Name string `yaml:"name" validate:"required,alphanum,lt=16"`
	// AwsAccountID は AWS アカウント ID を示す。数字 12 桁。https://docs.aws.amazon.com/ja_jp/general/latest/gr/acct-identifiers.html
	AwsAccountID string `yaml:"aws_account_id" validate:"required,number,len=12"`
	// TerraformerAwsCredential は Runtime 環境用の Terraform 実行ユーザのクレデンシャル。必須。
	TerraformerAwsCredential *TerraformerAwsCredentialMapper `yaml:"terraformer_aws_credential" validate:"required"`
	// Parameters は Runtime 環境におけるパラメータ。省略可能
	Parameters *RuntimeParametersMapper `yaml:"parameters"`
	// Activities は Runtime 環境に適用するアクティビティを示す。指定できる値には制限がある。
	Activities []string `yaml:"activities" validate:"required,dive,oneof=deployment_pipeline"`
}

// DeliveryParametersMapper は Delivery 環境用のパラメータセット
type DeliveryParametersMapper struct {
	GitLabRunnerRegistrationToken string `yaml:"gitlab_runner_registration_token" validate:"printascii"`
	VpcCidrBlock                  string `yaml:"vpc_cidr_block" validate:"required,cidr"`
}

// RuntimeParametersMapper は Runtime 環境用のパラメータセット
type RuntimeParametersMapper struct {
	// VpcCidrBlock は、作成されるVPCの CIDR ブロック
	VpcCidrBlock string `yaml:"vpc_cidr_block" validate:"required,cidr"`
	// Route53HostZone は Runtime環境に設定されるホストゾーン名
	Route53HostZone string `yaml:"route53_host_zone" validate:"omitempty,hostname_rfc1123"`
	// SesVerifiedMailAddress は SES の検証済みメールアドレスに設定する値
	SesVerifiedMailAddress string `yaml:"ses_verified_mail_address" validate:"omitempty,email"`
	// PulbicAlbInboundCidrBlocks は ALB にアクセスできる CIDR ブロック
	PulbicAlbInboundCidrBlocks []string `yaml:"public_alb_inbound_cidr_blocks" validate:"dive,cidr"`
}
