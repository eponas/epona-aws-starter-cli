package config

import (
	"bufio"
	"errors"
	"fmt"
	"os"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/command/apply/activity"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/command/apply/delivery"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/command/apply/pattern"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/command/apply/runtime"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/validator"
	"gopkg.in/yaml.v2"
)

func ParseApplyConfig(configFile string) (*ApplyConfig, error) {
	emptyConfig := &ApplyConfig{}

	f, err := os.Open(configFile)
	if err != nil {
		return emptyConfig, err
	}

	defer f.Close()

	d := yaml.NewDecoder(bufio.NewReader(f))

	mapper := &ApplyConfigurationFileMapper{}
	if err := d.Decode(&mapper); err != nil {
		return emptyConfig, err
	}

	// 単項目チェック
	if err := validator.Validate(mapper, "yaml"); err != nil {
		return emptyConfig, fmt.Errorf("%sが正しく読み込めませんでした: %w", configFile, err)
	}
	// 相関チェック
	if err := correlationCheck(mapper); err != nil {
		return emptyConfig, fmt.Errorf("%sが正しく読み込めませんでした: %v", configFile, err)
	}

	return buildApplyConfig(mapper)
}

func correlationCheck(m *ApplyConfigurationFileMapper) error {
	if err := checkDeploymentPipelineDelivery(m); err != nil {
		return err
	}
	if err := checkDeploymentPipelineRuntime(m); err != nil {
		return err
	}
	return nil
}

// checkDeploymentPipelineDelivery は Delivery 環境設定で deployment_pipeline アクティビティが
// 指定された時に必要パラメータがセットされているかを確認する
func checkDeploymentPipelineDelivery(m *ApplyConfigurationFileMapper) error {
	var has bool = false

	// Delivery 環境で "deployment_pipeline" を探す
	if m.Delivery.Activities != nil {
		for _, act := range m.Delivery.Activities {
			if act == "deployment_pipeline" {
				has = true
			}
		}
	}
	if !has {
		return nil
	}

	if (m.Delivery.Parameters != nil) && (m.Delivery.Parameters.GitLabRunnerRegistrationToken != "") {
		return nil
	}
	return errors.New("delivery.activitiesにdeployment_pipelineが指定されているにも関わらず、delivery.parameters.gitlab_runner_registration_tokenが未定義です")
}

// checkDeploymentPipelineRuntime は RUntime 環境設定で deployment_pipeline アクティビティが
// 指定された時に必要パラメータがセットされているかを確認する
func checkDeploymentPipelineRuntime(m *ApplyConfigurationFileMapper) error {
	// Runtimes 環境で "deployment_pipeline" を探す
	for _, rm := range m.Runtimes {
		var has bool = false
		for _, act := range rm.Activities {
			if act == "deployment_pipeline" {
				has = true
			}
		}
		if !has {
			continue
		}

		if rm.Parameters == nil {
			return fmt.Errorf("Runtime環境[%s]のactivitiesにdeployment_pipelineが指定されているにも関わらず、当該環境のparametersが未定義です", rm.Name)
		}
		if rm.Parameters.PulbicAlbInboundCidrBlocks == nil {
			return fmt.Errorf("Runtime環境[%s]のactivitiesにdeployment_pipelineが指定されているにも関わらず、当該環境のparameters.public_alb_inbound_cidr_blocksが未定義です", rm.Name)
		}
		if rm.Parameters.Route53HostZone == "" {
			return fmt.Errorf("Runtime環境[%s]のactivitiesにdeployment_pipelineが指定されているにも関わらず、当該環境のparameters.route53_host_zoneが未定義です", rm.Name)
		}
		if rm.Parameters.SesVerifiedMailAddress == "" {
			return fmt.Errorf("Runtime環境[%s]のactivitiesにdeployment_pipelineが指定されているにも関わらず、当該環境のparameters.ses_verified_mail_addressが未定義です", rm.Name)
		}
		return nil
	}

	return nil
}

func buildApplyConfig(mapper *ApplyConfigurationFileMapper) (*ApplyConfig, error) {
	config := &ApplyConfig{}

	config.ServiceName = mapper.ServiceName
	emptyUsers := []string{} // 現時点では、IAMユーザーの作成は行わない

	var deliveryCredential *pattern.AwsCredential

	if d := mapper.Delivery; d != nil {
		deliveryEnvironment := &delivery.DeliveryEnvironment{}
		c := d.TerraformerAwsCredential
		deliveryCredential = &pattern.AwsCredential{ActualEnvironmentAccountID: d.AwsAccountID, AccessKeyID: c.AccessKeyID, SecretAccessKey: c.SecretAccessKey, Region: c.Region}

		activities := []activity.Activity{}

		var parameters *delivery.Parameters
		if d.Parameters != nil {
			parameters = toDeliveryParameters(d.Parameters)
		}

		runtimeActivities := map[string][]string{}
		runtimeCredentials := map[string]*pattern.AwsCredential{}
		for _, r := range mapper.Runtimes {
			runtimeActivities[r.Name] = r.Activities
			c := r.TerraformerAwsCredential
			runtimeCredentials[r.Name] = &pattern.AwsCredential{ActualEnvironmentAccountID: r.AwsAccountID, AccessKeyID: c.AccessKeyID, SecretAccessKey: c.SecretAccessKey, Region: c.Region}
		}

		for _, a := range d.Activities {
			activity, err := delivery.NewActivity(a, config.ServiceName, emptyUsers, deliveryCredential, runtimeCredentials, parameters, runtimeActivities)

			if err != nil {
				return config, err
			}

			activities = append(activities, activity)
		}

		deliveryEnvironment.Activities = activities

		config.DeliveryEnvironment = deliveryEnvironment
	}

	config.RuntimeEnvironments = []*runtime.RuntimeEnvironment{}

	for _, r := range mapper.Runtimes {
		runtimeEnvironment := &runtime.RuntimeEnvironment{}
		runtimeEnvironment.Name = r.Name
		c := r.TerraformerAwsCredential
		runtimeCredential := &pattern.AwsCredential{ActualEnvironmentAccountID: r.AwsAccountID, AccessKeyID: c.AccessKeyID, SecretAccessKey: c.SecretAccessKey, Region: c.Region}

		activities := []activity.Activity{}

		parameters := toRuntimeParameters(r.Parameters)

		for _, a := range r.Activities {
			activity, err := runtime.NewActivity(a, config.ServiceName, r.Name, emptyUsers, deliveryCredential, runtimeCredential, parameters)

			if err != nil {
				return config, err
			}

			activities = append(activities, activity)
		}

		runtimeEnvironment.Activities = activities

		config.RuntimeEnvironments = append(config.RuntimeEnvironments, runtimeEnvironment)
	}

	return config, nil
}

func toDeliveryParameters(p *DeliveryParametersMapper) *delivery.Parameters {
	return &delivery.Parameters{
		GitLabRunnerRegistrationToken: p.GitLabRunnerRegistrationToken,
		VpcCidrBlock:                  p.VpcCidrBlock,
	}
}

func toRuntimeParameters(p *RuntimeParametersMapper) *runtime.Parameters {
	if p != nil {
		return &runtime.Parameters{
			VpcCidrBlock:               p.VpcCidrBlock,
			Route53HostZone:            p.Route53HostZone,
			SesVerifiedMailAddress:     p.SesVerifiedMailAddress,
			PublicAlbInboundCidrBlocks: p.PulbicAlbInboundCidrBlocks,
		}
	}
	return &runtime.Parameters{}
}
