package config

import (
	"errors"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"testing"

	pgvalidator "github.com/go-playground/validator/v10"
	"github.com/stretchr/testify/assert"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/validator"
)

type validationResult struct {
	tag   string
	param string
	field string
}

func TestParseApplyConfig(t *testing.T) {
	setup := func(t *testing.T, yaml string) (string, func()) {
		tempdir, err := ioutil.TempDir("", "apply-test")
		if err != nil {
			t.Fatal(err)
		}
		// Teardown
		cleanup := func() {
			// 作成した一時ディレクトリ配下を全削除
			os.RemoveAll(tempdir)
		}

		file := filepath.Join(tempdir, "test.yaml")
		if err := ioutil.WriteFile(file, []byte(yaml), 0600); err != nil {
			cleanup()
			t.Fatal(err)
		}

		return file, cleanup
	}

	t.Run("単項目バリデーション", func(t *testing.T) {

		testCases := []struct {
			name     string
			yaml     string
			expected []validationResult
		}{
			{
				name: "サービス名の指定がない",
				yaml: `
service_name:
delivery:
  aws_account_id: 123456789012
  terraformer_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  activities:
    - deployment_pipeline
runtimes:
  - name: staging
    aws_account_id: 987654321098
    terraformer_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
    activities:
      - deployment_pipeline
`,
				expected: []validationResult{{
					tag:   "required",
					param: "",
					field: "service_name",
				}},
			},
			{
				name: "delivery環境、runtime環境のactivityが指定されていない",
				yaml: `
service_name: service
delivery:
  aws_account_id: 123456789012
  terraformer_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  parameters:
    gitlab_runner_registration_token: mytoken
    vpc_cidr_block: 10.250.0.0/16
runtimes:
  - name: staging
    aws_account_id: 987654321098
    terraformer_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
`,
				expected: []validationResult{
					{
						tag:   "required",
						param: "",
						field: "delivery.activities",
					},
					{
						tag:   "required",
						param: "",
						field: "runtimes[0].activities",
					},
				},
			},
			{
				name: "サービス名の長さ超過",
				yaml: `
service_name: abcdefghijklmnopqrst
delivery:
  aws_account_id: 123456789012
  terraformer_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  activities:
    - deployment_pipeline
runtimes:
  - name: staging
    aws_account_id: 987654321098
    terraformer_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
    activities:
      - deployment_pipeline
`,
				expected: []validationResult{{
					tag:   "max",
					param: "16",
					field: "service_name",
				}},
			},
			{
				name: "delivery指定がない",
				yaml: `
service_name: service
runtimes:
  - name: staging
    aws_account_id: 987654321098
    terraformer_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
    activities:
      - deployment_pipeline
`,
				expected: []validationResult{{
					tag:   "required",
					param: "",
					field: "delivery",
				}},
			},
			{
				name: "runtimes指定がない",
				yaml: `
service_name: service
delivery:
  aws_account_id: 123456789012
  terraformer_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  activities:
    - deployment_pipeline
`,
				expected: []validationResult{{
					tag:   "required",
					param: "",
					field: "runtimes",
				}},
			},
			{
				name: "runtimesのキーがない",
				yaml: `
service_name: service
delivery:
  aws_account_id: 123456789012
  terraformer_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  activities:
    - deployment_pipeline
runtimes:
`,
				expected: []validationResult{{
					tag:   "required",
					param: "",
					field: "runtimes",
				}},
			},

			{
				name: "delivery.terraformer_aws_credential設定のキーがない",
				yaml: `
service_name: service
delivery:
  aws_account_id: 987654321098
  terraformer_aws_credential:
  activities:
    - deployment_pipeline
runtimes:
  - name: staging
    aws_account_id: 987654321098
    terraformer_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
    activities:
      - deployment_pipeline
`,
				expected: []validationResult{{
					tag:   "required",
					param: "",
					field: "delivery.terraformer_aws_credential",
				}},
			},
			{
				name: "delivery.terraformer_aws_credentialの指定がない",
				yaml: `
service_name: service
delivery:
  aws_account_id: 987654321098
  parameters: 
    gitlab_runner_registration_token: mytoken
    vpc_cidr_block: 10.250.0.0/16
  activities:
    - deployment_pipeline
runtimes:
  - name: staging
    aws_account_id: 987654321098
    terraformer_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
    activities:
      - deployment_pipeline
`,
				expected: []validationResult{{
					tag:   "required",
					param: "",
					field: "delivery.terraformer_aws_credential",
				}},
			},
			{
				name: "delivery.aws_account_idが数字で構成されていない",
				yaml: `
service_name: service
delivery:
  aws_account_id: hoge
  terraformer_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  activities:
    - deployment_pipeline
runtimes:
  - name: staging
    aws_account_id: 987654321098
    terraformer_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
    activities:
      - deployment_pipeline
`,
				expected: []validationResult{{
					tag:   "number",
					param: "",
					field: "delivery.aws_account_id",
				}},
			},
			{
				name: "delivery.aws_account_idの指定がない",
				yaml: `
service_name: service
delivery:
  terraformer_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  activities:
    - deployment_pipeline
runtimes:
  - name: staging
    aws_account_id: 987654321098
    terraformer_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
    activities:
      - deployment_pipeline
`,
				expected: []validationResult{{
					tag:   "required",
					param: "",
					field: "delivery.aws_account_id",
				}},
			},
			{
				name: "delivery.terraformer_aws_credential.access_key_idの指定がない",
				yaml: `
service_name: service
delivery:
  aws_account_id: 987654321098
  terraformer_aws_credential:
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  activities:
    - deployment_pipeline
runtimes:
  - name: staging
    aws_account_id: 987654321098
    terraformer_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
    activities:
      - deployment_pipeline
`,
				expected: []validationResult{{
					tag:   "required",
					param: "",
					field: "delivery.terraformer_aws_credential.access_key_id",
				}},
			},
			{
				name: "delivery.terraformer_aws_credential.secret_access_keyの指定がない",
				yaml: `
service_name: service
delivery:
  aws_account_id: 987654321098
  terraformer_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    region: ap-northeast-1
  activities:
    - deployment_pipeline
runtimes:
  - name: staging
    aws_account_id: 987654321098
    terraformer_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
    activities:
      - deployment_pipeline
`,
				expected: []validationResult{{
					tag:   "required",
					param: "",
					field: "delivery.terraformer_aws_credential.secret_access_key",
				}},
			},
			{
				name: "delivery.terraformer_aws_credential.regionの指定がない",
				yaml: `
service_name: service
delivery:
  aws_account_id: 987654321098
  terraformer_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
  activities:
    - deployment_pipeline
runtimes:
  - name: staging
    aws_account_id: 987654321098
    terraformer_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
    activities:
      - deployment_pipeline
`,
				expected: []validationResult{{
					tag:   "required",
					param: "",
					field: "delivery.terraformer_aws_credential.region",
				}},
			},
			{
				name: "delivery.activitiesにサポートしていない値が含まれている",
				yaml: `
service_name: service
delivery:
  aws_account_id: 123456789012
  terraformer_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  activities:
    - hoge
    - fuga
runtimes:
  - name: staging
    aws_account_id: 987654321098
    terraformer_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
    activities:
      - deployment_pipeline
`,
				expected: []validationResult{
					{
						tag:   "oneof",
						param: "deployment_pipeline",
						field: "delivery.activities[0]",
					},
					{
						tag:   "oneof",
						param: "deployment_pipeline",
						field: "delivery.activities[1]",
					},
				},
			},
			{
				name: "delivery.parameters.gitlab_runner_registration_tokenに印字可能なASCII文字以外が含まれる",
				yaml: `
service_name: service
delivery:
  aws_account_id: 123456789012
  terraformer_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  activities:
    - deployment_pipeline
  parameters:
` + "    gitlab_runner_registration_token: \"\thoge\"\n" +
					`    vpc_cidr_block: 10.250.0.0/16
runtimes:
  - name: staging
    aws_account_id: 987654321098
    terraformer_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
    activities:
      - deployment_pipeline
`,
				expected: []validationResult{{
					tag:   "printascii",
					param: "",
					field: "delivery.parameters.gitlab_runner_registration_token",
				}},
			},
			{
				name: "delivery.parameters.vpc_cidr_blockの指定が無い",
				yaml: `
service_name: service
delivery:
  aws_account_id: 123456789012
  terraformer_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  activities:
    - deployment_pipeline
  parameters:
    gitlab_runner_registration_token: mytoken
runtimes:
  - name: staging
    aws_account_id: 987654321098
    terraformer_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
    activities:
      - deployment_pipeline
`,
				expected: []validationResult{{
					tag:   "required",
					param: "",
					field: "delivery.parameters.vpc_cidr_block",
				}},
			},
			{
				name: "delivery.parameters.vpc_cidr_blockのCIDRが有効でない",
				yaml: `
service_name: service
delivery:
  aws_account_id: 123456789012
  terraformer_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  activities:
    - deployment_pipeline
  parameters:
    gitlab_runner_registration_token: mytoken
    vpc_cidr_block: 10.256.0.0/16
runtimes:
  - name: staging
    aws_account_id: 987654321098
    terraformer_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
    activities:
      - deployment_pipeline
`,
				expected: []validationResult{{
					tag:   "cidr",
					param: "",
					field: "delivery.parameters.vpc_cidr_block",
				}},
			},
			{
				name: "runtimes[0].nameの指定がない",
				yaml: `
service_name: service
delivery:
  aws_account_id: 123456789012
  terraformer_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  activities:
    - deployment_pipeline
runtimes:
  - name: 
    aws_account_id: 987654321098
    terraformer_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
    activities:
      - deployment_pipeline
`,
				expected: []validationResult{{
					tag:   "required",
					param: "",
					field: "runtimes[0].name",
				}},
			},
			{
				name: "runtimes[0].nameの文字数超過",
				yaml: `
service_name: service
delivery:
  aws_account_id: 123456789012
  terraformer_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  activities:
    - deployment_pipeline
runtimes:
  - name: 1234567891234567
    aws_account_id: 987654321098
    terraformer_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
    activities:
      - deployment_pipeline
`,
				expected: []validationResult{{
					tag:   "lt",
					param: "16",
					field: "runtimes[0].name",
				}},
			},
			{
				name: "runtimes[0].nameの文字種エラー",
				yaml: `
service_name: service
delivery:
  aws_account_id: 123456789012
  terraformer_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  activities:
    - deployment_pipeline
runtimes:
  - name: hoge'
    aws_account_id: 987654321098
    terraformer_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
    activities:
      - deployment_pipeline
`,
				expected: []validationResult{{
					tag:   "alphanum",
					param: "",
					field: "runtimes[0].name",
				}},
			},
			{
				name: "runtimes[0].terraformer_aws_credentialの指定がない",
				yaml: `
service_name: service
delivery:
  aws_account_id: 123456789012
  terraformer_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  activities:
    - deployment_pipeline
runtimes:
  - name: staging
    aws_account_id: 987654321098
    activities:
      - deployment_pipeline
`,
				expected: []validationResult{{
					tag:   "required",
					param: "",
					field: "runtimes[0].terraformer_aws_credential",
				}},
			},
			{
				name: "runtimes[0].activitiesにサポートされていないアクティビティが含まれる",
				yaml: `
service_name: service
delivery:
  aws_account_id: 123456789012
  terraformer_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  activities:
    - deployment_pipeline
runtimes:
  - name: staging
    aws_account_id: 987654321098
    terraformer_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
    activities:
      - hoge
      - fuga
`,
				expected: []validationResult{
					{
						tag:   "oneof",
						param: "deployment_pipeline",
						field: "runtimes[0].activities[0]",
					},
					{
						tag:   "oneof",
						param: "deployment_pipeline",
						field: "runtimes[0].activities[1]",
					},
				},
			},
			{
				name: "runtimes[0].parameters.vpc_cidr_blockの指定が無い",
				yaml: `
service_name: service
delivery:
  aws_account_id: 123456789012
  terraformer_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  activities:
    - deployment_pipeline
runtimes:
  - name: staging
    aws_account_id: 987654321098
    terraformer_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
    parameters:
      route53_host_zone: example.com
      ses_verified_mail_address: staging-admin@example.com
    activities:
      - deployment_pipeline
`,
				expected: []validationResult{
					{
						tag:   "required",
						param: "",
						field: "runtimes[0].parameters.vpc_cidr_block",
					},
				},
			},
			{
				name: "runtimes[0].parameters.vpc_cidr_blockのCIDRが有効でない",
				yaml: `
service_name: service
delivery:
  aws_account_id: 123456789012
  terraformer_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  activities:
    - deployment_pipeline
runtimes:
  - name: staging
    aws_account_id: 987654321098
    terraformer_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
    parameters:
      vpc_cidr_block: 10.256.0.0/16
      route53_host_zone: example.com
      ses_verified_mail_address: staging-admin@example.com
    activities:
      - deployment_pipeline
`,
				expected: []validationResult{
					{
						tag:   "cidr",
						param: "",
						field: "runtimes[0].parameters.vpc_cidr_block",
					},
				},
			},
			{
				name: "runtimes[0].parameters.route53_host_zoneがホストゾーンになっていない",
				yaml: `
service_name: service
delivery:
  aws_account_id: 123456789012
  terraformer_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  activities:
    - deployment_pipeline
runtimes:
  - name: staging
    aws_account_id: 987654321098
    terraformer_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
    parameters:
      vpc_cidr_block: 10.251.0.0/16
      route53_host_zone: hoge~
      ses_verified_mail_address: staging-admin@example.com
    activities:
      - deployment_pipeline
`,
				expected: []validationResult{
					{
						tag:   "hostname_rfc1123",
						param: "",
						field: "runtimes[0].parameters.route53_host_zone",
					},
				},
			},
			{
				name: "runtimes[0].parameters.ses_verified_mail_addressがメールアドレスになっていない",
				yaml: `
service_name: service
delivery:
  aws_account_id: 123456789012
  terraformer_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  activities:
    - deployment_pipeline
runtimes:
  - name: staging
    aws_account_id: 987654321098
    terraformer_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
    parameters:
      vpc_cidr_block: 10.251.0.0/16
      route53_host_zone: example.com
      ses_verified_mail_address: hoge
    activities:
      - deployment_pipeline
`,
				expected: []validationResult{
					{
						tag:   "email",
						param: "",
						field: "runtimes[0].parameters.ses_verified_mail_address",
					},
				},
			},
			{
				name: "runtimes[0].parameters.public_alb_inbound_cidr_blocksに有効でないCIDRがある",
				yaml: `
service_name: service
delivery:
  aws_account_id: 123456789012
  terraformer_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  activities:
    - deployment_pipeline
runtimes:
  - name: staging
    aws_account_id: 987654321098
    terraformer_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
    parameters:
      vpc_cidr_block: 10.251.0.0/16
      route53_host_zone: example.com
      ses_verified_mail_address: hoge@example.com
      public_alb_inbound_cidr_blocks:
        - 192.0.2.0/24
        - 198.51.100.0/24
        - 203.0.113.0/24
        - 256.255.255.255/32
    activities:
      - deployment_pipeline
`,
				expected: []validationResult{
					{
						tag:   "cidr",
						param: "",
						field: "runtimes[0].parameters.public_alb_inbound_cidr_blocks[3]",
					},
				},
			},
		}

		for _, tc := range testCases {
			path, cleanup := setup(t, tc.yaml)
			_, parseErr := ParseApplyConfig(path)

			t.Log(tc.name)

			// バリデーションエラーを発生させるテストケースであるため
			if assert.Error(t, parseErr, tc.name) {
				err := errors.Unwrap(parseErr)
				ve, ok := err.(*validator.ValidationError)
				if !ok {
					t.Errorf("戻り値がvalidator.ValidationErrorではありません: %T (%v)", err, parseErr)
					return
				}
				ves, ok := ve.Cause.(pgvalidator.ValidationErrors)
				if !ok {
					t.Errorf("戻り値がpgvalidator.ValidationErrorsではありません: %T", ve.Cause)
				}
				assert.Len(t, ves, len(tc.expected), tc.name)
				assert.Equal(t, tc.expected, mapValidationResult(ves), tc.name)
			}
			cleanup()
		}
	})

	t.Run("相関バリデーション", func(t *testing.T) {
		testCases := []struct {
			name      string
			yaml      string
			errorcase bool
			field     string // エラーメッセージに含まれるべきフィールド名
		}{
			{
				name: "正常系",
				yaml: `
service_name: servicehogehoge
delivery:
  aws_account_id: 123456789012
  terraformer_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  parameters:
    gitlab_runner_registration_token: hoge
    vpc_cidr_block: 10.250.0.0/16
  activities:
    - deployment_pipeline
runtimes:
  - name: staging
    aws_account_id: 987654321098
    terraformer_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
    parameters:
      vpc_cidr_block: 10.251.0.0/16
      route53_host_zone: example.com
      ses_verified_mail_address: hoge@example.com
      public_alb_inbound_cidr_blocks:
        - 192.0.2.0/24
    activities:
      - deployment_pipeline
`,
				errorcase: false,
			},
			{
				name: "delivery環境でdeployment_pipelineアクティビティを指定しているが、parametersがない",
				yaml: `
               service_name: service
               delivery:
                 aws_account_id: 123456789012
                 terraformer_aws_credential:
                   access_key_id: AKIA1234567890ABCDEF
                   secret_access_key: abcdefghijklmnopqrstuvwxyz
                   region: ap-northeast-1
                 activities:
                   - deployment_pipeline
               runtimes:
                 - name: staging
                   aws_account_id: 987654321098
                   terraformer_aws_credential:
                     access_key_id: AKIA1234567890ABCDEF
                     secret_access_key: abcdefghijklmnopqrstuvwxyz
                     region: ap-northeast-1
                   parameters:
                     vpc_cidr_block: 10.251.0.0/16
                     route53_host_zone: example.com
                     ses_verified_mail_address: hoge@example.com
                     public_alb_inbound_cidr_blocks:
                       - 192.0.2.0/24
                   activities:
                     - deployment_pipeline
               `,
				errorcase: true,
				field:     "parameters",
			},
			{
				name: "delivery環境でdeployment_pipelineアクティビティを指定しているが、parameters.gitlab_runner_registration_tokenがない",
				yaml: `
               service_name: service
               delivery:
                 aws_account_id: 123456789012
                 terraformer_aws_credential:
                   access_key_id: AKIA1234567890ABCDEF
                   secret_access_key: abcdefghijklmnopqrstuvwxyz
                   region: ap-northeast-1
                 activities:
                   - deployment_pipeline
               runtimes:
                 - name: staging
                   aws_account_id: 987654321098
                   terraformer_aws_credential:
                     access_key_id: AKIA1234567890ABCDEF
                     secret_access_key: abcdefghijklmnopqrstuvwxyz
                     region: ap-northeast-1
                   parameters:
                     vpc_cidr_block: 10.251.0.0/16
                     route53_host_zone: example.com
                     ses_verified_mail_address: hoge@example.com
                     public_alb_inbound_cidr_blocks:
                       - 192.0.2.0/24
                   activities:
                     - deployment_pipeline
               `,
				errorcase: true,
				field:     "parameters.gitlab_runner_registration_token",
			},
			{
				name: "runtime環境でdeployment_pipelineアクティビティを指定しているが、parametersがない",
				yaml: `
               service_name: service
               delivery:
                 aws_account_id: 123456789012
                 terraformer_aws_credential:
                   access_key_id: AKIA1234567890ABCDEF
                   secret_access_key: abcdefghijklmnopqrstuvwxyz
                   region: ap-northeast-1
                 activities:
                   - deployment_pipeline
               runtimes:
                 - name: staging
                   aws_account_id: 987654321098
                   terraformer_aws_credential:
                     access_key_id: AKIA1234567890ABCDEF
                     secret_access_key: abcdefghijklmnopqrstuvwxyz
                     region: ap-northeast-1
                   activities:
                     - deployment_pipeline
               `,
				errorcase: true,
				field:     "parameters",
			},
			{
				name: "runtime環境でdeployment_pipelineアクティビティを指定しているが、parameters.route53_host_zoneがない",
				yaml: `
service_name: servicehogehoge
delivery:
  aws_account_id: 123456789012
  terraformer_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  parameters:
    gitlab_runner_registration_token: hoge
    vpc_cidr_block: 10.250.0.0/16
  activities:
    - deployment_pipeline
runtimes:
  - name: staging
    aws_account_id: 987654321098
    terraformer_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
    parameters:
      vpc_cidr_block: 10.251.0.0/16
      ses_verified_mail_address: hoge@example.com
      public_alb_inbound_cidr_blocks:
        - 192.0.2.0/24
    activities:
      - deployment_pipeline
`,
				errorcase: true,
			},
			{
				name: "runtime環境でdeployment_pipelineアクティビティを指定しているが、parameters.ses_verified_mail_addressがない",
				yaml: `
service_name: servicehogehoge
delivery:
  aws_account_id: 123456789012
  terraformer_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  parameters:
    gitlab_runner_registration_token: hoge
    vpc_cidr_block: 10.250.0.0/16
  activities:
    - deployment_pipeline
runtimes:
  - name: staging
    aws_account_id: 987654321098
    terraformer_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
    parameters:
      vpc_cidr_block: 10.251.0.0/16
      route53_host_zone: example.com
      public_alb_inbound_cidr_blocks:
        - 192.0.2.0/24
    activities:
      - deployment_pipeline
`,
				errorcase: true,
				field:     "parameters.ses_verified_mail_address",
			},
			{
				name: "runtime環境でdeployment_pipelineアクティビティを指定しているが、parameters.public_alb_inbound_cidr_blocksがない",
				yaml: `
service_name: servicehogehoge
delivery:
  aws_account_id: 123456789012
  terraformer_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  parameters:
    gitlab_runner_registration_token: hoge
    vpc_cidr_block: 10.250.0.0/16
  activities:
    - deployment_pipeline
runtimes:
  - name: staging
    aws_account_id: 987654321098
    terraformer_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
    parameters:
      vpc_cidr_block: 10.251.0.0/16
      route53_host_zone: example.com
      ses_verified_mail_address: hoge@example.com
    activities:
      - deployment_pipeline
`,
				errorcase: true,
				field:     "parameters.public_alb_inbound_cidr_blocks",
			},
		}

		for _, tc := range testCases {
			path, cleanup := setup(t, tc.yaml)
			_, err := ParseApplyConfig(path)

			t.Log(tc.name)

			if tc.errorcase {
				assert.Error(t, err, tc.name)
				assert.Contains(t, err.Error(), tc.field, tc.name)
			} else {
				assert.NoError(t, err, tc.name)
			}

			cleanup()
		}
	})
}

func mapValidationResult(fes []pgvalidator.FieldError) []validationResult {
	ret := []validationResult{}
	for _, fe := range fes {
		ns := fe.Namespace()
		field := strings.SplitN(ns, ".", 2)[1]

		ret = append(ret, validationResult{
			tag:   fe.Tag(),
			param: fe.Param(),
			field: field,
		})
	}
	return ret
}
