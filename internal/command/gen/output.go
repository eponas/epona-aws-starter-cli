package gen

import (
	"context"
	"path/filepath"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/service"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/template"
)

func OutputInitConfigFileSeed(ctx context.Context, runtimeEnvironmentNames []string, outputFilePath string) error {
	serviceName, _ := service.GetName(ctx)

	fileName := filepath.Base(outputFilePath)

	err := template.EvalTemplateMergeToFile(fileName, map[string]interface{}{}, filepath.Join(filepath.Dir(outputFilePath), ".gitignore"))

	if err != nil {
		return err
	}

	binding := map[string]interface{}{
		"ServiceName":             serviceName,
		"RuntimeEnvironmentNames": runtimeEnvironmentNames,
	}

	return template.EvalTemplateWriteToFile(outputInitConfigSeedTemplate, binding, outputFilePath)
}

var (
	outputInitConfigSeedTemplate = `{{ $userRequiredValue := "[値を入力してください]" -}}
### "{{$userRequiredValue}}"と記述されている箇所は、利用者が指定するパラメーターになっています。
### init実行前に適切な値に設定してください。
### そのままだと実行できません。

### 構築するサービス名です ※この値は変更する必要はありません
service_name: {{.ServiceName}}

### Delivery環境の設定です
delivery:
  ## ★Delivery環境用のAWSアカウントにおける、Administrator権限を持ったユーザーのクレデンシャルを設定してください
  admin_aws_credential:
    access_key_id: {{$userRequiredValue}}
    secret_access_key: {{$userRequiredValue}}
    region: ap-northeast-1
  ## ★Terraform実行ユーザをDelivery環境の既存のIAMグループに所属させる場合、以下のコメントのようにterraformer_user_groupsにIAMグループ名を配列で設定してください
  ###  terraformer_user_groups:
  ###    - UserGroupName
  terraformer_user_groups: []

### Runtime環境の設定です
runtimes:
{{- range $index, $envName := .RuntimeEnvironmentNames }}
  ## 構築するRuntime環境の名前です
  - name: {{$envName}}
    ## ★Runtime / {{$envName}}環境のAWSアカウントにおける、Administrator権限を持ったユーザーのAWSクレデンシャルを設定してください
    admin_aws_credential:
      access_key_id: {{$userRequiredValue}}
      secret_access_key: {{$userRequiredValue}}
      region: ap-northeast-1
{{- end }}
`
)
