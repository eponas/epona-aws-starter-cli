package gen

import (
	"gitlab.com/eponas/epona-aws-starter-cli/internal/validator"
)

type ConfigGenMapper struct {
	ServiceName             string   `cli-option:"-service-name (-s)" validate:"required,printascii,max=16"`
	RuntimeEnvironmentNames []string `cli-option:"-runtime-name (-r)" validate:"required,dive,alphanum,lt=16"`
	OutputConfigFilePath    string   // no validate
}

func Validate(serviceName string, runtimeEnvironmentNames []string, outputConfigFilePath string) error {
	m := &ConfigGenMapper{
		ServiceName:             serviceName,
		RuntimeEnvironmentNames: runtimeEnvironmentNames,
		OutputConfigFilePath:    outputConfigFilePath,
	}

	return validator.Validate(m, "cli-option")
}
