package config

import (
	"gitlab.com/eponas/epona-aws-starter-cli/internal/command/init/terraformer"
)

type InitConfig struct {
	ServiceName         string
	DeliveryEnvironment *terraformer.DeliveryEnvironment
	RuntimeEnvironments []*terraformer.RuntimeEnvironment
}
