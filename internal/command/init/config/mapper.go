package config

// InitConfigurationFileMapper は init サブコマンドで読み込む設定ファイルを
// Go で扱えるようにするための Mapper
type InitConfigurationFileMapper struct {
	// ServiceName 必須。16文字以内の印字可能ASCII文字で構成。
	ServiceName string `yaml:"service_name" validate:"required,printascii,max=16"`
	// Delivery は Delivery環境用設定。必須。
	Delivery *DeliveryMapper `yaml:"delivery" validate:"required"`
	// Runtimes はRuntime環境用設定。必須。
	Runtimes []*RuntimeMapper `yaml:"runtimes" validate:"required,dive"`
}

// AdminAwsCredentialMapper は init サブコマンド用設定ファイル上の
// `admin_aws_credential` キー設定に対応するマッパー
type AdminAwsCredentialMapper struct {
	// AccessKeyID は Terraforme 実行ユーザのアクセスキー ID を示す。必須。
	AccessKeyID string `yaml:"access_key_id" validate:"required,printascii"`
	// SecretAccessKey は Terraforme 実行ユーザのシークレットアクセスキーを示す。必須。
	SecretAccessKey string `yaml:"secret_access_key" validate:"required,printascii"`
	// Region は AWS アカウントの対象リージョンを示す。必須。
	Region string `yaml:"region" validate:"required,printascii"`
}

// DeliveryMapper は init サブコマンド用設定ファイル上の `delivery` キー設定に対応するマッパー
type DeliveryMapper struct {
	// AdminAwsCredential は Delivery 環境用の Terraform 実行ユーザのクレデンシャル。必須。
	AdminAwsCredential *AdminAwsCredentialMapper `yaml:"admin_aws_credential" validate:"required"`
	// TerrafomerUserGroups は Delivery 環境用の Terraform 実行ユーザの所属グループ。
	TerraformerUserGroups []string `yaml:"terraformer_user_groups" validate:"dive,printascii,max=128"`
}

// RuntimeMapper は init サブコマンド用設定ファイル上の `runtime` キー設定に対応するマッパー
type RuntimeMapper struct {
	// Name は Runtime 環境名を示す。必須。
	Name string `yaml:"name" validate:"required,alphanum,lt=16"`
	// AdminAwsCredential は Runtime 環境用の Terraform 実行ユーザのクレデンシャル。必須。
	AdminAwsCredential *AdminAwsCredentialMapper `yaml:"admin_aws_credential" validate:"required"`
}
