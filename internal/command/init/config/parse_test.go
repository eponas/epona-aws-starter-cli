package config

import (
	"errors"
	"io/ioutil"
	"os"
	"path/filepath"
	"strings"
	"testing"

	pgvalidator "github.com/go-playground/validator/v10"
	"github.com/stretchr/testify/assert"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/validator"
)

type initValidationResult struct {
	tag   string
	param string
	field string
}

func TestParseInitConfig(t *testing.T) {
	setup := func(t *testing.T, yaml string) (string, func()) {
		tempdir, err := ioutil.TempDir("", "init-test")
		if err != nil {
			t.Fatal(err)
		}
		// Teardown
		cleanup := func() {
			// 作成した一時ディレクトリ配下を全削除
			os.RemoveAll(tempdir)
		}

		file := filepath.Join(tempdir, "test.yaml")
		if err := ioutil.WriteFile(file, []byte(yaml), 0600); err != nil {
			cleanup()
			t.Fatal(err)
		}

		return file, cleanup
	}

	t.Run("単項目バリデーション", func(t *testing.T) {

		testCases := []struct {
			name     string
			yaml     string
			expected []initValidationResult
		}{
			{
				name: "サービス名の指定がない",
				yaml: `
service_name:
delivery:
  admin_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  terraformer_user_groups:
    - test-terraformer-group
runtimes:
  - name: staging
    admin_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
`,
				expected: []initValidationResult{{
					tag:   "required",
					param: "",
					field: "service_name",
				}},
			},
			{
				name: "サービス名の長さ超過",
				yaml: `
service_name: abcdefghijklmnopqrst
delivery:
  admin_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  terraformer_user_groups:
    - test-terraformer-group
runtimes:
  - name: staging
    admin_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
`,
				expected: []initValidationResult{{
					tag:   "max",
					param: "16",
					field: "service_name",
				}},
			},
			{
				name: "delivery指定がない",
				yaml: `
service_name: service
runtimes:
  - name: staging
    admin_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
`,
				expected: []initValidationResult{{
					tag:   "required",
					param: "",
					field: "delivery",
				}},
			},
			{
				name: "runtimes指定がない",
				yaml: `
service_name: service
delivery:
  admin_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  terraformer_user_groups:
    - test-terraformer-group
`,
				expected: []initValidationResult{{
					tag:   "required",
					param: "",
					field: "runtimes",
				}},
			},
			{
				name: "runtimesのキーがない",
				yaml: `
service_name: service
delivery:
  admin_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  terraformer_user_groups:
    - test-terraformer-group
runtimes:
`,
				expected: []initValidationResult{{
					tag:   "required",
					param: "",
					field: "runtimes",
				}},
			},

			{
				name: "delivery.admin_aws_credential設定のキーがない",
				yaml: `
service_name: service
delivery:
  admin_aws_credential:
  terraformer_user_groups:
    - test-terraformer-group
runtimes:
  - name: staging
    admin_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
`,
				expected: []initValidationResult{{
					tag:   "required",
					param: "",
					field: "delivery.admin_aws_credential",
				}},
			},
			{
				name: "delivery.admin_aws_credentialの指定がない",
				yaml: `
service_name: service
delivery:
  terraformer_user_groups:
    - test-terraformer-group
runtimes:
  - name: staging
    admin_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
`,
				expected: []initValidationResult{{
					tag:   "required",
					param: "",
					field: "delivery.admin_aws_credential",
				}},
			},
			{
				name: "delivery.admin_aws_credential.access_key_idの指定がない",
				yaml: `
service_name: service
delivery:
  admin_aws_credential:
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  terraformer_user_groups:
    - test-terraformer-group
runtimes:
  - name: staging
    admin_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
`,
				expected: []initValidationResult{{
					tag:   "required",
					param: "",
					field: "delivery.admin_aws_credential.access_key_id",
				}},
			},
			{
				name: "delivery.admin_aws_credential.secret_access_keyの指定がない",
				yaml: `
service_name: service
delivery:
  admin_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    region: ap-northeast-1
  terraformer_user_groups:
    - test-terraformer-group
runtimes:
  - name: staging
    admin_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
`,
				expected: []initValidationResult{{
					tag:   "required",
					param: "",
					field: "delivery.admin_aws_credential.secret_access_key",
				}},
			},
			{
				name: "delivery.admin_aws_credential.regionの指定がない",
				yaml: `
service_name: service
delivery:
  admin_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
  terraformer_user_groups:
    - test-terraformer-group
runtimes:
  - name: staging
    admin_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
`,
				expected: []initValidationResult{{
					tag:   "required",
					param: "",
					field: "delivery.admin_aws_credential.region",
				}},
			},
			{
				name: "delivery.terraformer_user_groups[0]の文字種エラー",
				yaml: `
service_name: service
delivery:
  admin_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  terraformer_user_groups:
    - 非ascii文字種
runtimes:
  - name: staging
    admin_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
`,
				expected: []initValidationResult{{
					tag:   "printascii",
					param: "",
					field: "delivery.terraformer_user_groups[0]",
				}},
			},
			{
				name: "delivery.terraformer_user_groups[1]の文字数超過",
				yaml: `
service_name: service
delivery:
  admin_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  terraformer_user_groups:
    - 12345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678
    - 123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789
runtimes:
  - name: staging
    admin_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
`,
				expected: []initValidationResult{{
					tag:   "max",
					param: "128",
					field: "delivery.terraformer_user_groups[1]",
				}},
			},
			{
				name: "terraformer_user_groupsが必須でない(Error: サービス名の指定がない)",
				yaml: `
service_name:
delivery:
  admin_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
runtimes:
  - name: staging
    admin_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
`,
				expected: []initValidationResult{{
					tag:   "required",
					param: "",
					field: "service_name",
				}},
			},
			{
				name: "runtimes[0].nameの指定がない",
				yaml: `
service_name: service
delivery:
  admin_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  terraformer_user_groups:
    - test-terraformer-group
runtimes:
  - name: 
    admin_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
`,
				expected: []initValidationResult{{
					tag:   "required",
					param: "",
					field: "runtimes[0].name",
				}},
			},
			{
				name: "runtimes[0].nameの文字数超過",
				yaml: `
service_name: service
delivery:
  admin_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  terraformer_user_groups:
    - test-terraformer-group
runtimes:
  - name: 1234567891234567
    admin_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
`,
				expected: []initValidationResult{{
					tag:   "lt",
					param: "16",
					field: "runtimes[0].name",
				}},
			},
			{
				name: "runtimes[0].nameの文字種エラー",
				yaml: `
service_name: service
delivery:
  admin_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  terraformer_user_groups:
    - test-terraformer-group
runtimes:
  - name: hoge'
    admin_aws_credential:
      access_key_id: AKIA1234567890ABCDEF
      secret_access_key: abcdefghijklmnopqrstuvwxyz
      region: ap-northeast-1
`,
				expected: []initValidationResult{{
					tag:   "alphanum",
					param: "",
					field: "runtimes[0].name",
				}},
			},
			{
				name: "runtimes[0].admin_aws_credentialの指定がない",
				yaml: `
service_name: service
delivery:
  admin_aws_credential:
    access_key_id: AKIA1234567890ABCDEF
    secret_access_key: abcdefghijklmnopqrstuvwxyz
    region: ap-northeast-1
  terraformer_user_groups:
    - test-terraformer-group
runtimes:
  - name: staging
`,
				expected: []initValidationResult{{
					tag:   "required",
					param: "",
					field: "runtimes[0].admin_aws_credential",
				}},
			},
		}

		for _, tc := range testCases {
			path, cleanup := setup(t, tc.yaml)
			_, parseErr := ParseInitConfig(path)

			t.Log(tc.name)

			// バリデーションエラーを発生させるテストケースであるため
			if assert.Error(t, parseErr, tc.name) {
				err := errors.Unwrap(parseErr)
				ve, ok := err.(*validator.ValidationError)
				if !ok {
					t.Errorf("戻り値がvalidator.ValidationErrorではありません: %T (%v)", err, parseErr)
					return
				}
				ves, ok := ve.Cause.(pgvalidator.ValidationErrors)
				if !ok {
					t.Errorf("戻り値がpgvalidator.ValidationErrorsではありません: %T", ve.Cause)
				}
				assert.Len(t, ves, len(tc.expected), tc.name)
				assert.Equal(t, tc.expected, mapInitValidationResult(ves), tc.name)
			}
			cleanup()
		}
	})
}

func mapInitValidationResult(fes []pgvalidator.FieldError) []initValidationResult {
	ret := []initValidationResult{}
	for _, fe := range fes {
		ns := fe.Namespace()
		field := strings.SplitN(ns, ".", 2)[1]

		ret = append(ret, initValidationResult{
			tag:   fe.Tag(),
			param: fe.Param(),
			field: field,
		})
	}
	return ret
}
