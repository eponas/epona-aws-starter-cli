package config

import (
	"bufio"
	"fmt"
	"os"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/aws"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/command/init/terraformer"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/validator"
	"gopkg.in/yaml.v2"
)

func ParseInitConfig(configFile string) (*InitConfig, error) {
	emptyConfig := &InitConfig{}

	f, err := os.Open(configFile)

	if err != nil {
		return emptyConfig, err
	}

	defer f.Close()

	d := yaml.NewDecoder(bufio.NewReader(f))

	mapper := &InitConfigurationFileMapper{}

	err = d.Decode(&mapper)

	if err != nil {
		return emptyConfig, err
	}

	// 単項目チェック
	if err := validator.Validate(mapper, "yaml"); err != nil {
		return emptyConfig, fmt.Errorf("%sが正しく読み込めませんでした: %w", configFile, err)
	}

	return buildInitConfig(mapper)
}

func buildInitConfig(mapper *InitConfigurationFileMapper) (*InitConfig, error) {
	config := &InitConfig{}

	config.ServiceName = mapper.ServiceName

	runtimeNames := []string{}

	for _, r := range mapper.Runtimes {
		runtimeNames = append(runtimeNames, r.Name)
	}

	if d := mapper.Delivery; d != nil {
		c := d.AdminAwsCredential
		deliveryCredential := &terraformer.AwsCredential{AccessKeyID: c.AccessKeyID, SecretAccessKey: c.SecretAccessKey, Region: c.Region}

		accountID, err := aws.GetAccountID(deliveryCredential)

		if err != nil {
			return config, err
		}

		terraformerUserGroups := d.TerraformerUserGroups

		config.DeliveryEnvironment = terraformer.NewDeliveryEnvitonment(mapper.ServiceName, accountID, deliveryCredential, runtimeNames, terraformerUserGroups)
	}

	runtimeEnvironments := []*terraformer.RuntimeEnvironment{}

	for i, r := range mapper.Runtimes {
		c := r.AdminAwsCredential
		runtimeCredential := &terraformer.AwsCredential{AccessKeyID: c.AccessKeyID, SecretAccessKey: c.SecretAccessKey, Region: c.Region}

		accountID, err := aws.GetAccountID(runtimeCredential)

		if err != nil {
			return config, err
		}

		runtimeEnvironments = append(runtimeEnvironments, terraformer.NewRuntimeEnvitonment(config.ServiceName, r.Name, i, accountID, runtimeCredential))
		config.DeliveryEnvironment.RuntimeAwsAccountIDs = append(config.DeliveryEnvironment.RuntimeAwsAccountIDs, accountID)
	}

	config.RuntimeEnvironments = runtimeEnvironments

	return config, nil
}
