package terraformer

import (
	"context"
	"path/filepath"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/service"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/template"
)

func OutputApplyConfigFileSeed(ctx context.Context, deliveryEnvironment *DeliveryEnvironment, runtimeEnvironments []*RuntimeEnvironment, outputFilePath string) error {
	serviceName, _ := service.GetName(ctx)

	fileName := filepath.Base(outputFilePath)

	err := template.EvalTemplateMergeToFile(fileName, map[string]interface{}{}, filepath.Join(filepath.Dir(outputFilePath), ".gitignore"))

	if err != nil {
		return err
	}

	binding := map[string]interface{}{
		"ServiceName":         serviceName,
		"DeliveryEnvironment": deliveryEnvironment,
		"RuntimeEnvironments": runtimeEnvironments,
	}

	return template.EvalTemplateWriteToFile(outputApplyConfigSeedTemplate, binding, outputFilePath)
}

var (
	outputApplyConfigSeedTemplate = `{{ $userRequiredValue := "[値を入力してください]" -}}
### "{{$userRequiredValue}}"と記述されている箇所は、利用者が指定するパラメーターになっています。
### apply実行前に適切な値に設定してください。
### そのままだと実行できません。

### 構築するサービス名です ※この値は変更する必要はありません
service_name: {{.ServiceName}}

### Delivery環境の設定です
delivery:
  ## Delivery環境のAWSアカウントIDです
  aws_account_id: {{.DeliveryEnvironment.CreatedTerraformerMappings.delivery.TargetEnvironmentAwsAccountID}}
  ## Delivey環境構築用Terraform実行ユーザーのAWSクレデンシャルです
  terraformer_aws_credential:
    access_key_id: {{.DeliveryEnvironment.CreatedTerraformerMappings.delivery.AwsCredential.AccessKeyID}}
    secret_access_key: {{.DeliveryEnvironment.CreatedTerraformerMappings.delivery.AwsCredential.SecretAccessKey}}
    region: {{.DeliveryEnvironment.CreatedTerraformerMappings.delivery.AwsCredential.Region}}
  ## アクティビティで適用するpatternに必要なパラメーターです
  parameters:
    ## ★GitLab RunnerをGitLabに登録するための値です。GitLabからGitLab Runner用のトークンを取得して設定してください
    gitlab_runner_registration_token: {{$userRequiredValue}}
    ## 作成するVPCのCIDRブロックです。修正する場合は/16から/20の間で設定してください
    vpc_cidr_block: {{.DeliveryEnvironment.DefaultVpcCidrBlock}}
  ## Delivery環境で実現するアクティビティを並べます
  activities:
    - deployment_pipeline    ## デプロイメントパイプラインを実現するためのpatternを適用します

### Runtime環境の設定です
runtimes:
{{- $deliveryEnvironment := .DeliveryEnvironment -}}
{{- $runtimeEnvironments := .RuntimeEnvironments -}}
{{- range $envName, $terraformerMapping := $deliveryEnvironment.CreatedTerraformerMappings -}}
{{- if ne $envName "delivery"}}
  ## 構築するRuntime環境の名前です
  - name: {{$envName}}
    ## Runtime / {{$envName}}環境のAWSアカウントIDです
    aws_account_id: {{$terraformerMapping.TargetEnvironmentAwsAccountID}}
    ## Runtime / {{$envName}}環境構築用Terraform実行ユーザーの、AWSクレデンシャル情報です
    ## ※Terraform実行ユーザーは、Delivery環境に存在します
    terraformer_aws_credential:
      access_key_id: {{$terraformerMapping.AwsCredential.AccessKeyID}}
      secret_access_key: {{$terraformerMapping.AwsCredential.SecretAccessKey}}
      region: {{$terraformerMapping.AwsCredential.Region}}
    ## アクティビティで適用するpatternに必要なパラメーターです
    parameters:
      ## 作成するVPCのCIDRブロックです。修正する場合は/16から/20の間で設定してください
      {{- range $i, $runtimeEnvironment := $runtimeEnvironments -}}
      {{- if eq $envName $runtimeEnvironment.Name}}
      vpc_cidr_block: {{ $runtimeEnvironment.DefaultVpcCidrBlock }}
      {{- end -}}
      {{- end}}
      ## ★CloudFrontやALBに割り当てるDNSレコードに使用します。Route 53でドメインを取得し、そのホストゾーンを設定してください
      route53_host_zone: {{$userRequiredValue}}
      ## ★このツールがターゲットにしているチャットアプリケーションからのメール送信に利用します。SESでVerify済みのメールアドレスを指定してください
      ses_verified_mail_address: {{$userRequiredValue}}
      ## ALBに割り当てるセキュリティグループのインバウンド設定に反映されます。ALBからの接続を許可したいネットワークを指定してください
      public_alb_inbound_cidr_blocks:
        - 0.0.0.0/0
    activities:
      - deployment_pipeline    ## デプロイメントパイプラインを実現するためのpatternを適用します
{{- end -}}
{{- end}}
`
)
