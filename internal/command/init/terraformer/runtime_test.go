package terraformer

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewRuntimeEnvitonment(t *testing.T) {
	t.Run("VPCのCIDRのデフォルト値が正しいこと", func(t *testing.T) {
		r := NewRuntimeEnvitonment("runtimeTest", "test", 0, "123456789123", &AwsCredential{})
		assert.Equal(t, "10.251.0.0/16", r.DefaultVpcCidrBlock)
	})
	t.Run("Runtime環境が6個以上渡されたときに無効なCIDRが生成されないこと", func(t *testing.T) {
		r := NewRuntimeEnvitonment("runtimeTest", "test", 5, "123456789123", &AwsCredential{})
		assert.Equal(t, "10.0.0.0/16", r.DefaultVpcCidrBlock)
	})
}
