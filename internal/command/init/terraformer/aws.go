package terraformer

type AwsCredential struct {
	AccessKeyID     string
	SecretAccessKey string
	Region          string
}

func (c *AwsCredential) GetAccessKeyID() string {
	return c.AccessKeyID
}

func (c *AwsCredential) GetSecretAccessKey() string {
	return c.SecretAccessKey
}

func (c *AwsCredential) GetRegion() string {
	return c.Region
}
