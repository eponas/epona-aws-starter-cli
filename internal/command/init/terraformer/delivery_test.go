package terraformer

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestNewDeliveryEnvitonment(t *testing.T) {
	t.Run("VPCのCIDRのデフォルト値が正しいこと", func(t *testing.T) {
		d := NewDeliveryEnvitonment("deliveryTest", "123456789123", &AwsCredential{}, []string{}, []string{})
		assert.Equal(t, "10.250.0.0/16", d.DefaultVpcCidrBlock)
	})
}
