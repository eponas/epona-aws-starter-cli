package terraformer

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"regexp"

	"gitlab.com/eponas/epona-aws-starter-cli/internal/aws"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/logging"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/service"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/template"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/terraform"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/terraform/lang"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/version"
)

type RuntimeEnvironment struct {
	Name                             string
	AwsAccountID                     string
	AwsCredential                    *AwsCredential
	BackendBucketName                string
	LockTableName                    string
	TerraformExecutionRoleName       string
	CreatedTerraformExecutionRoleArn string
	DefaultVpcCidrBlock              string
}

type RuntimeEnvironmentInitializer interface {
	AlreadyExistingEnvironment(ctx context.Context) error
	CreateTerraformBackendStorage(ctx context.Context) error
	CreateExecutionRoleAndSetupBackend(ctx context.Context, deliveryEnvironment *DeliveryEnvironment) error
}

func NewRuntimeEnvitonment(serviceName string, runtimeName string, envIndex int, awsAccountID string, credential *AwsCredential) *RuntimeEnvironment {
	prefix := os.Getenv("EPONA_STARTER_PREFIX")

	var backendBucketName string
	var lockTableName string
	var terraformExecutionRoleName string
	var defaultVpcCidrBlock string

	backendBucketName = fmt.Sprintf("%s-%s-backend", serviceName, runtimeName)

	if prefix == "" {
		lockTableName = fmt.Sprintf("%s_terraform_tfstate_lock", serviceName)
		terraformExecutionRoleName = "TerraformExecutionRole"
	} else {
		lockTableName = fmt.Sprintf("%s_terraform_tfstate_lock", serviceName)
		terraformExecutionRoleName = fmt.Sprintf("%sTerraformExecutionRole", lang.Title(prefix))
	}

	// 後方互換性維持のため、デフォルトのCIDRを 10.251.0.0/16 から始めている
	// 作成するRuntime環境が増えたときに、無効なCIDR形式となることを防ぐため剰余をとっている
	defaultVpcCidrBlock = fmt.Sprintf("10.%d.0.0/16", (251+envIndex)%256)

	r := &RuntimeEnvironment{
		Name:                       runtimeName,
		BackendBucketName:          backendBucketName,
		LockTableName:              lockTableName,
		TerraformExecutionRoleName: terraformExecutionRoleName,
		AwsAccountID:               awsAccountID,
		AwsCredential:              credential,
		DefaultVpcCidrBlock:        defaultVpcCidrBlock,
	}

	return r
}

func (r *RuntimeEnvironment) AlreadyExistingEnvironment(ctx context.Context) error {
	logging.Infof("  [Runtime / %s] %s ロールの存在を確認します...", r.Name, r.TerraformExecutionRoleName)

	if exists, err := aws.ExistsIamRole(r.AwsCredential, r.TerraformExecutionRoleName); err != nil {
		return fmt.Errorf("[Runtime / %s] IAMへのアクセス時に、エラーが発生しました"+logging.ErrTraceDelimiter+"%v", r.Name, err)
	} else if exists {
		return fmt.Errorf("[Runtime / %s] %s ロールがすでに存在します", r.Name, r.TerraformExecutionRoleName)
	}

	logging.Infof("  [Runtime / %s] %s バケットの存在を確認します...", r.Name, r.BackendBucketName)

	if exists, err := aws.ExistsS3Bucket(r.AwsCredential, r.BackendBucketName); err != nil {
		return fmt.Errorf("[Runtime / %s] S3へのアクセス時に、エラーが発生しました"+logging.ErrTraceDelimiter+"%v", r.Name, err)
	} else if exists {
		return fmt.Errorf("[Runtime / %s] S3バケット %s がすでに存在します", r.Name, r.BackendBucketName)
	}

	logging.Infof("  [Runtime / %s] DynamoDB上の %s テーブルの存在を確認します...", r.Name, r.LockTableName)

	if exists, err := aws.ExistsDynamoDBTable(r.AwsCredential, r.LockTableName); err != nil {
		return fmt.Errorf("[Runtime / %s] DynamoDBへのアクセス時に、エラーが発生しました"+logging.ErrTraceDelimiter+"%v", r.Name, err)
	} else if exists {
		return fmt.Errorf("[Runtime / %s] DynamoDB上に %s テーブルがすでに存在しています", r.Name, r.LockTableName)
	}

	return nil
}

func (r *RuntimeEnvironment) CreateTerraformBackendStorage(ctx context.Context) error {
	logging.Infof("  [Runtime / %s] Terraform Backend用S3バケット %s を作成します", r.Name, r.BackendBucketName)

	if err := aws.CreateS3Bucket(r.AwsCredential, r.BackendBucketName, r.AwsCredential.Region); err != nil {
		return err
	}

	logging.Infof("  [Runtime / %s] Terraform Backend用S3バケット %s を作成しました", r.Name, r.BackendBucketName)

	return nil
}

func (r *RuntimeEnvironment) CreateExecutionRoleAndSetupBackend(ctx context.Context, deliveryEnvironment *DeliveryEnvironment) error {

	directory := filepath.Join("setup_terraform_accounts", "runtimes", r.Name)

	logging.Infof("  [Runtime / %s] Terraform構成ファイルを作成します", r.Name)

	if err := r.generateTerraformFiles(ctx, directory, deliveryEnvironment); err != nil {
		return err
	}

	logging.Infof("  [Runtime / %s] Terraform構成ファイルを作成しました", r.Name)

	logging.Infof("  [Runtime / %s] Terraform Backendの設定を行います", r.Name)

	if err := terraform.Init(r.AwsCredential, directory, "-backend-config=backend.config"); err != nil {
		return err
	}

	logging.Infof("  [Runtime / %s] Terraform Backendの設定を行いました", r.Name)

	logging.Infof("  [Runtime / %s] Terraform Backend用S3バケット %s をインポートします", r.Name, r.BackendBucketName)

	if err := terraform.Import(r.AwsCredential, directory, "-lock=false", "module.backend.aws_s3_bucket.tfstate", r.BackendBucketName); err != nil {
		return err
	}

	logging.Infof("  [Runtime / %s] Terraform Backend用S3バケット %s をインポートしました", r.Name, r.BackendBucketName)

	logging.Infof("  [Runtime / %s] Terraform実行用ロールの作成とDelivery環境との信頼関係の構築、State保存用のリソースを作成します", r.Name)

	if err := terraform.Apply(r.AwsCredential, directory, "-lock=false"); err != nil {
		return err
	}

	if output, err := terraform.Output(r.AwsCredential, directory); err != nil {
		return err
	} else {
		// 以下の形式の表記を、HCLでパースできないため（値が""で囲われていないことが原因）、文字列操作で代替する
		// terraform_execution_role_arn = arn:aws:iam::[Runtime環境のアカウントID]:role/TerraformExecutionRole
		re := regexp.MustCompile(`terraform_execution_role_arn\s+=\s+(.+)`)

		r.CreatedTerraformExecutionRoleArn = re.FindStringSubmatch(output)[1]
	}

	logging.Infof("  [Runtime / %s] Terraform実行用ロールの作成とDelivery環境との信頼関係の構築、State保存用のリソースを作成しました", r.Name)

	return nil
}

func (r *RuntimeEnvironment) newTemplateData(ctx context.Context) map[string]interface{} {
	data := map[string]interface{}{}

	data["EponaVersion"], _ = version.GetEponaVersion(ctx)
	data["TerraformVersion"], _ = version.GetTerraformVersion(ctx)
	data["AwsProviderVersion"], _ = version.GetAwsProviderVersion(ctx)
	data["ServiceName"], _ = service.GetName(ctx)
	data["Prefix"] = service.GetPrefix(ctx)
	data["Environment"] = r

	return data
}

func (r *RuntimeEnvironment) generateTerraformFiles(ctx context.Context, directory string, d *DeliveryEnvironment) error {
	mainData := r.newTemplateData(ctx)
	mainData["DeliveryEnvironment"] = d

	err := template.EvalTerraformTemplateWriteToFile(runtimeMainTfTemplate, mainData, filepath.Join(directory, "main.tf"))

	if err != nil {
		return fmt.Errorf("%sの作成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", "main.tf", err)
	}

	backendConfigData := r.newTemplateData(ctx)

	err = template.EvalTemplateWriteToFile(runtimeBackendConfigTemplate, backendConfigData, filepath.Join(directory, "backend.config"))

	if err != nil {
		return fmt.Errorf("%sの作成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", "backend.config", err)
	}

	err = template.EvalTerraformTemplateWriteToFile(runtimeOutputsTfTemplate, map[string]string{}, filepath.Join(directory, "outputs.tf"))

	if err != nil {
		return fmt.Errorf("%sの作成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", "outputs.tf", err)
	}

	versionsData := r.newTemplateData(ctx)

	err = template.EvalTerraformTemplateWriteToFile(runtimeVersionsTfTemplate, versionsData, filepath.Join(directory, "versions.tf"))

	if err != nil {
		return fmt.Errorf("%sの作成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", "versions.tf", err)
	}

	err = terraform.Format(directory)

	if err != nil {
		return fmt.Errorf("%s: terraform fmt時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", directory, err)
	}

	return nil
}

var (
	runtimeMainTfTemplate = `provider "aws" {
}

# Runtime環境上でのロール
module "terraformer_execution" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/new_environment/terraform_execution?ref={{.EponaVersion}}"

{{- if ne .Prefix ""}}
  prefix        = "{{.Prefix}}"
{{- end}}

{{- $envName := tfTitle .Environment.Name }}
  principals = ["{{index .DeliveryEnvironment.CreatedTerraformerArns $envName}}"]
}

module "backend" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/new_environment/backend?ref={{.EponaVersion}}"

  name          = "{{.ServiceName}}"
  environment   = "{{tfTitle .Environment.Name}}"
  force_destroy = false
}
`

	runtimeBackendConfigTemplate = `bucket         = "{{.Environment.BackendBucketName}}"
key            = "backend/terraform.tfstate"
encrypt        = true
dynamodb_table = "{{.Environment.LockTableName}}"
`

	runtimeOutputsTfTemplate = `output "terraform_execution_role_arn" {
  description = "Terraform実行ロールのARN"
  value       = module.terraformer_execution.arn
}
`

	runtimeVersionsTfTemplate = `terraform {
  required_version = "{{.TerraformVersion}}"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "{{.AwsProviderVersion}}"
    }
  }

  backend "s3" {
  }
}
`
)
