package terraformer

import (
	"context"
	"fmt"
	"os"
	"path/filepath"
	"regexp"

	"github.com/hashicorp/hcl/v2/hclparse"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/aws"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/logging"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/service"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/template"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/terraform"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/terraform/lang"
	"gitlab.com/eponas/epona-aws-starter-cli/internal/version"
)

type DeliveryEnvironment struct {
	AwsAccountID                     string
	AwsCredential                    *AwsCredential
	BackendBucketName                string
	StateBucketName                  string
	LockTableName                    string
	StateAccessRoleName              string
	TerraformExecutionRoleName       string
	DeliveryTerraformerName          string
	RuntimeNames                     []string // YAMLで定義した環境順で固定したいので、mapにはしない
	RuntimeAwsAccountIDs             []string
	RuntimeTerraformerNames          []string
	RuntimeStateBucketNames          []string
	RuntimeStateAccessRoleNames      []string
	TerraformerUserGroups            []string
	CreatedTerraformExecutionRoleArn string
	CreatedTerraformerArns           map[string]string
	CreatedTerraformerMappings       map[string]*TerraformerMapping
	DefaultVpcCidrBlock              string
}

type TerraformerMapping struct {
	TargetEnvironmentAwsAccountID string
	AwsCredential                 *AwsCredential
}

type DeliveryEnvironmentInitializer interface {
	AlreadyExistingEnvironment(ctx context.Context) error
	CreateTerraformBackendStorage(ctx context.Context) error
	CreateTerraformersAndSetupBackend(ctx context.Context, runtimeEnvironments []*RuntimeEnvironment) error
	AssignExecutionRoleToRuntimeTerraformers(ctx context.Context, runtimeEnvironments []*RuntimeEnvironment) error
	CreateTerraformersCredential(ctx context.Context) error
}

func NewDeliveryEnvitonment(serviceName string, awsAccountID string, credential *AwsCredential, runtimeNames []string, terraformerUserGroups []string) *DeliveryEnvironment {
	prefix := os.Getenv("EPONA_STARTER_PREFIX")

	var backendBucketName string
	var stateBucketName string
	var lockTableName string
	var stateAccessRoleName string
	var terraformExecutionRoleName string
	var deliveryTerraformerName string
	var runtimeTerraformerNames []string = []string{}
	var runtimeStateBucketNames []string = []string{}
	var runtimeStateAccessRoleNames []string = []string{}
	defaultVpcCidrBlock := "10.250.0.0/16"

	backendBucketName = fmt.Sprintf("%s-delivery-backend", serviceName)
	stateBucketName = fmt.Sprintf("%s-delivery-terraform-tfstate", serviceName)

	if prefix == "" {
		lockTableName = fmt.Sprintf("%s_terraform_tfstate_lock", serviceName)

		terraformExecutionRoleName = "TerraformExecutionRole"
		stateAccessRoleName = "DeliveryTerraformBackendAccessRole"

		deliveryTerraformerName = "DeliveryTerraformer"

		for _, rn := range runtimeNames {
			runtimeTerraformerNames = append(runtimeTerraformerNames, fmt.Sprintf("%sTerraformer", lang.Title(rn)))
			runtimeStateBucketNames = append(runtimeStateBucketNames, fmt.Sprintf("%s-%s-terraform-tfstate", serviceName, rn))
			runtimeStateAccessRoleNames = append(runtimeStateAccessRoleNames, fmt.Sprintf("%sTerraformBackendAccessRole", lang.Title(rn)))
		}
	} else {
		lockTableName = fmt.Sprintf("%s_terraform_tfstate_lock", serviceName)

		terraformExecutionRoleName = fmt.Sprintf("%sTerraformExecutionRole", lang.Title(prefix))
		stateAccessRoleName = fmt.Sprintf("%sDeliveryTerraformBackendAccessRole", lang.Title(prefix))

		deliveryTerraformerName = fmt.Sprintf("%sDeliveryTerraformer", lang.Title(prefix))

		for _, rn := range runtimeNames {
			runtimeTerraformerNames = append(runtimeTerraformerNames, fmt.Sprintf("%s%sTerraformer", lang.Title(prefix), lang.Title(rn)))
			runtimeStateBucketNames = append(runtimeStateBucketNames, fmt.Sprintf("%s-%s-terraform-tfstate", serviceName, rn))
			runtimeStateAccessRoleNames = append(runtimeStateAccessRoleNames, fmt.Sprintf("%s%sTerraformBackendAccessRole", lang.Title(prefix), lang.Title(rn)))
		}
	}

	d := &DeliveryEnvironment{
		AwsAccountID:                awsAccountID,
		AwsCredential:               credential,
		BackendBucketName:           backendBucketName,
		StateBucketName:             stateBucketName,
		LockTableName:               lockTableName,
		StateAccessRoleName:         stateAccessRoleName,
		TerraformExecutionRoleName:  terraformExecutionRoleName,
		DeliveryTerraformerName:     deliveryTerraformerName,
		RuntimeNames:                runtimeNames,
		RuntimeAwsAccountIDs:        []string{},
		RuntimeTerraformerNames:     runtimeTerraformerNames,
		RuntimeStateBucketNames:     runtimeStateBucketNames,
		RuntimeStateAccessRoleNames: runtimeStateAccessRoleNames,
		TerraformerUserGroups:       terraformerUserGroups,
		DefaultVpcCidrBlock:         defaultVpcCidrBlock,
	}

	return d
}

func (d *DeliveryEnvironment) AlreadyExistingEnvironment(ctx context.Context) error {
	logging.Infof("  [Delivery] %s ユーザーの存在を確認します...", d.DeliveryTerraformerName)

	if exists, err := aws.ExistsIamUser(d.AwsCredential, d.DeliveryTerraformerName); err != nil {
		return fmt.Errorf("[Delivery] IAMへのアクセス時に、エラーが発生しました"+logging.ErrTraceDelimiter+"%v", err)
	} else if exists {
		return fmt.Errorf("[Delivery] %s ユーザーがすでに存在します", d.DeliveryTerraformerName)
	}

	for _, rn := range d.RuntimeTerraformerNames {
		logging.Infof("  [Delivery] Runtime環境用 %s ユーザーの存在を確認します...", rn)

		if exists, err := aws.ExistsIamUser(d.AwsCredential, rn); err != nil {
			return fmt.Errorf("[Delivery] IAMへのアクセス時に、エラーが発生しました"+logging.ErrTraceDelimiter+"%v", err)
		} else if exists {
			return fmt.Errorf("[Delivery] Runtime環境用 %s ユーザーがすでに存在します", rn)
		}
	}

	logging.Infof("  [Delivery] %s ロールの存在を確認します...", d.TerraformExecutionRoleName)

	if exists, err := aws.ExistsIamRole(d.AwsCredential, d.TerraformExecutionRoleName); err != nil {
		return fmt.Errorf("[Delivery] IAMへのアクセス時に、エラーが発生しました"+logging.ErrTraceDelimiter+"%v", err)
	} else if exists {
		return fmt.Errorf("[Delivery] %s ロールがすでに存在します", d.TerraformExecutionRoleName)
	}

	logging.Infof("  [Delivery] %s バケットの存在を確認します...", d.BackendBucketName)

	if exists, err := aws.ExistsS3Bucket(d.AwsCredential, d.BackendBucketName); err != nil {
		return fmt.Errorf("[Delivery] S3へのアクセス時に、エラーが発生しました"+logging.ErrTraceDelimiter+"%v", err)
	} else if exists {
		return fmt.Errorf("[Delivery] S3バケット %s がすでに存在します", d.BackendBucketName)
	}

	logging.Infof("  [Delivery] %s バケットの存在を確認します...", d.StateBucketName)

	if exists, err := aws.ExistsS3Bucket(d.AwsCredential, d.StateBucketName); err != nil {
		return fmt.Errorf("[Delivery] S3へのアクセス時に、エラーが発生しました"+logging.ErrTraceDelimiter+"%v", err)
	} else if exists {
		return fmt.Errorf("[Delivery] S3バケット %s がすでに存在します", d.StateBucketName)
	}

	logging.Infof("  [Delivery] DynamoDB上の %s テーブルの存在を確認します...", d.LockTableName)

	if exists, err := aws.ExistsDynamoDBTable(d.AwsCredential, d.LockTableName); err != nil {
		return fmt.Errorf("[Delivery] DynamoDBへのアクセス時に、エラーが発生しました"+logging.ErrTraceDelimiter+"%v", err)
	} else if exists {
		return fmt.Errorf("[Delivery] DynamoDB上に %s テーブルがすでに存在しています", d.LockTableName)
	}

	logging.Infof("  [Delivery] %s ロールの存在を確認します...", d.StateAccessRoleName)

	if exists, err := aws.ExistsIamRole(d.AwsCredential, d.StateAccessRoleName); err != nil {
		return fmt.Errorf("[Delivery] IAMへのアクセス時に、エラーが発生しました"+logging.ErrTraceDelimiter+"%v", err)
	} else if exists {
		return fmt.Errorf("[Delivery] %s ロールがすでに存在します", d.StateAccessRoleName)
	}

	for i, runtimeStateBucketName := range d.RuntimeStateBucketNames {
		logging.Infof("  [Delivery] Runtime / %s 用 %s バケットの存在を確認します...", d.RuntimeNames[i], runtimeStateBucketName)

		if exists, err := aws.ExistsS3Bucket(d.AwsCredential, runtimeStateBucketName); err != nil {
			return fmt.Errorf("[Delivery] S3へのアクセス時に、エラーが発生しました"+logging.ErrTraceDelimiter+"%v", err)
		} else if exists {
			return fmt.Errorf("[Delivery] Runtime / %s 用 S3バケット %s がすでに存在します", d.RuntimeNames[i], runtimeStateBucketName)
		}
	}

	for i, runtimeStateAccessRoleName := range d.RuntimeStateAccessRoleNames {
		logging.Infof("  [Delivery] Runtime / %s 用 %s バケットの存在を確認します...", d.RuntimeNames[i], runtimeStateAccessRoleName)

		if exists, err := aws.ExistsIamRole(d.AwsCredential, runtimeStateAccessRoleName); err != nil {
			return fmt.Errorf("[Delivery] IAMへのアクセス時に、エラーが発生しました"+logging.ErrTraceDelimiter+"%v", err)
		} else if exists {
			return fmt.Errorf("[Delivery] Runtime / %s 用 %s ロールがすでに存在します", d.RuntimeNames[i], runtimeStateAccessRoleName)
		}
	}

	return nil
}

func (d *DeliveryEnvironment) CreateTerraformBackendStorage(ctx context.Context) error {
	logging.Infof("  [Delivery] Terraform Backend用S3バケット %s を作成します", d.BackendBucketName)

	if err := aws.CreateS3Bucket(d.AwsCredential, d.BackendBucketName, d.AwsCredential.Region); err != nil {
		return err
	}

	logging.Infof("  [Delivery] Terraform Backend用S3バケット %s を作成しました", d.BackendBucketName)

	return nil
}

func (d *DeliveryEnvironment) CreateTerraformersAndSetupBackend(ctx context.Context, runtimeEnvironments []*RuntimeEnvironment) error {
	directory := filepath.Join("setup_terraform_accounts", "delivery")

	logging.Info("  [Delivery] Terraform構成ファイルを作成します")

	if err := d.generateTerraformFiles(ctx, directory, runtimeEnvironments); err != nil {
		return err
	}

	logging.Info("  [Delivery] Terraform構成ファイルを作成しました")

	logging.Info("  [Delivery] Terraform Backendの設定を行います")

	if err := terraform.Init(d.AwsCredential, directory, "-backend-config=backend.config"); err != nil {
		return err
	}

	logging.Info("  [Delivery] Terraform Backendの設定を行いました")

	logging.Infof("  [Delivery] Terraform Backend用S3バケット %s をインポートします", d.BackendBucketName)

	if err := terraform.Import(d.AwsCredential, directory, "-lock=false", "module.delivery.aws_s3_bucket.backend_itself", d.BackendBucketName); err != nil {
		return err
	}

	logging.Infof("  [Delivery] Terraform Backend用S3バケット %s をインポートしました", d.BackendBucketName)

	logging.Info("  [Delivery] Terraform実行用ユーザーおよびState保存用のリソースを作成します")

	if err := terraform.Apply(d.AwsCredential, directory, "-lock=false"); err != nil {
		return err
	}

	if output, err := terraform.Output(d.AwsCredential, directory); err != nil {
		return err
	} else {
		// 以下の形式の表記を、HCLでパースできないため（値が""で囲われていないことが原因）、文字列操作で代替する
		// terraform_execution_role_arn = arn:aws:iam::[Delivery環境のアカウントID]:role/TerraformExecutionRole
		re := regexp.MustCompile(`terraform_execution_role_arn\s+=\s+(.+)`)
		d.CreatedTerraformExecutionRoleArn = re.FindStringSubmatch(output)[1]

		// TerraformerのARNは、HCLをパースして取得
		parser := hclparse.NewParser()

		outputAsHcl, _ := parser.ParseHCL([]byte(output), filepath.Join(directory, "main.tf"))
		output, _ := outputAsHcl.Body.JustAttributes()

		terraformerArn, _ := output["terraformer_arn"].Expr.Value(nil)

		d.CreatedTerraformerArns = map[string]string{}

		for env, value := range terraformerArn.AsValueMap() {
			d.CreatedTerraformerArns[env] = value.AsValueMap()["arn"].AsString()
		}
	}

	logging.Info("  [Delivery] Terraform実行用ユーザーおよびState保存用のリソースを作成しました")

	return nil
}

func (d *DeliveryEnvironment) newTemplateData(ctx context.Context) map[string]interface{} {
	data := map[string]interface{}{}

	data["EponaVersion"], _ = version.GetEponaVersion(ctx)
	data["TerraformVersion"], _ = version.GetTerraformVersion(ctx)
	data["AwsProviderVersion"], _ = version.GetAwsProviderVersion(ctx)
	data["ServiceName"], _ = service.GetName(ctx)
	data["Prefix"] = service.GetPrefix(ctx)
	data["Environment"] = d

	return data
}

func (d *DeliveryEnvironment) generateTerraformFiles(ctx context.Context, directory string, runtimeEnvironments []*RuntimeEnvironment) error {
	mainData := d.newTemplateData(ctx)
	mainData["RuntimeEnvironments"] = runtimeEnvironments

	err := template.EvalTerraformTemplateWriteToFile(deliveryMainTfTemplate, mainData, filepath.Join(directory, "main.tf"))

	if err != nil {
		return fmt.Errorf("%sの作成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", "main.tf", err)
	}

	backendConfigData := d.newTemplateData(ctx)

	err = template.EvalTemplateWriteToFile(deliveryBackendConfigTemplate, backendConfigData, filepath.Join(directory, "backend.config"))

	if err != nil {
		return fmt.Errorf("%sの作成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", "backend.config", err)
	}

	err = template.EvalTerraformTemplateWriteToFile(deliveryOutputsTfTemplate, map[string]string{}, filepath.Join(directory, "outputs.tf"))

	if err != nil {
		return fmt.Errorf("%sの作成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", "outputs.tf", err)
	}

	versionsData := d.newTemplateData(ctx)

	err = template.EvalTerraformTemplateWriteToFile(deliveryVersionsTfTemplate, versionsData, filepath.Join(directory, "versions.tf"))

	if err != nil {
		return fmt.Errorf("%sの作成時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", "versions.tf", err)
	}

	err = terraform.Format(directory)

	if err != nil {
		return fmt.Errorf("%s: terraform fmt時にエラーが発生しました"+logging.ErrTraceDelimiter+"%v", directory, err)
	}

	return nil
}

func (d *DeliveryEnvironment) AssignExecutionRoleToRuntimeTerraformers(ctx context.Context, runtimeEnvironments []*RuntimeEnvironment) error {
	directory := filepath.Join("setup_terraform_accounts", "delivery")

	logging.Info("  [Delivery] Terraform構成ファイルを再作成します")

	if err := d.generateTerraformFiles(ctx, directory, runtimeEnvironments); err != nil {
		return err
	}

	logging.Info("  [Delivery] Terraform構成ファイルを再作成しました")

	logging.Info("  [Delivery] 作成したTerraform実行用のユーザーが、各Runtime環境に作成したTerraform実行ロールを利用できるように設定します")

	if err := terraform.Apply(d.AwsCredential, directory); err != nil {
		return err
	}

	logging.Info("  [Delivery] 作成したTerraform実行用のユーザーが、各Runtime環境に作成したTerraform実行ロールを利用できるように設定しました")

	return nil
}

func (d *DeliveryEnvironment) CreateTerraformersCredential(ctx context.Context) error {

	logging.Info("  [Delivery] 各環境のTerraformer用のアクセスキーを作成します")

	d.CreatedTerraformerMappings = map[string]*TerraformerMapping{}

	logging.Infof("  [Delivery] Delivery環境用Terraform実行ユーザー %s のアクセスキーを作成します", d.DeliveryTerraformerName)

	if accessKey, err := aws.CreateAccessKey(d.AwsCredential, d.DeliveryTerraformerName); err != nil {
		return err
	} else {
		d.CreatedTerraformerMappings["delivery"] = &TerraformerMapping{
			TargetEnvironmentAwsAccountID: d.AwsAccountID,
			AwsCredential: &AwsCredential{
				AccessKeyID:     accessKey["AccessKeyId"],
				SecretAccessKey: accessKey["SecretAccessKey"],
				Region:          d.AwsCredential.Region,
			},
		}
	}

	logging.Infof("  [Delivery] Delivery環境用Terraform実行ユーザー %s のアクセスキーを作成しました", d.DeliveryTerraformerName)

	for i, n := range d.RuntimeNames {
		terraformerName := d.RuntimeTerraformerNames[i]
		logging.Infof("  [Delivery] Runtime / %s 環境用Terraform実行ユーザー %s のアクセスキーを作成します", n, terraformerName)

		if accessKey, err := aws.CreateAccessKey(d.AwsCredential, terraformerName); err != nil {
			return err
		} else {
			d.CreatedTerraformerMappings[n] = &TerraformerMapping{
				TargetEnvironmentAwsAccountID: d.RuntimeAwsAccountIDs[i],
				AwsCredential: &AwsCredential{
					AccessKeyID:     accessKey["AccessKeyId"],
					SecretAccessKey: accessKey["SecretAccessKey"],
					Region:          d.AwsCredential.Region,
				},
			}
		}

		logging.Infof("  [Delivery] Runtime / %s 環境用Terraform実行ユーザー %s のアクセスキーを作成しました", n, terraformerName)
	}

	logging.Info("  [Delivery] 各環境のTerraformer用のアクセスキーを作成しました")

	return nil
}

var (
	deliveryMainTfTemplate = `provider "aws" {
}

module "delivery" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/new_environment/delivery?ref={{.EponaVersion}}"

{{- if ne .Prefix ""}}
  prefix        = "{{.Prefix}}"
{{- end}}

  name          = "{{.ServiceName}}"
  force_destroy = false
  runtime_accounts = {
{{- range $runtimeEnvironment := .RuntimeEnvironments}}
    "{{tfTitle $runtimeEnvironment.Name}}" = "{{$runtimeEnvironment.AwsAccountID}}"
{{- end}}
  }

{{- if .Environment.TerraformerUserGroups }}
  user_groups = [
  {{- range $group := .Environment.TerraformerUserGroups }}
    "{{$group}}",
  {{- end}}
  ]
{{- end}}
}

module "cross_account_assume_role" {
  source = "git::https://gitlab.com/eponas/epona.git//modules/aws/new_environment/cross_account_assume_policy?ref={{.EponaVersion}}"

{{- if ne .Prefix ""}}
  prefix        = "{{.Prefix}}"
{{- end}}

  execution_role_map = {
{{- range  $index, $runtimeEnvironment := .RuntimeEnvironments }}
{{- if $runtimeEnvironment.CreatedTerraformExecutionRoleArn }}
    "{{tfTitle $runtimeEnvironment.Name}}" = {{$runtimeEnvironment.CreatedTerraformExecutionRoleArn}}
{{- end}}
{{- end}}
  }

  terraformer_users = module.delivery.terraformer_users
}
`

	deliveryBackendConfigTemplate = `bucket         = "{{.Environment.BackendBucketName}}"
key            = "backend/terraform.tfstate"
encrypt        = true
dynamodb_table = "{{.Environment.LockTableName}}"
`

	deliveryOutputsTfTemplate = `output "terraformer_arn" {
  description = "Terraform実行用ユーザのARN"
  value       = module.delivery.terraformer_users
}

output "terraform_execution_role_arn" {
  description = "Terraform実行ロールのARN"
  value       = module.delivery.terraform_execution_role_arn
}
`

	deliveryVersionsTfTemplate = `terraform {
  required_version = "{{.TerraformVersion}}"

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "{{.AwsProviderVersion}}"
    }
  }

  backend "s3" {
  }
}
`
)
