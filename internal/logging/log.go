package logging

import (
	"fmt"
	"io"
	"log"
	"os"
	"path/filepath"
	"time"
)

const (
	ErrTraceDelimiter = "\n    └─ "
)

var (
	logFilePath             string
	logFileAvailable        bool
	standardOutLogger       *log.Logger
	fileLogger              *log.Logger
	aggregateLoggers        []*log.Logger
	DirectStandardOutLogger io.Writer
	DirectFileLogger        io.Writer
)

func InitializeLoggers(subcommand string) {
	standardOutLogger = log.New(os.Stdout, "", log.LstdFlags)

	DirectStandardOutLogger = os.Stdout

	logsDir := "logs"
	if _, err := os.Stat(logsDir); os.IsNotExist(err) {
		if err := os.MkdirAll(logsDir, 0755); err != nil {
			standardOutLogger.Fatalf("ディレクトリ[%s]が作成できませんでした"+ErrTraceDelimiter+"%v", logsDir, err)
		}
	}

	gitIgnoreFilePath := filepath.Join(logsDir, ".gitignore")
	gitIgnoreFile, err := os.OpenFile(gitIgnoreFilePath, os.O_CREATE|os.O_TRUNC|os.O_RDWR, 0644)

	if err != nil {
		standardOutLogger.Fatalf("ファイル %s を作成できませんでした"+ErrTraceDelimiter+"%v", gitIgnoreFilePath, err)
	}

	defer gitIgnoreFile.Close()

	fmt.Fprintln(gitIgnoreFile, "*.log")

	now := time.Now()

	logFileName := fmt.Sprintf("%s-%s.log", subcommand, now.Format("20060102-150405"))
	logFilePath = filepath.Join(logsDir, logFileName)
	logFile, err := os.OpenFile(logFilePath, os.O_CREATE|os.O_APPEND|os.O_RDWR, 0644)

	if err != nil {
		standardOutLogger.Fatalf("ログファイル[%s]が作成できませんでした"+ErrTraceDelimiter+"%v", logFilePath, err)
	}

	usingStdOutOnly()

	Infof("ログはファイル %s に記録されます", logFilePath)

	logFileAvailable = true

	DirectFileLogger = logFile

	fileLogger = log.New(logFile, "", log.LstdFlags)

	usingFullLogger()
}

func InitializeStdOutLogger() {
	standardOutLogger = log.New(os.Stdout, "", log.LstdFlags)
	DirectStandardOutLogger = os.Stdout

	usingStdOutOnly()
}

func usingStdOutOnly() {
	aggregateLoggers = []*log.Logger{
		standardOutLogger,
	}
}

func usingFullLogger() {
	aggregateLoggers = []*log.Logger{
		standardOutLogger,
		fileLogger,
	}
}

func Debug(message interface{}) {
	simpleLog("DEBUG", message)
}

func Info(message interface{}) {
	simpleLog("INFO", message)
}

func Error(message interface{}) {
	simpleLog("ERROR", message)
}

func Fatal(message interface{}) {
	log.Fatalln(message)
}

func Debugf(messageFormat string, args ...interface{}) {
	simpleLogf("DEBUG", messageFormat, args...)
}

func Infof(messageFormat string, args ...interface{}) {
	simpleLogf("INFO", messageFormat, args...)
}

func Errorf(messageFormat string, args ...interface{}) {
	simpleLogf("ERROR", messageFormat, args...)
}

func Fatalf(messageFormat string, args ...interface{}) {
	for _, logger := range aggregateLoggers {
		logger.Fatalf("[FATAL]"+messageFormat, args...)
	}
}

func simpleLog(level string, message interface{}) {
	for _, logger := range aggregateLoggers {
		logger.Println("["+level+"]", message)
	}
}

func simpleLogf(level string, messageFormat string, args ...interface{}) {
	for _, logger := range aggregateLoggers {
		logger.Printf("["+level+"] "+messageFormat, args...)
	}
}

func PrintLogFileLocationAndDetail() {
	if logFileAvailable {
		usingStdOutOnly()
		defer usingFullLogger()
	}

	Info("実行時の詳細な情報は、ログファイルを確認してください")
	Infof("ログはファイル %s に記録されています", logFilePath)
}

func PrintLogFileLocation() {
	if logFileAvailable {
		usingStdOutOnly()
		defer usingFullLogger()
	}

	Infof("ログはファイル %s に記録されています", logFilePath)
}
