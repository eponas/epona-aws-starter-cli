#!/bin/bash -eu

workdir=dist-licenses # workdir=$(mktemp -d) とすると goxz でエラーが発生してしまう
rm -rf ${workdir}
mkdir -p ${workdir}
go-licenses save . --force --save_path ${workdir} --alsologtostderr

# go-licenses でソースをダウンロードすることになる場合、当該ファイルのパーミッションが
# 0666 になっており、結果として再実行ができなくなるというバグがある。
#   see: https://github.com/google/go-licenses/issues/11
# 暫定対応として、強制的に書き込み権限を付与する
chmod -R +w ${workdir}
goxz \
    -d dist \
    -n ${NAME} \
    -pv ${VERSION} \
    -build-ldflags \'"${LDFLAGS}"\' \
    -include "$(find ${workdir} -type f)"
