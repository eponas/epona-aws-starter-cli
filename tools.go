// +build tools

package main

import (
	// コマンドまでのパスを書く
	_ "github.com/Songmu/goxz/cmd/goxz"
	_ "github.com/boumenot/gocover-cobertura"
	_ "github.com/google/go-licenses"
)
