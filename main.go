package main

import "gitlab.com/eponas/epona-aws-starter-cli/cmd"

func main() {
	cmd.RunStarter()
}
