# Epona AWS Starter CLI

- `develop branch`: [![coverage report](https://gitlab.com/eponas/epona-aws-starter-cli/badges/develop/coverage.svg)](https://gitlab.com/eponas/epona-aws-starter-cli/-/commits/develop)

## About Epona AWS Starter CLI

Epona AWS Starter CLIは、Eponaを利用するための環境構築を簡易にすることを目的としたツールです。  
また[Epona AWS Getting Started](https://eponas.gitlab.io/epona_aws_getting_started/)が想定する環境の一部を構築し、Eponaに簡単に慣れ親しむこともできます。

Epona AWS Starter CLIを使用することで、以下の操作を容易に実行できます。

- Eponaモジュールを使用するための[Terraform実行環境のセットアップ](https://eponas.gitlab.io/epona_aws_getting_started/guide/1_environment_setup/#terraform%E5%AE%9F%E8%A1%8C%E7%92%B0%E5%A2%83%E3%82%92%E3%82%BB%E3%83%83%E3%83%88%E3%82%A2%E3%83%83%E3%83%97%E3%81%99%E3%82%8B)
- Epona AWS Getting Startedで想定する、[一部のpatternモジュールの適用](https://eponas.gitlab.io/epona_aws_getting_started/guide/2_apply_patterns/)

---

:information_source:   
現時点のEpona AWS Starter CLIでは、アプリケーションのCI/CDパイプラインの構築をターゲットにしています。  
それ以外のアクティビティも含めて全体を見たい場合は、[Epona AWS Getting Started](https://eponas.gitlab.io/epona_aws_getting_started/)を参照してください。

---

実行結果として、構築された環境に加えて以下も得ることができます。

- Eponaのpatternモジュールを使用したTerraform構成ファイル（設定ファイルより自動生成）

Epona AWS Starter CLIは、自動生成されたTerraform構成ファイルをそのまま使用して環境を構築しています。  
また環境構築後、ユーザーは生成されたTerraform構成ファイルを修正してTerraformを実行する、もしくは出力されたファイルを参考にEponaを利用することを想定しています。

Epona AWS Starter CLIは、Epona導入をするための環境構築の難易度の低減、Eponaに慣れ親しむための最初の敷居を下げることに特化しています。

このため、以下のような事項はEpona AWS Starter CLIの機能からはスコープアウトしています。

- 初期構築後の環境変更
- 構築される環境のカスタマイズ

利用の敷居を下げるため、必要最低限の設定のみが可能となっています。  
これは、設定可能な項目を増やしてカスタマイズ可能にすると、Epona AWS Starter CLIへの習熟度が要求されるようになるからです。  
また、ツールによる過度な環境の抽象化は、Eponaのコンセプトである[Self-Service](https://eponas.gitlab.io/epona/#self-service)からの逸脱を促進してしまいます。

それでは、目的に対して本末転倒な結果となります。

初期構築後は、Epona利用者が直接Terraform構成ファイルを編集、適用して運用してください。

---

:warning:  
Epona AWS Starter CLIが生成するTerraform構成ファイルは、そのままの状態での本番環境利用は想定していません。  
本番環境で利用する場合、各ファイルのパラメーターをサービスに見合った内容へ見直しを行い、環境へ変更を反映してください。

## System Requirements

[Epona AWS Getting Started](https://eponas.gitlab.io/epona_aws_getting_started/#system-requirements)にほぼ準じます。

Epona AWS Starter CLIを使用するには、以下のソフトウェアが必要です。

- AWS CLI v2
- [Terraform 0.14.10](https://releases.hashicorp.com/terraform/0.14.10/)

Epona AWS Starter CLIは、これらのソフトウェアをOSのシェルを使用して実行します。  
事前にインストールを行い、コマンドを実行できる状態にしてください。

:information_source: Terraformについては、指定のバージョン以外は利用できません。

それ以外の前提条件は、Epona AWS Starter CLIを使った環境構築をどこまで実施するかで変化します。

---

Epona AWS Getting Startedで想定する、一部のpatternモジュールの適用まで実施する場合は、以下が必要です。

- [AWS](https://aws.amazon.com/jp/)
  - 最低でも2つのAWSアカウント（Delivery環境 × 1、Runtime環境 × 1）が必要です
    - それぞれに、管理者権限を持ったIAMユーザーを用意してください
    - ※ 使用するリージョンは、アジアパシフィック (東京)(`ap-northeast-1`)としてください
  - Amazon SESで検証済みのEメールアドレスを保持していること
  - Amazon Route 53を使った独自ドメインを取得していること
- SaaS
  - [GitLab.com](https://gitlab.com/)
    - SaaS以外に利用できるGitLab環境がある場合は、そちらでも構いません
  - [Slack](https://slack.com/intl/ja-jp/)または[Microsoft Teams](https://www.microsoft.com/ja-jp/microsoft-365/microsoft-teams/group-chat-software)

---

Eponaモジュールを使用するための、Terraform実行環境のセットアップまでに留める場合は、以下が必要です。

- [AWS](https://aws.amazon.com/jp/)
  - 最低でも2つのAWSアカウント（Delivery環境 × 1、Runtime環境 × 1）が必要です
    - それぞれに、管理者権限を持ったIAMユーザーを用意してください
    - ※ 使用するリージョンは、`config-gen`が生成する設定ファイルを変更することで選択可能です

## Set up

Epona AWS Starter CLIは、[Releases](https://gitlab.com/eponas/epona-aws-starter-cli/-/releases)からダウンロードできます。

Windows、 macOS、LinuxそれぞれのOS用にバイナリがあります。  
ダウンロードした圧縮ファイルの展開後、環境変数`PATH`で実行パスを解決可能にしてください。

## Usage

Epona AWS Starter CLIは、以下の3つのコマンドを順に使用します。

1. `config-gen` … Epona AWS Starter CLIを使い始めるための設定ファイルを作成する
1. `init` … Eponaを使用するためのTerraform実行環境を構築する
1. `apply` … Eponaのpatternモジュールを組み込んだファイルを生成し、Terraformを実行して環境構築する

最後の`apply`を除いて、各コマンドは次に実行するコマンドのための設定ファイルを出力します。

各コマンドの使い方は、コマンドのヘルプを確認してください。

```shell
## Epona AWS Starter CLI全体のヘルプ
$ ./epona-aws-starter-cli -h


## config-genのヘルプ
$ ./epona-aws-starter-cli config-gen -h


## initのヘルプ
$ ./epona-aws-starter-cli init -h


## applyのヘルプ
$ ./epona-aws-starter-cli apply -h
```

出力された設定ファイルのパスは、実行ログに表示されます。  
また、設定ファイルは修正が必要になりますが、その意味は設定ファイル内にコメントとして記載されています。

---
:information_source:

プロキシ配下で本ツールを実行する場合は、プロキシサーバーの設定が必要になります。
[HTTPプロキシを使用する](https://docs.aws.amazon.com/ja_jp/cli/latest/userguide/cli-configure-proxy.html)を参考に、お使いのOSに応じてプロキシサーバーを設定してください。

Windowsで `setx` コマンドで設定した環境変数は、 `setx` コマンドを実施したターミナルでは参照できません。  
そのため `setx` コマンドで環境変数を設定した後で、新たなターミナルで本ツールを利用してください。

---
:warning:

Windowsの場合、Cドライブ直下にフォルダを作成し、当該フォルダで本ツールを実行することをお勧めします。
以下の例では、`epona-aws-starter-cli`フォルダ配下にて実行しています。

```shell
C:\epona-aws-starter-cli>epona-aws-starter-cli.exe init testdata/config-init.yml
```

これは、Windowsで`init`等実行した際、OSのパス長制限に起因して実行に失敗するケースがあるためです。

---

まずはEpona AWS Starter CLIを実行してみて、実行ログと出力された設定ファイルを見てみてください。

`apply`の実行後は、[Deploy Application](#deploy-application)に進みましょう。

## Deploy Application

Epona AWS Starter CLIを使って`apply`まで実行すると、アプリケーションをデプロイ可能な環境が構築されます。  
ただし、`apply`の実行時点ではまだアプリケーションはデプロイされていません。  
構築された環境にアプリケーションをデプロイするまでにはいくつかステップがあり、本章ではその手順を説明します。

:information_source: 厳密には、`apply`後は[ダミーのコンテナが動作している](https://eponas.gitlab.io/epona/guide/patterns/aws/public_traffic_container_service/#%E3%82%BF%E3%82%B9%E3%82%AF%E5%AE%9A%E7%BE%A9%E3%81%A7%E4%BD%BF%E7%94%A8%E3%81%99%E3%82%8B%E3%82%B3%E3%83%B3%E3%83%86%E3%83%8A%E3%81%AB%E3%81%A4%E3%81%84%E3%81%A6)状態になります。

### Deploy Target

Epona AWS Stater CLIを使用すると、Fintanの[SPA + REST API構成のサービス開発リファレンス](https://fintan.jp/?p=5952)で公開されている`example-chat`アプリケーションを動作させることを想定した環境が構築されます。

本章の手順に従い、アプリケーションをデプロイ後にブラウザでアクセスすると、チャットアプリケーションが動作していることを確認できるでしょう。

:information_source:

- アクセス先のURLは、`https://[サービス名].[Runtime環境に応じたRoute53のドメイン名]`となります
- `[サービス名]`は、Epona AWS Starter CLIのオプションや設定ファイルで指定したサービス名です
- `[Runtime環境に応じたRoute53のドメイン名]`は、`apply`実行時に設定ファイルで指定したドメイン名です

### Step to Deploy

`example-chat`をデプロイするには、以下の手順を実施します。

1. GitLabに`example-chat`用のリポジトリを作成する
1. `example-chat`のソースコードをGitHubより取得して、GitLabリポジトリに登録する
1. リポジトリ内にあるGitLab CI/CDの設定ファイル（`.gitlab-ci.yml`）を修正して、GitLabリポジトリに反映する
1. GitLab CI/CDのパイプラインで使用する環境変数を登録する
1. Runtime環境単位のブランチを作成して、バックエンド用アプリケーションの定義ファイルを登録する
1. デプロイ対象のRuntime環境へアクセスして、`example-chat`がデプロイされたことを確認する

:information_source: バックエンド用アプリケーションの定義ファイルの内容は、Runtime環境単位に異なります。

#### GitLabに`example-chat`用のリポジトリを作成する

以下のドキュメントを参考に、GitLab上に新規リポジトリを作成してください。  
[Working with projects](https://docs.gitlab.com/ee/user/project/working_with_projects.html)

:information_source: リポジトリ名称は任意で指定できますが、ここでは`example-chat`という名称で作成します。

#### `example-chat`のソースコードをGitHubより取得して、作成したGitLabリポジトリに登録する

GitLabのリポジトリを作成した後は、デプロイするアプリケーションのソースコードをリポジトリに追加します。

`example-chat`のソースコードは、GitHubリポジトリに登録されていますので、以降の手順に従ってGitLabリポジトリに登録してください。  
[Fintan-contents/example-chat・GitHub](https://github.com/Fintan-contents/example-chat)

GitHubの`example-chat`のコードをcloneします。  
:information_source: この手順では、`example-chat`の`1.1`を使用します。

```bash
$ git clone -b 1.1 https://github.com/Fintan-contents/example-chat.git
$ cd example-chat
```

cloneしたリポジトリに対して、GitLabリポジトリをoriginに登録してpushします。

```bash
$ git remote rename origin fintan-contents
$ git remote add origin [作成したGitLabのリポジトリURL]
$ git checkout -b master
$ git push origin master
```

これで、`example-chat`のソースコードがGitLabリポジトリに格納されました。

#### リポジトリ内にあるGitLab CI/CD の設定ファイル（`.gitlab-ci.yml`）を修正して、GitLabリポジトリに反映する

`example-chat`をデプロイするためには、GitLab CI/CDを利用してデプロイ対象となるアーティファクトを作成する必要があります。

:information_source:  
`example-chat`アプリケーションでは、バックエンドはコンテナ化されAmazon ECRへ、フロントエンドはAmazon S3へPushされます。  
いずれもDelivery環境です。

GitLab CI/CDのパイプライン構成は、`.gitlab-ci.yml`という設定ファイルで定義します。

GitHubから取得した`example-chat`のリポジトリ内には、すでに`.gitlab-ci.yml`ファイルが格納されています。  
ただし、この`.gitlab-ci.yml`ファイルの内容は構築した環境に合わせて修正する必要があります。

以下に、GitHub上に存在する`.gitlab-ci.yml`ファイルを編集し、コメントを加えたものを記載しています。  
格納されている`.gitlab-ci.yml`ファイルの記述内容を全て、以下のファイル名をクリックして展開された内容に置き換えてください。

また、置き換えた内容を見てみるといくつかの`TODO`コメントが含まれていることが確認できるでしょう。  
この`TODO`コメントの記載内容を見て、Epona AWS Starter CLIを実行した時の設定を元に`.gitlab-ci.yml`ファイルを修正してください。

<details>

<summary> `.gitlab-ci.yml` </summary>

```yaml
stages:
  - test
  - integration-test
  - build
  - deploy
  - build-for-aws
  - deploy-deployment-setting
  - deploy-to-aws
variables:
  # TODO: 以下の [Delivery環境のAWSアカウントID] に、Delivery環境のAWSアカウントIDを入力してください
  ECR_REGISTRY: [Delivery環境のAWSアカウントID].dkr.ecr.ap-northeast-1.amazonaws.com

test-frontend:
  image: node:12.16.1-slim
  stage: test
  script:
    - .ci/test-frontend.sh
  artifacts:
    reports:
      junit:
        - frontend/junit.xml

integration-test-backend:
  image: maven:3.6.3-jdk-11-slim
  stage: integration-test
  variables:
    POSTGRES_PASSWORD: "example"
    NABLARCH_DB_URL: "jdbc:postgresql://postgres:5432/postgres"
    NABLARCH_LETTUCE_SIMPLE_URI: "redis://redis:6379"
    MAIL_SMTP_HOST: "mail"
    MAIL_SMTP_PORT: "1025"
    MAIL_SMTP_USER: "user"
    MAIL_SMTP_PASSWORD: "password"
    AWS_S3_ENDPOINTOVERRIDE: "http://minio:9000"
  services:
    - name: postgres:latest
    - name: redis:latest
    - name: minio/minio:latest
      alias: minio
      command: ["--compat", "server", "/data"]
    - name: mailhog/mailhog:latest
      alias: mail
  script:
    - .ci/integration-test-backend.sh
  artifacts:
    reports:
      junit:
        - backend/target/surefire-reports/TEST-*.xml
        - backend/target/failsafe-reports/TEST-*.xml

integration-test-notifier:
  image: maven:3.6.3-jdk-11-slim
  stage: integration-test
  variables:
    NABLARCH_LETTUCE_SIMPLE_URI: "redis://redis:6379"
  services:
    - name: redis:latest
  script:
    - .ci/integration-test-notifier.sh
  artifacts:
    reports:
      junit:
        - notifier/target/surefire-reports/TEST-*.xml
        - notifier/target/failsafe-reports/TEST-*.xml

deploy-backend:
  image: maven:3.6.3-jdk-11-slim
  stage: deploy
  only:
    refs:
      - master
      # TODO: 以下に、 `config-gen` 実行時に `-runtime-name` で指定した Runtime環境名 を列挙してください
      - [Runtime環境名1]
      - [Runtime環境名2]
    changes:
      - backend/**/*
  script:
    - .ci/deploy-backend.sh

deploy-notifier:
  image: maven:3.6.3-jdk-11-slim
  stage: deploy
  only:
    refs:
      - master
      # TODO: 以下に、 `config-gen` 実行時に `-runtime-name` で指定した Runtime環境名 を列挙してください
      - [Runtime環境名1]
      - [Runtime環境名2]
    changes:
      - notifier/**/*
  script:
    - .ci/deploy-notifier.sh

deploy-nginx:
  image: docker:19.03.13
  stage: deploy
  services:
    - docker:19.03.13-dind
  only:
    refs:
      - master
      # TODO: 以下に、 `config-gen` 実行時に `-runtime-name` で指定した Runtime環境名 を列挙してください
      - [Runtime環境名1]
      - [Runtime環境名2]
    changes:
      - docker/nginx/**/*
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - .ci/deploy-nginx.sh

.deploy-aws:
  image: docker:19.03.13
  stage: deploy-to-aws
  # Epona の提供する GitLab Runner からであれば credential なしで ECR に PUSH 可能
  tags:
    - epona
  variables:
    AWS_DEFAULT_REGION: $EPONA_AWS_DEFAULT_REGION
  services:
    - name: docker:19.03.13-dind
  only:
    refs:
      # TODO: 以下に、 `config-gen` 実行時に `-runtime-name` で指定した Runtime環境名 を列挙してください
      - [Runtime環境名1]
      - [Runtime環境名2]
  before_script:
    - .ci/install-awscliv2-on-alpine.sh
    - SHORTEN_SERVICE_NAME=${SERVICE_NAME:0:8}
    - SHORTEN_ENV_NAME=${CI_COMMIT_REF_NAME:0:7}

deploy-backend-to-ecr:
  extends: .deploy-aws
  variables:
    IMAGE_PUSHED_TO_ECR: $CI_REGISTRY_IMAGE/example-chat-backend:$CI_COMMIT_REF_NAME
    ECR_REPOSITORY: ${ECR_REGISTRY}/chat-example-backend
  only:
    changes:
      - backend/**/*
  script:
    - .ci/deploy-to-ecr.sh

deploy-notifier-to-ecr:
  extends: .deploy-aws
  variables:
    IMAGE_PUSHED_TO_ECR: $CI_REGISTRY_IMAGE/example-chat-notifier:$CI_COMMIT_REF_NAME
    ECR_REPOSITORY: ${ECR_REGISTRY}/chat-example-notifier
  only:
    changes:
      - notifier/**/*
  script:
    - .ci/deploy-to-ecr.sh

.deploy-deployment-settings:
  image:
    name: amazon/aws-cli:2.1.4
    entrypoint: [""]
  stage: deploy-deployment-setting
  tags:
    - epona
  only:
    refs:
      # TODO: 以下に、 `config-gen` 実行時に `-runtime-name` で指定した Runtime環境名 を列挙してください
      - [Runtime環境名1]
      - [Runtime環境名2]
  variables:
    AWS_DEFAULT_REGION: $EPONA_AWS_DEFAULT_REGION
    DEPLOYMENT_SETTING_NAME: settings.zip
  before_script:
    - SHORTEN_ENV_NAME=${CI_COMMIT_REF_NAME:0:7}

deploy-backend-deployment-settings:
  extends: .deploy-deployment-settings
  variables:
    SETTING_DIR: backend/deploy
  only:
    changes:
      - backend/**/*
  script:
    - export S3_BUCKET_NAME=${SERVICE_NAME%-}-${SHORTEN_ENV_NAME%-}-chat-back-pipeline-source
    - .ci/deploy-settings.sh

deploy-notifier-deployment-settings:
  extends: .deploy-deployment-settings
  variables:
    SETTING_DIR: notifier/deploy
  only:
    changes:
      - notifier/**/*
  script:
    - export S3_BUCKET_NAME=${SERVICE_NAME%-}-${SHORTEN_ENV_NAME%-}-chat-ntfr-pipeline-source
    - .ci/deploy-settings.sh

deploy-nginx-backend-to-ecr:
  extends: .deploy-aws
  variables:
    IMAGE_PUSHED_TO_ECR: $CI_REGISTRY_IMAGE/nginx-backend:$CI_COMMIT_REF_NAME
    ECR_REPOSITORY: ${ECR_REGISTRY}/backend-nginx
  only:
    refs:
      # TODO: 以下に、 `config-gen` 実行時に `-runtime-name` で指定した Runtime環境名 を列挙してください
      - [Runtime環境名1]
      - [Runtime環境名2]
    changes:
      - docker/nginx/**/*
      - docker/nginx/*
  script:
    - .ci/deploy-to-ecr.sh

deploy-nginx-notifier-to-ecr:
  extends: .deploy-aws
  variables:
    IMAGE_PUSHED_TO_ECR: $CI_REGISTRY_IMAGE/nginx-notifier:$CI_COMMIT_REF_NAME
    ECR_REPOSITORY: ${ECR_REGISTRY}/notifier-nginx
  only:
    refs:
      # TODO: 以下に、 `config-gen` 実行時に `-runtime-name` で指定した Runtime環境名 を列挙してください
      - [Runtime環境名1]
      - [Runtime環境名2]
    changes:
      - docker/nginx/**/*
      - docker/nginx/*
  script:
    - .ci/deploy-to-ecr.sh

build-frontend:e2e:
  image: node:12.16.1-slim
  stage: build
  only:
    refs:
      - master
      # TODO: 以下に、 `config-gen` 実行時に `-runtime-name` で指定した Runtime環境名 を列挙してください
      - [Runtime環境名1]
      - [Runtime環境名2]
  variables:
    REACT_APP_BACKEND_BASE_URL: "http://example-chat-backend:8080"
  script:
    - .ci/build-frontend.sh
  artifacts:
    paths:
      - frontend/build

deploy-frontend:e2e:
  image: docker:19.03.12
  stage: deploy
  only:
    refs:
      - master
      # TODO: 以下に、 `config-gen` 実行時に `-runtime-name` で指定した Runtime環境名 を列挙してください
      - [Runtime環境名1]
      - [Runtime環境名2]
  services:
    - docker:19.03.12-dind
  before_script:
    - docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY
  script:
    - .ci/deploy-frontend.sh
  dependencies:
    - build-frontend:e2e


# TODO: `config-gen` 実行時に `-runtime-name` で指定した runtime環境の数だけ、 `build-frontend:aws:[Runtime環境名]` のジョブを作成してください
# ジョブ中の[Runtime環境名]は、 ジョブ名に含めた [Runtime環境名] に合わせて修正してください
build-frontend:aws:[Runtime環境名]:
  image: node:12.16.1-slim
  stage: build-for-aws
  only:
    refs:
      # TODO: ジョブ名で指定した `[Runtime環境名]` を1つ指定してください
      - [Runtime環境名]
    changes:
      - frontend/**/*
  variables:
    # TODO: runtime環境に合わせて、構築されたバックエンドアプリケーションのURLを指定してください
    # 記載に必要なホスト名は、自動生成された `runtimes/[runtime環境名]/public_traffic_container_service_backend/main.tf` の `dns.record_name` に記載されています。
    # 例: https://myservice-chat-example.myservice.example.com
    REACT_APP_BACKEND_BASE_URL: https://[バックエンドアプリケーションのホスト名]
  script:
    - .ci/build-frontend.sh
  artifacts:
    paths:
      - frontend/build

deploy-frontend-to-s3:
  extends: .deploy-aws
  variables:
    ARCHIVE_FILE_NAME: source.zip
  only:
    refs:
      # TODO: 以下に、 `config-gen` 実行時に `-runtime-name` で指定した Runtime環境名 を列挙してください
      - [Runtime環境名1]
      - [Runtime環境名2]
    changes:
      - frontend/**/*
  script:
    - export PUSH_TO_S3_BUCKET_NAME=${SHORTEN_SERVICE_NAME%-}-${SHORTEN_ENV_NAME%-}-static-resource
    - .ci/deploy-to-s3.sh
  dependencies:
    # TODO: 以下に、 build-frontend:aws:[runtime環境] で作成したジョブを列挙してください
    # 例: build-frontend:aws:production
    - build-frontend:aws:[Runtime環境名1]
    - build-frontend:aws:[Runtime環境名2]

cache:
  key:
    files:
      - frontend/package-lock.json
      - backend/pom.xml
  policy: pull-push
  paths:
    - .npm
    - .m2
```

</details>

修正後、`.gitlab-ci.yml`ファイルをコミットしてGitLabリポジトリの`master`ブランチに反映してください。

```bash
$ git add .gitlab-ci.yml
$ git commit -m "[任意のコミットメッセージを指定してください]"
$ git push origin master
```

#### GitLab CI/CDのパイプラインで使用する環境変数を登録する

GitLab CI/CDの構成ファイル（`.gitlab-ci.yml`）が修正できたら、GitLab CI/CDで利用する変数を設定します。  
ここで設定した変数は、`.gitlab-ci.yml`内で環境変数として利用しています。

作成したGitLabリポジトリをブラウザで開き、画面左のメニューから以下項目を開いてください。

`Settings` > `CI/CD`

ページに表示される`Variables`の右に表示されている`Expand`のボタンをクリックし、折り畳まれていた表示を展開します。  
展開された内容に表示された `Add Variable` のボタンをクリックし、以下のように設定してください。

|Key|Value|Type|Environment Scope|Flags (Protect variable)|Flags (Mask variable)|
|:---|:---|:---|:---|:---|:---|
|EPONA_AWS_DEFAULT_REGION|ap-northeast-1|Variable|All (default)|チェックを外す|チェックを外す|
|SERVICE_NAME|[`gen-config` で指定したサービス名]|Variable|All (default)|チェックを外す|チェックを外す|

#### Runtime環境単位のブランチを作成して、バックエンド用アプリケーションの定義ファイルを登録する

Epona AWS Starter CLIで構築される環境は、`example-chat`のバックエンドの動作にAWS Fargateを使用します。  
また、AWS Fargateへデプロイを行う方法としては、AWS CodeDeployを使用します。

AWS CodeDeployを使用してアプリケーションをデプロイするには、定義ファイルが必要です。  
Epona AWS Starter CLIの `apply` では、この定義ファイルを自動生成します。  
定義ファイルは、`appspec.yaml`ファイル、 `taskdef.json`ファイルの2つです。

---

:information_source:  
`appspec.yaml`ファイルおよび`taskdef.json`ファイルは、Runtime環境単位で作成されることに注意してください。  
また、これらのファイルはGitLab CI/CDにより実行されたパイプライン内でAmazon S3バケットへ配置されます。

---

これらのファイルを、作成したGitLabリポジトリの環境ごとのブランチに登録する必要があります。

---

:information_source:  
Eponaでは、開発フローとしてGitLab Flowを想定しています。

- [`GitLab Flow`](https://docs.gitlab.com/ee/topics/gitlab_flow.html)

そのため、構築されたパイプラインを使用するにはアプリケーションのデプロイ先となるRuntime環境ごとにブランチを作成する必要があります。

---

`appspec.yaml`ファイルおよび`taskdef.json`ファイルの生成場所と、リポジトリ内の配置ディレクトリの関係は以下となります。

|生成されるディレクトリ|コピー先となる`example-chat`のディレクトリ|用途|
|:---|:---|:---|
|`runtimes/[Runtime環境名]/cd_pipeline_backend_backend/sample-deploy-settings/`|`backend/deploy/`|REST APIのBackendアプリケーション定義|
|`runtimes/[Runtime環境名]/cd_pipeline_backend_notifier/sample-deploy-settings/`|`notifier/deploy/`|双方向通信用のNotifierアプリケーション定義|

以降の手順に従って、生成されたファイルを各ブランチに反映してください。

１． ソースコードを対象のRuntime環境にデプロイするため、Runtime環境名のブランチを新規に作成します。

```bash
# masterブランチから、各Runtime環境用のブランチを作成します。
$ git checkout -b [Runtime環境名] master

# git version 2.23.0 以上を利用している場合、以下コマンドでも新規ブランチを作成できます。
$ git switch -c [Runtime環境名] master
```

２． `appspec.yaml`ファイルおよび`taskdef.json`ファイルを`example-chat`リポジトリ内にコピーします。

```bash
$ cp [Epona AWS Starter CLIを実行したディレクトリ]/runtimes/[Runtime環境名]/cd_pipeline_backend_backend/sample-deploy-settings/* backend/deploy/
$ cp [Epona AWS Starter CLIを実行したディレクトリ]/runtimes/[Runtime環境名]/cd_pipeline_backend_notifier/sample-deploy-settings/* notifier/deploy/
```

３． 作成したブランチで、Runtime環境用の設定ファイルを反映させます。

```bash
# 全てのファイルをコミット対象とします
$ git add .
$ git commit
$ git push origin [Runtime環境名]
```

４．以上の作業を、Runtime環境の数だけ繰り返します。

`git push`を行ったタイミングで、GitLabで作成したリポジトリに対してGitLab CI/CDが起動し、AWS環境（Runtime環境）へのデプロイが実行されます。

#### デプロイ対象のRuntime環境へアクセスして、`example-chat`がデプロイされたことを確認する

GitLab CI/CDとAWS環境のデプロイが完了したら、以下のURLにアクセスすることで `example-chat` のアプリケーションが起動しています。

`https://[サービス名].[Runtime環境に応じたRoute53のドメイン名]`

以上で、Eponaを使用した環境構築および`example-chat`アプリケーションのデプロイ完了となります。
